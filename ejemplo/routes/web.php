<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\roles;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/ayuda','HomeController@ayuda')->name('ayuda');
Route::get('/', 'HomeController@index');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Rutas de cultivos/*---------------------------------------------Rutas Cultivos---------------------------------------------*/
Route::get('/cultivo','Cultivos@verCultivo')->name("cultivo");
Route::any('/AgregarCultivo','Cultivos@store')->name('Acultivo');
Route::any('/CultivoDatos/{id_user}', 'Cultivos@actualizar')->name('obtenerCultivo');
Route::any("/ModificarCultivo",'Cultivos@modificar')->name("actualizarcultivo");
Route::any("/BorrarCultivo/{id}",'Cultivos@eliminar');
Route::any("/exportarExcelCultivos",'Cultivos@exportarExcel')->name('exportarExcelCultivos');
Route::any("/obtenerCultivo",'HomeController@cultivo');
Route::any("/obtenernum",'HomeController@parcela');
Route::any("/VentasDiarias",'HomeController@ventas');
Route::any("/VentasCiclo",'HomeController@ventasCiclo');
/*---------------------------------------------Rutas Conagua---------------------------------------------*/
Route::get('/verConagua','Conaguas@verConagua')->name("verConagua");
Route::any('/ConaguaDatos/{id_user}', 'Conaguas@actualizar')->name('obtenerConagua');
Route::any("/modificarConagua",'Conaguas@modificar')->name("actualizarconagua");
Route::any("/BorrarConagua/{id}",'Conaguas@eliminar');
Route::any("/AgregarConagua",'Conaguas@store')->name("agregarConagua");
Route::any("/exportarExcelConagua",'Conaguas@exportarExcel')->name('exportarExcelConagua');
/*---------------------------------------------Rutas ejidatarios---------------------------------------------*/
//Ruta para mostrar catalogo de ejidatarios
Route::get('/ejidatario','Ejidatarios@verEjidatario')->name("ejidatario");
Route::any('/AgregarEjidatario','Ejidatarios@store')->name('Aejidatario');
Route::any('/EjidatarioDatos/{id_user}', 'Ejidatarios@actualizar')->name('obtenerEjidatario');
Route::any("/ModificarEjidatario",'Ejidatarios@modificar')->name("actualizarejidatario");
Route::any("/BorrarEjidatario/{id}",'Ejidatarios@eliminar');
Route::any("/exportarExcelEjidatarios",'Ejidatarios@exportarExcel')->name('exportarExcelEjidatarios');

/*---------------------------------------------Rutas usuarios------------------------------------------------*/
//Ruta para mostrar catalogo de usuarios
Route::get('/usuario','Usuarios@verUsuario')->name("usuario");
Route::any('/ModificarUser/{id_user}', 'Usuarios@actualizar')->name ('modificar');
Route::any('/editUser', 'Usuarios@editUser')->name ('editUser');
Route::any('/createUser', 'Usuarios@createUser')->name ('createUser');
Route::any("/BorrarUsuario/{id}",'Usuarios@eliminar');
Route::any('/cambioCon', 'Usuarios@cambiar')->name ('contra');
Route::any("/exportarExcelUsuarios",'Usuarios@exportarExcel')->name('exportarExcelUsuarios');
/*---------------------------------------------Rutas parcelas------------------------------------------------*/
//Ruta para mostrar catalogo de parcelas
Route::get('/parcela','Parcelas@verParcela')->name("parcela");
Route::any('/parcelaAgregar','Parcelas@store')->name("Aparcela");
Route::any('/parcelaEditar','Parcelas@actualizar')->name("editarParcela");
Route::any('/parcelaDividir','Parcelas@dividir')->name("DivParcela");
Route::any('/CultivoParcela/{id}','Parcelas@cultivo');
Route::any("/exportarExcelParcelas",'Parcelas@exportarExcel')->name('exportarExcelParcelas');

//Ruta para mostrar datos de una parcela en especifico
Route::get('/ParcelaDatos/{Id_parcela}','Parcelas@llenado')->name("parceladatos");
Route::any("/BorrarParcela/{id}",'Parcelas@eliminar');
/*---------------------------------------------Rutas ejidos------------------------------------------------*/
//Ruta para mostrar catalogo de ejidos
Route::get('/ejido','Ejidos@verEjido')->name("ejido");
Route::any('/AgregarEjido','Ejidos@store')->name('Aejido');
Route::any('/EjidoDatos/{id_user}', 'Ejidos@actualizar')->name('obtenerEjido');
Route::any("/ModificarEjido",'Ejidos@modificar')->name("actualizarejido");
Route::any("/BorrarEjidos/{id}",'Ejidos@eliminar');
Route::any("/exportarExcelEjidos",'Ejidos@exportarExcel')->name('exportarExcelEjidos');


/*---------------------------------------------Rutas conceptos------------------------------------------------*/
//Ruta para mostrar catalogo de conceptos
Route::get('/concepto','Conceptos@verConcepto')->name("concepto");
Route::any('/AgregarConcepto','Conceptos@store')->name("Aconcepto");
Route::any('/ConceptoDatos/{id_user}', 'Conceptos@actualizar')->name('obtenerConcepto');
Route::any("/ModificarConcepto",'Conceptos@modificar')->name("actualizarconcepto");
Route::any("/BorrarConcepto/{id}",'Conceptos@eliminar');
/*---------------------------------------------Rutas facturas------------------------------------------------*/
//Ruta para mostrar creacion de facturas
Route::get('/factura','Facturas@nuevaFactura')->name("factura");
Route::get('/mat/{id}','Facturas@byMatricula');
Route::get('/par/{id}','Facturas@byParcela');
Route::get('/con/{id}','Facturas@byConcepto');
Route::get('/ciclo/{id}','Facturas@facturaciclo');
//Ruta para ver facturas existentes
Route::get('/borrado/{id}','Facturas@borrado');
Route::get('/edicion/{id}','Facturas@edicion');
Route::any('/pago','Facturas@pagar');
Route::get('/verFactura','Facturas@verfacturar')->name("verfact");
Route::any("/crearFact",'Facturas@crear')->name("crearFact");
Route::any("/BorrarCFact",'Facturas@eliminarconcepto')->name("borrarxd");
Route::any("/corteCaja",'Facturas@exportarExcel')->name('CorteCaja');
//Ruta para cargar conceptos por facturas.
Route::any('/verConceptos/{id_Producto}','Facturas@verconceptos')->name ('datosModal');
Route::any("/ModificarFactura",'Facturas@modificar');
Route::get("/deudorFact/{id}",'Facturas@deudor');
/*---------------------------------------------Rutas Canaleros------------------------------------------------*/
Route::get('/canaleros','Canalero@verCanalero')->name("canalero");
Route::any('/calendarioCanalero','Canalero@calendario')->name("calendar");
Route::any('/AgregarCanalero','Canalero@store')->name("Acanalero");
Route::any("/ModificarCanalero",'Canalero@modificar')->name("actualizarcanalero");
Route::any('/CanaleroDatos/{id_user}', 'Canalero@actualizar')->name('obtenerCanalero');
Route::any("/BorrarCanalero/{id}",'canalero@eliminar');
Route::any("/horario",'canalero@verHorarios');
Route::any("/Modificarhorario",'canalero@modificarHorario')->name("modifH");
Route::any("/gethorario/{id}",'canalero@obtenerHorario');
Route::any("/exportarExcelCanaleros",'canalero@exportarExcel')->name('exportarExcelCanaleros');
/*Route::get('pdf', function(){

	$ejidatarios = Route::any('/ejidatarioPDF', 'Ejidatarios@PDF')->name('ejidatarioPDF');

	//$pdf = PDF::loadView('Canaleros/pdf', ['ejidatarios' => $ejidatarios]);
	//dd($ejidatarios);
	//return $pdf->download('libro_canaleros.pdf');
})->name('pdf');*/
Route::any('pdf', 'Ejidatarios@PDF')->name('pdf'); 

/*---------------------------------------------Rutas Bitacora------------------------------------------------*/
Route::get('/bitacora','Bitacoras@verBitacora')->name("bitacora");
Route::any('/fechaBitacora','Bitacoras@fechas')->name("bitacoraFecha");

/*---------------------------------------------Rutas Deudores------------------------------------------------*/
Route::get('/deudores','Deudores@verDeudores')->name("deudores");
Route::any('/PagarDatos/{id_user}', 'Deudores@actualizar')->name('obtenerDeuda');
Route::any("/ModificarDeuda'",'Deudores@pagar')->name("actualizaradeudo");
Route::any("/exportarExcelDeudores",'Deudores@exportarExcel')->name('exportarExcelDeudores');

/*-------------------------------------------------Rutas Ingresos---------------------------------------------*/
Route::get('/ingresos','Ingresos@verIngresos')->name("ingresos");
Route::any('/agregarIngreso','Ingresos@store')->name('agregarIngreso');
Route::any('/getIngreso', 'Ingresos@getIngreso')->name('getIngreso');
Route::any("/ingresoFecha",'Historico@ingresoFecha')->name('getfecha');
Route::any("/exportarExcelIngreso",'Ingresos@exportarExcel')->name("exportarExcelIngreso");
/*-------------------------------------------------Rutas Egresos---------------------------------------------*/
Route::get('/egresos','Egresos@verEgresos')->name("egresos");
Route::any('/agregarEgreso','Egresos@store')->name('agregarEgreso');
Route::any('/obtenerEgreso/{id}', 'Egresos@obtenerEgreso')->name('obtenerEgreso');
Route::any("/modificarEgreso",'Egresos@modificar')->name("modificarEgreso");
Route::any("/eliminarEgreso/{id}",'Egresos@eliminar')->name("eliminarEgreso");
Route::any("/getGastos",'Egresos@getGastos')->name("getGastos");
Route::any("/getGastoMes/{id}",'Egresos@getGastoMes')->name("getGastoMes");
Route::any("/filtrarFechasEgresos", "Egresos@filtrarFechas")->name("filtrarFechasEgresos");
/*-------------------------------------------------Rutas Historico---------------------------------------------*/
Route::get('/Historico','Historico@verHistorico')->name("historico");
Route::any("/getHistorico",'Historico@getHistorico')->name('getHistorico');
Route::any("/gettotal",'Historico@total')->name('gettotal');
Route::any("/exportarExcelHistorico",'Historico@exportarExcel')->name('exportarExcelHistorico');

/*-------------------------------------------------Rutas Reportes---------------------------------------------*/
Route::get('/Reportes', 'Reportes@index')->name("reportes");
Route::any("/ReporteMes/{id}",'Reportes@ventasMes');
Route::any("/ReporteParcela",'Reportes@parcelas');
Route::any("/ReporteCiclo/{id}",'Reportes@ventasCicloR');
Route::any("/ReporteIE",'Reportes@ingresosEgresos');
Route::any("/ReporteEgreso",'Reportes@egresos');
Route::any("/ReporteEgresos/{id}",'Reportes@egreso');
