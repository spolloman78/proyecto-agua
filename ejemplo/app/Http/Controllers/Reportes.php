<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\cultivo;
use App\canaleros;
use App\parcela;
use App\users;
use App\ejidatario;
use App\bitacora;
use App\factura;
use App\egreso;
use App\ciclo;
use App\ejido;

class Reportes extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

    }
    public function __construct()
    {
      $this->middleware('auth');
    //  $this->middleware('admin');
    }

    public function index()
    {
     $ciclos= ciclo::get();
     $ldate = date('Y');
     $ci=substr($ldate,2)+0;
     $cl=$ci-1;
     $ciclo=$cl."/".$ci;
     $deudoresT=DB::table('factura_concepto')
    ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
    ->join("parcela","parcela.Id_parcela","=","factura_concepto.Id_parcela")
    ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
    ->where('status_factura.Id_status_factura','=','3')
    ->count();
    $deudoresTe=DB::table('factura_concepto')
   ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
   ->join("ejidatario","ejidatario.Id_ejidatario","=","factura.Id_ejidatario")
   ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
   ->where('status_factura.Id_status_factura','=','3')
   ->select(DB::raw('count(ejidatario.Id_ejidatario)as total'))
   ->groupBy('ejidatario.Id_ejidatario')
   ->get();
    $num=0;
    foreach ($deudoresTe as $key) {
    $num++;
    }
    $cultivo=cultivo::count();
     $now = now()->format('Y-m-d');
     date_default_timezone_set("America/Mexico_City");
     $meso = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"][date("n") - 1];
     $anio = now()->format('Y');
     $mes= now()->format('m');
     $total=parcela::count();
     $ejidoT=ejido::count();
     $egr=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
     ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select(DB::raw('SUM(gasto_mes.monto) as totalDia'))
     ->whereYear('gasto_mes.fecha','=',$anio)->get();
     $deudores=DB::table('factura_concepto')
    ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
    ->join("parcela","parcela.Id_parcela","=","factura_concepto.Id_parcela")
    ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
    ->where('status_factura.Id_status_factura','=','3')
    ->count();
     $parcelaT=parcela::join('factura_concepto','factura_concepto.Id_parcela','=','parcela.Id_parcela')
     ->join('factura','factura.Id_factura','=','factura_concepto.Id_factura')
     ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')->where('factura.Id_ciclo','=','3')->count();
     $egresoD=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
     ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select(DB::raw('SUM(gasto_mes.monto) as totalDia'))
     ->whereDate('gasto_mes.fecha',$now)->get();
     $egresoM=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
     ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select(DB::raw('SUM(gasto_mes.monto) as totalDia'))
     ->whereMonth('gasto_mes.fecha','=',$mes)->whereYear('gasto_mes.fecha','=',$anio)->get();
     $egresoC=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
     ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select(DB::raw('SUM(gasto_mes.monto) as totalDia'),'concepto_gasto.descripcionG')
     ->groupby('concepto_gasto.descripcionG')->get();
      $dinero=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->select(DB::raw('SUM(factura_concepto.Total) as totalDia'))->whereMonth('factura.created_at','=',$mes)->whereYear('factura.created_at','=',$anio)->where('factura.Id_status_factura','!=','3')
      ->where('factura.Id_status_factura','!=','4')->get();
      $sum=DB::table('factura_concepto')
      ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
      ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->where('status_factura.Id_status_factura','=','3')
      ->select(DB::raw('sum(factura_concepto.Total)as total'))
      ->get();
      $eji=ejidatario::count();
      $sin=ejidatario::where('tarjeta','=','B-1000')->orwhere('tarjeta','=','1000')->count();
      $con=$eji-$sin;
      $dineroDia=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->select(DB::raw('SUM(factura_concepto.Total) as total'))->whereDate('factura.created_at',$now)->where('factura.Id_status_factura','!=','3')->where('factura.Id_status_factura','!=','4')->get();
      $dineroCiclo=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
     ->select(DB::raw('SUM(factura_concepto.Total) as totalCiclo'))->where('ciclo.Ciclo','=',$ciclo)->where('factura.Id_status_factura','!=','3')->where('factura.Id_status_factura','!=','4')->get();
        return view('Reportes/reportes',compact('anio','egr','con','sin','ciclos','deudoresT','cultivo','sum','num','deudoresTe','deudores','ejidoT','total','parcelaT','dinero','egresoC','egresoD','egresoM','dineroDia','meso','now','dineroCiclo','ciclo'));
    }
    public function cultivo(){
        $datos=cultivo::select('descripcion','cantidad_cultivo','capacidad_cultivo')->orderby('cantidad_cultivo')->get();
       return ($datos=array('datos' => $datos));
    }
    public function parcela(){
      $dato=canaleros::join('parcela','parcela.Id_canalero','=','canalero.Id_canalero')
      ->select('canalero.nombre',Db::raw('COUNT(parcela.Id_canalero) as cantidad'))->groupby('canalero.nombre')->get();
     $numero=parcela::count();
     $numero=array('total' => $numero);
     $dato=array('dato' => $dato);
     $datos = array_merge($dato, $numero);
    return ($datos=array('datos' => $datos));
    }
    public function movimientos(){
    $movimientos=bitacora::join('users.id','=','bitacora.Id_usuario')->select('users.name',Db::raw('COUNT(bitacora.Id_usuario) as cantidad'))->groupBy('users.Id_usuario')->get();
    return ($movimientos=array('movimientos' => $movimientos));
    }
    public function ventas(){
      // OBTENER VENTAS DE UN MES POR DIA
      $mes= now()->format('m');
      $anio = now()->format('Y');
      $datos=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->select(DB::raw('DATE(created_at) as date'),DB::raw('SUM(factura_concepto.Total) as totalDia'))->whereMonth('factura.created_at','=',$mes)->whereYear('factura.created_at','=',$anio)->where('factura.Id_status_factura','!=','3')
      ->where('factura.Id_status_factura','!=','4')->groupBy('date')->get();
      return ($datos=array('datos' => $datos));

    }
    public function ventasMes($id){
      $anio = now()->format('Y');
      $datos=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->select(DB::raw('DATE(created_at) as date'),DB::raw('SUM(factura_concepto.Total) as totalDia'))->whereMonth('factura.created_at','=',$id)->whereYear('factura.created_at','=',$anio)->where('factura.Id_status_factura','!=','3')
      ->where('factura.Id_status_factura','!=','4')->groupBy('date')->get();
      return ($datos=array('datos' => $datos));
    }
    public function ventasCiclo(){
      //Obtener ventas de un ciclo por Mes
      $ldate = date('Y');
      $ci=substr($ldate,2)+0;
      $cl=$ci-1;
      $ciclo=$cl."/".$ci;
      $anio = now()->format('Y');
      $datos=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->select(DB::raw('MONTH(created_at) as date'),DB::raw('SUM(factura_concepto.Total) as totalCiclo'))->where('ciclo.Ciclo','=',$ciclo)->where('factura.Id_status_factura','!=','3')->where('factura.Id_status_factura','!=','4')->whereYear('factura.created_at','=',$anio)->groupBy("date")->get();
      return ($datos=array('datos' => $datos));

    }
    public function ventasCicloR($id){
      $ldate = date('Y');
      $ci=substr($ldate,2)+0;
      $cl=$ci-1;
      $anio=2017+$id;
      $ciclo=$cl."/".$ci;

      $datos=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->select(DB::raw('MONTH(created_at) as date'),DB::raw('SUM(factura_concepto.Total) as totalCiclo'))->where('ciclo.Id_ciclo','=',$id)->where('factura.Id_status_factura','!=','3')->where('factura.Id_status_factura','!=','4')->whereYear('factura.created_at','=',$anio)->groupBy("date")->get();
      return ($datos=array('datos' => $datos));
    }

    public function egresos(){
      $anio = now()->format('Y');
      $mes= now()->format('m');
      $egreso=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
      ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')
      ->select(DB::raw('SUM(gasto_mes.monto) as totalDia'),'concepto_gasto.descripcionG')
      ->groupby('concepto_gasto.descripcionG')->get();
      $egresoM=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
       ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select(DB::raw('SUM(gasto_mes.monto) as totalDia'))
       ->whereMonth('gasto_mes.fecha','=',$mes)->whereYear('gasto_mes.fecha','=',$anio)->get();

       $numero=array('total' => $egresoM);
       $egreso=array('dato' => $egreso);
       $datos = array_merge($egreso, $numero);
    return ($datos=array('datos' => $datos));

    }
    public function parcelas(){
      $datos=DB::table('factura_concepto')
     ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
     ->join("parcela","parcela.Id_parcela","=","factura_concepto.Id_parcela")
     ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
     ->where('status_factura.Id_status_factura','=','3')->select('factura_concepto.Id_parcela',DB::raw('SUM(factura_concepto.Total) as Deuda'),DB::raw('SUM(factura_concepto.can_pagada) as pagado'))->groupBy('factura_concepto.Id_parcela')->get();
    return ($datos=array('datos' => $datos));
    }

    public function ingresosEgresos(Request $request){
      $gasto_mes = DB::table('gasto_mes')
      ->join('gasto', 'gasto_mes.Id_Gasto', "=", 'gasto.Id_gasto')
      ->join('concepto_gasto', 'gasto_mes.Id_Gasto', "=", 'concepto_gasto.Id_Concepto_gasto')
      ->whereNull('gasto_mes.deleted_at')
      ->whereBetween('gasto_mes.fecha', array($request->finicio, $request->ffin))
      ->select('*')
      ->orderBy('fecha', 'ASC')
      ->get();

      $egreso_total = $gasto_mes->sum("monto");

      $inicio=$request->finicio."T00:00";
      $fin=$request->ffin."T23:59";
      $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
      ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
      ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
      ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
      ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
      ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->join('abonos','abonos.Id_factura','=','factura.Id_factura')
      ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
      ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
      ->select('factura_concepto.Total','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','abonos.pago as Total')
     ->whereBetween('abonos.created_at', array($inicio, $fin))->get();

      $ingreso_total = $factura->sum("Total");

      $vars = [
        'gasto_mes' => $gasto_mes,
        'ingreso_mes' => $factura,
        'egreso_total' => $egreso_total,
        'ingreso_total' => $ingreso_total,
        'fecha_inicio' => $request->finicio,
        'fecha_fin' => $request->ffin
      ];
      //return view('Reportes.pdfIE', $vars);

      $pdf = \PDF::loadView('Reportes/pdfIE', $vars);
      return $pdf->download('Reporte_Entradas_Salidas.pdf');

    }

    public function egreso($mes){
      $datos=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
    ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select(DB::raw('SUM(gasto_mes.monto) as totalDia'),'concepto_gasto.descripcionG')
    ->groupby('concepto_gasto.descripcionG')->whereMonth('gasto_mes.fecha','=',$mes)->get();
      return ($datos=array('datos' => $datos));
    }

}
