@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Usuarios <i class="fas fa-users"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catálogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Usuarios</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<h4 class="card-title"  style="padding: 1% 0 0 0;">Catálogo de Usuarios</h4>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="d-flex align-items-center">
							<button class="btn btn btn-round ml-auto"  style="background-color:#00A631; color:white;" data-toggle="modal" data-target=".modal-new">
								<i class="fa fa-plus"></i>
								Usuario
							</button>
							<a class="btn btn btn-round"  style=" margin-left:20px; background-color:#00A631; color:white;" href="{{'exportarExcelUsuarios'}}">
								<i class="fas fa-file-excel fa-2x"></i>
							  </a>
						</div>
					</div>
				</div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                        <th>#<i class="fas fa-users"></i></th>
                        <th>Nombre</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Usuario</th>
                        <th>Rol</th>
                        <th>Operaciones</th>

                      </tr>
                    </thead>

                    <tbody>
                      @foreach($usuario as $row)
                        <tr>
                          <td>{!!$row->id!!}</td>
                          <td>{!!$row->name!!}</td>
                          <td>{!!$row->apPaterno!!}</td>
                          <td>{!!$row->apMaterno!!}</td>
                          <td>{!!$row->email!!}</td>
                          <td>{!!$row->role->descripcion!!}</td>
                          <td>
                            <div class="form-button-action">
                              <button type="button"  id="editar" name="editar" data-target=".modal" onclick="modalUser('{!!$row ->id!!}')" title="" class="btn btn-link btn-success btn-lg" data-original-title="Editar Registro">
                                <i class="fa fa-edit" ></i>
                              </button>
                              @if($row->id!=Auth::user()->id)
                              <button type="button" data-toggle="tooltip" title="Eliminar" class="btn btn-link btn-danger" onclick="eliminar('{!!$row ->id!!}')"data-original-title="Eliminar">
                              <i class="fa fa-times"></i>
                            </button>
                              @endif
                              <button type="button" data-toggle="tooltip" title="Cambiar Contraseña" class="btn btn-link btn-muted" onclick="cambiar('{!!$row ->id!!}')"data-original-title="Eliminar">
                              <i class="fas fa-lock"></i>
                            </button>
                            </div>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
		<!-------------------------------MODALES---------------------->
		<div class="modal fade modal" id="editarModal" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content" id="usuario">
			  <div class="modal-header no-bd">
				<h5 class="modal-title" id="usuario_edit">Editando al usuario: </h5>
				<button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
			  <div class="tab-content" id="myTabContent">
				  <div role="tabpanel" class="tab-pane active" id="datosUsuario">
					  <form id="user">
						{{csrf_field()}}
					  <div class="form-group form-group-default">
						<label for="nom_user">Nombre del usuario</label>
						<input type="text" class="form-control" id="nom_user" name="nom_user">
					  </div>
					  <div class="form-group form-group-default">
						<label for="apPaterno">Apellido paterno</label>
						<input type="text" class="form-control" id="apPaterno" name="apPaterno">
					  </div>
					  <div class="form-group form-group-default">
						<label for="apMaterno">Apellido materno</label>
						<input type="text" class="form-control" id="apMaterno" name="apMaterno">
					  </div>
					  <div class="form-group form-group-default">
						<label for="alias_usuario">Usuario</label>
						<input type="text" class="form-control" id="mail" name="mail">
					  </div>
					  <div class="form-group form-group-default">
						<label for="direccion">Rol</label>
						<select class="form-control" id="rol">
							<option value="0">Seleccione...</option>
							<option value="1">Administrador</option>
							<option value="2">Tesorero</option>
						</select>
					  </div>
					  <div class="form-group" id="datos">

					  </div>
					  <input type="text" class="form-control" id="id_user" name="id_user" hidden>
					</form>
				  </div>
			  </div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-success" onClick="editUser()">Guardar Cambios</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			  </div>
			</div>
		  </div>
		</div>
		<!------------------------------------MODAL PARA CREAR USUARIOS---------------------------->
		<div class="modal fade modal-new" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content" id="usuario">
			  <div class="modal-header no-bd">
				<h5 class="modal-title" id="usuario_edit">Creando usuario nuevo </h5>
				<button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
			  <div class="tab-content" id="myTabContent">
				  <div role="tabpanel" class="tab-pane active" id="datosUsuario">
					  <form id="user_new">
						{{csrf_field()}}
					  <div class="form-group form-group-default">
						<label for="nom_user_new">Nombre del usuario</label>
						<input type="text" class="form-control" id="nom_user_new" name="nom_user">
					  </div>
					  <div class="form-group form-group-default">
						<label for="apPaterno">Apellido paterno</label>
						<input type="text" class="form-control" id="apPaterno_new" name="apPaterno">
					  </div>
					  <div class="form-group form-group-default">
						<label for="apMaterno">Apellido materno</label>
						<input type="text" class="form-control" id="apMaterno_new" name="apMaterno">
					  </div>
					  <div class="form-group form-group-default">
						<label for="alias_usuario">Usuario</label>
						<input type="email" class="form-control" id="mail_new" name="mail">
					  </div>
            <div class="form-group form-group-default">
						<label for="alias_usuario">Password</label>
						<input type="password" class="form-control" id="password" name="password">
					  </div>
					  <div class="form-group form-group-default">
						<label for="direccion">Rol</label>
						<select class="form-control" name="rol">
							<option value="0">Seleccione...</option>
							<option value="1">Administrador</option>
							<option value="2">Tesorero</option>
						</select>
					  </div>
					</form>
				  </div>
			  </div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-success" onclick="createUser()">Guardar usuario</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			  </div>
			</div>
		  </div>
		</div>
		</div>

    <div class="modal fade " role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalPas">
      <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title  no-bd" >
              <span class="fw-mediumbold">
              Restablecer </span>
              <span class="fw-light">
                Contraseña <i class="fas fa-users" style="color:green;"></i>
              </span>
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
        <p class="small">En este panel usted podrá cambiar la contraseña del usuario en el sistema.<br/><Strong>La contraseña debe tener minimo 8 caracteres</strong></p>
          <form id="registroD"action="{{route('DivParcela')}}" method="post" >
              {{csrf_field()}}
                <p class="text-danger" id="textodividir" style="margin-left:10px;"></p>
            <div class="row">
                <div class="col-md-5">
                  <input id="Id_us" hidden name="id">
                    <div class="form-group form-group-default">                                    <label>Contraseña </label>
                      <input id="contra"  name="contra"type="password" class="form-control" placeholder="Contraseña" required>
                      </div>
                   </div>
                      <div class="col-sm-5">
                        <div class="form-group form-group-default">
                          <label>Repetir Contraseña</label>
                            <input id="repetir"  name="repetir"type="password" class="form-control"placeholder="Contraseña" required>
                                  </div>
                                </div>
                              </div>
          </div>
          <div class="modal-footer">
            <button type="button" id="dividir" class="btn btn-primary">  <i class="fas fa-lock"></i> Cambiar </button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
        </div>
      </div>
      </div>
      </div>
		<script>
    function cambiar(id){
        $("#Id_us").val(id);
        $('#modalPas').modal('show');
    }
    // ajax para modificar contraseña
  $("body").on("click","#dividir",function(event){
    var frm=$("#registroD");
    var datos = frm.serialize();
    var con=$("#contra").val();
    var con2=$("#repetir").val();
    if(con==con2 && con.length>=7){
    $.ajax({
      url:'/cambioCon/',
      type:'POST',
      data:datos
    }).done(function(res){
    $('#modalPas').modal('hide');
      swal({
          title: 'Contraseña Modificada',
          text: 'El registro se ha modificado.',
          icon : "success",
          buttons:false,
          timer:1000,
        });
        location.reload();
    });}else{
        document.getElementById('textodividir').innerHTML='Favor de Checar el Tamaño y las contraseñas.';
    }

  });
			 // Funcion para eliminar
            function eliminar(id){
            swal({
              title: '¿Seguro que desea eliminar el registro?',
              text: "¡Esta accion no es reversible!",
              type: 'warning',
              icon : "error",
              buttons:{
                confirm: {
                  text : 'Si, deseo eliminarlo!',
                  className : 'btn btn-danger',
                },
                cancel: {
                  text : 'Cancelar',
                  visible: true,
                  className: 'btn btn-warning'
                }
              }
            }).then((Delete) => {
              if (Delete) {
                $.ajax({
                url:'/BorrarUsuario/'+id,
                type:'get'
                }).done(function (res){
                swal({
                  title: 'Eliminado!',
                  text: 'El registro ha sido Eliminado.',
                  icon : "success",
                  buttons:false,
                  timer:3000,
                });
                  location.reload();
                  });
              } else {
                swal.close();
              }
            });
          }
       // aqui termina funcion de eliminar
			 function modalUser(id){
				$.ajax({
				  url:'ModificarUser/'+id,
				  type:'get'
				}).done(function (res){
          //alert(res.datos[0].name);
				  $('#usuario_edit').val(res.datos[0].name);
				  $('#nom_user').val(res.datos[0].name);
			      $('#apPaterno').val(res.datos[0].apPaterno);
				  $('#apMaterno').val(res.datos[0].apMaterno);
				  $('#mail').val(res.datos[0].email);
				  //if(res.datos[0].Id_rol == 1){
					  $('#rol').val(res.datos[0].Id_rol);
				//  }else{
					 // $('#rol').val("Tesorero");
				  //}
				  //alert(res.datos2[0].descripcion);
				  //$('#rol').val(res.datos2[0].descripcion);
				  $('#id_user').val(res.datos[0].id);

            $('#editarModal').modal('show');
				});
			  }

			function editUser(){
				var datos = $('#user').serialize();
				//alert(datos);
				$.ajax({
				  url:'editUser/',
				  type:'POST',
				  data:datos

				}).done(function(res){
        $('#editarModal').modal('hide');
          swal({
              title: 'Modificación Realizada',
              text: 'El registro se ha modificado.',
              icon : "success",
              buttons:false,
              timer:1000,
            });
            location.reload();
				  //alert(res.datos2[0].descripcion);
				  //$('#rol').val(res.datos2[0].descripcion);
				});
			  }

			function createUser(){
				var datos = $('#user_new').serialize();

				$.ajax({
				  url:'createUser/',
				  type:'POST',
				  data:datos,
          error:function(x,xd,xddx){
           alert(x.responseText);
          }
				}).done(function(res){
          $('#addRowModal').modal('hide');
          location.reload();
          swal({
            title: 'Agregado',
            text: 'El registro ha sido agregado con exito.',
            icon : "success",
            buttons:false,
            timer:3000,
          });

				});
			  }
		</script>
          @endsection
