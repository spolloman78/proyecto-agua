<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class horario extends Model
{
  protected $table = 'horario';
  use SoftDeletes; //Implementamos
  public  $primaryKey = 'Id_horario';
}
