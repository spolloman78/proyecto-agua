<?php

namespace App\Exports;
use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use App\Historico;
use App\bitacora;
use App\parcela;
use App\ejido;
use App\ejidatario;
use App\canaleros;
use App\municipio;
use App\tenencia;
use App\factura;
use App\facturaconcepto;

class HistoricosExport implements FromView
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $inicio;
    protected $fin;
    protected $cicloe;
    protected $tipo;

    public function __construct(Request $request)
    {
        $this->inicio = $request->inicio;
        $this->fin = $request->fin;
        $this->cicloe = $request->ciclo;
        $this->tipo = $request->tipo;
    }
    public function view(): View
    {
      $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
     $drawing = new Drawing();
     $drawing->setName('Logo');
     $drawing->setDescription('This is my logo');
     $drawing->setPath(public_path('\assets\img\logo-negroSmall.png'));
     $drawing->setHeight(90);
     $drawing->setCoordinates('B3');
      libxml_use_internal_errors(true);
      $in=$this->inicio;
      $fi=$this->fin;
      $inicio=$in."T00:00";
      $fin=$fi."T23:59";
      $ciclo=$this->cicloe;
      $tipo=$this->tipo;
      if($ciclo ==0){
        $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
        ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
        ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
        ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
        ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
        ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
        ->join('abonos','abonos.Id_factura','=','factura.Id_factura')
        ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
        ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
        ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
        ->select('factura_concepto.Total','abonos.pago as pagado','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','factura_concepto.Total')
        ->whereBetween('factura.created_at', array($inicio, $fin))->get();
        $dineroDia=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')
        ->select(DB::raw('SUM(abonos.pago) as total'))->whereBetween('factura.created_at', array($inicio, $fin))->get();

      }else{
      $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
      ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
      ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
      ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
      ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
      ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->join('abonos','abonos.Id_factura','=','factura.Id_factura')
      ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
      ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
      ->select('factura_concepto.Total','abonos.pago as pagado','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','factura_concepto.Total')
      ->where('factura.Id_ciclo','=',$ciclo)->get();
      $dineroDia=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')
        ->select(DB::raw('SUM(abonos.pago) as total'))->where('factura.Id_ciclo','=',$ciclo)->get();
      }
      if($tipo=="0"){}else{
        $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
        ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
        ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
        ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
        ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
        ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
        ->join('abonos','abonos.Id_factura','=','factura.Id_factura')
        ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
        ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
        ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
        ->select('factura_concepto.Total','abonos.pago as pagado','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta')
        ->where('concepto.tipo','=',$tipo)->get();
        $dineroDia=  $datos=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')->join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
          ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
          ->select(DB::raw('SUM(abonos.pago) as total'))->where('concepto.tipo','=',$tipo)->get();
}

        return view('Historico.excel',[
            'factura' => $factura,'dineroDia'=>$dineroDia
        ]);
    }
}
