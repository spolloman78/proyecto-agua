<?php

use Illuminate\Database\Seeder;

class estadosFactura extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('status_factura')->insert([
      'Id_status_factura'=>1,
      'descripcionEstado'=>'Creada'
      ]);
      DB::table('status_factura')->insert([
      'Id_status_factura'=>2,
      'descripcionEstado'=>'Completada'
      ]);
      DB::table('status_factura')->insert([
      'Id_status_factura'=>3,
      'descripcionEstado'=>'Deudor'
      ]);
      DB::table('status_factura')->insert([
      'Id_status_factura'=>4,
      'descripcionEstado'=>'Cancelada'
      ]);
    }
}
