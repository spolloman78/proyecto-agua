<?php

use Illuminate\Database\Seeder;

class tenencia extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('tenencia')->insert([
'Id_tenencia'=>1,
'descripcionT'=>'Ejidos'

]);
DB::table('tenencia')->insert([
'Id_tenencia'=>3,
'descripcionT'=>'Pequeña propiedad'

]);
DB::table('tenencia')->insert([
'Id_tenencia'=>4,
'descripcionT'=>'No Verificado'

]);
DB::table('tenencia')->insert([
'Id_tenencia'=>7,
'descripcionT'=>'Colonia Agricola'

]);
    }
}
