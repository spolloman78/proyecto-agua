<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConagua extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('conagua', function (Blueprint $table) {
Schema::disableForeignKeyConstraints();
          $table->unsignedInteger("Id_ejidatario");
          $table->Increments("Id_Conagua");
          $table->integer("USIPAD");
          $table->integer("CTA_SIPAD");
          $table->integer("UNI");
          $table->integer("ZO");
          $table->integer("MOD");
          $table->integer("SRA");
          $table->integer("SSRA");
          $table->integer("EST");
          $table->integer("GR");
          $table->integer("PRE");
          $table->integer("SUPFISICA");
          $table->integer("SUPRIEGO");
          $table->integer("SEC_ORIG");
          $table->softDeletes(); //Nueva línea, para el borrado lógico
         $table->foreign('Id_ejidatario')->references('Id_ejidatario')->on('ejidatario');

      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    DB::statement('SET FOREIGN_KEY_CHECKS = 0');
      Schema::dropIfExists('conagua');
      DB::statement('SET FOREIGN_KEY_CHECKS = 1');
  }
}
