<?php

use Illuminate\Database\Seeder;

class tipo_mov extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('tipo_movimiento')->insert([
       'Id_movimiento'=>1,
       'descripcion'=>'Creación',
       'deleted_at'=>null
      ]);
      DB::table('tipo_movimiento')->insert([
       'Id_movimiento'=>2,
       'descripcion'=>'Edición',
       'deleted_at'=>null
      ]);
      DB::table('tipo_movimiento')->insert([
       'Id_movimiento'=>3,
       'descripcion'=>'Borrado',
       'deleted_at'=>null
      ]);
      DB::table('tipo_movimiento')->insert([
       'Id_movimiento'=>4,
       'descripcion'=>'Exportación',
       'deleted_at'=>null
      ]);
      DB::table('tipo_movimiento')->insert([
       'Id_movimiento'=>5,
       'descripcion'=>'Pago',
       'deleted_at'=>null
      ]);
      DB::table('tipo_movimiento')->insert([
       'Id_movimiento'=>6,
       'descripcion'=>'Reportes',
       'deleted_at'=>null
      ]);
    }
}
