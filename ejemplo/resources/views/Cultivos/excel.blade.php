<center><h2>CÁTALOGO DE CULTIVOS</h2></center>
<h3>El numero de registro actual de cultivos</h3>
<table>
    <thead>
        <tr>
            <th>Descripción</th>
            <th>Capacidad de Cultivo</th>
            <th>Cantidad de Cultivos</th>
            <th>Prioridad</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cultivo as $cultivo)
            <tr class="">
                <td style="width:50;">{{$cultivo->descripcion}}</td>
                <td>{{$cultivo->capacidad_cultivo}}</td>
                <td>{{$cultivo->cantidad_cultivo}}</td>
                <td>{{$cultivo->prioridad}}</td>
            </tr>
        @empty
        @endforelse
    </tbody>
</table>
