<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ejido extends Model
{
    protected $table = 'ejido';
    public $timestamps = false;
    use SoftDeletes; //Implementamos
    protected $dates = ['deleted_at'];
    public  $primaryKey = 'Id_Ejido';
}
