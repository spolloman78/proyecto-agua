@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">CONAGUA 	<i class="icon-drop"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catálogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">CONAGUA </a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="card-title"  style="padding: 1% 0 0 0;">Catálogo de Ejidos CONAGUA<i class="fas fa-hat-cowboy"></i></h4>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-center">
                    <button class="btn btn btn-round ml-auto"  style="background-color:#00A631; color:white;" data-toggle="modal" data-target="#conagua">
                        <i class="fa fa-plus"></i>
                        CONAGUA
                      </button>
                      <a class="btn btn btn-round"  style="background-color:#00A631; color:white; margin-left:20px; " href="{{'exportarExcelConagua'}}">
                        <i class="fas fa-file-excel fa-2x"></i>

                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                        <th># 	<i class="icon-drop"></i></th>
                        <th>USIPAD</th>
                        <th>Nombre del Ejidatario</th>
                        <th>CTA_SIPAD</th>
                        <th>UNI</th>
                        <th>ZO</th>
                        <th>MOD</th>
                        <th>SRA</th>
                        <th>SSRA</th>
                        <th>EST</th>
                        <th>GR</th>
                        <th>PRE</th>
                        <th>SUPFISICA</th>
                        <th>SUPRIEGO</th>
                        <th>SEC_ORIG</th>
                        <th>OPERACIONES</th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($conagua as $row)
                        <tr>
                          <td>{!!$row->Id_Conagua!!}</td>
                          <td>{!!$row->USIPAD!!}</td>
                          <td>{!!$row->ejiname!!} {!!$row->apPaterno!!} {!!$row->apMaterno!!} </td>
                          <td>{!!$row->CTA_SIPAD!!}</td>
                          <td>{!!$row->UNI!!}</td>
                          <td>{!!$row->ZO!!}</td>
                          <td>{!!$row->MOD!!}</td>
                          <td>{!!$row->SRA!!}</td>
                          <td>{!!$row->SSRA!!}</td>
                          <td>{!!$row->EST!!}</td>
                          <td>{!!$row->GR!!}</td>
                          <td>{!!$row->PRE!!}</td>
                          <td>{!!$row->SUPFISICA!!}</td>
                          <td>{!!$row->SUPRIEGO!!}</td>
                          <td>{!!$row->SEC_ORIG!!}</td>
                          <td>
                            <div class="form-button-action">
                              <button type="button" data-toggle="tooltip" title="Modificar" class="btn btn-link btn-success btn-lg" data-original-title="Editar Registro" data-id="{{$row->Id_Conagua}}"
                               data-toggle="modal" data-target=".modal"   onclick="modalConagua('{{$row->Id_Conagua}}')"  >
                              <i class="fas fa-edit"></i>
                            </button>
                              <button type="button" data-toggle="tooltip" title="Eliminar" class="btn btn-link btn-danger" onclick="eliminar('{!!$row ->Id_Conagua!!}')"data-original-title="Eliminar">
                                <i class="fa fa-times"></i>
                              </button>
                            </div>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <br>

              </div>
            </div>
          </div>


          <!------Modal-------->
          <div class="modal fade " id="conagua" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog " role="document">
                <div class="modal-content">
                  <div class="modal-header no-bd ">
                    <h5 class="modal-title" >
                      <span class="fw-mediumbold">
                        Nuevo Registro
                      </span>
                      <span class="fw-light">
                        Conagua  <i class="icon-drop"></i>
                      </span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body ">
                    <p class="small">En este panel usted podrá  agregar nuevos conceptos de conagua en el sistema.</p>
                    <form id="agregarFormulario"action="" >
                        {{csrf_field()}}
                      <div class="row">
                        <div class="col-sm-12">
                           <p class="text-danger" id="textomensaje" style="margin-left:10px;"></p>

                        </div>
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Nombre del Ejidatario</label>
                            <select class="form-control js-example-basic-single" name="ejidatarios" id="ejidatarios" style="width:100%;">
                              @foreach($ejidatarios as $row)
                                <?php $valor = ($row->nombre)." ".($row->apPaterno)." ".($row->apMaterno)?>
                             <option value="{!!$row->Id_ejidatario!!}">{{$valor}} </option>
                           @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>CTA_SIPAD</label>
                            <input id="" type="number" name="CTA_SIPAD" class="form-control" placeholder="CTA_SIPAD" min="1" max="6000" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>UNI</label>
                            <input id="" type="text" name="UNI" class="form-control" placeholder="UNI" min="1" max="10" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>ZO</label>
                            <input id="" type="number" name="ZO" class="form-control" placeholder="ZO" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>MOD</label>
                            <input id="" type="number" name="MOD" class="form-control" placeholder="MOD" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>SRA</label>
                            <input id="" type="number" name="SRA" class="form-control" placeholder="SRA" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>SSRA</label>
                            <input id="" type="number" name="SSRA" class="form-control" placeholder="SSRA" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>EST</label>
                            <input id="" type="number" name="EST" class="form-control" placeholder="EST" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>GR</label>
                            <input id="" type="number" name="GR" class="form-control" placeholder="GR" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>PRE</label>
                            <input id="" type="number" name="PRE" class="form-control" placeholder="PRE" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>SUPFISICA</label>
                            <input id="" type="number" name="SUPFISICA" class="form-control" placeholder="SUPFISICA" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>SUPRIEGO</label>
                            <input id="" type="number" name="SUPRIEGO" class="form-control" placeholder="SUPRIEGO" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>SEC_ORIG</label>
                            <input id="" type="number" name="SEC_ORIG" class="form-control" placeholder="SEC_ORIG" min="0" max="100" required>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="modal-footer no-bd">
                    <button type="button" id="agregarc" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Agregar </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>

          <!------Modal Editar--->
          <div class="modal fade " id="editarCon" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog " role="document">
                <div class="modal-content">
                  <div class="modal-header no-bd ">
                    <h5 class="modal-title" >
                      <span class="fw-mediumbold">
                        Editar
                      </span>
                      <span class="fw-light">
                        Conagua  <i class="icon-drop"></i>
                      </span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body ">
                    <p class="small">En este panel usted podrá editar los conceptos de conagua en el sistema.</p>
                    <form id="modificard"action="{{route('actualizarconagua')}}" method="post" >
                        {{csrf_field()}}
                      <div class="row">

                        <div class="col-sm-12">
                           <p class="text-danger" id="textomensaje" style="margin-left:10px;"></p>
                          <div class="form-group form-group-default">
                            <label>Conagua</label>
                            <input id="Id_Conagua" name="Id_Conagua" type="text" class="form-control" placeholder="Id_Conagua" required>
                          </div>
                        </div>
                    

                        <div class="col-sm-8">
                      
                          <div class="form-group form-group-default">
                            <label>Nombre del Ejidatario</label>
                            <select class="form-control js-example-basic-single" style="width:100%;"name="ejidatarios" id="ejidatario" >
                              @foreach($ejidatarios as $row)
                                <?php $valor = ($row->nombre)." ".($row->apPaterno)." ".($row->apMaterno)?>
                             <option value="{!!$row->Id_ejidatario!!}">{{$valor}} </option>
                           @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group form-group-default">
                            <label>USIPAD</label>
                            <input id="USIPAD" type="text" name="USIPAD" class="form-control" placeholder="USIPAD" min="1" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>CTA_SIPAD</label>
                            <input id="CTA_SIPAD" type="number" name="CTA_SIPAD" class="form-control" placeholder="CTA_SIPAD" min="1" max="6000" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>UNI</label>
                            <input id="UNI" type="text" name="UNI" class="form-control" placeholder="UNI" min="1" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>ZO</label>
                            <input id="ZO" type="number" name="ZO" class="form-control" placeholder="ZO" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>MOD</label>
                            <input id="MOD" type="number" name="MOD" class="form-control" placeholder="MOD" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>SRA</label>
                            <input id="SRA" type="number" name="SRA" class="form-control" placeholder="SRA" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>SSRA</label>
                            <input id="SSRA" type="number" name="SSRA" class="form-control" placeholder="SSRA" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>EST</label>
                            <input id="EST" type="number" name="EST" class="form-control" placeholder="EST" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>GR</label>
                            <input id="GR" type="number" name="GR" class="form-control" placeholder="GR" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>PRE</label>
                            <input id="PRE" type="number" name="PRE" class="form-control" placeholder="PRE" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>SUPFISICA</label>
                            <input id="SUPFISICA" type="number" name="SUPFISICA" class="form-control" placeholder="SUPFISICA" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>SUPRIEGO</label>
                            <input id="SUPRIEGO" type="number" name="SUPRIEGO" class="form-control" placeholder="SUPRIEGO" min="0" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>SEC_ORIG</label>
                            <input id="SEC_ORIG" type="number" name="SEC_ORIG" class="form-control" placeholder="SEC_ORIG" min="0" max="100" required>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="modal-footer no-bd">
                    <button type="submit" id="modificarc" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Modificar </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>

          <script>
            // Funcion para eliminar
            function eliminar(id){
            swal({
              title: '¿Seguro que desea eliminar el registro?',
              text: "¡Esta accion no es reversible!",
              type: 'warning',
              icon : "error",
              buttons:{
                confirm: {
                  text : 'Si, deseo eliminarlo!',
                  className : 'btn btn-danger',
                },
                cancel: {
                  text : 'Cancelar',
                  visible: true,
                  className: 'btn btn-warning'
                }
              }
            }).then((Delete) => {
              if (Delete) {

                $.ajax({
                url:'/BorrarConagua/'+id,
                type:'get'
                }).done(function (res){

                swal({
                  title: 'Eliminado!',
                  text: 'El registro ha sido Eliminado.',
                  icon : "success",
                  buttons:false,
                  timer:3000,
                });
                  location.reload();
                  });
              } else {
                swal.close();
              }
            });
          }
       // aqui termina funcion de eliminar

            function modalConagua(id){
              var ejidatario = document.getElementById("ejidatario");
              $.ajax({
              url:'/ConaguaDatos/'+id,
              type:'get'
              
              }).done(function (res){
                $("#editarCon").modal('show');
                $("#Id_Conagua").val(id);
                $('#USIPAD').val(res.datos[0].USIPAD);
                $('#CTA_SIPAD').val(res.datos[0].CTA_SIPAD);
                $('#UNI').val(res.datos[0].UNI);
                $('#ZO').val(res.datos[0].ZO);
                $('#MOD').val(res.datos[0].MOD);
                $('#SRA').val(res.datos[0].SRA);
                $('#SSRA').val(res.datos[0].SSRA);
                $('#EST').val(res.datos[0].EST);
                $('#GR').val(res.datos[0].GR);
                $('#PRE').val(res.datos[0].PRE);
                $('#SUPRIEGO').val(res.datos[0].SUPRIEGO);
                $('#SUPFISICA').val(res.datos[0].SUPFISICA);
                $('#SEC_ORIG').val(res.datos[0].SEC_ORIG);

                document.getElementById("ejidatario").innerHTML += "<option value='"+res.datos[0].Id_ejidatario+"'selected >"+res.datos[0].ejiname+" "+res.datos[0].apPaterno+" "+res.datos[0].apMaterno+"</option>";
                ejidatario.remove(res.datos[0].Id_ejidatario);
                

              });
            }
            // funcion para agregar




            // funcioon para agregar
            $("body").on("click","#agregarc",function(event){
              var frm=$("#agregarFormulario");
              var datos = frm.serialize();

              $.ajaxSetup({
                   headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
               });
               $.ajax({
                type:'POST',
                url:'/AgregarConagua',
                data:datos,
              success:function(data){
                if(data=="error"){
                }else{
            location.reload();
                    $('#addRowModal').modal('hide');
                    swal({
                      title: 'Registro realizado',
                      text: 'El registro ha sido realizado con exito.',
                      icon : "success",
                      buttons:false,
                      timer:3000,
                    });

              }}

                    ,
                    error:function(x,xs,xt){
                      alert(x.responseText);
                      var xd=x.responseText.split("}");
                       var obj2=JSON.parse(xd[0]+'}');
                       var obj = JSON.parse(xd[1]+"}");
                       var atributos = "";
                       for(var aux in obj2){
                       $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}

                      document.getElementById('textoagregar').innerHTML='Favor de llenar los campos de manera adecuada';
                    }
              });
              });

// Funcion modificar
  $("body").on("click","#modificarc",function(event){
      var frm=$("#modificard");
      var datos = frm.serialize();
    $.ajaxSetup({
         headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    $.ajax({
       type:'POST',
        url:'/modificarConagua',
        data:datos,
        success:function(data){

        location.reload();
          $('#editarCon').modal('hide');
            swal({
              title: 'Modificado',
              text: 'El registro ha sido modificado con exito.',
              icon : "success",
              buttons:false,
              timer:3000,
            });
          }
          ,
          error:function(x,xs,xt){
            var xd=x.responseText.split("}");
            var obj2=JSON.parse(xd[0]+'}');
            var obj = JSON.parse(xd[1]+"}");
            var atributos = "";
            for(var aux in obj2){
              $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}

            }
          });
          });

          </script>
          @endsection
