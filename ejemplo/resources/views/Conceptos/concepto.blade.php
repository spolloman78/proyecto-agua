@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Conceptos <i class="flaticon-list"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catalogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Conceptos</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="card-title"  style="padding: 1% 0 0 0;">Catálogo de Conceptos<i class="fas fa-hat-cowboy"></i></h4>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-center">
                      <button class="btn btn btn-round ml-auto"  style="background-color:#00A631; color:white;" data-toggle="modal" data-target="#concepto">
                        <i class="fa fa-plus"></i>
                        Concepto
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>

                        <th>Concepto <i class="flaticon-list"></i></th>
                        <th>Descripción del Concepto</th>
                        <th>Precio</th>
                        <th>Tipo</th>
                        <th>No_Riesgos</th>
                        <th>Operaciones</th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($concepto as $row)
                      <tr>

                        <td>{!!$row->Id_Concepto!!}</td>
                        <td>{!!$row->descripcion_con!!}</td>
                        <td>{!!$row->precio!!}.00</td>
                        <td>{!!$row->tipo!!}</td>
                        <td>{!!$row->no_riesgos!!}</td>
                        <td>
                          <div class="form-button-action">

                            <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-success btn-lg" data-original-title="Editar Registro"data-toggle="modal" data-target="#modalEditar" onclick="modalConcepto('{!!$row ->Id_Concepto!!}')" title="">
                              <i class="fa fa-edit"></i>
                            </button>

                            <button type="button" data-toggle="tooltip" title="Eliminar" class="btn btn-link btn-danger" onclick="eliminar('{!!$row ->Id_Concepto!!}')"data-original-title="Eliminar">
                              <i class="fa fa-times"></i>
                            </button>
                          </div>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body">
            <!-- Modal -->
            <div class="modal fade " id="concepto" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog " role="document">
                <div class="modal-content">

                  <div class="modal-header no-bd ">
                    <h5 class="modal-title" >
                      <span class="fw-mediumbold">
                      Nuevo</span>
                      <span class="fw-light">
                        Concepto  <i class="flaticon-list" style="color:green; "></i>
                      </span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body ">

                    <p class="small">En este panel usted podrá agregar nuevos conceptos en el sistema.</p>
                    <form id="regeji"action="{{route('Aconcepto')}}" >
                        {{csrf_field()}}
                      <div class="row">

                        <div class="col-sm-12">
                           <p class="text-danger" id="textomensaje" style="margin-left:10px;"></p>
                          <div class="form-group form-group-default">
                            <label>Concepto</label>
                            <input id="addName"  name="concepto"type="text" class="form-control" placeholder="Concepto" required>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Descripción del Concepto</label>
                            <input id="addApp" type="text" name="descripcion" class="form-control" placeholder="Descripción del concepto" min="1" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>Precio $</label>
                            <input id="addApm" type="number" name="precio" class="form-control" placeholder="Precio" min="1" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>Tipo</label>
                            <input id="addTarj" type="text" name="tipo" class="form-control" placeholder="Tipo" min="1" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>No.Riesgos</label>
                            <input id="addTarj" type="number" name="riegos" class="form-control" placeholder="Riesgos" min="1" max="100" required>
                          </div>
                        </div>
                      </div>


                  </div>
                  <div class="modal-footer no-bd">
                    <button type="submit" id="addconcepto" class="btn" style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
  </form>
                  </div>
                </div>
              </div>
            </div>

            <!---MODAL PARA EDITAR-->
            <div class="modal fade " id="modalEditar" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog " role="document">
                <div class="modal-content">

                  <div class="modal-header no-bd ">
                    <h5 class="modal-title" >
                      <span class="fw-mediumbold">
                      Editar </span>
                      <span class="fw-light">
                        Concepto  <i class="flaticon-list" style="color:green; "></i>
                      </span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body ">

                    <p class="small">En este panel usted podrá agregar nuevos conceptos en el sistema.</p>
                    <form id="modificar" >
                        {{csrf_field()}}
                      <div class="row">

                        <div class="col-sm-12">
                           <p class="text-danger" id="textomensaje" style="margin-left:10px;"></p>
                          <div class="form-group form-group-default">
                            <label>Concepto</label>
                            <input id="Conceptoe"  name="Conceptoe" type="text" class="form-control" placeholder="Concepto" required>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Descripción del Concepto</label>
                            <input id="Descripcione" type="text" name="Descripcione" class="form-control" placeholder="Descripción del concepto" min="1" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>Precio $</label>
                            <input id="Precioe" type="number" name="Precioe" class="form-control" placeholder="Precio" min="1" max="6000" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>Tipo</label>
                            <input id="Tipoe" type="text" name="Tipoe" class="form-control" placeholder="Tipo" min="1" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group form-group-default">
                            <label>No.Riesgos</label>
                            <input id="Riesgose" type="number" name="Riesgose" class="form-control" placeholder="Riesgos" min="0" max="100" required>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="modal-footer no-bd">
                    <button type="submit" id="modificarc" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Modificar </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
  </form>
                  </div>
                </div>
              </div>
            </div>

          <script>
            // Funcion para eliminar
            function eliminar(id){
            swal({
              title: '¿Seguro que desea eliminar el registro?',
              text: "¡Esta accion no es reversible!",
              type: 'warning',
              icon : "error",
              buttons:{
                confirm: {
                  text : 'Si, deseo eliminarlo!',
                  className : 'btn btn-danger',
                },
                cancel: {
                  text : 'Cancelar',
                  visible: true,
                  className: 'btn btn-warning'
                }
              }
            }).then((Delete) => {
              if (Delete) {
                  var rest=id.replace("/", "P");
                $.ajax({
                url:'/BorrarConcepto/'+rest,
                type:'get'
                }).done(function (res){
                swal({
                  title: 'Eliminado!',
                  text: 'El registro ha sido Eliminado.',
                  icon : "success",
                  buttons:false,
                  timer:3000,
                });
                  location.reload();
                  });
              } else {
                swal.close();
              }
            });
          }
       // aqui termina funcion de eliminar
            function modalConcepto(id){
            var rest=id.replace("/", "P");
              $.ajax({
              url:'/ConceptoDatos/'+rest,
              type:'get'
              }).done(function (res){
                $("#id").val(id);
                $("#modalEditar").modal('show');
                $("#Conceptoe").val(res.datos[0].Id_Concepto);
                $('#Descripcione').val(res.datos[0].descripcion_con);
                $('#Precioe').val(res.datos[0].precio);
                $('#Tipoe').val(res.datos[0].tipo);
                $('#Riesgose').val(res.datos[0].no_riesgos);
              });
            }

            $("body").on("click","#addconcepto",function(event){
              event.preventDefault();
              var frm=$("#regeji");
              var datos = frm.serialize();
              $.ajaxSetup({
                   headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
               });
               $.ajax({
                type:'POST',
                url:'/AgregarConcepto/',
                data:datos,
              success:function(data){
              if(data=="error"){
            document.getElementById('textomensaje').innerHTML='Este ejidtario ya existe.';
                }else{
            document.getElementById('textomensaje').innerHTML='';
                $('#concepto').modal('hide');
                location.reload();
            }
                    },
                    error:function(x,xs,xt){
                       var xd=x.responseText.split("}");
                       var obj2=JSON.parse(xd[0]+'}');
                       var obj = JSON.parse(xd[1]+"}");
                       var atributos = "";
                       for(var aux in obj2){
                       $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}

        document.getElementById('textomensaje').innerHTML='Favor de llenar los campos de manera adecuada';
                    }
            });
            });

            //ajax  para modificcar
            $("body").on("click","#modificarc",function(event){
              var frm=$("#modificar");
              var datos = frm.serialize();
              $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
              $.ajax({
              type:'POST',
              url:'/ModificarConcepto/',
              data:datos,
              success:function(data){
              location.reload();
              $('#modalEditar').modal('hide');
              swal({
                title: "Modificacion Realizada",
                text: "El concepto se modificado con exito.",
                icon: "success",
                buttons: false,
                timer:3000,

              });
            }
            ,
            error:function(x,xs,xt){
              mensaje("Favor de llenar los campos de manera Adecuada","danger","top","Registro de Concepto");
            }
          });
        });
      </script>

          @endsection
