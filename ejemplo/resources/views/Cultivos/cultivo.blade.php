@extends('layouts.layout')

@section('content')

<div class="main-panel">

    <div class="content">

      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Cultivos <i class="fas fa-tree"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catálogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Cultivos</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="card-title"  style="padding: 1% 0 0 0;">Catálogo de Cultivos 19/20<i class="fas fa-hat-cowboy"></i></h4>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-center">
                      <button class="btn btn btn-round ml-auto"  style="background-color:#00A631; color:white;" data-toggle="modal"data-target="#addRowModal">
                        <i class="fa fa-plus"></i>
                        Cultivo
                      </button>
                      <a class="btn btn btn-round"  style=" margin-left:20px; background-color:#00A631; color:white;" href="{{'exportarExcelCultivos'}}">
                        <i class="fas fa-file-excel fa-2x"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                        <th># <i class="fas fa-tree"></i></th>
                        <th>Nombre del Cultivo</th>
                        <th>Riegos Totales</th>
                        <th></th>
                        <th>Riegos disponibles</th>
                        <th>Prioridad</th>
                        <th>Operaciones</th>
                      </tr>

                    </thead>

                    <script>
                      var contador=0;
                      var cap=0;
                    </script>
                    <tbody>
                      @foreach($cultivo as $row)
                        <tr>
                          <td>{!!$row->Id_cultivo!!}</td>
                          <td>{!!$row->descripcion!!}</td>
                          <td id="capacidad">{!!$row->capacidad_cultivo!!}</td>
                          <td id="disponibles">{!!$row->cantidad_cultivo!!}
                          @php
                            $valor2=($row->cantidad_cultivo+0)/($row->capacidad_cultivo)*100;
                            $valor=floatval($valor2);
                          @endphp

                          <script>
                          contador++;
                            var g=document.getElementById("basic-datatables");
                            var capacidad=g.rows[contador].cells[2].innerText;
                            var disponibles=g.rows[contador].cells[3].innerText;
                            cap=(disponibles/capacidad)*100;
                            var disponibles=g.rows[contador].cells[3].innerText=cap+" %";
                            //document.getElementsByClassName("progress-bar")[0].style.width = cap+"%";

                            </script>
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped bg-success" role="progressbar" id="porcentaje" @php echo "style='width:".  $valor."%'"; @endphp aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>

                            </td>
                            <td>{!!$row->cantidad_cultivo!!}</td>
                            <td>{!!$row->prioridad!!}</td>
                            <td>
                              <div class="form-button-action">
                                <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-success btn-lg" data-original-title="Editar Registro"data-toggle="modal" data-target="#modalEditar" onclick="modalCultivo('{!!$row ->Id_cultivo!!}')" title="">
                                  <i class="fa fa-edit"></i>
                                </button>
                                <button type="button" data-toggle="tooltip" title="Eliminar" class="btn btn-link btn-danger" onclick="eliminarCultivo('{!!$row ->Id_cultivo!!}')"data-original-title="Eliminar">
                                  <i class="fa fa-times"></i>
                                </button>
                              </div>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
          </div>



            </div>
          <div class="card-body">
            <!-- Modal -->
            <div class="modal fade " id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog " role="document">
                <div class="modal-content">

                  <div class="modal-header no-bd ">
                    <h5 class="modal-title" >
                      <span class="fw-mediumbold">
                      Nuevo</span>
                      <span class="fw-light">
                        Cultivo   <i class="fas fa-tree 2x" style="color:green; "></i>
                      </span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body ">

                    <p class="small">En este panel usted podrá agregar nuevos cultivos en el sistema.</p>
                    <form id="registro"action="{{route('Acultivo')}}" >
                        {{csrf_field()}}
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Nombre del cultivo</label>
                            <input id="addName"  name="descripcion"type="text" class="form-control" placeholder="Cultivo" required>
                          </div>
                        </div>
                        <div class="col-md-6 pr-0">
                          <div class="form-group form-group-default">
                            <label>Número de Riegos</label>
                            <input id="addPosition" type="number" name="capacidad" class="form-control" placeholder="0" min="1" max="100" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-group-default">
                            <label>Prioridad del Cultivo</label>

                        <select id="addOffice" type="number" name="prioridad"class="form-control" placeholder="Prioridad del cultivo" required >
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                         </div>
                        </div>
                      </div>

                  </div>
                  <div class="modal-footer no-bd">
                    <button type="submit" id="addRowButton" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
  </form>
                  </div>
                </div>
              </div>
            </div>

<!---MODAL PARA EDITAR-->
<div class="modal fade " id="modalEditar" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">

      <div class="modal-header no-bd ">
        <h5 class="modal-title" >
          <span class="fw-mediumbold">
          Editar </span>
          <span class="fw-light">
            Cultivo   <i class="fas fa-tree 2x" style="color:green; "></i>
          </span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
        <p class="small">En este panel usted podrá editar datos de los cultivos en el sistema, asi como mejorar la planeacion de riego.</p>
        <form id="modificar"action="{{route('actualizarcultivo')}}" >
            {{csrf_field()}}
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group form-group-default">
                <label>Nombre del cultivo</label>
                <input id="descripcionc"  name="descripcionc"type="text" class="form-control" placeholder="Cultivo" required>
              </div>
            </div>
            <div class="col-md-6 ">
              <div class="form-group form-group-default">
                <label>Número de Riegos</label>
                <input id="id" type="number" name="id" class="form-control" placeholder="0" min="1" max="100" hidden>
                <input id="capacidadcultivo" type="number" name="capacidadcultivo" class="form-control" placeholder="0" min="1" max="100" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-group-default">
                <label>Número de riegos disponibles</label>
                <input id="capacidaddisponible" type="number" name="capacidaddisponible" class="form-control" placeholder="0" min="1" max="100" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-group-default">
                <label>Prioridad del Cultivo</label>
<select id="prioridadC"class="form-control"name="prioridad">
  <option></option>
</select>
             </div>
            </div>
          </div>
      </div>
      <div class="modal-footer no-bd">
        <button type="button" id="modificarc" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Modificar </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
</form>
      </div>
    </div>
  </div>
</div>

    <script>
        // Funcion para eliminar
            function eliminarCultivo(id){
            swal({
              title: '¿Seguro que desea eliminar el registro?',
              text: "¡Esta accion no es reversible!",
              type: 'warning',
              icon : "error",
              buttons:{
                confirm: {
                  text : 'Si, deseo eliminarlo!',
                  className : 'btn btn-danger',
                },
                cancel: {
                  text : 'Cancelar',
                  visible: true,
                  className: 'btn btn-warning'
                }
              }
            }).then((Delete) => {
              if (Delete) {
                $.ajax({
                url:'/BorrarCultivo/'+id,
                type:'get'
                }).done(function (res){
                swal({
                  title: 'Eliminado!',
                  text: 'El registro ha sido Eliminado.',
                  icon : "success",
                  buttons:false,
                  timer:3000,
                });
                  location.reload();
                  });
              } else {
                swal.close();
              }
            });}
       // aqui termina funcion de eliminar


    function modalCultivo(id){

     $.ajax({
       url:'/CultivoDatos/'+id,
       type:'get'
     }).done(function (res){
  $("#id").val(id);
       $("#modalEditar").modal('show');
       $('#descripcionc').val(res.datos[0].descripcion);
       $('#capacidadcultivo').val(res.datos[0].capacidad_cultivo);
    //   $('#prioridadc').val(res.datos[0].prioridad);
       $('#capacidaddisponible').val(res.datos[0].cantidad_cultivo);
       for (var i = 0; i < 5; i++) {
if(i==res.datos[0].prioridad){
  document.getElementById("prioridadC").innerHTML += "<option value='"+i+"'selected >"+i+"</option>";

}else{
  document.getElementById("prioridadC").innerHTML += "<option value='"+i+"'>"+i+"</option>";
}
              }
     });
     }


//ajax para registrar un cultivo
    $("body").on("click","#addRowButton",function(event){
      event.preventDefault();
      var frm=$("#registro");
      var datos = frm.serialize();
      $.ajaxSetup({
           headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });
       $.ajax({
        type:'POST',
        url:'/AgregarCultivo/',
        data:datos,
      success:function(data){
        if(data=="error"){
     mensaje("Este Cultivo ya existe","warning","top","Registro de Cultivos");
        }else{
            $('#addRowModal').modal('hide');
        location.reload();
    //  mensaje("Registro realizado con exito","success","bottom","Registro de Cultivos");
      swal({
        title: "Registro Realizado",
        text: "Registro realizado con exito.",
        icon: "success",
        buttons: false,
        timer:3000,
      });
        }
            },
            error:function(x,xs,xt){
              var xd=x.responseText.split("}");
              var obj2=JSON.parse(xd[0]+'}');
              var obj = JSON.parse(xd[1]+'}');
              var xd=obj.errores+"";
              var atributos = "";
             for(var aux in obj2){
             $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}
              mensaje("Favor de llenar los campos de manera Adecuada","danger","top","Registro de Cultivos");
            }
    });
    });

  	</script>
    <script>
    //Script para cargar datos del cultivo

    //ajax  para modificcar
 $("body").on("click","#modificarc",function(event){
   var frm=$("#modificar");
   var datos = frm.serialize();
   $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
     type:'POST',
     url:'/ModificarCultivo/',
     data:datos,
   success:function(data){
location.reload();
         $('#modalEditar').modal('hide');
     swal({
       title: "Modificacion Realizada",
       text: "Modificacion realizada con exito",
       icon: "success",
       buttons: false,
       timer:3000,
     });
//   mensaje("Modificacion realizado con exito","success","bottom","Registro de Cultivos");
         }

         ,
         error:function(x,xs,xt){
          var xd=x.responseText.split("}");
           var obj2=JSON.parse(xd[0]+'}');
           var obj = JSON.parse(xd[1]+"}");
           var atributos = "";
           for(var aux in obj2){
           $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}

         }
   });
   });



    </script>
          @endsection
