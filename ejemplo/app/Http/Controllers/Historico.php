<?php

namespace App\Http\Controllers;
use App\bitacora;
use App\parcela;
use App\ejido;
use App\ejidatario;
use App\canaleros;
use App\municipio;
use App\tenencia;
use App\factura;
use App\ciclo;
use App\concepto;
use App\facturaconcepto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Exports\HistoricosExport;

class Historico extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }
    public function __construct()
    {
      $this->middleware('auth');
      $this->middleware('admin');
    }

    public function verHistorico(){

    $ciclo=ciclo::get();
    $tipo=concepto::select('tipo')->groupBy('tipo')->get();

     return view("Historico.historico",compact('ciclo','tipo'));
    }
    public function total(Request $request){
      $in=$request->get('inicio');
      $fi=$request->get('fin');
      $inicio=$in."T00:00";
      $fin=$fi."T23:59";
        if($request->get('ciclo') ==0){
      $datos=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')
        ->select(DB::raw('SUM(abonos.pago) as total'))->whereBetween('abonos.created_at', array($inicio, $fin))->get();
      }else{
        $datos=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')
          ->select(DB::raw('SUM(abonos.pago) as total'))->where('factura.Id_ciclo','=',$request->get('ciclo'))->get();
      }
      if($request->get('tipo')=="0"){}else{
          $datos=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')->join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
          ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
          ->select(DB::raw('SUM(abonos.pago) as total'))->where('concepto.tipo','=',$request->get('tipo'))->get();
                }
      return ($datos=array('datos' => $datos));
    }
    public function getHistorico(Request $request){

      $in=$request->get('inicio');
      $fi=$request->get('fin');
      $inicio=$in."T00:00";
      $fin=$fi."T23:59";

      if($request->get('ciclo') ==0){
        $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
        ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
        ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
        ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
        ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
        ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
        ->join('abonos','abonos.Id_factura','=','factura.Id_factura')
        ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
        ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
        ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
        ->select('factura_concepto.Total','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','abonos.pago as Total')
       ->whereBetween('abonos.created_at', array($inicio, $fin))->get();

     }else{
      $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
      ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
      ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
      ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
      ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
      ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->join('abonos','abonos.Id_factura','=','factura.Id_factura')
      ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
      ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
      ->select('factura_concepto.Total','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','abonos.pago as Total')
      ->where('factura.Id_ciclo','=',$request->get('ciclo'))->get();
      }
      if($request->get('tipo')=="0"){}else{
        $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
        ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
        ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
        ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
        ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
        ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
        ->join('abonos','abonos.Id_factura','=','factura.Id_factura')
        ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
        ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
        ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
        ->select('factura_concepto.Total','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','abonos.pago as Total')
        ->where('concepto.tipo','=',$request->get('tipo'))->get();
}
        //return response()->json($factura);
   return datatables($factura)->toJson();
    }


    public function exportarExcel(Request $request){
      $export = new HistoricosExport($request);
      $usuario=Auth::user()->id;
      $bitacora=new bitacora();
      $bitacora->Id_usuario=$usuario;
      $bitacora->id_bitacora=0;
      $bitacora->modulo="Historico";
      $bitacora->Id_movimiento=4;
      $bitacora->save();
      return $export->download('Historico.xlsx');
      //return Excel::download(new UsersExport, 'usuarios.xlsx');
    }
}
