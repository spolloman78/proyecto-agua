<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMunicipio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('municipio', function (Blueprint $table) {
          $table->Increments("Id_municipio");
        //  $table->("Id_municipio");
          $table->string("descripcion");
          $table->softDeletes(); //Nueva línea, para el borrado lógico

      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    DB::statement('SET FOREIGN_KEY_CHECKS = 0');
      Schema::dropIfExists('municipio');
      DB::statement('SET FOREIGN_KEY_CHECKS = 1');
  }
}
