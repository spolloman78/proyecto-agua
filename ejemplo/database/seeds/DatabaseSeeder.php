<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(roles::class);
        $this->call(municipio::class);
        $this->call(tenencia::class);
        $this->call(canaleros::class);
        $this->call(ciclo::class);
        $this->call(Users::class);
        $this->call(ejido::class);
        $this->call(tipo_mov::class);
        $this->call(estadosFactura::class);
        $this->call(concepto_gasto::class);
      //  $this->call(ejidatario::class);
      //  $this->call(parcela::class);


    }
}
