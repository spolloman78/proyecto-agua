<?php

namespace App\Http\Controllers;
use App\egreso;
use App\gasto_mes;
use App\conceptogasto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\bitacora;

class Egresos extends Controller
{
  public function __invoke(Request $request)
  {

  }

  public function __construct()
  {
    $this->middleware('auth');
    //$this->middleware('admin');
  }

  public function verEgresos()
  {
    $conceptogasto=conceptogasto::all();
    $egreso=egreso::
    join("concepto_gasto","concepto_gasto.Id_Concepto_gasto","=","gasto.Id_Concepto_gasto")
    ->select("*")->get();

    $granTotal=DB::table('gasto')->get()->sum("monto_total");

    $now = now()->format('Y-m-d');
    $anio = now()->format('Y');
    return view("Egresos.egreso",compact('egreso','conceptogasto','now','anio','granTotal'));
  }

  public function store(Request $request){
    //if($request->ajax()){
      $v = \Validator::make($request->all(), [
        'concepto' => 'required',
        'monto' => 'required',
        'fecha' => 'required',
        'periodo' => 'required'
      ]);
      
      if ($v->fails()){
        echo $v->errors();
        return response()->json(['errors'=>$v->errors()->all()]);
      } else {

        $gastos=DB::table('gasto')
        ->where('Id_Concepto_gasto','=', $request->concepto)
        ->where('periodo', '=', $request->periodo)
        ->first();

        if (is_null($gastos)){
          $egreso = new egreso();
          $egreso->Id_gasto = 0;
          $egreso->Id_Concepto_gasto = $request->concepto;
          $egreso->monto_total = $request->monto;
          $egreso->periodo = $request->periodo;
          $egreso->save();

          $ultimaTupla = DB::table('gasto')->latest('Id_gasto')->first();    

          $gasto_mes = new gasto_mes();
          $gasto_mes->Id_gasto_mes = 0;
          $gasto_mes->Id_Gasto = $ultimaTupla->Id_gasto;
          $gasto_mes->monto = $request->monto;
          $gasto_mes->fecha = $request->fecha;
          $gasto_mes->descripcion = $request->descripcion;
          $gasto_mes->save();

        } else {

          $gasto_mes = new gasto_mes();
          $gasto_mes->Id_gasto_mes = 0;
          $gasto_mes->Id_Gasto = $gastos->Id_gasto;
          $gasto_mes->monto = $request->monto;
          $gasto_mes->fecha = $request->fecha;
          $gasto_mes->descripcion = $request->descripcion;
          $gasto_mes->save();

          $egreso=egreso::find($gastos->Id_gasto);
          $egreso->monto_total = $egreso->monto_total + $request->monto;
          $egreso->save();

        }

        //Registrar a bitácora
        $usuario=Auth::user()->id;
        $bitacora=new bitacora();
        $bitacora->Id_usuario=$usuario;
        $bitacora->id_bitacora=0;
        $bitacora->modulo="Egresos";
        $bitacora->Id_movimiento=1;
        $bitacora->save();

      }

  }

  public function obtenerEgreso($id){
    $datos = DB::table('gasto')
    ->join("concepto_gasto","concepto_gasto.Id_Concepto_gasto","=","gasto.Id_Concepto_gasto")
    ->join("gasto_mes","gasto_mes.Id_Gasto","=","gasto.Id_gasto")
    ->where('Id_gasto_mes', '=', $id)
    ->select('*')
    ->get();
    return ($datos=array('datos' => $datos));
  }

  public function modificar(Request $request){
    if($request->ajax()){
        $v = \Validator::make($request->all(), [
          'monto' => 'required',
        ]);
      if ($v->fails()){
        echo $v->errors();
        return "ERROR 1989i #BUYTICKETSTOTHEXTOUR";
      } else {
        $gasto_mes = gasto_mes::find($request->id_gasto);
        $monto_ant = $gasto_mes->monto;
        $gasto_mes->monto = $request->monto;
        $gasto_mes->descripcion = $request->descripcion;
        $gasto_mes->save();

        $egreso=egreso::find($gasto_mes->Id_Gasto);
        $egreso->monto_total = $egreso->monto_total - $monto_ant + $request->monto;
        $egreso->save();

        $usuario=Auth::user()->id;
        $bitacora=new bitacora();
        $bitacora->Id_usuario=$usuario;
        $bitacora->id_bitacora=0;
        $bitacora->modulo="Egresos";
        $bitacora->Id_movimiento=2;
        $bitacora->save();
      }
    }else{
      return "ERROR 19899 #BUYTICKETSTOTHEXTOUR";
    }
  }

  public function eliminar($id){
    $gasto_mes = gasto_mes::find($id);
    
    $egreso = egreso::find($gasto_mes->Id_Gasto);
    $egreso->monto_total = $egreso->monto_total - $gasto_mes->monto;

    if($egreso->monto_total <= 0){ //Eliminar todos los subgastos y el gasto general
      $gasto_meses = gasto_mes::all();
      foreach($gasto_meses as $g_m){
        if($g_m->Id_Gasto == $egreso->Id_gasto) $g_m->delete();
      }
      $egreso->delete();
    } else {
      $egreso->save();
      $gasto_mes->delete();
    }
    
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Egresos";
    $bitacora->Id_movimiento=3;
    $bitacora->save();
  }

  public function getGastos(){
    $egresos=egreso::
    join("concepto_gasto","concepto_gasto.Id_Concepto_gasto","=","gasto.Id_Concepto_gasto")
    ->whereNull('gasto.deleted_at')
    ->select("*")->get();
    //return $egreso;
    //
    //
    return datatables($egresos)->toJson();
  }

  public function getGastoMes($id){
    $gasto_mes = DB::table('gasto_mes')
    ->where('Id_Gasto', '=', $id)
    ->whereNull('deleted_at')
    ->select('*')
    ->get();

    return response()->json($gasto_mes);
  }

  public function filtrarFechas(Request $request){
    $gasto_mes = DB::table('gasto_mes')
    ->join('gasto', 'gasto_mes.Id_Gasto', "=", 'gasto.Id_gasto')
    ->join('concepto_gasto', 'gasto_mes.Id_Gasto', "=", 'concepto_gasto.Id_Concepto_gasto')
    ->whereNull('gasto_mes.deleted_at')
    ->whereBetween('gasto_mes.fecha', array($request->finicio, $request->ffin))
    ->select('*')
    ->get();
    return response()->json($gasto_mes);
  }

}
