<?php

use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
'id'=>1,
'name'=>'Susana ',
'apPaterno'=>'Adiccion',
'apMaterno'=>'Perez',
'Id_rol'=>1,
'email'=>"admin@admin.com",
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null


]);
  DB::table('users')->insert([
'id'=>2,
'name'=>'Jose ',
'apPaterno'=>'Hernandez',
'apMaterno'=>'Soco',
'Id_rol'=>2,
'email'=>"tesorero@admin.com",
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null

]);
    }
}
