<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGasto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasto', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->increments("Id_gasto");
            $table->unsignedInteger("Id_Concepto_gasto");
            $table->float("monto_total");
            $table->string("periodo",200);
            $table->softDeletes(); //Nueva línea, para el borrado lógico
            $table->timestamps();
            $table->foreign('Id_Concepto_gasto')->references('Id_Concepto_gasto')->on('concepto_gasto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('gasto');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
