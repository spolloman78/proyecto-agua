<?php

namespace App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class tipo_movimiento extends Model
{
    protected $table = 'tipo_movimiento';
    use SoftDeletes; //Implementamos
    protected $dates = ['deleted_at'];
}
