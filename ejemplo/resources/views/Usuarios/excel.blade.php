<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>A. Paterno</th>
            <th>A. Materno</th>
            <th>Rol</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($user as $user)
            <tr class="">
                <td>{{$user->name}}</td>
                <td>{{$user->apPaterno}}</td>
                <td>{{$user->apMaterno}}</td>
                <td>{{$user->Id_rol}}</td>
                <td>{{$user->email}}</td>
            </tr>
        @empty
        @endforelse
    </tbody>
</table>