<?php

namespace App\Http\Controllers;
use App\concepto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\bitacora;
class Conceptos extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request){
      //
  }

  public function __construct(){
      $this->middleware('auth');
      $this->middleware('admin');
  }

  public function verConcepto(){
    $concepto=concepto::get();
    return view("Conceptos/concepto",compact('concepto'));
  }

  public function eliminar($id){
    $valor=str_replace("P","/" , $id);
    concepto::find($valor)->delete();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Concepto Facturas";
    $bitacora->Id_movimiento=3;
    $bitacora->save();
  }

  public function store(Request $request){
    if($request->ajax()){
      $v = \Validator::make($request->all(), [
          'concepto' => ['required','string'],
          'descripcion' => ['required','string'],
          'precio'    => ['required','integer'],
          'tipo' => 'required'
      ]);
      if ($v->fails()) {
        echo $v->errors();
        return response()->json(['errors'=>$v->errors()->all()]);
      } else {
        $concepto=DB::table('concepto')
        ->where('Id_Concepto','=',$request->concepto)->count();
        if($concepto>0){
          echo "error";
        }else{
          $concepto=new concepto();
          $concepto->Id_Concepto=$request->concepto;
          $concepto->descripcion_con=$request->descripcion;
          $concepto->precio=$request->precio;
          $concepto->tipo=$request->tipo;
          $concepto->no_riesgos=$request->riegos;
          $concepto->save();
          $usuario=Auth::user()->id;
          $bitacora=new bitacora();
          $bitacora->Id_usuario=$usuario;
          $bitacora->id_bitacora=0;
          $bitacora->modulo="Conceptos Facturas";
          $bitacora->Id_movimiento=1;
          $bitacora->save();
        }
      }
    }else{
    }}

  public function actualizar($id){
    $valor=str_replace("P","/" , $id);
    $datos = DB::table('concepto')
    ->where('Id_Concepto', '=', $valor)
    ->select('*')
    ->get();

    return ($datos=array('datos' => $datos));
  }

  public function modificar(Request $request){
    echo $request->id;
    if($request->ajax()){
      $v = \Validator::make($request->all(), [
        'Conceptoe' => ['required','string'],
        'Descripcione' => ['required','string'],
        'Precioe'    => ['required','integer'],
        'Tipoe' => 'required',

      ]);
      if ($v->fails()) {

        //return response()->json(['errors'=>$v->errors()->all()]);
        return Response::json(array('errors' => $v->getMessageBag()->toArray()));
      } else {
        $concepto=concepto::find($request->Conceptoe);
        $concepto->descripcion_con=$request->Descripcione;
        $concepto->precio=$request->Precioe;
        $concepto->tipo=$request->Tipoe;
        $concepto->no_riesgos=$request->Riesgose;
        $concepto->save();
        $usuario=Auth::user()->id;
        $bitacora=new bitacora();
        $bitacora->Id_usuario=$usuario;
        $bitacora->id_bitacora=0;
        $bitacora->modulo="Conceptos Facturas";
        $bitacora->Id_movimiento=2;
        $bitacora->save();
      }
    }else{
    
    }
  }
}
