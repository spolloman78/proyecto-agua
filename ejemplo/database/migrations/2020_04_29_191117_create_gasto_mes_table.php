<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGastoMesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasto_mes', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->increments("Id_gasto_mes");
            $table->unsignedInteger("Id_Gasto");
            $table->float("monto"); 
            $table->date("fecha"); 
            $table->longText("descripcion"); 
            $table->softDeletes(); //Nueva línea, para el borrado lógico
            $table->timestamps();
            $table->foreign('Id_Gasto')->references('Id_gasto')->on('gasto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('gasto_mes');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
