<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class facturaconcepto extends Model
{
  protected $table = 'factura_concepto';
  use SoftDeletes; //Implementamos
  protected $dates = ['deleted_at'];
  public $timestamps = false;
  public  $primaryKey = 'Id_factura';
  protected $keyType = 'string';

}
