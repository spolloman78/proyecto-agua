@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Parcelas <i class=" icon-direction"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catálogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Parcelas</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-sm-6 col-md-3">
            <div class="card card-stats card-round">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5">
                    <div class="icon-big text-center">
                      <i class="icon-direction text-info"></i>
                    </div>
                  </div>
                  <div class="col-7 col-stats">
                    <div class="numbers">
                      <p class="card-category"># Parcelas</p>
                      <h4 class="card-title">{{$total}}</h4>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3">
              <div class="card card-stats card-round">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5">
                      <div class="icon-big text-center">
                        <i class="icon-map text-muted"></i>
                      </div>
                    </div>
                    <div class="col-7 col-stats">
                      <div class="numbers">
                        <p class="card-category"># Ejidos</p>
                        <h4 class="card-title">{{$ejidoT}}</h4>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="card card-stats card-round">
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-5">
                        <div class="icon-big text-center">
                          <i class="fas fa-tree text-success"></i>
                        </div>
                      </div>
                      <div class="col-7 col-stats">
                        <div class="numbers">
                          <p class="card-category">Parcelas con Cultivos</p>
                          <h4 class="card-title">{{$parcelaT}}</h4>
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-3">
                  <div class="card card-stats card-round">
                    <div class="card-body ">
                      <div class="row">
                        <div class="col-5">
                          <div class="icon-big text-center">
                            <i class="flaticon-coins text-warning"></i>
                          </div>
                        </div>
                        <div class="col-7 col-stats">
                          <div class="numbers">
                            <p class="card-category">Parcelas Deudoras</p>
                            <h4 class="card-title">{{$deudores}}</h4>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="card-title"  style="padding: 1% 0 0 0;">Catálogo de Parcelas<i class="fas fa-hat-cowboy"></i></h4>
                  </div>
                  @if(Auth::user()->role->descripcion != "Tesorero")
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-center">

                    <button class="btn  btn-round ml-auto"  style="background-color:#00A631; color:white;" data-toggle="modal" data-target="#addRowModal">
                        <i class="fa fa-plus"></i>
                        Parcela
                      </button>
                      <a class="btn btn btn-round"   style="background-color:#00A631; color:white; margin-left:20px;"href="{{'exportarExcelParcelas'}}">
                        <i class="fas fa-file-excel fa-2x"></i>
                        </a>

                    </div>
                  </div>
                  @endif
                </div>
              </div>
              <div class="card-body">

                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                        <th># <i class=" icon-direction"></i></th>
                       <th>Ejidatario</th>

                      <!--  <th>Id. Ejido</th>-->
                        <th>Municipio</th>
                        <th>Canalero</th>
                        <th>Nombre ejido</th>
                        <th>Cuenta</th>
                          <th>SEC</th>
                          <th>CP</th>
                          <th>LT</th>
                          <th>SLT</th>
                          <th>RA</th>
                          <th>PC</th>
                          <th>TE</th>
                          <th>SR</th>
                          <th>EQ</th>
                          <th>Superficie física</th>
                          <th>PRO</th>
                          @if(Auth::user()->role->descripcion != "Tesorero")
                          <th class="text-center">Operaciones</th>@endif
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($parcela as $row)


                      <tr>
                        <td>{!!$row->Id_parcela!!}</td>
                        <td>{!!$row->ejidon!!} {!!$row->apPaterno!!} {!!$row->apMaterno!!}</td>

                      <!--  <td>{!!$row->Id_Ejido!!}</td>-->
                        <td>{!!$row->muni!!}</td>
                        <td>{!!$row->nombre!!}</td>
                        <td>{!!$row->nombre_ej!!}</td>
                        <td>{!!$row->cuenta!!}</td>
                        <td>{!!$row->SEC!!}</td>
                        <td>{!!$row->CP!!}</td>
                        <td>{!!$row->LT!!}</td>
                        <td>{!!$row->SLT!!}</td>
                        <td>{!!$row->RA!!}</td>
                        <td>{!!$row->PC!!}</td>
                        <td>{!!$row->TE!!}</td>
                        <td>{!!$row->SR!!}</td>
                        <td>{!!$row->EQ!!}</td>
                        <td>{!!$row->sup_Fis!!}</td>
                        <td>{!!$row->pro!!}</td>
                        @if(Auth::user()->role->descripcion != "Tesorero")
                        <td>
                          <div class="form-button-action">
                            <button type="button" data-toggle="tooltip" title="Modificar" class="btn btn-link btn-success btn-lg" data-original-title="Ver Detalles" data-id="{{$row->Id_parcela}}"
                               data-toggle="modal" data-target="#modificarParcela"   onclick="modalEdit('{{$row->Id_parcela}}')"  >
                              <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" data-toggle="tooltip" title="Dividir" class="btn btn-link btn-info" onclick="modalDividir('{{$row->Id_parcela}}')" data-original-title="Dividir">
                              <i class="fas fa-crop"></i>
                            </button>

                            <button class="btn  btn-link btn-warning" data-original-title="Detalles" title="Detalles"  data-toggle="modal"  onclick="modalCultivo('{{$row->Id_parcela}}','{!!$row->ejidon!!}  {!!$row->apPaterno!!}  {!!$row->apMaterno!!}')">
                            <i class="fa fa-info"></i>
                            </button>

                            <button type="button" data-toggle="tooltip" title="Eliminar" class="btn btn-link btn-danger" onclick="eliminar('{!!$row ->Id_parcela!!}')"data-original-title="Eliminar">
                              <i class="fa fa-times"></i>
                            </button>

                          </div>
                        </td>@endif
                      </tr>

    @endforeach
                    </tbody>
                  </table>
                </div>
                <br>

              </div>
            </div>
          </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="addRowModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" >
              <span class="fw-mediumbold">
                Registrar</span>
              <span class="fw-light">
                Parcela   <i class="icon-direction" style="color:green; "></i>
              </span>
            </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <p class="small">En este panel usted podrá realizar registros de nuevas parcelas en el sistema.</p>
              <form id="registros"action="{{route('Aparcela')}}">
                {{csrf_field()}}
                  <p class="text-danger" id="textoagregar" style="margin-left:10px;"></p>
                <div class="row">
                  <div class="col-md-3">
                      <div class="form-group form-group-default">
                        <label for="cuenta">Ejido</label>
                          <select id="" class="form-control js-example-basic-single" style="width: 100%;"name="Id_Ejido">
                            @foreach($ejidos as $row)
                                 <option value="{!!$row->Id_Ejido!!}"> {!!$row->nombre_ej!!}</option>
                               @endforeach
                          </select>
                      </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group form-group-default">
                      <label for="concepto">Canalero</label>
                        <select class="form-control js-example-basic-single" style="width: 100%;"name= "Id_canalero" id="">
                          @foreach($canalero as $row)
                         <option value="{!!$row->Id_canalero!!}"> {!!$row->nombre!!}</option>
                       @endforeach
                        </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group form-group-default">
                      <label for="void">Ejidatario</label>
                        <select class="form-control js-example-basic-single" style="width: 100%;"name="Id_ejidatario" id="">
                          @foreach($ejidatarios as $row)
                            <?php $valor = ($row->nombre)." ".($row->apPaterno)." ".($row->apMaterno)?>
                         <option value="{!!$row->Id_ejidatario!!}">{{$valor}} </option>
                        @endforeach
                        </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group form-group-default">
                      <label>Municipio</label>
                        <select class="form-control js-example-basic-single" style="width: 100%;"name="Id_municipio">
                          @foreach($municipio as $row)
                         <option value="{!!$row->Id_municipio!!}"> {!!$row->descripcion!!}</option>
                       @endforeach
                        </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group form-group-default">
                      <label>Cuenta</label>
                        <input id=""  name="cuenta"type="text" class="form-control" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group form-group-default">
                      <label>CP</label>
                      <input id=""  name="CP"type="number" class="form-control" placeholder="CP" required>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group form-group-default">
                      <label>SEC</label>
                        <input id=""  name="SEC"type="number" class="form-control" placeholder="SEC" required>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group form-group-default">
                      <label>LT</label>
                        <input id=""  name="LT" type="number" class="form-control" placeholder="LT" required>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group form-group-default">
                      <label>SLT</label>
                      <input id=""  name="SLT" type="number" class="form-control" placeholder="SLT" required>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group form-group-default">
                      <label>RA</label>
                      <input id=""  name="RA" type="number" class="form-control" placeholder="RA" required>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group form-group-default">
                      <label>PC</label>
                        <input id=""  name="PC" type="number" class="form-control" placeholder="PC" required>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group form-group-default">
                      <label>TE</label>
                      <select id="" class="form-control js-example-basic-single" style="width: 100%;"name="TE">
                        @foreach($tenencia as $row)
                             <option value="{!!$row->Id_tenencia!!}"> {!!$row->descripcionT!!}</option>
                           @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group form-group-default">
                      <label>SR</label>
                        <input id=""  name="SR" type="number" class="form-control" placeholder="SR" required>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group form-group-default">
                      <label>EQ</label>
                        <input id=""  name="EQ" type="number" class="form-control" placeholder="EQ" required>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group form-group-default">
                      <label>PRO</label>
                        <input id=""  name="pro" type="number" class="form-control" placeholder="PRO" required>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group form-group-default">
                      <label>Superficie fisica</label>
                      <input id=""  name="sup"type="number" class="form-control" min="0" step="0.01" max="200"placeholder="Superficie Fisica" required>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" id="agregar" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Agregar </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

<!------Modal Editar--->
          <div class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal_grande">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" >
                    <span class="fw-mediumbold">
                    Datos de la</span>
                    <span class="fw-light">
                      Parcela   <i class="icon-direction" style="color:green; "></i>
                    </span>
                  </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
              <p class="small">En este panel usted podrá modificar los registros de parcelas en el sistema.</p>
                <form id="registroM"action="{{route('editarParcela')}}" method="post" >
                    {{csrf_field()}}
                  <div class="row">
                      <div class="col-md-3">
                        <input id="Id_parcela"name="Id_parcela" hidden >
                          <div class="form-group form-group-default">
                              <label for="cuenta">Ejido</label>
                              <select id="ejido" style="width: 100%;"class="form-control js-example-basic-single"name="ejido">
                           @foreach($ejidos as $row)
                                <option value="{{!!$row->Id_Ejido!!}}"> {!!$row->nombre_ej!!}</option>
                              @endforeach
                                </select>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                            <label for="concepto">Canalero</label>
                            <select class="form-control js-example-basic-single"name= "canalero" id="canalero">
                               @foreach($canalero as $row)
                              <option value="{{!!$row->Id_canalero!!}}"> {!!$row->nombre!!}</option>
                            @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group form-group-default">
                              <label for="void">Ejidatario</label>
                              <select class="form-control js-example-basic-single"style="width: 100%;"name="ejidatarios" id="ejidatario">
                                @foreach($ejidatarios as $row)
                                  <?php $valor = ($row->nombre)."".($row->apPaterno)." ".($row->apMaterno)?>
                               <option value="{{!!$row->Id_ejidatario!!}}">{{$valor}} </option>
                             @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="col-md-3">
                            <div class="form-group form-group-default">
                              <label>Municipio</label>
                              <select class="form-control js-example-basic-single" style="width: 100%;" name="municipio"id="municipio">
                                @foreach($municipio as $row)
                               <option value="{{!!$row->Id_municipio!!}}"> {!!$row->descripcion!!}</option>
                             @endforeach
                              </select>
                            </div>
                        </div>
                                      <div class="col-md-4">
                                        <div class="form-group form-group-default">
                                          <label>Cuenta</label>
                                          <input id="cuenta"  name="cuenta"type="text" class="form-control" placeholder="Cuenta" required>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="form-group form-group-default">
                                          <label>CP</label>
                                          <input id="CP"  name="CP"type="number" class="form-control" placeholder="CP" required>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="form-group form-group-default">
                                          <label>SEC</label>
                                          <input id="SEC"  name="SEC"type="number" class="form-control" placeholder="SEC" required>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="form-group form-group-default">
                                          <label>LT</label>
                                          <input id="LT"  name="LT"type="number" class="form-control" placeholder="LT" required>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="form-group form-group-default">
                                          <label>SLT</label>
                                          <input id="SLT"  name="SLT"type="number" class="form-control" placeholder="SLT" required>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="form-group form-group-default">
                                          <label>RA</label>
                                          <input id="RA"  name="RA"type="number" class="form-control" placeholder="RA" required>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                          <label>PC</label>
                                          <input id="PC"  name="PC"type="number" class="form-control" placeholder="PC" required>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                          <label>TE</label>
                                          <select id="TE"  name="TE" class="form-control"  required>
                                          @foreach($tenencia as $row)
                                               <option value="{!!$row->Id_tenencia!!}"> {!!$row->descripcionT!!}</option>
                                             @endforeach
                                           </select>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                          <label>SR</label>
                                          <input id="SR"  name="SR"type="number" class="form-control" placeholder="SR" required>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                          <label>EQ</label>
                                          <input id="EQ"  name="EQ"type="number" class="form-control" placeholder="EQ" required>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                          <label>PRO</label>
                                          <input id="pro"  name="PRO"type="number" class="form-control" placeholder="PRO" required>
                                        </div>
                                      </div>

                                      <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                          <label>Superficie fisica</label>
                                          <input id="sup"  name="sup"type="number" class="form-control" min="0" step="0.01" max="200"placeholder="Superficie Fisica" required>
                                        </div>
                                      </div>
                                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" id="modificar" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-edit"></i> Modificar </button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
              </form>
              </div>
            </div>
            </div>
            </div>

<!-- Modal dividir-->
          <div class="modal fade " role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal_detalles">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title  no-bd" >
                    <span class="fw-mediumbold">
                    Dividir </span>
                    <span class="fw-light">
                      Parcela   <i class="icon-direction" style="color:green; "></i>
                    </span>
                  </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
              <p class="small">En este panel usted podrá dividir las parcelas en el sistema.<br/><Strong>Es necesario no repetir el numero de tarjeta e indicar la medida de la parcela a dividir.</strong></p>
                <form id="registroD"action="{{route('DivParcela')}}" method="post" >
                    {{csrf_field()}}
                      <p class="text-danger" id="textodividir" style="margin-left:10px;"></p>
                  <div class="row">
                      <div class="col-md-4">
                        <input id="Id_parcela1"name="Id_parcela" hidden>
                          <div class="form-group form-group-default">
                              <label for="void">Ejidatario</label>
                              <select class="form-control js-example-basic-single" style="width:100%;"name="ejidatarios1" id="ejidatarios1">
                                @foreach($ejidatarios as $row)
                                  <?php $valor = ($row->nombre).($row->apPaterno)." ".($row->apMaterno)?>
                               <option value="{{!!$row->Id_ejidatario!!}}">{{$valor}} </option>
                             @endforeach
                              </select>
                          </div>
                      </div>

                                      <div class="col-md-3">
                                        <div class="form-group form-group-default">
                                          <label>Cuenta </label>
                                          <input id="cuenta2"  name="cuenta"type="text" class="form-control" placeholder="Cuenta" required>
                                        </div>
                                      </div>


                                      <div class="col-sm-5">
                                        <div class="form-group form-group-default">
                                          <label>Superficie fisica</label>
                                          <input id="supo"  name="supo"type="number" class="form-control" min="0" step="0.01" max="200"placeholder="Superficie Fisica" required>
                                        </div>
                                      </div>
                                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" id="dividir" class="btn btn-info">  <i class="fa fa-crop"></i>  Dividir </button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
              </form>
              </div>
            </div>
            </div>
            </div>

            <!-- Modal info-->
          <div class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="addRowModal1">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title  no-bd" >
                    <span class="fw-mediumbold">
                    Información </span>
                    <span class="fw-light">
                      Parcela        <i id="icono" class="fas fa-money-bill-wave fa-2x"></i>
                    </span>
                  </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <div class="modal-body">
               <p class="small">En este panel usted podrá ver informacion de las parcelas en el sistema.</p>
                <form id=""action="" method="post" >
                    {{csrf_field()}}

                       <h5 class="text-danger" id="textomensaje" style="margin-left:60px;"></h5>
                       <h5 class="text-success" id="textomensaje2" style="margin-left:70px;"></h5>

                  <div class="row">
                    <div class="col-md-5">
                          <div class="form-group form-group-default">
                              <label for="void">Cultivo</label>
                                <input id="cultivo" name="cultivo" class="form-control" disabled>
                          </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-group-default">
                          <label>Deuda</label>
                          <input id="pagos" name="pagos"class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group form-group-default">
                          <label>Superficie de Riego</label>
                          <input id="Superficie"  name="sup" class="form-control" disabled>
                        </div>
                      </div>


                      <div class="col-md-5">
                        <div class="form-group form-group-default">
                          <label>Ciclo Agrario</label>
                          <input id="CicloA"  name="sup" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="form-group form-group-default">
                          <label>Fecha de Visita</label>
                          <input id="Fechax"  name="sup" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <input id="Id_parcela1"name="Id_parcela" hidden>
                          <div class="form-group form-group-default">
                              <label for="void">Ejidatario</label>
                                <input id="ejida" name="ejida" class="form-control" disabled>

                          </div>
                      </div>
                  </div>
                </div>
                <div class="modal-footer">

                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
              </form>
              </div>
            </div>
            </div>


<script>

</script>


          <script>
          function eliminar(id){
            swal({
              title: '¿Seguro que desea eliminar el registro?',
              text: "¡Esta accion no es reversible!",
              type: 'warning',
              icon : "error",
              buttons:{
                confirm: {
                  text : 'Si, deseo eliminarlo!',
                  className : 'btn btn-danger',
                },
                cancel: {
                  text : 'Cancelar',
                  visible: true,
                  className: 'btn btn-warning'
                }
              }
            }).then((Delete) => {
              if (Delete) {
                $.ajax({
                url:'/BorrarParcela/'+id,
                type:'get'
                }).done(function (res){
                swal({
                  title: 'Eliminado!',
                  text: 'El registro ha sido Eliminado.',
                  icon : "success",
                  buttons:false,
                  timer:3000,
                });
                  location.reload();
                  });
              } else {
                swal.close();
              }
            });
          }

          function modalCultivo(id,ejido){

            $.ajax({
              url:'/CultivoParcela/'+ id,
              type:'get',
               success:function(data){
                 if(data.datos==null){
                  $('#addRowModal1').modal('hide');

                  swal({
                    title: 'Sin Cultivos',
                    text: 'No tiene cultivos registrados en este Ciclo agrario.',
                    icon : "info",
                    buttons:false,
                    timer:3000,
                  });
              }else{
                $('#addRowModal1').modal('show');
                $('#cultivo').val(data.datos.cultivo);
                $('#Superficie').val(data.datos.sup_cultivo);
                $('#ejida').val(ejido);
                $('#Fechax').val(data.datos.created_at);
                $('#CicloA').val(data.datos.ciclo);
                $('#pagos').val("$ "+data.datos.can_pagada);
              }
              },error:function(x,xs,xt){
                      alert("Error  no se pudo encontrar informacion "+xt);
                  }
          });
          $.ajax({
                  url: '/deudorFact/'+id,
                  type:"GET",
                  dataType:"json",
                  success:function(data){

                    if(data.estado==3){
                      var a=document.getElementById("icono");
                       a.style.color="red";
                    document.getElementById('textomensaje').innerHTML='Presenta un ADEUDO';
                    document.getElementById('textomensaje2').innerHTML='';
                    }else{
                      $('#pagos').val("$ 0.00");
                      var a=document.getElementById("icono");
                       a.style.color="green";
                      document.getElementById('textomensaje').innerHTML='';
                      document.getElementById('textomensaje2').innerHTML='No presenta Adeudos';
                     }
                  },error:function(x,xd,xdd){
                    alert(x.responseText);
                  }});

        }
          function modalEdit(id){
            var ejido = document.getElementById("ejido");
            var muni = document.getElementById("municipio");
            var ejidatario = document.getElementById("ejidatario");
            var canalero = document.getElementById("canalero");
            var te = document.getElementById("TE");
               $.ajax({
                 url:'/ParcelaDatos/'+id,
                 type:'get'

               }).done(function (res){
                $("#modal_grande").modal('show');
                $("#Id_parcela").val(id);
                $("#cuenta").val(res.datos[0].cuenta);
                $("#SEC").val(res.datos[0].SEC);
                $("#CP").val(res.datos[0].CP);
                $("#LT").val(res.datos[0].LT);
                $("#SLT").val(res.datos[0].SLT);
                $("#RA").val(res.datos[0].RA);
                $("#PC").val(res.datos[0].PC);
              //  $("#TE").val(res.datos[0].TE);
                $("#SR").val(res.datos[0].SR);
                $("#EQ").val(res.datos[0].EQ);
                $("#sup").val(res.datos[0].sup_Fis);
                $("#pro").val(res.datos[0].pro);

                document.getElementById("ejido").innerHTML += "<option value='"+res.datos[0].Id_Ejido+"'selected >"+res.datos[0].nombre_ej+"</option>";
                ejido.remove(res.datos[0].Id_Ejido);
                document.getElementById("canalero").innerHTML += "<option value='"+res.datos[0].Id_canalero+"'selected >"+res.datos[0].nombre+"</option>";
                canalero.remove(res.datos[0].Id_canalero);
                document.getElementById("municipio").innerHTML += "<option value='"+res.datos[0].Id_municipio+"'selected >"+res.datos[0].descripcion+"</option>";
                municipio.remove(res.datos[0].Id_municipio);
                document.getElementById("ejidatario").innerHTML += "<option value='"+res.datos[0].Id_ejidatario+"'selected >"+res.datos[0].ejidon+" "+res.datos[0].apPaterno+" "+res.datos[0].apMaterno+"</option>";
                ejidatario.remove(res.datos[0].Id_ejidatario);
                document.getElementById("TE").innerHTML += "<option value='"+res.datos[0].Id_tenencia+"'selected >"+res.datos[0].TE+"</option>";
              //  te.remove(res.datos[0].Id_tenencia);
               });
               }
               function modalDividir(id){
                 var ejido = document.getElementById("ejido");
                 var muni = document.getElementById("municipio");
                 var ejidatario = document.getElementById("ejidatarios1");
                 var canalero = document.getElementById("canalero");
                    $.ajax({
                      url:'/ParcelaDatos/'+id,
                      type:'get',
                      dataType: 'json'
                    }).done(function (res){
                     $("#modal_detalles").modal('show');
                    // $("#Id_parcela").val(id);
                      document.getElementById("ejidatarios1").innerHTML += "<option value='"+res.datos[0].Id_ejidatario+"'selected >"+res.datos[0].ejidon+" "+res.datos[0].apPaterno+" "+res.datos[0].apMaterno+"</option>";
                     $("#cuenta2").val(res.datos[0].cuenta);
                     $("#supo").val(res.datos[0].sup_Fis);
                     $("#Id_parcela1").val(id);
                    });
                    }

               //ajax para modificar
               $("body").on("click","#modificar",function(event){
                 var frm=$("#registroM");
                 var datos = frm.serialize();
                 $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });
                  $.ajax({
                   type:'POST',
                   url:'/parcelaEditar',
                   data:datos,
                 success:function(data){

               location.reload();
                       $('#modal_grande').modal('hide');
                       swal({
                         title: 'Modificado',
                         text: 'El registro ha sido modificado con exito.',
                         icon : "success",
                         buttons:false,
                         timer:3000,
                       });
                       }
                       ,
                       error:function(x,xs,xt){
                         var xd=x.responseText.split("}");
                          var obj2=JSON.parse(xd[0]+'}');
                          var obj = JSON.parse(xd[1]+"}");
                          var atributos = "";
                          for(var aux in obj2){
                          $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}


                       }
                 });
                 });
            //Ajax para agregar xd
            $("body").on("click","#agregar",function(event){
              var frm=$("#registros");
              var datos = frm.serialize();
              alert(datos);
              $.ajaxSetup({
                   headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
               });
               $.ajax({
                type:'POST',
                url:'/parcelaAgregar',
                data:datos,
              success:function(data){
                if(data=="error"){
                }else{
            location.reload();
                    $('#addRowModal').modal('hide');
                    swal({
                      title: 'Parcela registrada',
                      text: 'El registro ha sido realizado con exito.',
                      icon : "success",
                      buttons:false,
                      timer:3000,
                    });

              }}

                    ,
                    error:function(x,xs,xt){

                      var xd=x.responseText.split("}");
                       var obj2=JSON.parse(xd[0]+'}');
                       var obj = JSON.parse(xd[1]+"}");
                       var atributos = "";
                       for(var aux in obj2){
                       $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}

                      document.getElementById('textoagregar').innerHTML='Favor de llenar los campos de manera adecuada';
                    }
              });
              });




            //Ajax para dividir la parcela
            $("body").on("click","#dividir",function(event){
              var frm=$("#registroD");
              var datos = frm.serialize();
              $.ajaxSetup({
                   headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
               });
               $.ajax({
                type:'POST',
                url:'/parcelaDividir',
                data:datos,
              success:function(data){
            document.getElementById('textodividir').innerHTML='';
                if(data=="error"){
              document.getElementById('textodividir').innerHTML='Favor de Checar los datos.';
                }else{
            location.reload();
                    $('#modal_detalles').modal('hide');
                swal({
                  title: "Parcela Dividida",
                  text: "La parcela ha sido Dividida con exito.",
                  icon: "success",
                  buttons:false,
                  timer:3000,
                });

              }}                    ,
                    error:function(x,xs,xt){
                      var xd=x.responseText.split("}");
                       var obj2=JSON.parse(xd[0]+'}');
                       var obj = JSON.parse(xd[1]+"}");
                       var atributos = "";
                       for(var aux in obj2){
                       $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}

                    }
              });
              });

          </script>
          @endsection
