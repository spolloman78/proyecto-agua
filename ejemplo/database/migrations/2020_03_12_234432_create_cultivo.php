<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCultivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cultivo', function (Blueprint $table) {
            $table->increments("Id_cultivo");
            $table->string("descripcion");
            $table->Integer("capacidad_cultivo");
            $table->Integer("cantidad_cultivo");
            $table->Integer("prioridad");
            $table->softDeletes(); //Nueva línea, para el borrado lógico

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('cultivo');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
