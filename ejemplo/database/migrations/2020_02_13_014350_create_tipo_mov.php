<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoMov extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tipo_movimiento', function (Blueprint $table) {

$table->increments("Id_movimiento");
$table->string("descripcion");
$table->softDeletes(); //Nueva línea, para el borrado lógico

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('tipo_movimiento');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
