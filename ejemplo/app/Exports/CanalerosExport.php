<?php

namespace App\Exports;
use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use App\Canaleros;

class CanalerosExport implements FromView
{
      use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view():View
    {
      $canaleros=canaleros::get();
      return view('Canaleros.excel',[
          'canaleros' => $canaleros
      ]);

    }
}
