<?php

namespace App\Http\Controllers;
use App\ejidatario;
use App\bitacora;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
use App\Exports\EjidatariosExport;


class Ejidatarios extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {

  }

  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('admin')->except(['verEjidatario', 'store']);

  }

  public function eliminar($id){
    ejidatario::find($id)->delete();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Ejidatarios";
    $bitacora->Id_movimiento=3;
    $bitacora->save();
  }

  public function verEjidatario()
  {
    $eji=ejidatario::count();
    $sin=ejidatario::where('tarjeta','=','B-1000')->orwhere('tarjeta','=','1000')->count();
    $ejidatario=ejidatario::get();
 $con=$eji-$sin;
    return view("Ejidatarios/ejidatario",compact('ejidatario','eji','sin','con'));
  }

  public function store(Request $request){
    if($request->ajax()){
      $v = \Validator::make($request->all(), [
         'nombre' => 'required',
         'apPaterno' => 'required',
         'apMaterno'    => 'required',
         'tarjeta' => 'required'
      ]);
      if ($v->fails())
      {
        echo $v->errors();
        return response()->json(['errors'=>$v->errors()->all()]);
        //return Response::json(array('errors' => $v->getMessageBag()->toArray()));
      }else {
        $cultivos=DB::table('ejidatario')
          ->where('tarjeta','=',$request->tarjeta)->count();

        if($cultivos>0){
          echo "error";
        }else{
          $ejidatario=new ejidatario();
          $ejidatario->Id_ejidatario=0;
          $ejidatario->nombre=$request->nombre;
          $ejidatario->apPaterno=$request->apPaterno;
          $ejidatario->apMaterno=$request->apMaterno;
          $ejidatario->tarjeta=$request->tarjeta;
          $ejidatario->tarjeta_ant="000";
          $ejidatario->save();
          $usuario=Auth::user()->id;
          $bitacora=new bitacora();
          $bitacora->Id_usuario=$usuario;
          $bitacora->id_bitacora=0;
          $bitacora->modulo="Ejidatarios";
          $bitacora->Id_movimiento=1;
          $bitacora->save();
        }
      }
    }else{

    }
  }

  public function actualizar($id_ejidatario){
    $datos = DB::table('ejidatario')
      ->where('Id_ejidatario', '=', $id_ejidatario)
      ->select('*')
      ->get();

    return ($datos=array('datos' => $datos));
  }

  public function modificar(Request $request){
    echo $request->id;
    if($request->ajax()){
      $v = \Validator::make($request->all(), [
           'nombre' => 'required',
           'apMaterno' => 'required',
           'apPaterno'    => 'required',
           'tarjeta'    => 'required',
           'tarjeta_ant'    => 'required',
          ]);

      if ($v->fails())
      {
        echo $v->errors();
        return response()->json(['errors'=>$v->errors()->all()]);
      } else {
        $ejidatario=ejidatario::find($request->id);
        $ejidatario->nombre=$request->nombre;
        $ejidatario->apPaterno=$request->apPaterno;
        $ejidatario->apMaterno=$request->apMaterno;
        $ejidatario->tarjeta=$request->tarjeta;
        $ejidatario->tarjeta_ant=$request->tarjeta_ant;
        $ejidatario->save();
        $usuario=Auth::user()->id;
        $bitacora=new bitacora();
        $bitacora->Id_usuario=$usuario;
        $bitacora->id_bitacora=0;
        $bitacora->modulo="Ejidatarios";
        $bitacora->Id_movimiento=2;
        $bitacora->save();
      }
    } else {

    }

  }

  public function exportarExcel(){
    $export = new EjidatariosExport();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Ejidatarios";
    $bitacora->Id_movimiento=4;
    $bitacora->save();
    return $export->download('Ejidatarios.xlsx');
    //return Excel::download(new UsersExport, 'usuarios.xlsx');
  }
	
  public function PDF(){
	  $ejidatario=DB::table('ejidatario')
	  ->join('parcela', 'parcela.Id_ejidatario', "=", 'ejidatario.Id_ejidatario')
	  ->select('parcela.Id_parcela', 'parcela.SEC', 'parcela.CP', 'parcela.LT', 'parcela.SLT', 'parcela.RA', 				 'parcela.PC', 'ejidatario.apPaterno', 'ejidatario.apMaterno', 'ejidatario.nombre',
			   'ejidatario.tarjeta', 'ejidatario.Id_ejidatario', 'parcela.Id_ejidatario')
	  ->where('parcela.Id_parcela', '<', '200')
	  ->whereRaw('parcela.Id_ejidatario', 'ejidatario.Id_ejidatario')
	  ->get();
	  //dd($ejidatario);
	  //return ($ejidatario);
	  $pdf = \PDF::loadView('Canaleros/pdf', ['ejidatarios' => $ejidatario]);
	//dd($ejidatarios);
	  return $pdf->download('libro_canaleros.pdf');
	  
  }

}
