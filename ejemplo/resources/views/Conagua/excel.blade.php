<center><h2>CÁTALOGODE DE REGISTROS DE CONAGUA</h2></center>
<table>
<thead>
  <tr>
    <th>#</th>
    <th>USIPAD</th>
    <th>Nombre del Ejidatario</th>
    <th>CTA_SIPAD</th>
    <th>UNI</th>
    <th>ZO</th>
    <th>MOD</th>
    <th>SRA</th>
    <th>SSRA</th>
    <th>EST</th>
    <th>GR</th>
    <th>PRE</th>
    <th>SUPFISICA</th>
    <th>SUPRIEGO</th>
    <th>SEC_ORIG</th>
  </tr>
</thead>

<tbody>
  @forelse($conagua as $row)
    <tr>
      <td>{!!$row->Id_Conagua!!}</td>
      <td>{!!$row->USIPAD!!}</td>
      <td>{!!$row->nombre!!} {!!$row->apPaterno!!} {!!$row->apMaterno!!} </td>
      <td>{!!$row->CTA_SIPAD!!}</td>
      <td>{!!$row->UNI!!}</td>
      <td>{!!$row->ZO!!}</td>
      <td>{!!$row->MOD!!}</td>
      <td>{!!$row->SRA!!}</td>
      <td>{!!$row->SSRA!!}</td>
      <td>{!!$row->EST!!}</td>
      <td>{!!$row->GR!!}</td>
      <td>{!!$row->PRE!!}</td>
      <td>{!!$row->SUPFISICA!!}</td>
      <td>{!!$row->SUPRIEGO!!}</td>
      <td>{!!$row->SEC_ORIG!!}</td>
    </tr>
      @empty
      @endforelse
    </tbody>
  </table>
