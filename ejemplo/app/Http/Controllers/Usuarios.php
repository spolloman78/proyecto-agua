<?php

namespace App\Http\Controllers;
use App\User;
use App\bitacora;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Exports\UsuariosExport;

class Usuarios extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

	}

    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}

    public function verUsuario()
    {
      $usuario=User::all();
      return view("Usuarios/usuario",compact('usuario'));
    }

    public function eliminar($id){
    	User::find($id)->delete();
  	}

	public function actualizar($id_usuario){
		$datos = DB::table('users')
			->where('id', '=', $id_usuario)
			->select('*')
			->get();
		$data=DB::table('roles')
		   ->select('*')
		   ->get();

		return ($datos=array('datos' => $datos));
			//return response()->json($datos);
	}

	public function editUser(Request $request){
		//dd($request->all());
		$id_usuario = $request->input('id_user');
		//echo $id_usuario;
		$user = User::find($id_usuario);
    $user->name = $request->input('nom_user');
    $user->apPaterno = $request->input('apPaterno');
		$user->apMaterno = $request->input('apMaterno');
		$user->Id_rol = $request->input('rol');
		$user->email = $request->input('mail');
        $user->save();
		$usuario=Auth::user()->id;
		$bitacora=new bitacora();
		$bitacora->Id_usuario=$usuario;
		$bitacora->id_bitacora=0;
		$bitacora->modulo="Usuarios";
		$bitacora->Id_movimiento=2;
		$bitacora->save();
	}

	public function createUser(Request $request){
		$user = new User;
    $user->name = $request->nom_user;
    $user->apPaterno = $request->apPaterno;
		$user->apMaterno = $request->apMaterno;
    $user->password=bcrypt($request->password);
		$user->Id_rol = $request->rol;
		$user->email = $request->mail;
    $user->save();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Usuarios";
    $bitacora->Id_movimiento=1;
    $bitacora->save();
		return response()->json($user);
	}
  public function cambiar(Request $request){

  $usuario=User::find($request->id);
  $usuario->password=bcrypt($request->contra);
  $usuario->save();
  $usuario=Auth::user()->id;
  $bitacora=new bitacora();
  $bitacora->Id_usuario=$usuario;
  $bitacora->id_bitacora=0;
  $bitacora->modulo="Usuarios";
  $bitacora->Id_movimiento=2;
  $bitacora->save();
  }

	public function exportarExcel(){
		$export = new UsuariosExport();
		$usuario=Auth::user()->id;
		$bitacora=new bitacora();
		$bitacora->Id_usuario=$usuario;
		$bitacora->id_bitacora=0;
		$bitacora->modulo="Usuarios";
		$bitacora->Id_movimiento=4;
		$bitacora->save();
		return $export->download('Usuarios.xlsx');
		//return Excel::download(new UsersExport, 'usuarios.xlsx');
	  }

}
