@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Canaleros <i class="fas fa-fill-drip"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catálogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Canaleros</a>
            </li>
          </ul>

        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">

              <div class="card-header">
                <h4 class="card-title">Catálogo de Canaleros<i class="fas fa-hat-cowboy"></i></h4>

                <div class="d-flex align-items-center">


                  <button class="btn btn btn-round ml-auto"  style="background-color:#00A631; color:white;" data-toggle="modal" data-target="#canalero">
                    <i class="fa fa-plus"></i>
                     Canaleros
                  </button>
                  <a class="btn btn btn-round"  style="background-color:#00A631; color:white; margin-left:20px;" href="{{'exportarExcelCanaleros'}}">
                  <i class="fas fa-file-excel fa-2x"></i>

                  </a>


                </div>

              </div>
              <div class="card-body">

                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                        <th># <i class="fas fa-fill-drip"></i></th>
                        <th>Nombre</th>
                        <th>A. Paterno</th>
                        <th>A. Materno</th>
                        <th>Telefono</th>
                        <th>Operaciones</th>
                      </tr>
                    </thead>

                    <tbody>
                    @foreach($canalero as $row)
                      <tr>
                        <td>{!!$row->Id_canalero!!}</td>
                        <td>{!!$row->nombre!!}</td>
                        <td>{!!$row->paterno!!}</td>
                        <td>{!!$row->materno!!}</td>
                        <td>{!!$row->telefono!!}</td>
                        <td>
                          <div class="form-button-action">
                            <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-success btn-lg" data-original-title="Editar Registro"data-toggle="modal" data-target="#modalEditar" onclick="modalCanalero('{!!$row ->Id_canalero!!}')" title="">
                              <i class="fa fa-edit"></i>
                            </button>
                            <button type="button" data-toggle="tooltip" title="Eliminar" class="btn btn-link btn-danger" onclick="eliminar('{!!$row ->Id_canalero!!}')"data-original-title="Eliminar">
                              <i class="fa fa-times"></i>
                            </button>

                          </div>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <br>
                
              </div>
            </div>
          </div>


    <!-- Modal -->
    <div class="modal fade " id="canalero" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" >
              <span class="fw-mediumbold">
                Registrar</span>
              <span class="fw-light">
                Canalero   <i class="fas fa-fill-drip" style="color:green; "></i>
              </span>
            </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <p class="small">En este panel usted podrá realizar registros de nuevos canaleros en el sistema.</p>
              <form id="regeji" action="{{route('Acanalero')}}">
                {{csrf_field()}}
                <p class="text-danger" id="mensaje1"></p>
                <div class="row">
                  <div class="col-md-5">
                      <div class="form-group form-group-default">
                        <label>Nombre</label>
                            <input id="addName"  name="nombre"type="text" class="form-control" placeholder="Nombre" required>
                      </div>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group form-group-default">
                      <label>A. Paterno</label>
                      <input id="addApp"  name="paterno"type="text" class="form-control" placeholder="A. Paterno" required>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group form-group-default">
                      <label>A. Materno</label>
                        <input id="addApm"  name="materno"type="text" class="form-control" placeholder="A. Materno" required>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group form-group-default">
                      <label>Telefono</label>
                            <input id="addTarj"  name="telefono" type="text" class="form-control" placeholder="Telefono" required>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" id="addcanalero" class="btn" style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Agregar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

<!------Modal Editar--->
          <div class="modal fade " id="modalEditar" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" >
                    <span class="fw-mediumbold">
                    Datos de</span>
                    <span class="fw-light">
                      Canaleros   <i class="fas fa-fill-drip" style="color:green; "></i>
                    </span>
                  </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
              <p class="small">En este panel usted podrá modificar el registros de canaleros en el sistema.</p>
                <form id="registroM"action="{{route('actualizarcanalero')}}" method="post" >
                    {{csrf_field()}}
                <p class="text-danger" id="mensaje2"></p>
                  <div class="row">
                      <div class="col-md-5">
                        <input id="id"name="id" hidden >
                          <div class="form-group form-group-default">
                            <label>Nombre</label>
                            <input id="nombree"  name="nombree" type="text" class="form-control" placeholder="Nombre" required>
                          </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group form-group-default">
                          <label>A. Paterno</label>
                            <input id="paternoe"  name="paternoe" type="text" class="form-control" placeholder="A. Paterno" required>
                        </div>
                      </div>
                      <div class="col-md-5">
                          <div class="form-group form-group-default">
                              <label>A. Materno</label>
                            <input id="maternoe"  name="maternoe" type="text" class="form-control" placeholder="Materno" required>
                          </div>
                      </div>
                      <div class="col-md-5">
                            <div class="form-group form-group-default">
                              <label>Telefono</label>
                            <input id="telefonoe"  name="telefonoe" type="number" class="form-control" placeholder="Telefono" min="1" max="9999999999" required>
                            </div>
                      </div>
                  </div>
                <div class="modal-footer">
                  <button type="button" id="modificar" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-edit"></i> Modificar </button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
              </form>
              </div>
            </div>
            </div>
            </div>

          <script>
        // Funcion para eliminar
            function eliminar(id){
            swal({
              title: '¿Seguro que desea eliminar el registro?',
              text: "¡Esta accion no es reversible!",
              type: 'warning',
              icon : "error",
              buttons:{
                confirm: {
                  text : 'Si, deseo eliminarlo!',
                  className : 'btn btn-danger',
                },
                cancel: {
                  text : 'Cancelar',
                  visible: true,
                  className: 'btn btn-warning'
                }
              }
            }).then((Delete) => {
              if (Delete) {
                $.ajax({
                url:'/BorrarCanalero/'+id,
                type:'get'
                }).done(function (res){
                swal({
                  title: 'Eliminado',
                  text: 'El registro ha sido Eliminado.',
                  icon : "success",
                  buttons:false,
                  timer:3000,
                });
                  location.reload();
                  });
              } else {
                swal.close();
              }
            });
          }
       // aqui termina funcion de eliminar

          function modalCanalero(id){
            var rest=id.replace("/", "P");
              $.ajax({
              url:'/CanaleroDatos/'+rest,
              type:'get'
              }).done(function (res){
                $("#id").val(id);
                $("#modalEditar").modal('show');
                $("#nombree").val(res.datos[0].nombre);
                $('#paternoe').val(res.datos[0].paterno);
                $('#maternoe').val(res.datos[0].materno);
                $('#telefonoe').val(res.datos[0].telefono);
              });
            }

            $("body").on("click","#addcanalero",function(event){
              event.preventDefault();
              var frm=$("#regeji");
              var datos = frm.serialize();
              $.ajaxSetup({
                   headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
               });
               $.ajax({
                type:'POST',
                url:'/AgregarCanalero/',
                data:datos,
              success:function(data){
              if(data=="error"){
                swal({
                  title: 'Reigstro Existente',
                  text: 'Este canalero ya existe.',
                  icon : "warning",
                  buttons:false,
                  timer:1000,
                });
             mensaje("Este ejidatario ya existe","warning","top","Registro de Canaleros");
            $document.getElementById('textomensaje').innerHTML='Este canalero ya existe.';
                }else{
                //    $document.getElementById('textomensaje').innerHTML='';
                $('#canalero').modal('hide');
                location.reload();
                swal({
                  title: 'Registrado',
                  text: 'El registro ha sido agregado con exito.',
                  icon : "success",
                  buttons:false,
                  timer:3000,
                });
            }
                    },
                    error:function(x,xs,xt){
             var xd=x.responseText.split("}");
             var obj2=JSON.parse(xd[0]+'}');
             var obj = JSON.parse(xd[1]+'}');
             var xd=obj.errores+"";
             var atributos = "";
            for(var aux in obj2){
            $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');
                      }
             document.getElementById('mensaje1').innerHTML='Favor de llenar los campos de manera adecuada';
                    }
            });
            });

            //ajax  para modificcar
            $("body").on("click","#modificar",function(event){
              var frm=$("#registroM");
              var datos = frm.serialize();
              $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
              $.ajax({
              type:'POST',
              url:'/ModificarCanalero/',
              data:datos,
              success:function(data){
              location.reload();
              $('#modalEditar').modal('hide');
              swal({
                title: "Modificado",
                text: "El registro ha sido modificado con exito.",
                icon: "success",
                buttons:false,
                timer:3000,
              });
            }
            ,error:function(jqXHR, json,xd){
             var xd=jqXHR.responseText.split("}");
             var obj2=JSON.parse(xd[0]+'}');
             var obj = JSON.parse(xd[1]+'}');
             var xd=obj.errores+"";
             var atributos = "";
             for(var aux in obj2){
          $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');
              }

             document.getElementById('mensaje2').innerHTML='Favor de llenar los campos de manera adecuada';
            }
          });
        });
      </script>
          @endsection
