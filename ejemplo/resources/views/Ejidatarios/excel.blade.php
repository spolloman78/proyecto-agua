<center><h2>CÁTALOGO DE EJIDATARIOS</h2></center>
<h3>El numero de registro actual de ejidatarios</h3>
<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Tarjeta</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($ejidatarios as $ejidatario)
            <tr class="">
                <td>{{$ejidatario->nombre}}</td>
                <td>{{$ejidatario->apPaterno}}</td>
                <td>{{$ejidatario->apMaterno}}</td>
                <td>{{$ejidatario->tarjeta}}</td>
            </tr>
        @empty
        @endforelse
    </tbody>
</table>
