<?php

namespace App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class egreso extends Model
{
    protected $table = 'gasto';
    
    use SoftDeletes; //Implementamos
    protected $dates = ['deleted_at'];
    public  $primaryKey = 'Id_gasto';
}
