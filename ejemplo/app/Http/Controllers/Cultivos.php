<?php

namespace App\Http\Controllers;
use App\cultivo;
use App\bitacora;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Exports\CultivosExport;
class Cultivos extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    //
  }
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('admin');
  }

   public function verCultivo()
  {
    $cultivo=cultivo::get();
    return view("Cultivos.cultivo",compact('cultivo'));
  }

  public function eliminar($id){
    cultivo::find($id)->delete();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Cultivos";
    $bitacora->Id_movimiento=3;
    $bitacora->save();
  }


  public function store(Request $request){
    if($request->ajax()){
      $v = \Validator::make($request->all(), [
          'descripcion' => 'required',
          'prioridad' => 'required',
          'capacidad'    => 'required'
      ]);

      if ($v->fails()){
        echo $v->errors();
        return response()->json(['errors'=>$v->errors()->all()]);
        //return Response::json(array('errors' => $v->getMessageBag()->toArray()));
      } else {
        $cultivos=DB::table('cultivo')->where('descripcion','=',$request->descripcion)->count();
        if($cultivos>0){
          echo "error";
        } else {
          $cultivo=new cultivo();
          $cultivo->Id_cultivo=0;
          $cultivo->descripcion=$request->descripcion;
          $cultivo->prioridad=$request->prioridad;
          $cultivo->capacidad_cultivo=$request->capacidad;
          $cultivo->cantidad_cultivo=$request->capacidad;
          $cultivo->save();
          $usuario=Auth::user()->id;
          $bitacora=new bitacora();
          $bitacora->Id_usuario=$usuario;
          $bitacora->id_bitacora=0;
          $bitacora->modulo="Cultivos";
          $bitacora->Id_movimiento=1;
          $bitacora->save();
        }
      }
    }else{

    }
  }

  public function actualizar($id_cultivo){
    $datos = DB::table('cultivo')
      ->where('Id_cultivo', '=', $id_cultivo)
      ->select('*')
      ->get();
    return ($datos=array('datos' => $datos));
  }

  public function modificar(Request $request){

    if($request->ajax()){
      $v = \Validator::make($request->all(), [
           'descripcionc' => 'required',
           'prioridad' => 'required',
           'capacidaddisponible'    => 'required',
          ]);
      if ($v->fails()){
        echo $v->errors();
        return response()->json(['errors'=>$v->errors()->all()]);
        //return Response::json(array('errors' => $v->getMessageBag()->toArray()));
      } else {
        $cultivo=cultivo::find($request->id);
        $cultivo->descripcion=$request->descripcionc;
        $cultivo->prioridad=$request->prioridad;
        $cultivo->capacidad_cultivo=$request->capacidadcultivo;
        $cultivo->cantidad_cultivo=$request->capacidaddisponible;
        $cultivo->save();
        $usuario=Auth::user()->id;
        $bitacora=new bitacora();
        $bitacora->Id_usuario=$usuario;
        $bitacora->id_bitacora=0;
        $bitacora->modulo="Cultivos";
        $bitacora->Id_movimiento=2;
        $bitacora->save();
      }
    }else{

    }
  }

  public function exportarExcel(){
    $export = new CultivosExport();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Cultivos";
    $bitacora->Id_movimiento=4;
    $bitacora->save();
    return $export->download('Cultivos.xlsx'); 
    //return Excel::download(new UsersExport, 'usuarios.xlsx');
  }
}
