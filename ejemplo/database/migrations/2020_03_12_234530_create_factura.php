<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('factura', function (Blueprint $table) {
       Schema::disableForeignKeyConstraints();
        $table->string("Id_factura",30);
        $table->primary("Id_factura");
        $table->unsignedInteger("Id_status_factura");
        $table->unsignedInteger("Id_ejidatario");
        $table->unsignedInteger("Id_ciclo");
        $table->foreign('Id_ejidatario')->references('Id_ejidatario')->on('ejidatario');
        $table->foreign('Id_ciclo')->references('Id_ciclo')->on('ciclo');
        $table->foreign('Id_status_factura')->references('Id_status_factura')->on('status_factura');
        $table->timestamps();
        $table->softDeletes(); //Nueva línea, para el borrado lógico

    });

}

/**
 * Reverse the migrations.
 *
 * @return void
 */
public function down()
{
  DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    Schema::dropIfExists('factura');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');
}
}
