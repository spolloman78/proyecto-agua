<center><h2>Historico de Facturas</h2></center>
<table>
    <thead>
        <tr>
          <th>Factura</th>
          <th>Fecha</th>
          <th>Tarjeta</th>
          <th>Usuario</th>
          <th>Ciclo</th>
          <th>Cuenta</th>
          <th>SEC</th>
          <th>Eji.</th>
          <th>Nombre del Ejido</th>
          <th>Municipio</th>
          <th>Cultivo</th>
          <th>Superficie fisica</th>
          <th>Concepto</th>
          <th>Tipo</th>
          <th>Cantidad</th>
          <th>Total</th>
          <th>Importe</th>
          <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($factura as $row)
            <tr class="">
              <td>{!!$row->Id_factura!!}</td>
              <td>{!!$row->fecha!!}</td>
              <td>{!!$row->tarjeta!!}</td>
              <td>{!!$row->nombre!!}{!!$row->apPaterno!!}{!!$row->apMaterno!!}</td>
              <td>{!!$row->ciclo!!}</td>
              <td>{!!$row->cuenta!!}</td>
              <td>{!!$row->SEC!!}</td>
              <td>{!!$row->Id_Ejido!!}</td>
              <td>{!!$row->ejidos!!}</td>
              <td>{!!$row->descripcion!!}</td>
              <td>{!!$row->cultivo!!}</td>
              <td>{!!$row->sup_cultivo!!}</td>
              <td>{!!$row->descripcion_con!!}</td>
              <td>{!!$row->tipo!!}</td>
              <td>{!!$row->sup_Fis!!}</td>
              <td>$ {!!$row->Total!!}</td>
              <td>$ {!!$row->pagado!!}</td>
              <td>{!!$row->status_fact!!}</td>
            </tr>
        @empty
        @endforelse
    </tbody>
</table>
<h4> Total
@foreach($dineroDia as $dinero)

 $ {{$dinero->total}}.00</h2>

 @endforeach
</h4>
