<?php

namespace App\Exports;

use App\factura;
use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Support\Facades\DB;
class FacturasExport implements FromView
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {          $now = now()->format('Y-m-d');
              $dineroDia=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')
              ->select(DB::raw('SUM(abonos.pago) as total'))->whereDate('abonos.created_at',$now)
              ->where('factura.Id_status_factura','!=','4')->get();
              $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
              ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
              ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
              ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
              ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
              ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
              ->join('abonos','abonos.Id_factura','=','factura.Id_factura')
              ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
              ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
              ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
              ->select('factura_concepto.Total','abonos.pago as pagado','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','factura_concepto.Total')
              ->whereDate('abonos.created_at',$now)
              ->get();
            //  echo $dineroDia;
            return view('Facturas.excel',[
                  'factura' => $factura,'dineroDia'=>$dineroDia,'now'=>$now
              ]);
    }
}
