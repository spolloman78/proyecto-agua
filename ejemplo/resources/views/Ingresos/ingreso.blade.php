@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Ingresos <i class="fas fa-cart-plus"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Caja</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Ingresos</a>
            </li>
          </ul>
        </div>
        <div class="row">

        <div class="col-sm-6 col-md-4">
          <div class="card card-stats card-round">
            <div class="card-body ">
              <div class="row">
                <div class="col-5">
                  <div class="icon-big text-center">
                    <i class="fas fa-cart-plus  text-danger"></i>
                  </div>
                </div>
                <div class="col-7 col-stats">
                  <div class="numbers">
                    <p class="card-category">Ingresos Diarios</p>
                    <h4 class="card-title">@foreach($dineroDia as $row)
                  $  {{$row->total}}</h4>@endforeach
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="card card-stats card-round">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5">
                    <div class="icon-big text-center">
                      <i class="flaticon-coins text-primary"></i>
                    </div>
                  </div>
                  <div class="col-7 col-stats">
                    <div class="numbers">
                      <p class="card-category">Ingresos Ciclo Agrario</p>
                      <h4 class="card-title">@foreach($dineroCiclo as $row)
                      $  {{$row->total}}@endforeach</h4>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <div class="col-sm-6 col-md-4">
            <div class="card card-stats card-round">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5">
                    <div class="icon-big text-center">
                      <i class="fas fa-dollar-sign text-success"></i>
                    </div>
                  </div>
                  <div class="col-7 col-stats">
                    <div class="numbers">
                      <p class="card-category">Ingresos Mes</p>
                   @foreach($dinero as $row)
                      <h4 class="card-title"> $ {{$row->total}}</h4>
                   @endforeach
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">

                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="card-title"  style="padding: 1% 0 0 0;">Ingresos</h4>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-center">


                    </div>
                  </div>
                </div>

              </div>
              <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                      <form id="user" method="post"action="{{route('exportarExcelIngreso')}}">
                      {{csrf_field()}}
                        <div class="form-group form-group-default">
                            <label for="num_factura">Desde:</label>
                            <input type=date name="inicio" required class="form-control form-control-sm" id="inicio" value="{{$anio}}-01-01" min="2018-05-01" max="{{$now}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-default">
                            <label for="num_factura">Hasta:</label>
                            <input type=date name="fin" required class="form-control form-control-sm" id="fin" value="{{$now}}" max="{{$now}}">
                        </div>
                    </div>
                    <div  hidden class="col-md-3">
                        <div class="form-group form-group-default">
                            <label for="num_factura">Ciclo Agrario:</label>
                            <select hidden class="form-control form-control-sm" id="ciclo" name="ciclo" >
                            <option value="0">Todos</option>

                            </select>
                        </div>
                        </div>
                        <div hidden class="col-md-3">
                            <div class="form-group form-group-default" >
                                <label for="num_factura">Tipo</label>
                                <select hidden  class="form-control form-control-sm" name="tipo" id="tipo">
                                <option value="0">Todos</option>
                                </select>
                            </div>
                              </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-center">
                    <button class="btn btn btn-round ml-auto"  style="  background-color:#00A631; color:white;">
                        <i class="fas fa-file-excel fa-2x"></i>
                      </button>
                    </form>
                </div>
              </div>
                <br>
                <div class="table-responsive">

                    <table id="example" class="display table table-striped table-hover" >
                      <thead>
                        <tr>
                          <th>Factura</th>
                          <th>Fecha</th>
                          <th>Tarjeta</th>
                          <th>Usuario</th>
                          <th>Ciclo</th>
                          <th>Cuenta</th>
                          <th>SEC</th>
                          <th>Eji.</th>
                          <th>Nombre del Ejido</th>
                          <th>Municipio</th>
                          <th>Cultivo</th>
                          <th>Superficie fisica</th>
                          <th>Concepto</th>
                          <th>Tipo</th>
                          <th>Cantidad</th>
                          <th>Importe</th>
                          <th>Status</th>
                        </tr>
                      </thead>

                    </table>

                </div>
                <br>

              </div>
            </div>
          </div>

        </div>

		<!-------------------------------MODALES---------------------->
		<div class="modal fade modal" id="addRowIngreso" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content" id="usuario">
			  <div class="modal-header no-bd">
				<h5 class="modal-title" id="usuario_edit">Añadir nuevo ingreso </h5>
				<button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
			  <div class="tab-content" id="myTabContent">
				  <div role="tabpanel" class="tab-pane active">
            <form id="user">
              {{csrf_field()}}
              <div class="form-group form-group-default">
                <label for="nom_user">Concepto</label>
                <input type="text" class="form-control" id="concepto" name="concepto">
              </div>
              <div class="form-group form-group-default">
                <label for="apPaterno">Monto</label>
                <input type="number" class="form-control" id="monto" name="monto" min="0">
              </div>
              <input type="text" class="form-control" id="id_user" name="id_user" hidden>
            </form>
				  </div>
			  </div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-success" onClick="editUser()">Guardar Cambios</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			  </div>
			</div>
		  </div>
		</div>
    <script>
    $(document).ready(function() {

    var table = $('#example').DataTable( {
      'processing': true,
       "serverSide": true,
       "bDestroy": true,
       "ajax": "{{url('getIngreso')}}",
            eys: true,
            sort: true,
            searching: true,
            select: true,
            ordering: true,
            bJQueryUI: true,
            sPaginationType: "full_numbers",
            displayStart: 0,
            stateSave: true,
            autoWidth: false,
            paging: true,
            fixedColumns: false,
            columnReorder: true,
        "columns": [
            { data: "Id_factura" },
            { data: "fecha" },
            { data: "tarjeta" },
            { data: "nombre"},

      //      { data: "apPaterno" },
        //    { data: "apMaterno" },
            { data: "ciclo" },
            { data: "cuenta" },
            { data: "SEC" },
            { data: "Id_Ejido" },
            { data: "ejidos" },
            { data: "descripcion" },
            { data: "cultivo" },
            { data: "sup_cultivo" },
            { data: "descripcion_con" },
            { data: "tipo" },
            { data: "sup_Fis" },
            { data: "Total" },
            { data: "status_fact" },
        ],
        "order": [[1, 'asc']]
    } );

    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
    $("#inicio").change(function() {
    cambio();

    });
    $("#fin").change(function() {
    cambio();

    });

  });
    function cambio(){
      var frm=$("#datosFil");
      var datos = frm.serialize();
      var dat=JSON.stringify({inicio: $('#inicio').val(),fin: $('#fin').val()  });
      var d;
      $("#example").dataTable().fnDestroy();
           var table = $('#example').DataTable( {
             'processing': true,
              "serverSide": true,
              "bDestroy": true,
             "ajax": {
          url: '/getHistorico',
          type: "POST",
        data: {"_token": "{{ csrf_token() }}",'inicio':$("#inicio").val(),'fin':$('#fin').val(),"ciclo": $('#ciclo').val(),"tipo":$('#tipo').val()}
      },            eys: true,
                   sort: true,
                   searching: true,
                   select: true,
                   ordering: true,
                   bJQueryUI: true,
                   sPaginationType: "full_numbers",
                   displayStart: 0,
                   stateSave: true,
                   autoWidth: false,
                   paging: true,
                   fixedColumns: false,
                   columnReorder: true,
               "columns": [
                   { data: "Id_factura" },
                   { data: "fecha" },
                   { data: "tarjeta" },
                   { data: "nombre"},
             //      { data: "apPaterno" },
               //    { data: "apMaterno" },
                   { data: "ciclo" },
                   { data: "cuenta" },
                   { data: "SEC" },
                   { data: "Id_Ejido" },
                   { data: "ejidos" },
                   { data: "descripcion" },
                   { data: "cultivo" },
                   { data: "sup_cultivo" },
                   { data: "descripcion_con" },
                   { data: "tipo" },
                   { data: "sup_Fis" },
                   { data: "Total" },
                   { data: "status_fact" },
               ],
               "order": [[1, 'asc']]
           } );

            // Add event listener for opening and closing details
            $('#example tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row( tr );

                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                }
            } );
    }
  </script>
@endsection
