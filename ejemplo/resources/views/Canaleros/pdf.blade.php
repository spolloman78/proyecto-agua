<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Libro de canaleros</title>
<style>
	table, th ,td{
		border: 1px solid black;
	}
	
	#titulo{
		text-align: center;
	}
	
	#ejidatarios{
		width: 700px;
	}
	table td:nth-child(1) {
	 	width: 100px;
	}
</style>
</head>
<body>
	<div id="titulo"><h1>LIBRO DE CANALEROS</h1></div>
	<table id="ejidatarios">
	<tr>
		<th>TARJETA CPLTSLRAPCO SRiUP FIS</th>
		<th>P DREN</th>
		<th>S DREN</th>
		<th>CULTIVO</th>
		<th>HF</th>
		<th>P1</th>
		<th>R1</th>
		<th>T1</th>
		<th>P2</th>
		<th>R2</th>
	</tr>
		@foreach($ejidatarios as $ejidatario)
			<tr>
				<td>
					{{$ejidatario->apPaterno}} {{$ejidatario->apMaterno}} {{$ejidatario->nombre}}
					<br>{{$ejidatario->tarjeta}} {{$ejidatario->SEC}} {{$ejidatario->CP}} {{$ejidatario->LT}} {{$ejidatario->SLT}} {{$ejidatario->RA}} {{$ejidatario->PC}}
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				
			</tr>
		@endforeach
	</table>
</body>
</html>