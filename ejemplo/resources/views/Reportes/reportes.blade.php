@extends('layouts.layout')
@section('content')
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
          <h4 class="page-title">Reportes <i class="flaticon-file-1"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catálogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Reportes</a>
            </li>
          </ul>
        </div>
      <div class="row">
  <div class="col-md-12">
              <ul class="nav nav-pills nav-success nav-pills-no-bd nav-pills-icons justify-content-center" id="pills-tab-with-icon" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="pills-home-tab-icon" data-toggle="pill" href="#pills-home-icon" role="tab" aria-controls="pills-home-icon" aria-selected="true">
                    <i class="icon-direction"></i>
                  Parcelas
                  </a>
                </li>
                @if(Auth::user()->role->descripcion!="Tesorero")
                <li class="nav-item">
                  <a class="nav-link" id="pills-cultivos-tab-icon" data-toggle="pill" href="#pills-cultivos-icon" role="tab" aria-controls="pills-cultivos-icon" aria-selected="false">
                    <i class="icon-drop"></i>
                    Riegos
                  </a>
                </li>@endif
                <li class="nav-item">
                  <a class="nav-link" id="pills-profile-tab-icon" data-toggle="pill" href="#pills-profile-icon" role="tab" aria-controls="pills-profile-icon" aria-selected="false">
                    <i class="fas fa-cart-plus"></i>
                    Ingresos
                  </a>
                </li>@if(Auth::user()->role->descripcion=="Tesorero")
                <li class="nav-item">
                  <a class="nav-link" id="pills-contact-tab-icon" data-toggle="pill" href="#pills-contact-icon" role="tab" aria-controls="pills-contact-icon" aria-selected="false">
                    <i class="fas fa-cart-arrow-down"></i>
                    Egresos
                  </a>
                </li>
                @endif
              </ul>
              <div class="tab-content mt-2 mb-3" id="pills-with-icon-tabContent">
                <div class="tab-pane fade show active" id="pills-home-icon" role="tabpanel" aria-labelledby="pills-home-tab-icon">
                  <div class="row">

                  <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                      <div class="card-body ">
                        <div class="row">
                          <div class="col-5">
                            <div class="icon-big text-center">
                              <i class="icon-direction text-info"></i>
                            </div>
                          </div>
                          <div class="col-7 col-stats">
                            <div class="numbers">
                              <p class="card-category"># Parcelas</p>
                              <h4 class="card-title">{{$total}}</h4>
                            </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                      <div class="card card-stats card-round">
                        <div class="card-body ">
                          <div class="row">
                            <div class="col-5">
                              <div class="icon-big text-center">
                                <i class="icon-map text-muted"></i>
                              </div>
                            </div>
                            <div class="col-7 col-stats">
                              <div class="numbers">
                                <p class="card-category"># Ejidos</p>
                                <h4 class="card-title">{{$ejidoT}}</h4>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-3">
                        <div class="card card-stats card-round">
                          <div class="card-body ">
                            <div class="row">
                              <div class="col-5">
                                <div class="icon-big text-center">
                                  <i class="fas fa-tree text-success"></i>
                                </div>
                              </div>
                              <div class="col-7 col-stats">
                                <div class="numbers">
                                  <p class="card-category">Parcelas con Cultivos</p>
                                  <h4 class="card-title">{{$parcelaT}}</h4>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                          <div class="card card-stats card-round">
                            <div class="card-body ">
                              <div class="row">
                                <div class="col-5">
                                  <div class="icon-big text-center">
                                    <i class="flaticon-coins text-warning"></i>
                                  </div>
                                </div>
                                <div class="col-7 col-stats">
                                  <div class="numbers">
                                    <p class="card-category">Parcelas Deudoras</p>
                                    <h4 class="card-title">{{$deudores}}</h4>
                                  </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        <div class="col-md-6">
                          <div class="card">
                            <div class="card-header">
                              <div class="row">
                                <div class="card-title">
                                  <h4 class="card-title"  style="padding: 1% 0 0 0;">Parcelas asignadas a Canaleros</h4>
                                </div>
                              </div>
                            </div>
                            <div class="card-body">
                              <div class="chart-container">
                                <canvas id="pieChart" style="width: 50%; height: 50%"></canvas>


                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="card">
                            <div class="card-header">
                              <div class="row">
                                <div class="card-title">
                                  <h4 class="card-title"  style="padding: 1% 0 0 0;">Parcelas deudoras</h4>
                                </div>
                              </div>
                            </div>
                            <div class="card-body">
                              <div class="chart-container">
                                <canvas id="barChart2" style="width: 50%; height: 50%"></canvas>


                              </div>
                            </div>
                          </div>
                        </div>  <div class="col-md-6 ml-auto mr-auto">
                           <div class="card">
                             <div class="card-header">
                               <div class="row">
                                 <div class="card-title">
                                   <h4 class="card-title"  style="padding: 1% 0 0 0;">Pagos Deudas por Parcela</h4>
                                 </div>
                                 <div class="ml-auto col-sm-2 text-align:right">
                                   <div class="d-flex ">

                                   </div>
                                 </div>
                               </div>
                             </div>
                             <div class="card-body">
                               <div class="chart-container">
                                 <canvas id="multipleBarChart2"></canvas>
                               </div>
                             </div>
                           </div>
                          </div>


                      </div>
                </div>
                <div class="tab-pane fade" id="pills-profile-icon" role="tabpanel" aria-labelledby="pills-profile-tab-icon">
                  <div class="row">
                    <div class="col-sm-6 col-md-3">
                      <div class="card card-stats card-round">
                        <div class="card-body ">
                          <div class="row">
                            <div class="col-5">
                              <div class="icon-big text-center">
                                <i class="icon-map text-muted"></i>
                              </div>
                            </div>
                            <div class="col-7 col-stats">
                              <div class="numbers">
                                <p class="card-category">Parcelas Deudoras</p>
                                <h4 class="card-title">{{$deudoresT}}</h4>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>


                  </div>
                  <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                      <div class="card-body ">
                        <div class="row">
                          <div class="col-5">
                            <div class="icon-big text-center">
                              <i class="flaticon-users text-danger"></i>
                            </div>
                          </div>
                          <div class="col-7 col-stats">
                            <div class="numbers">
                              <p class="card-category">Ejidatarios Deudores</p>
                              <h4 class="card-title">{{$num}}</h4>
                            </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                      <div class="card card-stats card-round">
                        <div class="card-body ">
                          <div class="row">
                            <div class="col-5">
                              <div class="icon-big text-center">
                                <i class="flaticon-coins text-primary"></i>
                              </div>
                            </div>
                            <div class="col-7 col-stats">
                              <div class="numbers">
                                <p class="card-category">Ingresos Totales</p>
                                <h4 class="card-title">   @foreach($dineroCiclo as $row)
                                $  {{$row->totalCiclo}}@endforeach</h4>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    <div class="col-sm-6 col-md-3">
                      <div class="card card-stats card-round">
                        <div class="card-body ">
                          <div class="row">
                            <div class="col-5">
                              <div class="icon-big text-center">
                                <i class="fas fa-dollar-sign text-success"></i>
                              </div>
                            </div>
                            <div class="col-7 col-stats">
                              <div class="numbers">
                                <p class="card-category">Deuda Total</p>
                                @foreach($sum  as $row)
                                <h4 class="card-title"> $ {{$row->total}}</h4>
                                @endforeach
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-header">
                        <div class="row">
                          <div class="card-title">
                            <h4 class="card-title"  style="padding: 1% 0 0 0;">Resumen Ingresos</h4>
                          </div>
                          <div class="ml-auto col-sm-4 text-align:right">
                            <div class="d-flex ">

                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="card-body pb-0">
                        <div class="d-flex">
                          <div class="avatar avatar-lg">
                           <span class="avatar-title rounded-circle border border-white" style="background-color:#00A631;">I</span>
                          </div>
                          <div class="flex-1 pt-1 ml-2">
                            <h6 class="fw-bold mt-3 mb-0">  Ingresos de {{$now}}</h6>
                            <small class="text-muted">Ingresos por dia</small>
                          </div>
                          <div class="d-flex ml-auto align-items-center">
                            <h3 class="text-info fw-bold">
                              @foreach($dineroDia as $row)
                            $  {{$row->total}}
                          @endforeach</h3>
                          </div>
                        </div>
                        <div class="separator-dashed"></div>
                        <div class="d-flex">
                          <div class="avatar avatar-lg">
                              <span class="avatar-title rounded-circle border border-white" style="background-color:#00A631;">I</span>
                              </div>
                          <div class="flex-1 pt-1 ml-2">

                            <h6 class="fw-bold mt-3 mb-0">Ingresos de {{$meso}}</h6>

                          <small class="text-muted">Ingresos por mes</small>
                          </div>
                          <div class="d-flex ml-auto align-items-center">
                            <h3 class="text-info fw-bold">  @foreach($dinero as $row)
                            $  {{$row->totalDia}}
                            <input id="totalmes" value="{{$row->totalDia}}" hidden/>
                              @endforeach</h3>
                          </div>
                        </div>
                        <div class="separator-dashed"></div>
                        <div class="d-flex">
              <div class="avatar avatar-lg">
                            <span class="avatar-title rounded-circle border border-white" style="background-color:#00A631;">I</span>
                          </div>

                          <div class="flex-1 pt-1 ml-2">
                            <h6 class="fw-bold mt-3 mb-0">Ciclo Agrario {{$ciclo}}</h6>

          <small class="text-muted">Ingresos por ciclo agrario</small>
                          </div>
                          <div class="d-flex ml-auto align-items-center">
                            <h3 class="text-info fw-bold">
                               @foreach($dineroCiclo as $row)
                            $  {{$row->totalCiclo}}
                            <input id="totalCiclo" value="{{$row->totalCiclo}}" hidden/>
                              @endforeach</h3>
                          </div>
                        </div>
                        <div class="separator-dashed"></div>
                        <div class="pull-in">

                        </div>
                      </div>
                    </div>
                  </div>

          <div class="col-md-6">
             <div class="card">
               <div class="card-header">
                 <div class="row">
                   <div class="card-title">
                     <h4 class="card-title"id="cicloxd"  style="padding: 1% 0 0 0;">Ingresos por mes durante el ciclo </h4>
                   </div>
                   <div class="ml-auto col-sm-4 text-align:right">
                     <div class="d-flex ">
                       <select class="form-control js-example-basic-single dynamic"  name="cicloS" id="cicloS">
                          @foreach($ciclos as $row)
                         <option value="{{$row->Id_ciclo}}">{{$row->Ciclo}}</option>
                         @endforeach
                       </select>
                     </div>
                   </div>
                 </div>
               </div>
               <div class="card-body">
                 <div class="chart-container">
                   <canvas id="lineChart"></canvas>
                 </div>
               </div>
             </div>
           </div>
           <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                 <div class="row">
                   <div class="card-title">
                     <h4 class="card-title" id="mesxd" style="padding: 1% 0 0 0;">Ingresos  a lo largo del mes de {{$meso}} </h4>
                   </div>
                   <div class="ml-auto col-sm-4 text-align:right">
                     <div class="d-flex ">
                       <select class="form-control js-example-basic-single dynamic" name="mes" id="mes">
                         <option value="1">Enero</option>
                         <option value="2">Febrero</option>
                         <option value="3">Marzo</option>
                         <option value="4">Abril</option>
                         <option value="5">Mayo</option>
                         <option value="6">Junio</option>
                         <option value="7">Julio</option>
                         <option value="8">Agosto</option>
                         <option value="9">Septiembre</option>
                         <option value="10">Octubre</option>
                         <option value="11">Noviembre</option>
                         <option value="12">Diciembre</option>
                       </select>
                     </div>
                   </div>
                 </div>
               </div>
                <div class="card-body">
                  <div class="chart-container">
                    <canvas id="barChart"></canvas>
                  </div>
                </div>
              </div>
            </div>
                </div>
                  </div>
                  <div class="tab-pane fade" id="pills-cultivos-icon" role="tabpanel" aria-labelledby="pills-cultivos-tab-icon">
                   <div class="row">
                     <div class="col-sm-6 col-md-3">
                       <div class="card card-stats card-round">
                         <div class="card-body ">
                           <div class="row">
                             <div class="col-5">
                               <div class="icon-big text-center">
                                 <i class="flaticon-remove-user-1 text-danger"></i>
                               </div>
                             </div>
                             <div class="col-7 col-stats">
                               <div class="numbers">
                                 <p class="card-category">Excentos de servicio</p>
                                 <h4 class="card-title">{{$sin}}</h4>
                               </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                       <div class="col-sm-6 col-md-3">
                         <div class="card card-stats card-round">
                           <div class="card-body ">
                             <div class="row">
                               <div class="col-5">
                                 <div class="icon-big text-center">
                                   <i class="flaticon-user-2 text-success"></i>
                                 </div>
                               </div>
                               <div class="col-7 col-stats">
                                 <div class="numbers">
                                   <p class="card-category">Con servicio</p>
                                   <h4 class="card-title">{{$con}}</h4>
                                 </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                    <div class="col-sm-6 col-md-3">
                      <div class="card card-stats card-round">
                        <div class="card-body ">
                          <div class="row">
                            <div class="col-5">
                              <div class="icon-big text-center">
                                <i class="icon-direction text-info"></i>
                              </div>
                            </div>
                            <div class="col-7 col-stats">
                              <div class="numbers">
                                <p class="card-category">Parcelas con Cultivos</p>
                                <h4 class="card-title">{{$parcelaT}}</h4>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-3">
                        <div class="card card-stats card-round">
                          <div class="card-body ">
                            <div class="row">
                              <div class="col-5">
                                <div class="icon-big text-center">
                                  <i class="fas fa-tree text-success"></i>
                                </div>
                              </div>
                              <div class="col-7 col-stats">
                                <div class="numbers">
                                  <p class="card-category">Cultivos</p>
                                  <h4 class="card-title">{{$cultivo}}</h4>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <div class="col-md-12">
                     <div class="card">
                       <div class="card-header">
                         <div class="row">
                           <div class="card-title">
                             <h4 class="card-title"  style="padding: 1% 0 0 0;">Distribucion de Cultivos</h4>
                           </div>
                           <div class="ml-auto col-sm-2 text-align:right">
                             <div class="d-flex ">

                             </div>
                           </div>
                         </div>
                       </div>
                       <div class="card-body">
                         <div class="chart-container">
                           <canvas id="multipleBarChart"></canvas>
                         </div>
                       </div>
                     </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="pills-contact-icon" role="tabpanel" aria-labelledby="pills-contact-tab-icon">
                <div class="row">
                  <div class="col-sm-6 col-md-4">
                    <div class="card card-stats card-round">
                      <div class="card-body ">
                        <div class="row">
                          <div class="col-5">
                            <div class="icon-big text-center">
                              <i class="fas fa-cart-arrow-down text-primary"></i>
                            </div>
                          </div>
                          <div class="col-7 col-stats">
                            <div class="numbers">
                              <p class="card-category">Total de Gastos</p>
                              <h4 class="card-title">@foreach($egresoM as $row)
                              $  {{$row->totalDia}} @endforeach</h4>
                            </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                      <div class="card card-stats card-round">
                        <div class="card-body ">
                          <div class="row">
                            <div class="col-5">
                              <div class="icon-big text-center">
                                <i class="fas fa-dollar-sign text-success"></i>
                              </div>
                            </div>
                            <div class="col-7 col-stats">
                              <div class="numbers">
                                <p class="card-category">Gastos de Hoy </p>
                                <h4 class="card-title">  @foreach($egresoD as $row)
                                $  {{$row->totalDia}}
                              @endforeach</h4>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <div class="card card-stats card-round">
                          <div class="card-body ">
                            <div class="row">
                              <div class="col-5">
                                <div class="icon-big text-center">
                                  <i class="flaticon-coins text-primary"></i>
                                </div>
                              </div>
                              <div class="col-7 col-stats">
                                <div class="numbers">
                                  <p class="card-category">Ingresos Totales</p>
                                  <h4 class="card-title">   @foreach($dineroCiclo as $row)
                                  $  {{$row->totalCiclo}}@endforeach</h4>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                <div class="col-md-6">
                  <div class="card">
                    <div class="card-header">
                      <div class="card-title">Resumen Gastos</div>
                    </div>
                    <div class="card-body pb-0">
                      <div class="d-flex">
                        <div class="avatar avatar-lg">
                         <span class="avatar-title rounded-circle border border-white" style="background-color:#F97D16 ;">E</span>
                        </div>
                        <div class="flex-1 pt-1 ml-2">

                          <h6 class="fw-bold mt-3 mb-0">  Egresos de {{$now}}</h6>

                          <small class="text-muted">Gastos por dia</small>
                        </div>
                        <div class="d-flex ml-auto align-items-center">
                          <h3 class="text-info fw-bold">
                            @foreach($egresoD as $row)
                          $  {{$row->totalDia}}
                        @endforeach</h3>
                        </div>
                      </div>
                      <div class="separator-dashed"></div>
                      <div class="d-flex">
                        <div class="avatar avatar-lg">
                            <span class="avatar-title rounded-circle border border-white"style="background-color:#F97D16;">E</span>
                            </div>
                        <div class="flex-1 pt-1 ml-2">

                          <h6 class="fw-bold mt-3 mb-0">Egresos de {{$meso}}</h6>

                        <small class="text-muted">Egresos por mes</small>
                        </div>
                        <div class="d-flex ml-auto align-items-center">
                          <h3 class="text-info fw-bold">  @foreach($egresoM as $row)
                          $  {{$row->totalDia}}
                          <input id="totalmes" value="{{$row->totalDia}}" hidden/>
                            @endforeach</h3>
                        </div>
                      </div>
                      <div class="separator-dashed"></div>
                      <div class="d-flex">
                        <div class="avatar avatar-lg">
                            <span class="avatar-title rounded-circle border border-white"style="background-color:#F97D16;">E</span>
                            </div>
                        <div class="flex-1 pt-1 ml-2">

                          <h6 class="fw-bold mt-3 mb-0">Egresos del {{$anio}}</h6>

                        <small class="text-muted">Egresos por Año</small>
                        </div>
                        <div class="d-flex ml-auto align-items-center">
                          <h3 class="text-info fw-bold">  @foreach($egr as $row)
                          $  {{$row->totalDia}}
                          <input id="totalmes" value="{{$row->totalDia}}" hidden/>
                            @endforeach</h3>
                        </div>
                      </div>
                        <div class="separator-dashed"></div>
                </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="card-title">
                        <h4 class="card-title"  style="padding: 1% 0 0 0;">Distribucion de Gastos</h4>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="chart-container">
                      <canvas id="pieChart2" style="width: 50%; height: 50%"></canvas>


                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                 <div class="card">
                   <div class="card-header">
                    <div class="row">
                      <div class="card-title">
                        <h4 class="card-title" id="egresoxd" style="padding: 1% 0 0 0;">Gastos a lo largo del mes de {{$meso}} </h4>
                      </div>
                      <div class="ml-auto col-sm-4 text-align:right">
                        <div class="d-flex ">
                          <select class="form-control js-example-basic-single dynamic" name="egresoM" id="egresoM">
                            <option value="1">Enero</option>
                            <option value="2">Febrero</option>
                            <option value="3">Marzo</option>
                            <option value="4">Abril</option>
                            <option value="5">Mayo</option>
                            <option value="6">Junio</option>
                            <option value="7">Julio</option>
                            <option value="8">Agosto</option>
                            <option value="9">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
                            <option value="12">Diciembre</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                   <div class="card-body">
                     <div class="chart-container">
                       <canvas id="barChart3"></canvas>
                     </div>
                   </div>
                 </div>
               </div>
            </div>
          </div>
         </div>




     <!--<div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="card-title">Doughnut Chart</div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="doughnutChart" style="width: 50%; height: 50%"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="card-title">Radar Chart</div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="radarChart"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="card-title">Bubble Chart</div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="bubbleChart"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="card-title">Multiple Line Chart</div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="multipleLineChart"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Cantidad de Cultivos Disponibles</div>
              </div>
              <div class="card-body">

                <div class="chart-container">
                  <canvas id="barChart"></canvas>
                </div>
              </div>
            </div>
          </div>
          <footer class="footer">
            <div class="container-fluid">
              <nav class="pull-left">

              </nav>
              <div class="copyright ml-auto">
              <img src="../assets/img/itsi-login.png" height="59" width="60"class="navbar-brand"/>
              </div>
            </div>
          </footer>

-->
      </div>
    </div>
  </div>

</div>



<!-- End Custom template -->
</div>

<script src="../../assets/js/plugin/chart.js/chart.min.js"></script>
	<script src="../assets/js/plugin/chart-circle/circles.min.js"></script>

<script>
var lineChart = document.getElementById('lineChart').getContext('2d'),
barChart = document.getElementById('barChart').getContext('2d'),
barChart2 = document.getElementById('barChart2').getContext('2d'),
barChart3 = document.getElementById('barChart3').getContext('2d'),
pieChart = document.getElementById('pieChart').getContext('2d'),
pieChart2 = document.getElementById('pieChart2').getContext('2d'),
multipleBarChart = document.getElementById('multipleBarChart').getContext('2d'),
ParcelaBarChart = document.getElementById('multipleBarChart2').getContext('2d')
doughnutChart = document.getElementById('doughnutChart').getContext('2d'),
radarChart = document.getElementById('radarChart').getContext('2d'),
bubbleChart = document.getElementById('bubbleChart').getContext('2d'),
multipleLineChart = document.getElementById('multipleLineChart').getContext('2d'),

htmlLegendsChart = document.getElementById('htmlLegendsChart').getContext('2d');
  var myBarChart,myChart,parcelaB,myLineChart,mypieChart2,myBarChart3;

</script>
<script>
myBarChart = new Chart(barChart, {
 type: 'bar',
 data: {
   labels:[],
   datasets : [{
     label: "Capacidad de Cultivos",
     backgroundColor: 'rgb(15, 193, 116)',
     borderColor: 'rgb(0, 166, 49 )',
     data: [],
   }],
 },
 options: {
   responsive: true,
   maintainAspectRatio: false,
   scales: {
     yAxes: [{
       ticks: {
         beginAtZero:true
       }
     }]
   },
 }
});
 myChart = new Chart(barChart2, {
 type: 'bar',
 data: {
   labels:[],
   datasets : [{
     label: "Deuda por Parcela",
     backgroundColor: 'rgb(238, 225, 34 )',
     borderColor: 'rgb(238, 225, 34  )',
     data: [],
   }],
 },
 options: {
   responsive: true,
   maintainAspectRatio: false,
   scales: {
     yAxes: [{
       ticks: {
         beginAtZero:true
       }
     }]
   },
 }
});
myBarChart = new Chart(barChart, {
 type: 'bar',
 data: {
   labels:[],
   datasets : [{
     label: "Capacidad de Cultivos",
     backgroundColor: 'rgb(15, 193, 116)',
     borderColor: 'rgb(0, 166, 49 )',
     data: [],
   }],
 },
 options: {
   responsive: true,
   maintainAspectRatio: false,
   scales: {
     yAxes: [{
       ticks: {
         beginAtZero:true
       }
     }]
   },
 }
});
 myBarChart3 = new Chart(barChart3, {
 type: 'bar',
 data: {
   labels:[],
   datasets : [{
     label: "Egresos por Mes",
     backgroundColor: 'rgb(238, 225, 34 )',
     borderColor: 'rgb(238, 225, 34  )',
     data: [],
   }],
 },
 options: {
   responsive: true,
   maintainAspectRatio: false,
   scales: {
     yAxes: [{
       ticks: {
         beginAtZero:true
       }
     }]
   },
 }
});
parcelaB = new Chart(ParcelaBarChart, {
  type: 'bar',
  data: {
    labels:[],
    datasets : [{
      label: "Cantidad Pagada",
      backgroundColor: '#09709E  ',
      borderColor: '#09709E',
      data:[],
    }, {
      label: "Total Deuda",
      backgroundColor: '#F27556 ',
      borderColor: '#F27556',
      data:[],
    }],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position : 'bottom'
    },
    title: {
      display: true,
      text: 'Parcelas'
    },
    tooltips: {
      mode: 'index',
      intersect: false
    },
    responsive: true,
    scales: {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true
      }]
    }
  }
});
 myLineChart = new Chart(lineChart, {
  type: 'line',
  data: {
    labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dec"],
    datasets: [{
      label: "Ingresos por Mes",
      borderColor: "#1d7af3",
      pointBorderColor: "#FFF",
      pointBackgroundColor: "#1d7af3",
      pointBorderWidth: 2,
      pointHoverRadius: 4,
      pointHoverBorderWidth: 1,
      pointRadius: 4,
      backgroundColor: 'transparent',
      fill: true,
      borderWidth: 2,
      data: [],
    }]
  },
  options : {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'bottom',
      labels : {
        padding: 10,
        fontColor: '#1d7af3',
      }
    },
    tooltips: {
      bodySpacing: 4,
      mode:"nearest",
      intersect: 0,
      position:"nearest",
      xPadding:10,
      yPadding:10,
      caretPadding:10
    },
    layout:{
      padding:{left:15,right:15,top:15,bottom:15}
    }
  }
});

mypieChart2 = new Chart(pieChart2,{
 type: 'pie',
 data: {
   datasets: [{
     data: [],
     backgroundColor :["#6CC40C","#0CA463","#0A7D62","#23A249","#17D594","#E1F215","#F56822","#F5A522","#45F108","#9CF215","#A3D751","#E1C903","#5DA586","#80FC54"],
     borderWidth: 0
   }],
   labels: [],
 },
 options : {
   responsive: true,
   maintainAspectRatio: false,
   legend: {
     position : 'bottom',
     labels : {
       fontColor: 'rgb(154, 154, 154)',
       fontSize: 11,
       usePointStyle : true,
       padding: 20
     }
   },
   pieceLabel: {
     render: 'percentage',
     fontColor: 'white',
     fontSize: 14,
   },
   tooltips: false,
   layout: {
     padding: {
       left: 20,
       right: 20,
       top: 20,
       bottom: 20
     }
   }
 }});




/*
$.ajax({
url:'/obtenerCultivo',
type:'get'
}).done(function (cultivo){

            });*/
      //      var 	barChartr = document.getElementById('barChart').getContext('2d');

  var total=$("#totalmes").val();
      $.ajax({
      url:'/ReporteEgreso',
      type:'get'
      }).done(function (cultivo){
        myBarChart3.data.labels=[];
        myBarChart3.data.datasets[0].data=[];
         mypieChart2.data.labels=[];
         mypieChart2.data.datasets[0].data=[];
          for (var i=0; i< cultivo.datos.dato.length; i++) {
                   mypieChart2.data.labels.push(cultivo.datos.dato[i].descripcionG);
                   myBarChart3.data.labels.push(cultivo.datos.dato[i].descripcionG);
                  var cantiad=(total/cultivo.datos.dato[i].totalDia)*100;
                    myBarChart3.data.datasets[0].data.push(cultivo.datos.dato[i].totalDia);
                  mypieChart2.data.datasets[0].data.push(cantiad);

                    }
                          myBarChart3.update();
                          mypieChart2.update();


      });
      $(document).ready(function() {
           $('select[name="mes"]').on('change', function(){
               var xd = $(this).val();

                 var selecte = document.getElementById("mes");
                 var respusta=selecte.options[xd-1].text;
             document.getElementById('mesxd').innerHTML="Ingresos  a lo largo del mes de "+respusta;
             $.ajax({
             url:'/ReporteMes/'+xd,
             type:'get'
             }).done(function (cultivo){
                myBarChart.data.labels=[];
                myBarChart.data.datasets[0].data=[];
                 for (var i=0; i< cultivo.datos.length; i++) {
                           myBarChart.data.labels.push(cultivo.datos[i].date);
                               var cantiad=cultivo.datos[i].totalDia;
                           myBarChart.data.datasets[0].data.push(cantiad);
                           }



                                 // re-render the chart
                                 myBarChart.update();


             });});
             $('select[name="egresoM"]').on('change', function(){
                var xd = $(this).val();
               var selecte = document.getElementById("egresoM");
               var respusta=selecte.options[xd-1].text;
              document.getElementById('egresoxd').innerHTML="Gastos  a lo largo del mes de "+respusta;

               $.ajax({
               url:'/ReporteEgresos/'+xd,
               type:'get'
               }).done(function (cultivo){
                 myBarChart3.data.labels=[];
                 myBarChart3.data.datasets[0].data=[];

                   for (var i=0; i< cultivo.datos.length; i++) {

                            myBarChart3.data.labels.push(cultivo.datos[i].descripcionG);
                             myBarChart3.data.datasets[0].data.push(cultivo.datos[i].totalDia);

                             }
                                   myBarChart3.update();




               });});




                    $.ajax({
                    url:'/ReporteParcela',
                    type:'get'
                    }).done(function (cultivo){
                       myChart.data.labels=[];
                       myChart.data.datasets[0].data=[];
                       parcelaB.data.labels=[];
                       parcelaB.data.datasets[0].data=[];
                       parcelaB.data.datasets[1].data=[];
                        for (var i=0; i< cultivo.datos.length; i++) {
                                  myChart.data.labels.push(cultivo.datos[i].Id_parcela);
                                  parcelaB.data.labels.push(cultivo.datos[i].Id_parcela);
                                      var cantiad=cultivo.datos[i].Deuda-cultivo.datos[i].pagado;
                                  myChart.data.datasets[0].data.push(cantiad);
                                  parcelaB.data.datasets[1].data.push(cantiad);
                                  parcelaB.data.datasets[0].data.push(cultivo.datos[i].pagado);

                                  }
                                        myChart.update();
                                        parcelaB.update();


                    });

            $('select[name="cicloS"]').on('change', function(){
              var xd = $(this).val();
                var selecte = document.getElementById("cicloS");
                var respusta=selecte.options[xd-1].text;
            document.getElementById('cicloxd').innerHTML="Ingresos por mes durante el ciclo "+respusta;
              $.ajax({
              url:'/ReporteCiclo/'+xd,
              type:'get'
              }).done(function (cultivo){
                var marks = [];
                myLineChart.data.datasets[0].data=[];
                  for (var i=0; i< cultivo.datos.length; i++) {
                myLineChart.data.datasets[0].data.push(cultivo.datos[i].totalCiclo);
                marks.push(cultivo.datos[i].totalCiclo);
                            }
                            myLineChart.update();
            });
              });
    });

                $.ajax({
                url:'/obtenernum',
                type:'get'
                }).done(function (cultivo){
                  var name = [];
                  var marks = [];
                  var total=cultivo.datos.total;
                    for (var i=0; i< cultivo.datos.dato.length; i++) {

                                 name.push(cultivo.datos.dato[i].nombre);

                                  var cantiad=cultivo.datos.dato[i].cantidad/total*100;
                                  marks.push(cantiad);

                              }

                var myPieChart = new Chart(pieChart, {
                  type: 'pie',
                  data: {
                    datasets: [{
                      data: marks,
                      backgroundColor :["#6CC40C","#0CA463","#0A7D62","#23A249","#17D594"],
                      borderWidth: 0
                    }],
                    labels: name
                  },
                  options : {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                      position : 'bottom',
                      labels : {
                        fontColor: 'rgb(154, 154, 154)',
                        fontSize: 11,
                        usePointStyle : true,
                        padding: 20
                      }
                    },
                    pieceLabel: {
                      render: 'percentage',
                      fontColor: 'white',
                      fontSize: 14,
                    },
                    tooltips: false,
                    layout: {
                      padding: {
                        left: 20,
                        right: 20,
                        top: 20,
                        bottom: 20
                      }
                    }
                  }
                }) });

                $.ajax({
                url:'/obtenerCultivo',
                type:'get'
                }).done(function (cultivo){
                  var name = [];
                  var marks = [];
                  var valores= [];
                    for (var i=0; i< cultivo.datos.length; i++) {

                                 name.push(cultivo.datos[i].descripcion.substr(0,3));

                                 var cantiad=cultivo.datos[i].capacidad_cultivo-cultivo.datos[i].cantidad_cultivo;
                                 marks.push(cantiad);
                                 valores.push(cultivo.datos[i].cantidad_cultivo);
                              }

                var myMultipleBarChart = new Chart(multipleBarChart, {
                  type: 'bar',
                  data: {
                    labels:name,
                    datasets : [{
                      label: "# Cultivos Ocupados",
                      backgroundColor: '#FA9916',
                      borderColor: '#FA9916',
                      data:marks,
                    }, {
                      label: "# Cultivos Disponibles",
                      backgroundColor: '#0FC174',
                      borderColor: '#0FC174',
                      data: valores,
                    }],
                  },
                  options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                      position : 'bottom'
                    },
                    title: {
                      display: true,
                      text: 'Cultivos'
                    },
                    tooltips: {
                      mode: 'index',
                      intersect: false
                    },
                    responsive: true,
                    scales: {
                      xAxes: [{
                        stacked: true,
                      }],
                      yAxes: [{
                        stacked: true
                      }]
                    }
                  }
                });});
                $.ajax({
                url:'/ReporteCiclo/2',
                type:'get'
                }).done(function (cultivo){

                  var marks = [];
                  var total=cultivo.datos.total;
                    for (var i=0; i< cultivo.datos.length; i++) {
                                  marks.push(cultivo.datos[i].totalCiclo);

                              }
            });
                $.ajax({
                url:'/VentasDiarias',
                type:'get'
                }).done(function (cultivo){
                  var name = [];
                  var marks = [];
                    for (var i=0; i< cultivo.datos.length; i++) {

                                 name.push(cultivo.datos[i].date);
                                 myBarChart.data.labels.push(cultivo.datos[i].date);
                                     var cantiad=cultivo.datos[i].totalDia;
                                     marks.push(cantiad);
                                 myBarChart.data.datasets[0].data.push(cantiad);

                              }
 myBarChart.update();

                });


</script>
      <script>








    		var myDoughnutChart = new Chart(doughnutChart, {
    			type: 'doughnut',
    			data: {
    				datasets: [{
    					data: [10, 20, 30],
    					backgroundColor: ['#f3545d','#fdaf4b','#1d7af3']
    				}],

    				labels: [
    				'Red',
    				'Yellow',
    				'Blue'
    				]
    			},
    			options: {
    				responsive: true,
    				maintainAspectRatio: false,
    				legend : {
    					position: 'bottom'
    				},
    				layout: {
    					padding: {
    						left: 20,
    						right: 20,
    						top: 20,
    						bottom: 20
    					}
    				}
    			}
    		});

    		var myRadarChart = new Chart(radarChart, {
    			type: 'radar',
    			data: {
    				labels: ['Running', 'Swimming', 'Eating', 'Cycling', 'Jumping'],
    				datasets: [{
    					data: [20, 10, 30, 2, 30],
    					borderColor: '#1d7af3',
    					backgroundColor : 'rgba(29, 122, 243, 0.25)',
    					pointBackgroundColor: "#1d7af3",
    					pointHoverRadius: 4,
    					pointRadius: 3,
    					label: 'Team 1'
    				}, {
    					data: [10, 20, 15, 30, 22],
    					borderColor: '#716aca',
    					backgroundColor: 'rgba(113, 106, 202, 0.25)',
    					pointBackgroundColor: "#716aca",
    					pointHoverRadius: 4,
    					pointRadius: 3,
    					label: 'Team 2'
    				},
    				]
    			},
    			options : {
    				responsive: true,
    				maintainAspectRatio: false,
    				legend : {
    					position: 'bottom'
    				}
    			}
    		});

    		var myBubbleChart = new Chart(bubbleChart,{
    			type: 'bubble',
    			data: {
    				datasets:[{
    					label: "Car",
    					data:[{x:25,y:17,r:25},{x:30,y:25,r:28}, {x:35,y:30,r:8}],
    					backgroundColor:"#716aca"
    				},
    				{
    					label: "Motorcycles",
    					data:[{x:10,y:17,r:20},{x:30,y:10,r:7}, {x:35,y:20,r:10}],
    					backgroundColor:"#1d7af3"
    				}],
    			},
    			options: {
    				responsive: true,
    				maintainAspectRatio: false,
    				legend: {
    					position: 'bottom'
    				},
    				scales: {
    					yAxes: [{
    						ticks: {
    							beginAtZero:true
    						}
    					}],
    					xAxes: [{
    						ticks: {
    							beginAtZero:true
    						}
    					}]
    				},
    			}
    		});

    		var myMultipleLineChart = new Chart(multipleLineChart, {
    			type: 'line',
    			data: {
    				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    				datasets: [{
    					label: "Python",
    					borderColor: "#1d7af3",
    					pointBorderColor: "#FFF",
    					pointBackgroundColor: "#1d7af3",
    					pointBorderWidth: 2,
    					pointHoverRadius: 4,
    					pointHoverBorderWidth: 1,
    					pointRadius: 4,
    					backgroundColor: 'transparent',
    					fill: true,
    					borderWidth: 2,
    					data: [30, 45, 45, 68, 69, 90, 100, 158, 177, 200, 245, 256]
    				},{
    					label: "PHP",
    					borderColor: "#59d05d",
    					pointBorderColor: "#FFF",
    					pointBackgroundColor: "#59d05d",
    					pointBorderWidth: 2,
    					pointHoverRadius: 4,
    					pointHoverBorderWidth: 1,
    					pointRadius: 4,
    					backgroundColor: 'transparent',
    					fill: true,
    					borderWidth: 2,
    					data: [10, 20, 55, 75, 80, 48, 59, 55, 23, 107, 60, 87]
    				}, {
    					label: "Ruby",
    					borderColor: "#f3545d",
    					pointBorderColor: "#FFF",
    					pointBackgroundColor: "#f3545d",
    					pointBorderWidth: 2,
    					pointHoverRadius: 4,
    					pointHoverBorderWidth: 1,
    					pointRadius: 4,
    					backgroundColor: 'transparent',
    					fill: true,
    					borderWidth: 2,
    					data: [10, 30, 58, 79, 90, 105, 117, 160, 185, 210, 185, 194]
    				}]
    			},
    			options : {
    				responsive: true,
    				maintainAspectRatio: false,
    				legend: {
    					position: 'top',
    				},
    				tooltips: {
    					bodySpacing: 4,
    					mode:"nearest",
    					intersect: 0,
    					position:"nearest",
    					xPadding:10,
    					yPadding:10,
    					caretPadding:10
    				},
    				layout:{
    					padding:{left:15,right:15,top:15,bottom:15}
    				}
    			}
    		});



    		// Chart with HTML Legends



    		var myHtmlLegendsChart = new Chart(htmlLegendsChart, {
    			type: 'line',
    			data: {
    				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    				datasets: [ {
    					label: "Subscribers",
    					borderColor: gradientStroke2,
    					pointBackgroundColor: gradientStroke2,
    					pointRadius: 0,
    					backgroundColor: gradientFill2,
    					legendColor: '#f3545d',
    					fill: true,
    					borderWidth: 1,
    					data: [154, 184, 175, 203, 210, 231, 240, 278, 252, 312, 320, 374]
    				}, {
    					label: "New Visitors",
    					borderColor: gradientStroke3,
    					pointBackgroundColor: gradientStroke3,
    					pointRadius: 0,
    					backgroundColor: gradientFill3,
    					legendColor: '#fdaf4b',
    					fill: true,
    					borderWidth: 1,
    					data: [256, 230, 245, 287, 240, 250, 230, 295, 331, 431, 456, 521]
    				}, {
    					label: "Active Users",
    					borderColor: gradientStroke,
    					pointBackgroundColor: gradientStroke,
    					pointRadius: 0,
    					backgroundColor: gradientFill,
    					legendColor: '#177dff',
    					fill: true,
    					borderWidth: 1,
    					data: [542, 480, 430, 550, 530, 453, 380, 434, 568, 610, 700, 900]
    				}]
    			},
    			options : {
    				responsive: true,
    				maintainAspectRatio: false,
    				legend: {
    					display: false
    				},
    				tooltips: {
    					bodySpacing: 4,
    					mode:"nearest",
    					intersect: 0,
    					position:"nearest",
    					xPadding:10,
    					yPadding:10,
    					caretPadding:10
    				},
    				layout:{
    					padding:{left:15,right:15,top:15,bottom:15}
    				},
    				scales: {
    					yAxes: [{
    						ticks: {
    							fontColor: "rgba(0,0,0,0.5)",
    							fontStyle: "500",
    							beginAtZero: false,
    							maxTicksLimit: 5,
    							padding: 20
    						},
    						gridLines: {
    							drawTicks: false,
    							display: false
    						}
    					}],
    					xAxes: [{
    						gridLines: {
    							zeroLineColor: "transparent"
    						},
    						ticks: {
    							padding: 20,
    							fontColor: "rgba(0,0,0,0.5)",
    							fontStyle: "500"
    						}
    					}]
    				},
    				legendCallback: function(chart) {
    					var text = [];
    					text.push('<ul class="' + chart.id + '-legend html-legend">');
    					for (var i = 0; i < chart.data.datasets.length; i++) {
    						text.push('<li><span style="background-color:' + chart.data.datasets[i].legendColor + '"></span>');
    						if (chart.data.datasets[i].label) {
    							text.push(chart.data.datasets[i].label);
    						}
    						text.push('</li>');
    					}
    					text.push('</ul>');
    					return text.join('');
    				}
    			}
    		});

    		var myLegendContainer = document.getElementById("myChartLegend");

    		// generate HTML legend
    		myLegendContainer.innerHTML = myHtmlLegendsChart.generateLegend();

    		// bind onClick event to all LI-tags of the legend
    		var legendItems = myLegendContainer.getElementsByTagName('li');
    		for (var i = 0; i < legendItems.length; i += 1) {
    			legendItems[i].addEventListener("click", legendClickCallback, false);
    		}

    	</script>
    </div>
</div>

@endsection
