<?php

namespace App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class factura extends Model
{
    protected $table = 'factura';
    use SoftDeletes; //Implementamos
    protected $dates = ['deleted_at'];
    public  $primaryKey = 'Id_factura';
    protected $keyType = 'string';
}
