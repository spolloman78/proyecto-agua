<?php

namespace App\Exports;
use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

use App\User;

class UsuariosExport implements FromView
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $user=user::get();
        return view('Usuarios.excel',[
            'user' => $user
        ]);
    }
}
