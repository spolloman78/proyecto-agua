
<center><h2>CÁTALOGO DE CANALEROS</h2></center><table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Telefono</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($canaleros as $canal)
            <tr class="">
                <td>{{$canal->nombre}}</td>
                <td>{{$canal->paterno}}</td>
                <td>{{$canal->materno}}</td>
                <td>{{$canal->telefono}}</td>
            </tr>
        @empty
        @endforelse
    </tbody>
</table>
