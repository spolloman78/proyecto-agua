<?php

use Illuminate\Database\Seeder;

class concepto_gasto extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>1,
		'descripcionG'=>'Sueldo de Personal y Compensaciones de Administrativos'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>2,
		'descripcionG'=>'Papeleria y Utiles'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>3,
		'descripcionG'=>'Medicamentos'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>4,
		'descripcionG'=>'ISR Retención de Salarios'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>5,
		'descripcionG'=>'Pago de Casetas'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>6,
		'descripcionG'=>'Pasajes y Viáticos'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>7,
		'descripcionG'=>'Teléfono'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>8,
		'descripcionG'=>'Pago de Préstamos'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>9,
		'descripcionG'=>'Pago de Deudas'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>10,
		'descripcionG'=>'Préstamos'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>11,
		'descripcionG'=>'Luz Oficinas'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>12,
		'descripcionG'=>'Alimentos'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>13,
		'descripcionG'=>'Comisión por Manejo de Cuenta'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>14,
		'descripcionG'=>'Renta de Muebles'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>15,
		'descripcionG'=>'Honorarios'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>16,
		'descripcionG'=>'Pagos'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>17,
		'descripcionG'=>'Aguinaldos'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>18,
		'descripcionG'=>'Despensa a Trabajadores'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>19,
		'descripcionG'=>'Apoyos'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>20,
		'descripcionG'=>'Devolución Importe de Recibos'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>21,
		'descripcionG'=>'Gastos Juicio Mercantil 975/218'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>22,
		'descripcionG'=>'2% y 3% Sobre Nóminas'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>23,
		'descripcionG'=>'Equipo para Oficina'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>24,
		'descripcionG'=>'Otros'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>25,
		'descripcionG'=>'Cooperaciones'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>26,
		'descripcionG'=>'Pago al IMSS e INFONAVIT'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>27,
		'descripcionG'=>'Mantenimiento de Oficinas'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>28,
		'descripcionG'=>'Cheque Devuelto'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>29,
		'descripcionG'=>'Intereses por Préstamo'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>30,
		'descripcionG'=>'Transferencia a la Cuenta'
		]);

     //Datos de Conservación

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>31,
		'descripcionG'=>'Sueldo de Personal y Compensaciones'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>32,
		'descripcionG'=>'Gasolina y combustibles'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>33,
		'descripcionG'=>'Aceite y Estopa'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>34,
		'descripcionG'=>'Abono a Maquinaria'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>35,
		'descripcionG'=>'Liquidación de Factura'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>36,
		'descripcionG'=>'Programa Anual de Conservación'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>37,
		'descripcionG'=>'Mantenimiento de Maquinaria'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>38,
		'descripcionG'=>'Refacciones Maquinaria'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>39,
		'descripcionG'=>'Refrendo Extemporaneo'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>40,
		'descripcionG'=>'Traslado de Maquinaria'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>41,
		'descripcionG'=>'Viajes de Tierra'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>42,
		'descripcionG'=>'Renta de Equipo'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>43,
		'descripcionG'=>'Reparación de Equipo'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>44,
		'descripcionG'=>'Traslados'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>45,
		'descripcionG'=>'Abono de Reparación'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>46,
		'descripcionG'=>'Abono a Factura'
		]);


      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>47,
		'descripcionG'=>'Compra de Equipo'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>48,
		'descripcionG'=>'Anticipos'
		]);

      //Datos de Operación

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>49,
		'descripcionG'=>'Sueldos y Compensaciones'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>50,
		'descripcionG'=>'Mantenimiento Equipo de Transporte'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>51,
		'descripcionG'=>'Refacciones Equipo de Transporte'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>52,
		'descripcionG'=>'Pago de Agua en Bloque'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>53,
		'descripcionG'=>'Teléfono'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>54,
		'descripcionG'=>'Vulcanizadora'
		]);

      DB::table('concepto_gasto')->insert([
		'Id_Concepto_gasto'=>55,
		'descripcionG'=>'Elaboración Plan de Riegos'
		]);
    }
}
