<?php

namespace App\Http\Controllers;
use App\ejido;
use App\bitacora;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Exports\EjidosExport;

class Ejidos extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {

  }

  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('admin');
  }

  public function eliminar($id){
    ejido::find($id)->delete();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Ejidos";
    $bitacora->Id_movimiento=3;
    $bitacora->save();
  }

  public function verEjido()
  {
    $ejido=ejido::all();
    return view("Ejidos.ejido",compact('ejido'));
  }

  public function store(Request $request){
    if($request->ajax()){
      $v = \Validator::make($request->all(), [
        'nombre_ej' => 'required',
        ]);

      if ($v->fails()) {
        echo $v->errors();
        return response()->json(['errors'=>$v->errors()->all()]);
      //return Response::json(array('errors' => $v->getMessageBag()->toArray()));
      }else {
        $cultivos=DB::table('ejido')->where('nombre_ej','=',$request->nombre_ej)->count();
        if ($cultivos>0) {
          echo "error";
        } else {
          $ejido=new ejido();
          $ejido->Id_Ejido=0;
          $ejido->nombre_ej=$request->nombre_ej;
          $ejido->save();
          $usuario=Auth::user()->id;
          $bitacora=new bitacora();
          $bitacora->Id_usuario=$usuario;
          $bitacora->id_bitacora=0;
          $bitacora->modulo="Ejidos";
          $bitacora->Id_movimiento=1;
          $bitacora->save();
        }
      }
    }else{

    }
  }

  public function actualizar($id_ejido){
    $datos = DB::table('ejido')
      ->where('Id_Ejido', '=', $id_ejido)
      ->select('*')
      ->get();

    return ($datos=array('datos' => $datos));
  }


  public function modificar(Request $request){
    echo $request->id;
    if($request->ajax()){
      $v = \Validator::make($request->all(), [
          'nombre_ej' => 'required',
        ]);

        if ($v->fails()){
          echo $v->errors();
          return response()->json(['errors'=>$v->errors()->all()]);
        } else {
          $ejido=ejido::find($request->id);
          $ejido->nombre_ej=$request->nombre_ej;
          $ejido->save();
          $usuario=Auth::user()->id;
          $bitacora=new bitacora();
          $bitacora->Id_usuario=$usuario;
          $bitacora->id_bitacora=0;
          $bitacora->modulo="Ejidos";
          $bitacora->Id_movimiento=2;
          $bitacora->save();
        }
    } else {

    }
  }

  public function exportarExcel(){
    $export = new EjidosExport();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Ejidos";
    $bitacora->Id_movimiento=4;
    $bitacora->save();
    return $export->download('Ejidos.xlsx');
    //return Excel::download(new UsersExport, 'usuarios.xlsx');
  }


}
