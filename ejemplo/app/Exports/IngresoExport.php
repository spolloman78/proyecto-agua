<?php

namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Historico;
use App\bitacora;
use App\parcela;
use App\ejido;
use App\ejidatario;
use App\canaleros;
use App\municipio;
use App\tenencia;
use App\factura;
use App\facturaconcepto;
use Maatwebsite\Excel\Concerns\FromCollection;

class IngresoExport implements FromView
{
  use Exportable;
  /**
  * @return \Illuminate\Support\Collection
  */
  protected $inicio;
  protected $fin;
  protected $cicloe;

  public function __construct(Request $request)
  {
      $this->inicio = $request->inicio;
      $this->fin = $request->fin;
  }
  public function view(): View
  {
    $in=$this->inicio;
    $fi=$this->fin;
    $inicio=$in."T00:00";
    $fin=$fi."T23:59";
    $dineroDia=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')
    ->select(DB::raw('SUM(abonos.pago) as total'))->whereBetween('factura.created_at', array($inicio, $fin))->get();
      $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
      ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
      ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
      ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
      ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
      ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
      ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
      ->select('factura_concepto.Total','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','factura_concepto.Total')
      ->whereBetween('factura.created_at', array($inicio, $fin))->get();
      return view('Ingresos.excel',[
          'factura' => $factura,'inicio'=>$inicio,'fin'=>$fin,'dineroDia'=>$dineroDia
      ]);
  }
}
