@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Deudores <i class="flaticon-coins"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catalogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Deudores</a>
            </li>
          </ul>
        </div>
        <div class="row">

            <div class="col-sm-6 col-md-4">
              <div class="card card-stats card-round">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5">
                      <div class="icon-big text-center">
                        <i class="icon-map text-muted"></i>
                      </div>
                    </div>
                    <div class="col-7 col-stats">
                      <div class="numbers">
                        <p class="card-category">Parcelas Deudoras</p>
                        <h4 class="card-title">{{$deudoresT}}</h4>
                      </div>

                      </div>
                    </div>
                  </div>
                </div>


          </div>
          <div class="col-sm-6 col-md-4">
            <div class="card card-stats card-round">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5">
                    <div class="icon-big text-center">
                      <i class="flaticon-users text-danger"></i>
                    </div>
                  </div>
                  <div class="col-7 col-stats">
                    <div class="numbers">
                      <p class="card-category">Ejidatarios Deudores</p>
                      <h4 class="card-title">{{$num}}</h4>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="card card-stats card-round">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5">
                      <div class="icon-big text-center">
                        <i class="fas fa-dollar-sign text-success"></i>
                      </div>
                    </div>
                    <div class="col-7 col-stats">
                      <div class="numbers">
                        <p class="card-category">Deuda Total</p>
                        @foreach($sum  as $row)
                        <h4 class="card-title"> $ {{$row->total}}</h4>
                        @endforeach
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="card-title">Catalogo de Deudores<i class="fas fa-hat-cowboy"></i></h4>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-right">
                       <a class="btn btn btn-round ml-auto"  style=" margin-left:20px; background-color:#00A631; color:white;" href="{{'exportarExcelDeudores'}}">
                        <i class="fas fa-file-excel fa-2x"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">


                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                      <th># <i class="flaticon-coins"></i></th>
                      <th>Ejidatario</th>
                      <th>Parcela</th>
                       <th>Pagado</th>
                        <th>Deuda</th>
                        <th>Ciclo</th>

                          <th>Fecha</th>
                          <th class="text-center">Operaciones</th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($deudores as $row)


                      <tr>
                        <td>{!!$row->Id_factura!!}</td>
                        <td>{!!$row->nombre!!} {!!$row->apPaterno!!} {!!$row->apMaterno!!}</td>
                        <td>{!!$row->tarjeta!!}</td>
                        <td> {!!$row->total!!}</td>
<?php $valor=""; $valor = ($row->nombre).($row->apPaterno)."   ".($row->apMaterno)?>
<?php $deuda=0; $deuda=($row->total+0)-($row->pagado+0); ?>
                        <td>{{$deuda}}</td>
                        <td>{!!$row->ciclo!!}</td>

                        <td>{!!$row->data!!}</td>
                        <td>
                          <div class="form-button-action">
                            <button type="button" data-toggle="tooltip" title="Pagar adeudo" class="btn btn-link btn-success btn-lg" data-original-title="Pagar Adeudo"
                               data-toggle="modal" data-target="#pagarad"   onclick="modalPago('{{$row->Id_factura}}','{{$deuda}}',' {{$valor}}','{{$row->Id_factura}}')"  >
                              <i class="fas fa-dollar-sign"></i>
                            </button>

                          </div>
                        </td>
                      </tr>

    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <!---MODAL PARA PAGAR-->
<div class="modal fade  bd-example-modal-lg" id="pagarad" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header no-bd ">
        <h5 class="modal-title" >
          <span class="fw-mediumbold">
          Pagar </span>
          <span class="fw-light">
            Adeudo  <i class="flaticon-coins" style="color:green; "></i>
          </span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
        <p class="small">En este panel usted podrá ingresar la cantidad a pagar de una factura.</p>
        <form id="pagaradeudo"action="{{route('actualizaradeudo')}}" >
            {{csrf_field()}}
            <p class="text-danger" id="textopago"></p>
          <div class="row">
            <div class="col-sm-5">
              <div class="form-group form-group-default"  >
                <label>Ejidatario</label>
                  <input id="deuda" type="text" hidden name="deuda" class="form-control">
                <input id="folio" type="text" hidden name="id_factura" class="form-control" placeholder="0" min="1" max="100">
                <input id="Ejidatario" type="text" disabled class="form-control" placeholder="Cultivo" required>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group form-group-default">
              <label>Deuda</label>
             <input id="id" type="number" required disabled name="id" class="form-control" placeholder="0">
               </div>
               </div>
            <div class="col-sm-4">
              <div class="form-group form-group-default">
                <label>Cantidad a pagar</label>
                <input id="cantidad"  name="cantidad"type="number" class="form-control" placeholder="Cantidad a Pagar" min="0" max="{{$valor}}"required>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="form-group form-group-default" >
            <label>Conceptos</label>
            <table id="conceptos"  class="display table table-striped table-hover" >
              <thead>
                <tr>
                  <th>Parcela</th>
                  <th>Concepto</th>
                  <th>Superficie de Riego</th>
                  <th>Cultivo</th>
                  <th>Importe</th>
                </tr>
                </thead>
                <tbody id="cuerpo">
                  <tr>
                    <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                 </table>
               </div>
             </div>
          </div>
      </div>
      <div class="modal-footer no-bd">
        <button type="button" id="pagarcan" class="btn"  style="background-color:#00A631; color:white;">  <i class="fas fa-dollar-sign"></i> Pagar </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
</form>
      </div>
    </div>
  </div>
</div>
<script>
  function modalPago(id,deuda,ejido,folio){
$("#cantidad").closest('.form-group').removeClass('has-error');
document.getElementById('textopago').innerHTML="";
$("#id").val(deuda);
$("#deuda").val(deuda);
$("#folio").val(folio);
$("#Ejidatario").val(ejido);
$("#pagarad").modal('show');
$.ajax({
  url:'verConceptos/'+ id,
  type:'get',
   success:function(data){
          $('#cuerpo').html(data);
  },error:function(x,xs,xt){
          alert("Error  no se pudo encontrar informacion "+xt);
      } });

     }
     $("body").on("click","#pagarcan",function(event){
       var frm=$("#pagaradeudo");
       var datos = frm.serialize();
       $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
         type:'POST',
         url:"{{route('actualizaradeudo')}}",
         data:datos,
       success:function(data){
      if(data=="Error"){
  document.getElementById('textopago').innerHTML="Favor de proporcionar un cantidad menor.";
      }else{
        $('#pagarad').modal('hide');
      location.reload();

         swal({
           title: "Pago Realizado",
           text: "Pago realizado con exito.",
           icon: "success",
           buttons:false,
           timer:3000
         });
       }}
             ,
             error:function(x,xs,xt){
               alert(x.responseText);
               $("#cantidad").closest('.form-group').addClass('has-error');
        document.getElementById('textopago').innerHTML="Favor de llenar los campos";

             }
       });
       });

</script>
          @endsection
