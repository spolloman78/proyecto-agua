<?php

namespace App\Exports;
use App\Invoice;
use App\Parcela;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class ParcelasExport implements FromView
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view():view
    {
      $parcela=parcela::
      join("ejidatario","ejidatario.Id_ejidatario","=","parcela.Id_ejidatario")
      ->join("ejido","ejido.Id_Ejido","=","parcela.Id_Ejido")
      ->join("tenencia",'tenencia.Id_tenencia','=','parcela.Id_tenencia')
      ->join("municipio",'municipio.Id_municipio','=','parcela.Id_municipio')
      ->join("canalero",'canalero.Id_canalero','=','parcela.Id_canalero')
      ->select('parcela.Id_parcela','ejido.Id_Ejido','parcela.Id_municipio','municipio.descripcion as muni','ejidatario.nombre as ejidon','ejidatario.apPaterno','ejidatario.apMaterno','ejido.nombre_ej','parcela.CP','parcela.SEC','parcela.cuenta',
       'parcela.LT','parcela.SLT','parcela.RA','parcela.PC','tenencia.Id_tenencia as TE','parcela.SR','parcela.EQ','parcela.sup_Fis','parcela.pro','canalero.nombre')->get();
       return view('Parcelas.excel',['parcela'=>$parcela]);
    }
}
