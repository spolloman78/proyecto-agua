<center><h2>CÁTALOGO DE PARCELAS</h2></center>
<table>
    <thead>
        <tr>
          <th>#</th>
          <th>Ejidatario</th>
           <th>Municipio</th>
           <th>Canalero</th>
           <th>Nombre ejido</th>
           <th>Cuenta</th>
             <th>SEC</th>
             <th>CP</th>
             <th>LT</th>
             <th>SLT</th>
             <th>RA</th>
             <th>PC</th>
             <th>TE</th>
             <th>SR</th>
             <th>EQ</th>
             <th>Superficie física</th>
             <th>PRO</th>
        </tr>
    </thead>
    <tbody>
      @forelse($parcela as $row)
      <tr>
        <td>{!!$row->Id_parcela!!}</td>
        <td>{!!$row->ejidon!!} {!!$row->apPaterno!!}{!!$row->apMaterno!!}</td>
        <td>{!!$row->muni!!}</td>
        <td>{!!$row->nombre!!}</td>
        <td>{!!$row->nombre_ej!!}</td>
        <td>{!!$row->cuenta!!}</td>
        <td>{!!$row->SEC!!}</td>
        <td>{!!$row->CP!!}</td>
        <td>{!!$row->LT!!}</td>
        <td>{!!$row->SLT!!}</td>
        <td>{!!$row->RA!!}</td>
        <td>{!!$row->PC!!}</td>
        <td>{!!$row->TE!!}</td>
        <td>{!!$row->SR!!}</td>
        <td>{!!$row->EQ!!}</td>
        <td>{!!$row->sup_Fis!!}</td>
        <td>{!!$row->pro!!}</td>
        @empty
        @endforelse
    </tbody>
</table>
