<?php

namespace App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ingreso extends Model
{
    protected $table = 'ingresos';
    public $timestamps = false;
    use SoftDeletes; //Implementamos
    protected $dates = ['deleted_at'];
    public  $primaryKey = 'Id_Ingreso';
}