@extends('layouts.app')

@section('content')
<script>
	    function showPage() {

		console.log("Entra al SP");
		setTimeout(function(){
			console.log("Espera...");
			document.getElementById("loader").style.display = "none";
			console.log("Termina el Stout");
			document.getElementById("myDiv").style.display = "block";

		},5000);



    }
</script>
<body class="app header-fixed sidebar-fixed sidebar-lg-show" style="background-image: url('assets/img/fondo1.jpg'); background-position: center;" onload="showPage()" style="margin:0;">
<div class="center">

    <div id="loader"></div>

</div>

<link rel="stylesheet" href="{{ asset('assets/css/loader.css') }}" />
<div style="display:none;" id="myDiv" class="animate-bottom">
<div class="container">
  <div class="row align-items-center justify-content-center auth">
      <div class="col-md-6 col-lg-5">
            <div class="card">
                <div class="card-block justify-content-center auth">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <center>
                        <div class="auth-header" style="margin-top: 5%;">
                          <img class="mb-4"  src="{{ asset('assets/img/logo-negroSmall.png') }}" alt="" width="250" height="120">
                        </div>
                      <div class="auth-header";>
                          <img class="mb-4"  src="{{ asset('assets/img/itsi-login.png') }}" alt="" width="125" height="50">
                        </div>
                        <div class="auth-body">

                              <div class="form-group" class="{'has-danger': errors.has('email'), 'has-success': this.fields.email && this.fields.email.valid }">
                              <label for="email">Correo Electronico</label>
                                                      <div class="col-md-12">
                                                                        <div class="input-group-addon">
                                                                          <i class="input-icon input-icon--mail"></i>
                                                                        </div>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>
                      </div>

                        <div class="form-group">
                              <label for="password">Contraseña</label>
                                      <div class="col-md-12">
                                          <div class="input-group-addon"><i class="input-icon input-icon--lock"></i></div>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
												<div class="form-group ">
											<div class="col-md-12">
											<button type="submit" class="btn btn-primary btn-block ">
											Iniciar sesión
											</button>
											</div>
														</div>
                        <div class="form-group">

                                <div class="form-check">
                                    <input class="form-grou" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                <h6>  Si no recuerdas tu contraseña <br/>favor de comunicarse con el administrador ITSÏ </h6>
                                    </label>
                                </div>

                        </div>


	

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
