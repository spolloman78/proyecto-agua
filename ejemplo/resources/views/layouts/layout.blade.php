<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>ITSÏ | {{Auth::user()->role->descripcion}} </title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/favicon.ico" type="image/x-icon"/>
	<!-- Fonts and icons -->
	<script src="../assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel='stylesheet' href='https://fullcalendar.io/js/fullcalendar-3.1.0/fullcalendar.min.css' />
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/atlantis.min.css">
	<link rel="stylesheet" href="../assets/css/loader.css">
	<link rel="stylesheet" href="../assets/css/tabla.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="../assets/css/demo.css">

	<!-- SELECT2 Buscar en select -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

		<!-- SELECT2 Buscar en select -->
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />-->

		<!--select picker
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
		-->
	<!--select picker
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	-->
</head>
<body>

	<div class="wrapper">

		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="green2">

				<a href="{{route('home')}}" class="logo">
				<img src="../assets/img/logo-blanco.png" height="60" width="110" class="navbar-brand" style="margin-left:20px;  margin-right: auto; display: block;">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="green2">

				<div class="container-fluid">
					{{--
					<div class="collapse" id="search-nav">
						<form class="navbar-left navbar-form nav-search mr-md-3">
							<div class="input-group">
								<div class="input-group-prepend">
									<button type="submit" class="btn btn-search pr-1">
										<i class="fa fa-search search-icon"></i>
									</button>
								</div>
								<input type="text" placeholder="Buscar ..." class="form-control">
							</div>
						</form>
					</div>
					--}}

					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
					{{--
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-envelope"></i>
							</a>
							<ul class="dropdown-menu messages-notif-box animated fadeIn" aria-labelledby="messageDropdown">
								<li>
									<div class="dropdown-title d-flex justify-content-between align-items-center">
										Mensajes
										<a href="#" class="small">Mark all as read</a>
									</div>
								</li>
								<li>
									<div class="message-notif-scroll scrollbar-outer">
										<div class="notif-center">
											<a href="#">
												<div class="notif-img">
													<img src="../assets/img/jm_denis.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Jimmy Denis</span>
													<span class="block">
														How are you ?
													</span>
													<span class="time">5 minutes ago</span>
												</div>
											</a>
											<a href="#">
												<div class="notif-img">
													<img src="../assets/img/chadengle.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Chad</span>
													<span class="block">
														Ok, Thanks !
													</span>
													<span class="time">12 minutes ago</span>
												</div>
											</a>
											<a href="#">
												<div class="notif-img">
													<img src="../assets/img/mlane.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Jhon Doe</span>
													<span class="block">
														Ready for the meeting today...
													</span>
													<span class="time">12 minutes ago</span>
												</div>
											</a>
											<a href="#">
												<div class="notif-img">
													<img src="../assets/img/talha.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Talha</span>
													<span class="block">
														Hi, Apa Kabar ?
													</span>
													<span class="time">17 minutes ago</span>
												</div>
											</a>
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);">See all messages<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell"></i>
								<span class="notification">4</span>
							</a>
							<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
								<li>
									<div class="dropdown-title">You have 4 new notification</div>
								</li>
								<li>
									<div class="notif-scroll scrollbar-outer">
										<div class="notif-center">
											<a href="#">
												<div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
												<div class="notif-content">
													<span class="block">
														New user registered
													</span>
													<span class="time">5 minutes ago</span>
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-success"> <i class="fa fa-comment"></i> </div>
												<div class="notif-content">
													<span class="block">
														Rahmad commented on Admin
													</span>
													<span class="time">12 minutes ago</span>
												</div>
											</a>
											<a href="#">
												<div class="notif-img">
													<img src="../assets/img/profile2.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="block">
														Reza send messages to you
													</span>
													<span class="time">12 minutes ago</span>
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-danger"> <i class="fa fa-heart"></i> </div>
												<div class="notif-content">
													<span class="block">
														Farrah liked Admin
													</span>
													<span class="time">17 minutes ago</span>
												</div>
											</a>
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>--}}
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
								<i class="fas fa-layer-group"></i>
							</a>
							<div class="dropdown-menu quick-actions quick-actions-info animated fadeIn ">
								<div class="quick-actions-header  bg-success">
									<span class="title mb-1">Operaciones Rapidas</span>
									<span class="subtitle op-8"></span>
								</div>
								<div class="quick-actions-scroll scrollbar-outer">
									<div class="quick-actions-items">
										<div class="row m-0">
											@if(Auth::user()->role->descripcion!="Tesorero")
											<a class="col-6 col-md-4 p-0" href="{{route('CorteCaja')}}">
												<div class="quick-actions-item">
													<i class="flaticon-file-1"></i>
													<span class="text" style="color:green;">Corte de Caja</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="{{route('factura')}}">
												<div class="quick-actions-item">
													<i class="flaticon-list"></i>
													<span class="text" style="color:green;">Captura de Factura</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="{{route('pdf')}}">
												<div class="quick-actions-item">
													<i class="flaticon-pen"></i>
													<span class="text" style="color:green;">Libro de Canaleros</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="{{route('calendar')}}">
												<div class="quick-actions-item">
													<i class="flaticon-calendar"></i>
													<span class="text" style="color:green;">Calendario de Riegos</span>
												</div>
											</a>
											@endif
											@if(Auth::user()->role->descripcion=="Tesorero")
											<a class="col-6 col-md-4 p-0" href="{{route('ingresos')}}">
												<div class="quick-actions-item">
													<i class="flaticon-cart"></i>
													<span class="text" style="color:green;">Ingresos</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="{{route('egresos')}}">
												<div class="quick-actions-item">
													<i class="flaticon-cart-1"></i>
													<span class="text"style="color:green;">Egresos</span>
												</div>
											</a>@endif
											<a class="col-6 col-md-4 p-0" data-toggle="modal" data-target="#modalRepIE">
												<div class="quick-actions-item">
													<i class="flaticon-file"></i>
													<span class="text" style="color:green;">Reporte de Ingresos y Egresos</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="{{route('reportes')}}">
												<div class="quick-actions-item">
													<i class="flaticon-graph-2"></i>
													<span class="text"style="color:green;">Reportes ITSI</span>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</li>

						<!-- FOTO PERFIL -->
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">

								<div class="avatar  avatar-online">
									<?php $palabra=Auth::user()->name;
									$pal=substr($palabra,0,2);
									$pal=strtoupper($pal);
									 ?>
									<span class="avatar-title rounded-circle border border-white" style="background-color:#534559;">	<img src="../assets/img/profile.jpg" alt="..." class="avatar-img "></span>
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="../assets/img/profile.jpg" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4>	{{Auth::user()->name}} 	</h4>
												<p class="text-muted">{{Auth::user()->email}} </p><button class="btn btn btn-round ml-auto"  style="background-color:#00A631; color:white;" data-toggle="modal" data-target="#Perfil">
								<i class="fas fa-user-circle"></i>
								Ver Perfil
							</button>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown-divider"></div>
						    <a class="dropdown-item" href="{{route('ayuda')}}">Ayuda</a>
										{{--<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Configuración</a>--}}
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="{{ url('/logout') }}">Cerrar Sesión</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="../assets/img/profile.jpg"class="avatar-img rounded-circle "/>

						</div>
						<div class="info">
							<a> <!--data-toggle="collapse" href="#collapseExample" aria-expanded="true"-->
								<span>
									{{Auth::user()->name}}
									<span class="user-level">{{Auth::user()->role->descripcion}}</span>

								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">

									{{--<li>
										<a href="#edit">
											<span class="link-collapse">Editar Perfil</span>
										</a>
									</li>
									<li>
										<a href="#settings">
											<span class="link-collapse">Configuración</span>
										</a>
									</li>--}}
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-success">
						@if(Auth::user()->role->descripcion != "Tesorero")
							<li class="nav-item ">
								<a href="{{route('usuario')}}">
									<i class="fas fa-users"></i>
									<p>Usuarios</p>
								</a>
							</li>
							<li class="nav-item ">
								<a href="{{route('canalero')}}">
									<i class="fas fa-fill-drip"></i>
									<p>Canaleros</p>
								</a>
							</li>
						@endif
							<li class="nav-item ">
								<a href="{{route('ejidatario')}}">
									<i class="icon-people"></i>
									<p>Ejidatarios</p>
								</a>
							</li>
							<li class="nav-item ">
								<a data-toggle="collapse" href="#sidebarLayouts">
									<i class="icon-map"></i>
									<p>Ejidos</p>
									<span class="caret"></span>
								</a>
								<div class="collapse" id="sidebarLayouts">
									<ul class="nav nav-collapse">
										<li>
											@if(Auth::user()->role->descripcion != "Tesorero")
												<a href="{{route('ejido')}}">
													<span class="sub-item">Registro de Ejidos</span>
												</a>
											@endif
											<a href="{{route('parcela')}}">
												<span class="sub-item">Registro de Parcelas</span>
											</a>
											@if(Auth::user()->role->descripcion != "Tesorero")
												<a href="{{route('verConagua')}}">
													<span class="sub-item">Registro de CONAGUA</span>
												</a>
											@endif
										</li>
									</ul>
								</div>
							</li>
						@if(Auth::user()->role->descripcion!="Tesorero")
							<li class="nav-item ">
								<a href="{{route('cultivo')}}">
									<i class="icon-drop"></i>
									<p>Cultivos</p>
								</a>
							</li>
						@endif
						<!--
						@if(Auth::user()->role->descripcion!="Administrador")
							<li class="nav-item ">
								<a data-toggle="collapse" href="#tables">
									<i class="icon-tag"></i>
									<p>Tesoreria</p>
									<span class="caret"></span>
								</a>
								<div class="collapse" id="tables">
									<ul class="nav nav-collapse">
										<li>
											<a href="{{route('egresos')}}">
												<span class="sub-item">Egresos</span>
											</a>
										</li>
										<li>
											<a href="{{route('ingresos')}}">
												<span class="sub-item">Ingresos</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
						@endif
						-->
							<li class="nav-item ">
								<a data-toggle="collapse" href="#charts">
									<i class="icon-basket"></i>
									<p>Caja</p>
									<span class="caret"></span>
								</a>
								<div class="collapse" id="charts">
									<ul class="nav nav-collapse">
	               @if(Auth::user()->role->descripcion=="Tesorero")
										<li>
											<a href="{{route('ingresos')}}">
												<span class="sub-item">Ingresos</span>
											</a>
										</li>
										<li>
											<a href="{{route('egresos')}}">
												<span class="sub-item">Egresos</span>
											</a>
										</li>
										@endif
										@if(Auth::user()->role->descripcion!="Tesorero")
											<li>
												<a href="{{route('concepto')}}">
													<span class="sub-item">Conceptos</span>
												</a>
											</li>
											<li>
												<a href="{{route('factura')}}">
													<span class="sub-item">Captura de Factura</span>
												</a>
											</li>
											<li>
												<a href="{{route('verfact')}}">
													<span class="sub-item">Bitacora de Riego</span>
												</a>
											</li>
											<li>
												<a href="{{route('historico')}}">
													<span class="sub-item">Historico de Facturas</span>
												</a>
											</li>

										@endif

									</ul>
								</div>
							</li>
								@if(Auth::user()->role->descripcion!="Tesorero")
								<li class="nav-item ">
									<a href="{{route('deudores')}}">
										<i class="flaticon-coins"></i>
										<p>Deudores</p>
									</a>
								</li>
								<li class="nav-item ">
									<a href="{{route('bitacora')}}">
										<i class="flaticon-file-1"></i>
										<p>Bitácora</p>
									</a>
								</li>
								@endif
								<li class="nav-item ">
									<a href="{{route('ayuda')}}">
							   	<i class="flaticon-round"></i>
										<p>Ayuda</p>
									</a>
								</li>
				</div>
			</div>
		</div>

		 <div class="loader-wrapper">
			<div class="loader1"><span class="loader-inner"></span></div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="modalRepIE" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Reporte de Ingresos y Egresos</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form id="ReporteIE" method="POST" action="/ReporteIE">
					<div class="modal-body">
							<div class="row">
									@php $fecha = date("Y") . "-" . date("m") . "-" . date("d"); @endphp
									{{csrf_field()}}
									<div class="col-md-6">
										<div class="form-group form-group-default">
											<label for="num_factura">Desde:</label>
											<input type=date class="form-control form-control-sm" id="fecha_ini" name="finicio" value="{{ $fecha }}" max="{{ $fecha }}" >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group form-group-default">
											<label for="num_factura">Hasta:</label>
											<input type=date class="form-control form-control-sm" id="fecha_fin" name="ffin" value="{{ $fecha }}" max="{{ $fecha }}">
										</div>
									</div>
							</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-success">Generar</button>
					</div>
				</form>
				</div>
			</div>
		</div>

		@yield('content')
		<!-- End Custom template -->
	</div>
	<!-- Modal de Perfil -->
	<div class="modal fade modal" id="Perfil" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content" id="usuario">
			  <div class="modal-header no-bd">
				<h5 class="modal-title" id="usuario_edit">Perfil del Usuario:</h5>
				<h5> &nbsp; {{Auth::user()->name}} {{Auth::user()->apPaterno}} {{Auth::user()->apMaterno}} </h5>
				<button type="button" class="close"  data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
					  <form id="user">
							<div class="row">
								<div class="col-sm-2">
									<div class="avatar avatar-lg avatar-online">
										<?php $palabra=Auth::user()->name;
										$pal=substr($palabra,0,2);
										$pal=strtoupper($pal);
										 ?>
										<span class="avatar-title rounded-circle border border-white" style="background-color:#00A631;">{{$pal}}</span>
									</div>
						</div>
						{{csrf_field()}}
						<div class="col-sm-9">
							<div class="form-group form-group-default">
							<label for="alias_usuario">Usuario</label>
							<h5>&nbsp; {{Auth::user()->email}}</h5>
							</div>
						</div>
						<div class="col-sm-12">
							<br/>
					  <div class="form-group form-group-default">
						<label for="nom_user">Nombre del usuario:</label><h5>&nbsp; {{Auth::user()->name}}</h5>
					  </div>
					</div>
					<div class="col-sm-12">
					  <div class="form-group form-group-default">
						<label for="apPaterno">Apellido paterno</label> <h5>&nbsp; {{Auth::user()->apPaterno}}</h5>
					  </div>
					</div>
					<div class="col-sm-12">
					  <div class="form-group form-group-default">
						<label for="apMaterno">Apellido materno</label><h5>&nbsp; {{Auth::user()->apMaterno}}</h5>
					  </div>
					</div>

					<div class="col-sm-12">
					  <div class="form-group form-group-default">
						<label for="direccion">Rol</label>
						<h5>&nbsp; {{Auth::user()->role->descripcion}}</h5>
					  </div>
					</div>

					  <input type="text" class="form-control" id="id_user" name="id_user" hidden>
					</form>
				  </div>
				</div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			  </div>
			</div>
		  </div>
		</div>
	<!--   Core JS Files   -->

	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="../assets/js/core/popper.min.js"></script>
	<script src="../assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="../assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="../assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>

	<!-- Chart JS -->
	<script src="../assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="../assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Datatables -->
	<script src="../assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<script src="../assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

	<!-- Sweet Alert -->
	<script src="../assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Atlantis JS -->
	<script src="../assets/js/atlantis.min.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="../assets/js/setting-demo.js"></script>
	<script src="../assets/js/demo.js"></script>
	<!-- select picker
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
	-->
	<!-- SELECT2 Buscar en select -->
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>



	<!-- select picker
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
	-->
	<!-- SELECT2 Buscar en select -->
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

	<script>
	// funcion para generar una notificacion
	function mensaje(mensaje,tipo,ubicacion,titulo){
		var placementFrom = ubicacion;
		var placementAlign = "right";
		var state =tipo ;
		var content = {};
		content.message = mensaje;
		content.title = titulo;
		content.icon = 'flaticon-add';
		content.url = "{{route('cultivo')}}";
		content.target = '_blank';
		$.notify(content,{
			type: state,
			placement: {
				from: placementFrom,
				align: placementAlign
			},
			time: 3000,
			delay: 20,
		});
	}

	$(window).on("load",function(){
		$(".loader-wrapper").fadeOut("slow");
	});

	$(document).ready(function() {
			$('#basic-datatables').DataTable({
			});

			$('#multi-filter-select').DataTable( {
				"pageLength": 5,
				initComplete: function () {
					this.api().columns().every( function () {
						var column = this;
						var select = $('<select class="form-control"><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
								);

							column
							.search( val ? '^'+val+'$' : '', true, false )
							.draw();
						} );

						column.data().unique().sort().each( function ( d, j ) {
							select.append( '<option value="'+d+'">'+d+'</option>' )
						} );
					} );
				}
			});

			$('.js-example-basic-single').select2();
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});

			var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

			// SELECT2 Buscar en select
			$('.js-example-basic-single').select2();

		});
	</script>
</body>
</html>
