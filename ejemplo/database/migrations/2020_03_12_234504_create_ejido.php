<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEjido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ejido', function (Blueprint $table) {

              $table->Increments("Id_Ejido");
              $table->string('nombre_ej');
              $table->softDeletes(); //Nueva línea, para el borrado lógico



      //$table->timestamps();
    });
}

/**
 * Reverse the migrations.
 *
 * @return void
 */
public function down()
{    DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    Schema::dropIfExists('ejido');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');
}
}
