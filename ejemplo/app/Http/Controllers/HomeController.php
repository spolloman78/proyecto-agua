<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cultivo;
use App\canaleros;
use App\parcela;
use App\users;
use App\bitacora;
use App\factura;
use App\egreso;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
     $ldate = date('Y');
     $ci=substr($ldate,2)+0;
     $cl=$ci-1;
     $ciclo=$cl."/".$ci;
     $now = now()->format('Y-m-d');
     date_default_timezone_set("America/Mexico_City");
     $meso = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"][date("n") - 1];
     $anio = now()->format('Y');
     $mes= now()->format('m');
     $egresoD=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
     ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select(DB::raw('SUM(gasto_mes.monto) as totalDia'),'concepto_gasto.descripcionG')
     ->groupby('concepto_gasto.descripcionG')->whereDate('gasto_mes.fecha',$now)->get();
     $egresoM=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
     ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select(DB::raw('SUM(gasto_mes.monto) as totalDia'))
     ->whereYear('gasto_mes.fecha','=',$anio)->get();
     $egresoC=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
     ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select(DB::raw('SUM(gasto_mes.monto) as totalDia'),'concepto_gasto.descripcionG')
     ->groupby('concepto_gasto.descripcionG')->get();
      $dinero=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->select(DB::raw('SUM(factura_concepto.Total) as totalDia'))->whereMonth('factura.created_at','=',$mes)->whereYear('factura.created_at','=',$anio)->where('factura.Id_status_factura','!=','3')
      ->where('factura.Id_status_factura','!=','4')->get();
      $dineroDia=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->select(DB::raw('SUM(factura_concepto.Total) as total'))->whereDate('factura.created_at',$now)->where('factura.Id_status_factura','!=','3')->where('factura.Id_status_factura','!=','4')->get();
      $dineroCiclo=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
     ->select(DB::raw('SUM(factura_concepto.Total) as totalCiclo'))->where('ciclo.Ciclo','=',$ciclo)->where('factura.Id_status_factura','!=','3')->where('factura.Id_status_factura','!=','4')->get();
        return view('home',compact('dinero','egresoC','egresoD','egresoM','dineroDia','meso','now','dineroCiclo','ciclo'));
    }
    public function cultivo(){
        $datos=cultivo::select('descripcion','cantidad_cultivo','capacidad_cultivo')->orderby('cantidad_cultivo')->get();
       return ($datos=array('datos' => $datos));
    }
    public function parcela(){
      $dato=canaleros::join('parcela','parcela.Id_canalero','=','canalero.Id_canalero')
      ->select('canalero.nombre',Db::raw('COUNT(parcela.Id_canalero) as cantidad'))->groupby('canalero.nombre')->get();
     $numero=parcela::count();
     $numero=array('total' => $numero);
     $dato=array('dato' => $dato);
     $datos = array_merge($dato, $numero);
    return ($datos=array('datos' => $datos));
    }
    public function movimientos(){
    $movimientos=bitacora::join('users.id','=','bitacora.Id_usuario')->select('users.name',Db::raw('COUNT(bitacora.Id_usuario) as cantidad'))->groupBy('users.Id_usuario')->get();
return ($movimientos=array('movimientos' => $movimientos));
    }
    public function ayuda(){
      return view('Faq.ayuda');
    }
    public function ventas(){
      // OBTENER VENTAS DE UN MES POR DIA
      $mes= now()->format('m');
      $anio = now()->format('Y');
      $datos=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->select(DB::raw('DATE(created_at) as date'),DB::raw('SUM(factura_concepto.Total) as totalDia'))->whereMonth('factura.created_at','=',$mes)->whereYear('factura.created_at','=',$anio)->where('factura.Id_status_factura','!=','3')
      ->where('factura.Id_status_factura','!=','4')->groupBy('date')->get();
      return ($datos=array('datos' => $datos));

    }
    public function ventasCiclo(){
      //Obtener ventas de un ciclo por Mes
      $ldate = date('Y');
      $ci=substr($ldate,2)+0;
      $cl=$ci-1;
      $ciclo=$cl."/".$ci;
      $anio = now()->format('Y');
      $datos=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->select(DB::raw('MONTH(created_at) as date'),DB::raw('SUM(factura_concepto.Total) as totalCiclo'))->where('ciclo.Ciclo','=',$ciclo)->where('factura.Id_status_factura','!=','3')->where('factura.Id_status_factura','!=','4')->whereYear('factura.created_at','=',$anio)->groupBy("date")->get();
      return ($datos=array('datos' => $datos));

    }
    public function egresos(){
      $egreso=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
      ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select(DB::raw('SUM(gasto_mes.monto) as totalDia'),'concepto_gasto.descripcionG')
      ->groupby('concepto_gasto.descripcionG')->get();
     $egreso=egreso::join('concepto_gasto','concepto_gasto.Id_Concepto_gasto','=','gasto.Id_Concepto_gasto')
     ->join('gasto_mes','gasto_mes.Id_gasto','=','gasto.Id_gasto')->select('gasto_mes.monto','concepto_gasto.descripcionG')->get();
     //select(DB::raw('SUM(.Total) as totalDia'))
    }
}
