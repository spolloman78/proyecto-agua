@extends('layouts.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://uicdn.toast.com/tui-calendar/latest/tui-calendar.css" />

<body>
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <h4 class="page-title">Calendario Canaleros</h4>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Calendario Canaleros</div>

              </div>
              <div class="card-body">



    <div id="calendar"></div>
</body>

<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/jquery.min.js'></script>
<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/jquery-ui.min.js'></script>
<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/moment.min.js'></script>
<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/fullcalendar.min.js'></script>
<script>
var busyTimes = new Array();
var className = Array('fc-primary', 'card-success', 'fc-black', 'fc-success', 'fc-info', 'fc-warning', 'fc-danger-solid', 'fc-warning-solid', 'fc-success-solid', 'fc-black-solid', 'fc-success-solid', 'fc-primary-solid');
var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();
var calendario=$("#calendar").fullCalendar({
header:{
        left:   'prev,next today',
        center: 'title',
        locale: 'es',
        right:  'month,agendaWeek,agendaDay,listWeek'},
        selectable : true,
        selectHelper: true,
         eventSources: [{
           url:'/horario'
         }
       ],
        monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
 dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
        select: function(start, end) {
        },
    })
        console.log(busyTimes);
$.ajax({
url:'/horario/',
type:'get'
}).done(function (data){
  $.each(data.datos, function(idx, opt) {
busyTimes = [
                {
                    title: data.datos[idx].Id_parcela,
                    start: data.datos[idx].created_at,
                    end: data.datos[idx].updated_at,
                    className: "success"
        }
            ]

});

});
  console.log(busyTimes);
//$('#calendar').fullCalendar('renderEvents', busyTimes);
</script>
@endsection
