<?php

namespace App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ejidatario extends Model
{
    protected $table = 'ejidatario';
    public $timestamps = false;
    use SoftDeletes; //Implementamos
    protected $dates = ['deleted_at'];
    public  $primaryKey = 'Id_ejidatario';
}
