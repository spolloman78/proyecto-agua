<?php

namespace App\Exports;

use App\Conagua;
use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class ConaguasExport implements FromView
{
  use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view():view
    {
      $conagua=conagua::join('ejidatario', 'ejidatario.Id_ejidatario', '=', 'conagua.Id_ejidatario')
      ->select('*')->get();
      return view('Conagua.excel',['conagua'=>$conagua]);
    }
}
