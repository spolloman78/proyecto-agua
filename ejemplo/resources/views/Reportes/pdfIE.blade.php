<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Reporte de ingresos y egresos</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="../assets/js/core/bootstrap.min.js"></script>
</head>
<style>
    body{
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        font-style: normal;
        font-size: 12px;
    }
    h1 {
        font-size: 16px;
        font-weight: 700;
        line-height: 26.4px;
    }
    h2 {
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        font-style: normal;
        font-size: 14px;
    }
    blockquote {

        font-size: 21px;
        font-weight: 400;
        line-height: 30px;
    }
    pre {
        font-size: 13px;
        font-weight: 400;
        line-height: 18.5714px;
    }
</style>
<body>
    <div class="text-center">
        ASOCIACION DE AGRICULTORES DEL VALLE <br>
        ALVARO OBREGON-TARIMBARO A.C. <br>
        MODULO III <br>
        INGRESOS Y EGRESOS DEL {{$fecha_inicio}} AL {{$fecha_fin}}<br><br>
    </div>


	<div class="text-center"><h2>I N G R E S O S (ENTRADAS)</h2></div>
    <table class="table table-bordered text-center" style="width:100%">

        <thead>
            <tr>
                <th>#</th>
                <th>Fecha</th>
                <th>Descripción</th>
                <th>Monto</th>
            </tr>
        </thead>
        <tbody>
            @php $cont=1; @endphp
            @foreach($ingreso_mes as $gm)
                <tr>
                    <td> {{$cont++}} </td>
                    <td> 
                        @php 
                            $fecha = explode(" ", $gm->fecha);
                            echo $fecha[0];
                        @endphp 
                    </td>
                    <td> {{$gm->descripcion_con}} </td>
                    <td> {{$gm->Total}} </td>
                </tr>
            @endforeach
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;">
                        <div class="text-right">
                            <h2>TOTAL</h2>
                        </div>
                    </td>
                    <td>
                        <h2>$ {{$ingreso_total}}</h2>
                    </td>
                </tr>
        </tbody>
	</table>
    <br><br>
	<div class="text-center"><h2>E G R E S O S (SALIDAS)</h2></div>
    <table class="table table-bordered text-center" style="width:100%">

        <thead>
            <tr>
                <th>#</th>
                <th>Fecha</th>
                <th>Concepto</th>
                <th>Descripción</th>
                <th>Monto</th>
            </tr>
        </thead>
        <tbody>
            @php $cont=1; @endphp
            @foreach($gasto_mes as $gm)
                <tr>
                    <td> {{$cont++}} </td>
                    <td> {{$gm->fecha}} </td>
                    <td> {{$gm->descripcionG}} </td>
                    <td> {{$gm->descripcion}} </td>
                    <td> {{$gm->monto}} </td>
                </tr>
            @endforeach
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;">
                        <div class="text-right">
                            <h2>TOTAL</h2>
                        </div>
                    </td>
                    <td>
                        <h2>$ {{$egreso_total}}</h2>
                    </td>
                </tr>
        </tbody>
	</table>


</body>
</html>
