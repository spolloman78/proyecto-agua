<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario', function (Blueprint $table) {
          $table->increments("Id_horario");
          $table->unsignedInteger("Id_parcela");
          $table->string("Id_factura");
          $table->foreign('Id_factura')->references('Id_factura')->on('factura');
          $table->foreign('Id_parcela')->references('Id_parcela')->on('parcela');
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horario');
    }
}
