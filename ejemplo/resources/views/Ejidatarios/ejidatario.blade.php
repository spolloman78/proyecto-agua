@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Ejidatarios  <i class="icon-people"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catálogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Ejidatarios</a>
            </li>
          </ul>
        </div>
        <div class="row">

          <div class="col-sm-6 col-md-4">
            <div class="card card-stats card-round">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5">
                    <div class="icon-big text-center">
                      <i class="flaticon-users text-info"></i>
                    </div>
                  </div>
                  <div class="col-7 col-stats">
                    <div class="numbers">
                      <p class="card-category">Ejidatarios</p>
                      <h4 class="card-title">{{$eji}}</h4>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="card card-stats card-round">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5">
                      <div class="icon-big text-center">
                        <i class="flaticon-remove-user-1 text-danger"></i>
                      </div>
                    </div>
                    <div class="col-7 col-stats">
                      <div class="numbers">
                        <p class="card-category">Excentos de servicio</p>
                        <h4 class="card-title">{{$sin}}</h4>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="card card-stats card-round">
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-5">
                        <div class="icon-big text-center">
                          <i class="flaticon-user-2 text-success"></i>
                        </div>
                      </div>
                      <div class="col-7 col-stats">
                        <div class="numbers">
                          <p class="card-category">Con servicio</p>
                          <h4 class="card-title">{{$con}}</h4>
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="card-title"  style="padding: 1% 0 0 0;">Catálogo de Ejidatarios<i class="fas fa-hat-cowboy"></i></h4>
                  </div>@if(Auth::user()->role->descripcion != "Tesorero")
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-center">
                      <button class="btn btn btn-round ml-auto"  style="background-color:#00A631; color:white;" data-toggle="modal" data-target="#addRowEjido">
                        <i class="fa fa-plus"></i>
                        Ejidatario
                      </button>
                      <a class="btn btn btn-round"  style=" margin-left:20px; background-color:#00A631; color:white;" href="{{'exportarExcelEjidatarios'}}">
                        <i class="fas fa-file-excel fa-2x"></i>
                      </a>
                    </div>
                  </div>@endif
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                        <th># <i class="icon-people"></i></th>
                        <th>Nombre</th>

                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>

                        <th>Tarjeta</th>
                        <th>Tarjeta Anterior</th>
                      @if(Auth::user()->role->descripcion != "Tesorero")
                      <th>Operaciones</th>
                       @endif
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($ejidatario as $row)

                        <tr>
                          <td>{!!$row->Id_ejidatario!!}</td>
                          <td>{!!$row->nombre!!}</td>
                          <td>{!!$row->apPaterno!!}</td>
                          <td>{!!$row->apMaterno!!}</td>
                          <td>{!!$row->tarjeta!!}</td>
                          <td>{!!$row->tarjeta_ant!!}</td>
                        @if(Auth::user()->role->descripcion != "Tesorero")  <td>
                            <div class="form-button-action">
                              <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-success btn-lg" data-original-title="Editar Registro"data-toggle="modal" data-target="#modalEdeji" onclick="modalEjidatario('{!!$row ->Id_ejidatario!!}')" title="">
                              <i class="fa fa-edit"></i>
                            </button>
                              <button type="button" data-toggle="tooltip" title="Eliminar" class="btn btn-link btn-danger" onclick="eliminar('{!!$row ->Id_ejidatario!!}')"data-original-title="Eliminar">
                              <i class="fa fa-times"></i>
                            </button>
                            </div>
                          </td>@endif
                        </tr>

                      @endforeach
                    </tbody>
                  </table>
                    </div>
                <br>

              </div>
            </div>
          </div>

          <div class="card-body">
            <!-- Modal -->
            <div class="modal fade " id="addRowEjido" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog " role="document">
                <div class="modal-content">

                  <div class="modal-header no-bd ">
                    <h5 class="modal-title" >
                      <span class="fw-mediumbold">
                      Nuevo</span>
                      <span class="fw-light">
                        Ejidatario   <i class="icon-people" style="color:green; "></i>
                      </span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body ">

                    <p class="small">En este panel usted podrá agregar nuevos ejidatarios en el sistema.</p>
                    <form id="regeji"action="{{route('Aejidatario')}}" >
                        {{csrf_field()}}
                      <div class="row">

                        <div class="col-sm-12">
                           <p class="text-danger" id="textomensaje1" style="margin-left:10px;"></p>
                          <div class="form-group form-group-default">
                            <label>Nombre del ejidatario</label>
                            <input id="addName"  name="nombre"type="text" class="form-control" placeholder="Nombre" required>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Apellido Paterno</label>
                            <input id="addApp" type="text" name="apPaterno" class="form-control" placeholder="Apellido Paterno" min="1" max="100" required>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Apellido Materno</label>
                            <input id="addApm" type="text" name="apMaterno" class="form-control" placeholder="Apellido Materno" min="1" max="100" required>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Tarjeta</label>
                            <input id="addTarj" type="text" name="tarjeta" class="form-control" placeholder="Tarjeta" min="1" max="100" required>
                          </div>
                        </div>

                      </div>


                  </div>
                  <div class="modal-footer no-bd">
                    <button type="submit" id="addEjidatario" class="btn " style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
  </form>
                  </div>
                </div>
              </div>

            </div>

<!---MODAL PARA EDITAR-->
<div class="modal fade " id="modalEdeji" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">

      <div class="modal-header no-bd ">
        <h5 class="modal-title" >
          <span class="fw-mediumbold">
          Editar </span>
          <span class="fw-light">
            Ejidatario  <i class="icon-people" style="color:green; "></i>
          </span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
        <p class="small">En este panel usted podrá editar datos de los ejidatarios en el sistema.</p>
        <form id="modificarej"action="{{route('actualizarejidatario')}}" >
            {{csrf_field()}}
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group form-group-default">
                <label>Nombre del ejidatario</label>
                <input id="nombre"  name="nombre"type="text" class="form-control" placeholder="Nombre" required>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group form-group-default">
                <label>Apellido Paterno</label>
                <input id="apPaterno"  name="apPaterno"type="text" class="form-control" placeholder="Apellido Paterno" required>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group form-group-default"  >
                <label>Apellido Materno</label>
                <input id="apMaterno"  name="apMaterno"type="text" class="form-control" placeholder="Apellido Materno" required>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group form-group-default">
                <label>Tarjeta</label>
                <input id="tarjeta"  name="tarjeta"type="text" class="form-control" placeholder="Tarjeta" required>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group form-group-default">
                <label>Tarjeta Anterior</label>
                <input id="tarjeta_ant"  name="tarjeta_ant"type="text" class="form-control" placeholder="Tarjeta Anterior" required>
              </div>
            </div>
          </div>
      </div>
        <input id="id" type="number" name="id" class="form-control" placeholder="0" min="1" max="100" hidden>
      <div class="modal-footer no-bd">
        <button type="button" id="modificarejid" class="btn"style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Modificar </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
</form>
      </div>
    </div>
  </div>
</div>
           <script>
            // Funcion para eliminar
            function eliminar(id){
            swal({
              title: '¿Seguro que desea eliminar el registro?',
              text: "¡Esta accion no es reversible!",
              type: 'warning',
              icon : "error",
              buttons:{
                confirm: {
                  text : 'Si, deseo eliminarlo!',
                  className : 'btn btn-danger',
                },
                cancel: {
                  text : 'Cancelar',
                  visible: true,
                  className: 'btn btn-warning'
                }
              }
            }).then((Delete) => {
              if (Delete) {
                $.ajax({
                url:'/BorrarEjidatario/'+id,
                type:'get'
                }).done(function (res){
                swal({
                  title: 'Eliminado!',
                  text: 'El registro ha sido Eliminado.',
                  icon : "success",
                  buttons:false,
                  timer:3000,
                });
                  location.reload();
                  });
              } else {
                swal.close();
              }
            });
          }
       // aqui termina funcion de eliminar
            function modalEjidatario(id){

     $.ajax({
       url:'/EjidatarioDatos/'+id,
       type:'get'
     }).done(function (res){
  $("#id").val(id);
       $("#modalEdeji").modal('show');
       $('#nombre').val(res.datos[0].nombre);
       $('#apPaterno').val(res.datos[0].apPaterno);
       $('#apMaterno').val(res.datos[0].apMaterno);
       $('#tarjeta').val(res.datos[0].tarjeta);
       $('#tarjeta_ant').val(res.datos[0].tarjeta_ant);
     });
     }

    $("body").on("click","#addEjidatario",function(event){
      event.preventDefault();
      var frm=$("#regeji");

      var datos = frm.serialize();
      $.ajaxSetup({
           headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });
       $.ajax({
        type:'POST',
        url:'/AgregarEjidatario/',
        data:datos,
      success:function(data){
        if(data=="error"){
     mensaje("Este ejidatario ya existe","warning","top","Registro de Ejidatarios");
    $document.getElementById('textomensaje1').innerHTML='Este ejidtario ya existe.';
        }else{
          $('#addEjidatario').modal('hide');
          location.reload();
          swal({
            title: 'Registrado',
            text: 'El registro ha sido agregado con exito.',
            icon : "success",
            buttons:false,
            timer:3000,
          })
          document.getElementById('textomensaje1').innerHTML='';}
            },
            error:function(x,xs,xt){
              var xd=x.responseText.split("}");
              var obj2=JSON.parse(xd[0]+'}');
              var obj = JSON.parse(xd[1]+'}');
              var xd=obj.errores+"";
              var atributos = "";
             $('input[name="nombre"]').closest('.form-group').removeClass('has-error');
             $('input[name="apPaterno"]').closest('.form-group').removeClass('has-error');
             $('input[name="apMaterno"]').closest('.form-group').removeClass('has-error');
             $('input[name="tarjeta"]').closest('.form-group').removeClass('has-error');
             for(var aux in obj2){
             $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}
             document.getElementById('textomensaje1').innerHTML='Favor de llenar los campos de manera adecuada';
            }
    });
    });

    </script>
        <script>

 $("body").on("click","#modificarejid",function(event){
   alert("");
   var frm=$("#modificarej");
   var datos = frm.serialize();
   $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
     type:'POST',
     url:'/ModificarEjidatario/',
     data:datos,
   success:function(data){
location.reload();
         $('#modalEdeji').modal('hide');

     swal({
       title: "Modificacion Realizada",
       text: 'El registro ha sido modificado con exito.',
       icon: "success",
       buttons: false,
       timer:3000,
     });
       },error:function(x,xs,xt){
           var xd=x.responseText.split("}");
           var obj2=JSON.parse(xd[0]+'}');
           var obj = JSON.parse(xd[1]+'}');
           var xd=obj.errores+"";
           var atributos = "";
          for(var aux in obj2){
          $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}        }
           //mensaje("Favor de llenar los campos de manera adecuada","danger","top","Registro de Ejidatarios");
   });
   });



    </script>
          @endsection
