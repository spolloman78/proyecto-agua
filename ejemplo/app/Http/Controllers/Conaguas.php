<?php

namespace App\Http\Controllers;
use App\ejidatario;
use App\conagua;
use App\bitacora;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
use App\Exports\ConaguasExport;

class Conaguas extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function __invoke(Request $request)
     {

     }

     public function __construct()
     {
       $this->middleware('auth');
       $this->middleware('admin');
     }

     public function verConagua(){
       $ejidatarios=ejidatario::all();
       $conagua=conagua::join('ejidatario', 'ejidatario.Id_ejidatario', '=', 'conagua.Id_ejidatario')
       ->select('conagua.Id_Conagua','conagua.Id_ejidatario','ejidatario.nombre as ejiname','ejidatario.apMaterno','ejidatario.apPaterno','conagua.USIPAD','conagua.CTA_SIPAD','conagua.UNI', 'conagua.ZO', 'conagua.MOD', 'conagua.SRA', 'conagua.SSRA', 'conagua.EST', 'conagua.GR', 'conagua.PRE','conagua.SUPFISICA', 'conagua.SUPRIEGO', 'conagua.SEC_ORIG')
       ->get();
       return view('Conagua.conagua',compact("conagua","ejidatarios"));
     }

     public function eliminar($id){
      conagua::find($id)->delete();
      $usuario=Auth::user()->id;
      $bitacora=new bitacora();
      $bitacora->Id_usuario=$usuario;
      $bitacora->id_bitacora=0;
      $bitacora->modulo="Conagua";
      $bitacora->Id_movimiento=3;
      $bitacora->save();
  }

  public function store(Request $request){
    if($request->ajax()){
      $v = \Validator::make($request->all(), [
          'ejidatarios' => 'required',
           'CTA_SIPAD'    => 'required',
           'UNI' => 'required',
           'ZO'    => 'required',
           'MOD' => 'required',
           'SRA'    => 'required',
           'SSRA' => 'required',
           'EST'    => 'required',
           'GR' => 'required',
           'PRE'    => 'required',
           'SUPFISICA' => 'required',
           'SUPRIEGO'    => 'required',
           'SEC_ORIG' => 'required'
      ]);

      if ($v->fails()){
        echo $v->errors();
        return response()->json(['errors'=>$v->errors()->all()]);
        //return Response::json(array('errors' => $v->getMessageBag()->toArray()));
      }  else {
          $conagua=new conagua();
          $conagua->Id_Conagua=0;
          $conagua->Id_ejidatario=$request->ejidatarios;
          $conagua->USIPAD=0;
          $conagua->CTA_SIPAD=$request->CTA_SIPAD;
          $conagua->UNI=$request->UNI;
          $conagua->ZO=$request->ZO;
          $conagua->MOD=$request->MOD;
          $conagua->SRA=$request->SRA;
          $conagua->SSRA=$request->SSRA;
          $conagua->EST=$request->EST;
          $conagua->GR=$request->GR;
          $conagua->PRE=$request->PRE;
          $conagua->SUPFISICA=$request->SUPFISICA;
          $conagua->SUPRIEGO=$request->SUPRIEGO;
          $conagua->SEC_ORIG=$request->SEC_ORIG;
          $conagua->save();
          $usuario=Auth::user()->id;
          $bitacora=new bitacora();
          $bitacora->Id_usuario=$usuario;
          $bitacora->id_bitacora=0;
          $bitacora->modulo="Conagua";
          $bitacora->Id_movimiento=1;
          $bitacora->save();
        }
    }else{

    }
  }

     public function actualizar($id){
       $datos = DB::table('conagua')
       ->join('ejidatario', 'ejidatario.Id_ejidatario', '=', 'conagua.Id_ejidatario')
       ->select('conagua.Id_Conagua','conagua.Id_ejidatario','ejidatario.nombre as ejiname','ejidatario.apMaterno','ejidatario.apPaterno','conagua.USIPAD','conagua.CTA_SIPAD','conagua.UNI', 'conagua.ZO', 'conagua.MOD', 'conagua.SRA', 'conagua.SSRA', 'conagua.EST', 'conagua.GR', 'conagua.PRE','conagua.SUPFISICA', 'conagua.SUPRIEGO', 'conagua.SEC_ORIG')
       ->where('Id_Conagua', '=', $id)
       ->get();
       return ($datos=array('datos' => $datos));
     }

     public function modificar(Request $request){
      
    if($request->ajax()){

       $conagua=conagua::find($request->Id_Conagua);
        $conagua->Id_ejidatario=$request->ejidatarios;
        $conagua->USIPAD=$request->USIPAD;
        $conagua->CTA_SIPAD=$request->CTA_SIPAD;
        $conagua->UNI=$request->UNI;
        $conagua->ZO=$request->ZO;
        $conagua->MOD=$request->MOD;
        $conagua->SRA=$request->SRA;
        $conagua->SSRA=$request->SSRA;
        $conagua->EST=$request->EST;
        $conagua->GR=$request->GR;
        $conagua->PRE=$request->PRE;
        $conagua->SUPFISICA=$request->SUPFISICA;
        $conagua->SUPRIEGO=$request->SUPRIEGO;
        $conagua->SEC_ORIG=$request->SEC_ORIG;
        $conagua->save();
        $usuario=Auth::user()->id;
        $bitacora=new bitacora();
        $bitacora->Id_usuario=$usuario;
        $bitacora->id_bitacora=0;
        $bitacora->modulo="Conagua";
        $bitacora->Id_movimiento=2;
        $bitacora->save();
     }else{

     }


     }
     public function exportarExcel(){
       $export = new ConaguasExport();
       $usuario=Auth::user()->id;
       $bitacora=new bitacora();
       $bitacora->Id_usuario=$usuario;
       $bitacora->id_bitacora=0;
       $bitacora->modulo="Conagua";
       $bitacora->Id_movimiento=4;
       $bitacora->save();
       return $export->download('Registro ejidos Conagua.xlsx');
       //return Excel::download(new UsersExport, 'usuarios.xlsx');
     }

}
