@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Egresos <i class="fas fa-cart-arrow-down"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Caja</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Egresos</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="card-title"  style="padding: 1% 0 0 0;">Egresos<i class="fas fa-hat-cowboy"></i></h4>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-center">
                      <button class="btn btn btn-round ml-auto"  style="background-color:#00A631; color:white;" data-toggle="modal" data-target="#addRowEgreso">
                        <i class="fa fa-plus"></i>
                        Egreso
                      </button>
                    </div>
                  </div>
                </div>

              </div>
              <div class="card-body">
                <form id="filtrarFechas">
                  <div class="row">

                        {{csrf_field()}}
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label for="num_factura">Desde:</label>
                                <input type=date class="form-control form-control-sm" id="fecha_ini" name="finicio" value="{{$anio}}-01-01" min="2018-01-01" max="{{$now}}" onchange="filtrarFecha()">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label for="num_factura">Hasta:</label>
                                <input type=date class="form-control form-control-sm" id="fecha_fin" name="ffin" value="{{$now}}" max="{{$now}}" onchange="filtrarFecha()">
                            </div>
                        </div>
                  </div>
                </form>
                <br>
                <div class="table-responsive" id="tablaPeriodo">

                  <table id="example" class="display table table-striped table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Concepto</th>
                            <th>Monto Total $</th>
                            <th>Periodo</th>
                            <th></th>
                        </tr>
                    </thead>
                  </table>

                </div>

                <div class="table-responsive" id="tablaFecha" hidden>
                    <table id="tableFecha" class="table table-striped table-bordered text-center" style="width:100%">
                        <thead>
                        <tr>
                            <th>Concepto</th>
                            <th>Descripción</th>
                            <th>Fecha de Registro</th>
                            <th>Monto</th>
                            <th>Operaciones</th>
                        </tr>
                        </thead>
                        <tbody id="tablaFechaBody">

                        </tbody>

                    </table>
                </div>

                <br>


                  <div class="d-flex justify-content-end">
                    <button hidden id="verPeriodos" class="btn btn btn-round ml-auto"  style="background-color:#00A631; color:white;" onclick="reload()">
                      Ver por periodos
                    </button>
                  </div>
                  <div class="d-flex justify-content-center">

                    <div class="col-sm-6 col-md-4">
                      <div class="card card-stats card-round">
                        <div class="card-body" style="background-color: rgba(0,0,0,.05);">
                          <div class="row">
                            <div class="col-5">
                              <div class="icon-big text-center">
                                <i class="fas fa-cart-arrow-down text-danger"></i>
                              </div>
                            </div>
                            <div class="col-7 col-stats">
                              <div class="numbers">
                                <p class="card-category">TOTAL</p>
                                <h4 class="card-title">
                                  <p id="granTotal">$ {{$granTotal}}<p>
                                </h4>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>



              </div>
            </div>
          </div>

        </div>

		<!-------------------------------MODAL NUEVO---------------------->
		<div class="modal fade modal" id="addRowEgreso" role="dialog" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content" id="usuario">
			  <div class="modal-header no-bd">
				<h5 class="modal-title" id="usuario_edit">Añadir nuevo egreso </h5>
				<button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
			  <div class="tab-content" id="myTabContent">
				  <div role="tabpanel" class="tab-pane active">
            <form id="registro">
              {{csrf_field()}}

              <div class="form-group form-group-default">
                <label>Concepto</label>
                <select id="concepto" name="concepto" class="form-control js-example-basic-single" style="width: 100%;"name="Id_Ejido" required>
                  @foreach($conceptogasto as $row)
                    <option value="{!!$row->Id_Concepto_gasto!!}"> {!!$row->descripcionG!!}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group form-group-default">
                <label>Descripción</label>
                <input type="text" class="form-control" id="descripcionN" name="descripcion" required>
              </div>
              <div class="form-group form-group-default">
                <label>Monto</label>
                <input type="number" class="form-control" id="monto" name="monto" min="0" required>
              </div>
              <div class="form-group form-group-default">
                <label>Fecha</label>
                <input type="date" class="form-control" id="fechaN" name="fecha" required>
              </div>

              <div class="form-group form-group-default">
                <label>Periodo</label>
                <input type="text" class="form-control" id="periodoN" name="periodo" required readonly>
              </div>
              <input type="text" class="form-control" id="id_user" name="id_user" hidden>
            </form>
				  </div>
			  </div>
			  </div>
			  <div class="modal-footer">
				<button type="submit" id="addRowButton" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Agregar</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			  </div>
			</div>
		  </div>
		</div>
    <!-------------------------------MODAL EDITAR---------------------->
		<div class="modal fade modal" id="modalEditar" role="dialog" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header no-bd">
				<h5 class="modal-title">Editar gasto </h5>
				<button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
			  <div class="tab-content" id="myTabContent">
				  <div role="tabpanel" class="tab-pane active">
            <form id="modificar">
              {{csrf_field()}}
              <input type="number" class="form-control" id="conceptoE2" name="concepto" hidden>
              <div class="form-group form-group-default">
                <label>Concepto</label>
                <select id="conceptoE" name="conceptoE" class="form-control js-example-basic-single" style="width: 100%;"name="Id_Ejido" disabled required>

                </select>
              </div>
              <div class="form-group form-group-default">
                <label>Descripción</label>
                <input type="text" class="form-control" id="descripcionE" name="descripcion" required>
              </div>
              <div class="form-group form-group-default">
                <label>Monto</label>
                <input type="number" class="form-control" id="montoE" name="monto" min="0">
              </div>
              <div class="form-group form-group-default">
                <label>Fecha</label>
                <input type="text" class="form-control" id="fechaE" name="fecha" disabled>
              </div>
              <div class="form-group form-group-default">
                <label>Periodo</label>
                <input type="text" class="form-control" id="periodoE" name="periodo" disabled>
              </div>
              <input type="number" class="form-control" id="id_gasto" name="id_gasto" hidden>
            </form>
				  </div>
			  </div>
			  </div>
			  <div class="modal-footer">
				<button type="button" id="modificarc" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Modificar </button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			  </div>
			</div>
		  </div>
		</div>
  <script>
function reload(){
  location.reload();
}

function filtrarFecha(){
  var frm=$("#filtrarFechas");
  var datos = frm.serialize();
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $.ajax({
    type:'POST',
    url:'/filtrarFechasEgresos/',
    data:datos,
    success:function(data){
      if(data!="error"){
        var granTotal = 0;
        document.getElementById("tablaFecha").removeAttribute("hidden");
        document.getElementById("verPeriodos").removeAttribute("hidden");
        document.getElementById("tablaPeriodo").setAttribute("hidden", true);

        var tablaDatos = $("#tablaFechaBody");
        tablaDatos.empty();
          $(data).each(function(key,value){
          tablaDatos.append(
          '<tr class="hide">'+
            '<td class="pt-3-half">'+value.descripcionG+'</td>'+
            '<td class="pt-3-half">'+value.descripcion+'</td>'+
            '<td class="pt-3-half">'+value.fecha+'</td>'+
            '<td class="pt-3-half">'+value.monto+'</td>'+
            '<td>'+
              '<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-success btn-lg" data-original-title="Editar Registro" data-toggle="modal" data-target="#modalEditar" onclick="modalEgreso('+value.Id_gasto_mes+')" title=""><i class="fa fa-edit" ></i></button>'+
              '<button type="button" data-toggle="tooltip" title="Eliminar" class="btn btn-link btn-danger" onclick="eliminarEgreso('+value.Id_gasto_mes+')" data-original-title="Eliminar"><i class="fa fa-times"></i></button>'+
            '</td>'+
          '</tr>');
          granTotal += value.monto;
        });
        $("#tableFecha").dataTable();
        document.getElementById("granTotal").innerHTML="$ "+granTotal

      }
    },
    error:function(x,xs,xt){
      mensaje("Error");
      swal({
        title: "Error",
        text: "Erroe",
        icon: "error",
        buttons: {
          confirm: {
            text: "Cerrar",
            value: true,
            visible: true,
            className: "btn btn-danger",
            closeModal: true
          }
        }
      });
    }
  });


}

/* Formatting function for row details - modify as you need */
function format ( d ) {
  var txt = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"><tr><td><strong>Descripción</strong></td><td><strong>Fecha de registro</strong></td><td><strong>Monto</strong></td><td><strong>Operaciones</strong></td>';
  var txt2 = '';
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "/getGastoMes/"+d.Id_gasto,
    async: false,
    success: function(response){

      $.each(response, function(key, value){
        txt +=
            '<tr>'+
                '<td>'+value.descripcion+'</td>'+
                '<td>'+value.fecha+'</td>'+
                '<td>'+value.monto+'</td>'+
                '<td>'+
                '<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-success btn-lg" data-original-title="Editar Registro" data-toggle="modal" data-target="#modalEditar" onclick="modalEgreso('+value.Id_gasto_mes+')" title=""><i class="fa fa-edit" ></i></button>'+
                '<button type="button" data-toggle="tooltip" title="Eliminar" class="btn btn-link btn-danger" onclick="eliminarEgreso('+value.Id_gasto_mes+')" data-original-title="Eliminar"><i class="fa fa-times"></i></button>'
                +'</td>'+
            '</tr>';
      });
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {

    }
  });
  txt += '</table>'
  return txt;
}

$(document).ready(function() {

$("#fechaN").change(function() {
  //alert($(this).val());
  var str = $(this).val();
  var f = str.split("-");
  var d = new Date(f[0], f[1], f[2]);
  var meses = [ "diciembre", "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre"];
  $("#periodoN").val( meses[ d.getMonth() ]+" - "+f[0]);
  //document.getElementById("periodoN").value = ""+meses[ d.getMonth() ]+" - "+f[0];
  //alert( meses[ d.getMonth()-1 ]+" - "+f[0] )
  //$("#fechaE").val( meses[ d.getMonth()-1 ]+" - "+f[0] );
});

    var table = $('#example').DataTable( {
        "serverSide": true,
        "ajax": "{{ url('getGastos') }}",
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { data: "descripcionG" },
            { data: "monto_total" },
            { data: "periodo" },
            {
              data: "Id_gasto",
              "visible": false,
            },
        ],
        "order": [[1, 'asc']]
    } );

    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );
  </script>
  <script>
  // Funcion para eliminar
    function eliminarEgreso(id){
      swal({
        title: '¿Seguro que desea eliminar el registro?',
        text: "¡Esta accion no es reversible!",
        type: 'warning',
        icon : "error",
        buttons:{
          confirm: {
            text : 'Si, deseo eliminarlo!',
            className : 'btn btn-danger',
          },
          cancel: {
            text : 'Cancelar',
            visible: true,
            className: 'btn btn-warning'
          }
        }
      }).then((Delete) => {
        if (Delete) {
          $.ajax({
          url:'/eliminarEgreso/'+id,
          type:'get'
          }).done(function (res){
          swal({
            title: 'Eliminado!',
            text: 'El registro ha sido Eliminado.',
            icon : "success",
            buttons:false,
            timer:3000,
          });
            location.reload();
            });
        } else {
          swal.close();
        }
      });
    }
  // aqui termina funcion de eliminar


    function modalEgreso(id_gasto_mes){

      $.ajax({
       url:'/obtenerEgreso/'+id_gasto_mes,
       type:'get'
      }).done(function (res){

        $("#id_gasto").val(id_gasto_mes);
        $("#modalEditar").modal('show');
        $('#conceptoE2').val(res.datos[0].Id_Concepto_gasto);
        $('#montoE').val(res.datos[0].monto);
        $('#descripcionE').val(res.datos[0].descripcion);
        /*var str = res.datos[0].created_at;
        var str2 = str.split(" ");
        var f = str2[0].split("-");
        var d = new Date(f[0], f[1], f[2]);
        var meses = [ "enero", "febrero", "marzo", "abril", "mayo", "junio","julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" ];
        */
        $("#fechaE").val( res.datos[0].fecha );
        $("#periodoE").val( res.datos[0].periodo );
        document.getElementById("conceptoE").innerHTML += "<option value='"+res.datos[0].Id_Concepto_gasto+"'selected >"+res.datos[0].descripcionG+"</option>";
        conceptoE.remove(res.datos[0].Id_Concepto_gasto);

     });
    }


    //ajax para registrar un egreso
    $("body").on("click","#addRowButton",function(event){
      event.preventDefault();
      var frm=$("#registro");
      var datos = frm.serialize();
      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      $.ajax({
        type:'POST',
        url:'/agregarEgreso/',
        data:datos,
        success:function(data){
          if(data=="error"){
            mensaje("Este egreso ya existe","warning","top","Registro de Egresos");
          }else{
            $('#addRowModal').modal('hide');
            location.reload();
            mensaje("Registro realizado con exito","success","bottom","Registro de Egresos");
            swal({
              title: "Registro Realizado!",
              text: "",
              icon: "success",
              buttons: {
                confirm: {
                  text: "Cerrar",
                  value: true,
                  visible: true,
                  className: "btn btn-success",
                  closeModal: true
                }
              }
            });
          }
        },
        error:function(x,xs,xt){
          mensaje("Ya ingreso este egreso para este mes","danger","top","Registro de Egresos");
          swal({
            title: "Error",
            text: "Ya ingreso este egreso para este mes",
            icon: "error",
            buttons: {
              confirm: {
                text: "Cerrar",
                value: true,
                visible: true,
                className: "btn btn-danger",
                closeModal: true
              }
            }
          });
        }
      });
    });

  	</script>
    <script>
    //Script para cargar datos del egreso

    //ajax  para modificcar
    $("body").on("click","#modificarc",function(event){
      var frm=$("#modificar");
      var datos = frm.serialize();
      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
        type:'POST',
        url:'/modificarEgreso/',
        data:datos,
        success:function(data){
          location.reload();
          $('#modalEditar').modal('hide');
          swal({
            title: "Modificacion Realizada!",
            text: "",
            icon: "success",
            buttons: {
              confirm: {
                text: "Cerrar",
                value: true,
                visible: true,
                className: "btn btn-success",
                closeModal: true
              }
            }
          });

        },
        error:function(x,xs,xt){
          alert(x.responseText);
        //  mensaje("Favor de llenar los campos de manera Adecuada","danger","top","Registro de Egresos");
        }
      });
   });
  </script>
@endsection
