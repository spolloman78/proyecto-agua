<?php

namespace App\Http\Controllers;
use App\canaleros;
use App\bitacora;
use App\horario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Exports\CanalerosExport;

class Canalero extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('admin');
  }
  public function verCanalero()
  {
    $canalero=canaleros::get();
    return view("Canaleros/canaleros",compact('canalero'));
  }
  public function eliminar($id){
    canaleros::find($id)->delete();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Canaleros";
    $bitacora->Id_movimiento=3;
    $bitacora->save();
  }
  public function calendario(){
    return view("Canaleros.calendario");
  }
public function verHorarios(){
$datos=  horario::join("parcela","parcela.Id_parcela",'=',"horario.Id_parcela")
->join("canalero","canalero.Id_canalero",'=',"parcela.Id_canalero")
->join("factura",'factura.Id_factura','=','horario.Id_factura')
->join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
->select("horario.Id_parcela",'factura_concepto.Id_Concepto','horario.created_at as start','horario.updated_at as end','concepto.tipo','canalero.nombre','factura_concepto.descripcionM')->get();
foreach ($datos as $key ) {
  if($key->tipo=="MAQ"){
    $key->title=" Renta de Maquinaria \n Responsable: ".$key->descripcionM."\n Maquina: ".$key->Id_Concepto;
    $key->className="card-warning  card-stats text-danger";
  }else{
  $key->title=" Riego de parcela\n Canalero Encargado: ".$key->nombre."\n Parcela: ".$key->Id_parcela."\n Servicio de: ".$key->Id_Concepto;
  $key->className="card-success card-stats text-muted";}
}
return json_encode($datos);
}
public function modificarHorario(Request $request){
  $in=$request->inicio;
  $fi=$request->fin;
  $inicio=$in."T00:00";
  $fin=$fi."T23:59";
  $horario=horario::find($request->Id_horario);
  if($horario){
  $horario->created_at=$inicio;
  $horario->updated_at=$fin;
  $horario->save();

  }else{
//$ho=new horario();
//$ho->Id_factura=$request->folio;
}
}
public function obtenerHorario($id){
  $datos= horario::where("Id_factura",'=',$id)->get();
  foreach ($datos as $key) {
  $key->fecha=$key->updated_at->format('Y-m-d');
  $key->fechai=$key->created_at->format('Y-m-d');

  }
  return ($datos=array('datos' => $datos));

}
  public function store(Request $request){
    if($request->ajax()){
      $v = \Validator::make($request->all(), [
          'nombre' => ['required','string'],
          'paterno' => ['required','string'],
          'materno'    => ['required','string'],
          'telefono' => 'required'
      ]);
      if ($v->fails()) {
        echo $v->errors();
        return response()->json(['errors'=>$v->errors()->all()]);
      } else {
        $concepto=DB::table('canalero')
        ->where('nombre','=',$request->nombre)
        ->where('paterno','=',$request->paterno)->count();
        if($concepto>0){
          echo "error";
        }else{
          $canalero=new canaleros();
          $canalero->Id_canalero=0;
          $canalero->nombre=$request->nombre;
          $canalero->paterno=$request->paterno;
          $canalero->materno=$request->materno;
          $canalero->telefono=$request->telefono;
          $canalero->save();
          $usuario=Auth::user()->id;
          $bitacora=new bitacora();
          $bitacora->Id_usuario=$usuario;
          $bitacora->id_bitacora=0;
          $bitacora->modulo="Canaleros";
          $bitacora->Id_movimiento=1;
          $bitacora->save();
        }
      }
    }else{

  }}

  public function actualizar($id){
    $valor=str_replace("P","/" , $id);
    $datos = DB::table('canalero')
    ->where('Id_canalero', '=', $valor)
    ->select('*')
    ->get();
  //  $datos=canaleros::find($valor)->get();
    return ($datos=array('datos' => $datos));
  }

  public function modificar(Request $request){
    if($request->ajax()){
      $v = \Validator::make($request->all(), [
        'nombree' => ['required','string'],
        'paternoe' => ['required','string'],
        'maternoe'    => ['required','string'],
        'telefonoe' => 'required',
      ]);
      if ($v->fails()) {
        echo $v->errors();
        return response()->json(['errores'=>$v->errors()->all()]);
      } else {
        $canalero=canaleros::find($request->id);
        $canalero->nombre=$request->nombree;
        $canalero->paterno=$request->paternoe;
        $canalero->materno=$request->maternoe;
        $canalero->telefono=$request->telefonoe;
        $canalero->save();
        $usuario=Auth::user()->id;
        $bitacora=new bitacora();
        $bitacora->Id_usuario=$usuario;
        $bitacora->id_bitacora=0;
        $bitacora->modulo="Canaleros";
        $bitacora->Id_movimiento=2;
        $bitacora->save();
      }
    }else{

    }
  }
  public function exportarExcel(){
    $export = new CanalerosExport();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Canaleros";
    $bitacora->Id_movimiento=4;
    $bitacora->save();
    return $export->download('canaleros.xlsx');
    //return Excel::download(new UsersExport, 'usuarios.xlsx');
  }

  public function exportarPDF(){
	  $pdf = PDF::loadView('vista');
	  return $pdf ->download('archivo.pdf');
  }

}
