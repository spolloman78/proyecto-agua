@extends('layouts.layout')

@section('content')
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="main-panel">

    <div class="content">

      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Historico <i class="flaticon-list"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Historico</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-sm-6 col-md-4 md-auto">
            <div class="card card-stats card-round">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5">
                    <div class="icon-big text-center">
                      <i class="fas fa-cart-plus fa-2x text-primary"></i>
                    </div>
                  </div>
                  <div class="col-7 col-stats">
                    <div class="numbers">
                      <p class="card-category">Ingresos Totales</p>
                      <h4 class="card-title" id="totald">
                    $  0</h4>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="card-title"  style="padding: 1% 0 0 0;">Historico<i class="fas fa-hat-cowboy"></i></h4>
                  </div>
                    <form id="datosFil" action="{{route('exportarExcelHistorico')}}" method="post">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-right">
                      <button class="btn btn btn-round" type="submit"  style="  background-color:#00A631; color:white;"  >
                        <i class="fas fa-file-excel fa-2x"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">

                    {{csrf_field()}}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group form-group-default">
                            <label for="num_factura">Desde:</label>
                            <input type=date class="form-control form-control-sm" id="inicio" name="inicio" min="2018-01-01" value="2020-01-06">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-default">
                            <label for="num_factura">Hasta:</label>
                            <input type=date class="form-control form-control-sm" id="fin" name="fin" value="2020-05-06">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-default">
                            <label for="num_factura">Ciclo Agrario:</label>
                            <select  class="form-control form-control-sm" id="ciclo" name="ciclo" >
                            <option value="0">Todos</option>
                            @foreach($ciclo as $row)
                            <option value="{{$row->Id_ciclo}}">{{$row->Ciclo}}</option>
                            @endforeach
                            </select>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label for="num_factura">Tipo</label>
                                <select  class="form-control form-control-sm" name="tipo" id="tipo">
                                  <option value="0">Todos</option>
                                  @foreach($tipo as $row)
                                  <option value="{{$row->tipo}}">{{$row->tipo}}</option>
                                  @endforeach
                                </select>
                            </div>
                              </div>

                     </form>

              </div>
                <div class="table-responsive">

                  <table id="example" class="display table table-striped table-hover" style="width:100%">
                    <thead>
                        <tr>
                          <th>Id_factura</th>
                          <th>Fecha</th>
                          <th>Tarjeta</th>
                          <th>Usuario</th>
                          <th>Ciclo</th>
                          <th>Cuenta</th>
                          <th>SEC</th>
                          <th>Eji.</th>
                          <th>Nombre del Ejido</th>
                          <th>Municipio</th>
                          <th>Cultivo</th>
                          <th>Superficie fisica</th>
                          <th>Concepto</th>
                          <th>Tipo</th>
                          <th>Cantidad</th>
                          <th>Importe</th>
                          <th>Status</th>
                        </tr>
                    </thead>
                  </table>

                </div>

                </div>
              </div>
            </div>
          </div>
        </div>


<script>
$(document).ready(function() {
      });
function cambio(){
  var frm=$("#datosFil");
  var datos = frm.serialize();
  var dat=JSON.stringify({inicio: $('#inicio').val(),fin: $('#fin').val(), ciclo: $('#ciclo').val()  });
  var d;
   $("#example").dataTable().fnDestroy();
        var table = $('#example').DataTable( {
          'processing': true,
           "serverSide": true,
           "bDestroy": true,
          "ajax": {
       url: '/getHistorico',
       type: "POST",
     data: {"_token": "{{ csrf_token() }}",'inicio':$("#inicio").val(),'fin':$('#fin').val(),"ciclo": $('#ciclo').val(),"tipo":$('#tipo').val()}
   },            eys: true,
                sort: true,
                searching: true,
                select: true,
                ordering: true,
                bJQueryUI: true,
                sPaginationType: "full_numbers",
                displayStart: 0,
                stateSave: true,
                autoWidth: false,
                paging: true,
                fixedColumns: false,
                columnReorder: true,
            "columns": [
                { data: "Id_factura" },
                { data: "fecha" },
                { data: "tarjeta" },
                { data: "nombre"},
          //      { data: "apPaterno" },
            //    { data: "apMaterno" },
                { data: "ciclo" },
                { data: "cuenta" },
                { data: "SEC" },
                { data: "Id_Ejido" },
                { data: "ejidos" },
                { data: "descripcion" },
                { data: "cultivo" },
                { data: "sup_cultivo" },
                { data: "descripcion_con" },
                { data: "tipo" },
                { data: "sup_Fis" },
                { data: "Total" },
                { data: "status_fact" },
            ],
            "order": [[1, 'asc']]
        } );

        // Add event listener for opening and closing details
        $('#example tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );
}
function totales(){
  $.ajax({
  url:'/gettotal/',
  type:'Post',
  data: {"_token": "{{ csrf_token() }}",'inicio':$("#inicio").val(),'fin':$('#fin').val(), ciclo: $('#ciclo').val(),"tipo":$('#tipo').val()}
  }).done(function (cultivo){
    if(cultivo.datos[0].total==null) { document.getElementById('totald').innerHTML="$0.00";
  }else{
    document.getElementById('totald').innerHTML="$ "+cultivo.datos[0].total;}
  });
}
$("#inicio").change(function() {
document.getElementById("ciclo").options.item(0).selected = 'selected';
document.getElementById("tipo").options.item(0).selected = 'selected';
cambio();
totales();

});
$("#fin").change(function() {
document.getElementById("ciclo").options.item(0).selected = 'selected';
document.getElementById("tipo").options.item(0).selected = 'selected';
cambio();
totales();
});
$("#ciclo").change(function() {
document.getElementById("tipo").options.item(0).selected = 'selected';
cambio();
totales();

});
$("#tipo").change(function() {
document.getElementById("ciclo").options.item(0).selected = 'selected';
cambio();
totales();

});
$("body").on("click","#exportar",function(event){
  event.preventDefault();
  var frm=$("#datosFil");
  var datos = frm.serialize(); alert(datos);
  $.ajaxSetup({
       headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });
   $.ajax({
    type:'POST',
    url:"/exportarExcelHistorico/",
    data:datos,
  success:function(data){
  },error:function(x,d,o){
    alert(x.responseText);
  }
});
});

</script>



          @endsection
