<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCanalero extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('canalero', function (Blueprint $table) {
            $table->increments("Id_canalero");
            $table->string("nombre");
            $table->string("paterno");
            $table->string("materno");
            $table->string("telefono");
            $table->softDeletes(); //Nueva línea, para el borrado lógico
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('canalero');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
