<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConcepto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('concepto', function (Blueprint $table) {
      $table->string("Id_Concepto");
      $table->primary("Id_Concepto");
      $table->string("descripcion_con");
      $table->float("precio");
      $table->string("tipo");
      $table->string("unidad");
      $table->Integer("no_riesgos");
      $table->softDeletes(); //Nueva línea, para el borrado lógico

  });
}

/**
* Reverse the migrations.
*
* @return void
*/
public function down()
{
DB::statement('SET FOREIGN_KEY_CHECKS = 0');
  Schema::dropIfExists('concepto');
  DB::statement('SET FOREIGN_KEY_CHECKS = 1');
}
}
