<?php

namespace App\Http\Controllers;
use App\bitacora;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Bitacoras extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('admin');
  }

  public function verBitacora(){
    $now = now()->format('Y-m-d');
    $anio = now()->format('Y');
  $bitacora=bitacora::join("users",'users.id','=','bitacora.Id_usuario')
  ->join("tipo_movimiento",'tipo_movimiento.Id_movimiento','=','bitacora.Id_movimiento')->get();
  return view("Bitacora/bitacora",compact('bitacora','now','anio'));
}
public function fechas(Request $request){
  $output="";
  $inicio=$request->inicio ."T00:00";
  $fin=$request->fin."T23:59";
  $bitacora=bitacora::join("users",'users.id','=','bitacora.Id_usuario')
  ->join("tipo_movimiento",'tipo_movimiento.Id_movimiento','=','bitacora.Id_movimiento')->whereBetween('created_at', array($inicio, $fin))->get();
  if($bitacora)
  {
    foreach ($bitacora as $key => $product) {
     $output.='<tr>'.
    '<td>'.$product->id_bitacora.'</td>'.
    '<td>'.$product->name." ".$product->apPaterno." ".$product->apMaterno.'</td>'.
    '<td>'.$product->descripcion.'</td>'.
    '<td>'.$product->modulo.'</td>'.
    '<td>'.$product->created_at.'</td>'.
    '</tr>';
    }
    return Response($output);
  }
}
}
