@extends('layouts.layout')

@section('content')

<div class="main-panel">

    <div class="content">

      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Ejidos <i class="icon-map"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catalogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Ejidos</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="card-title"  style="padding: 1% 0 0 0;">Catalogo de Ejidos<i class="fas fa-hat-cowboy"></i></h4>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-flex align-items-center">
                      <button class="btn btn btn-round ml-auto"  style="background-color:#00A631; color:white;" data-toggle="modal" data-target="addRowEjido" onclick="crear()">
                        <i class="fa fa-plus"></i>
                        Ejido
                      </button>
                      <a class="btn btn btn-round"  style=" margin-left:20px; background-color:#00A631; color:white;" href="{{'exportarExcelEjidos'}}">
                        <i class="fas fa-file-excel fa-2x"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                        <th># <i class="icon-map"></i></th>
                        <th>Nombre del Ejido</th>
                        <th>Operaciones</th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($ejido as $row)
                      <tr>
                        <td>{!!$row->Id_Ejido!!}</td>
                        <td>{!!$row->nombre_ej!!}</td>

                        <td>
                          <div class="form-button-action">
                              <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-success btn-lg" data-original-title="Editar Registro"data-toggle="modal" data-target="#editarej" onclick="modalEjido('{!!$row ->Id_Ejido!!}')" title="">
                              <i class="fa fa-edit"></i>
                            </button>
                              <button type="button" data-toggle="tooltip" title="Eliminar" class="btn btn-link btn-danger" onclick="eliminar('{!!$row ->Id_Ejido!!}')"data-original-title="Eliminar">
                              <i class="fa fa-times"></i>
                            </button>

                            </div>
                        </td>
                      </tr>

    @endforeach
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
          </div>



            </div>
          <div class="card-body">
            <!-- Modal -->
            <div class="modal fade " id="addRowEjido" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog " role="document">
                <div class="modal-content">

                  <div class="modal-header no-bd ">
                    <h5 class="modal-title" >
                      <span class="fw-mediumbold">
                      Nuevo</span>
                      <span class="fw-light">
                        Ejido   <i class="icon-map 2x" style="color:green; "></i>
                      </span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body ">

                    <p class="small">En este panel usted podrá agregar nuevos ejidos en el sistema.</p>
                    <form id="regej"action="{{route('Aejido')}}" >
                        {{csrf_field()}}
                        <p class="text-danger" id="textodividir" style="margin-left:10px;"></p>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default" id="eji">
                            <label>Nombre del ejido</label>
                            <input id="addName"  name="nombre_ej"type="text" class="form-control" placeholder="Ejido" required>
                          </div>
                        </div>
                      </div>

                  </div>
                  <div class="modal-footer no-bd">
                    <button type="button" id="addEjido" class="btn" style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
  </form>
                  </div>
                </div>
              </div>
            </div>
<!---MODAL PARA EDITAR-->
<div class="modal fade " id="editarej" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">

      <div class="modal-header no-bd ">
        <h5 class="modal-title" >
          <span class="fw-mediumbold">
          Editar </span>
          <span class="fw-light">
            Ejido  <i class="fas fa-tree 2x" style="color:green; "></i>
          </span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
        <p class="small">En este panel usted podrá editar datos de los ejidos en el sistema.</p>
        <form id="editarejido"action="{{route('actualizarejido')}}" >
            {{csrf_field()}}
          <div class="row">
            <input id="id" type="number" name="id" class="form-control" placeholder="0" min="1" max="100" hidden="">
            <div class="col-sm-12">
              <div class="form-group form-group-default">
                <label>Nombre del ejido</label>
                <input id="nombre_ej"  name="nombre_ej"type="text" class="form-control" placeholder="Nombre" required>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer no-bd">
        <button type="button" id="ejidoed" class="btn"  style="background-color:#00A631; color:white;">  <i class="fa fa-plus"></i> Modificar </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
</form>
      </div>
    </div>
  </div>
</div>




    <script>
    function crear(){
      $('#addRowEjido').modal('show');
    }
      // Funcion para eliminar
            function eliminar(id){
            swal({
              title: '¿Seguro que desea eliminar el registro?',
              text: "¡Esta accion no es reversible!",
              type: 'warning',
              icon : "error",
              buttons:{
                confirm: {
                  text : 'Si, deseo eliminarlo!',
                  className : 'btn btn-danger',
                },
                cancel: {
                  text : 'Cancelar',
                  visible: true,
                  className: 'btn btn-warning'
                }
              }
            }).then((Delete) => {
              if (Delete) {
                $.ajax({
                url:'/BorrarEjidos/'+id,
                type:'get'
                }).done(function (res){
                swal({
                  title: 'Eliminado!',
                  text: 'El registro ha sido Eliminado.',
                  icon : "success",
                  buttons:false,
                  timer:3000,
                });
                  location.reload();
                  });
              } else {
                swal.close();
              }
            });
          }
       // aqui termina funcion de eliminar

       function modalEjido(id){

     $.ajax({
       url:'/EjidoDatos/'+id,
       type:'get'
     }).done(function (res){
       $("#id").val(id);
       $("#editarej").modal('show');
       $('#nombre_ej').val(res.datos[0].nombre_ej);
     });
     }


    $("body").on("click","#addEjido",function(event){
      event.preventDefault();
      var frm=$("#regej");
    var datos = frm.serialize();
      $.ajaxSetup({
           headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });
       $.ajax({
        type:'POST',
        url:'/AgregarEjido/',
        data:datos,
      success:function(data){
    if(data=="error"){
      mensaje("Este Ejido ya existe","warning","top","Registro de Cultivos");
      document.getElementById('textodividir').innerHTML='Este Ejido ya existe';
    }else{
      location.reload();
      swal({
        title: 'Agregado',
        text: 'El registro ha sido agregado con exito.',
        icon : "success",
        buttons:false,
        timer:3000,
      });
        $('#addRowEjido').modal('hide');}
            },
            error:function(x,xs,xt){      
              var xd=x.responseText.split("}");
              var obj2=JSON.parse(xd[0]+'}');
              var obj = JSON.parse(xd[1]+'}');
              var xd=obj.errores+"";
              var atributos = "";
             for(var aux in obj2){
             $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');
                       }
              document.getElementById('textodividir').innerHTML='Favor de llenar los campos de manera adecuada';}
    });
    });

    </script>
      <script>

 $("body").on("click","#ejidoed",function(event){
   var frm=$("#editarejido");
   var datos = frm.serialize();
   $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
     type:'POST',
     url:'/ModificarEjido/',
     data:datos,
   success:function(data){
location.reload();
         $('#editarej').modal('hide');

     swal({
         title: 'Modificación Realizada',
         text: 'El registro se ha modificado.',
         icon : "success",
         buttons:false,
         timer:1000,
       });
         },error:function(x,xs,xt){
           var xd=x.responseText.split("}");
           var obj2=JSON.parse(xd[0]+'}');
           var obj = JSON.parse(xd[1]+'}');
           var xd=obj.errores+"";
           var atributos = "";
           for(var aux in obj2){
           $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');
                    }
         }
   });
   });


   </script>
          @endsection
