<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEjidatario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ejidatario', function (Blueprint $table) {
//$table->String("Id_ejidatario",25);
//$table->primary("Id_ejidatario");
$table->Increments("Id_ejidatario");
$table->string('nombre');
$table->string('apPaterno',20);
$table->string('apMaterno',20);
//$table->string("telefono",20);
$table->string("tarjeta",40);
$table->string("tarjeta_ant",40);
$table->softDeletes(); //Nueva línea, para el borrado lógico
    //  $table->timestamps();
  });
}

/**
* Reverse the migrations.
*
* @return void
*/
public function down()
{DB::statement('SET FOREIGN_KEY_CHECKS = 0');
  Schema::dropIfExists('ejidatario');
  DB::statement('SET FOREIGN_KEY_CHECKS = 1');
}
}
