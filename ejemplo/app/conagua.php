<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class conagua extends Model
{
    protected $table = 'conagua';
    public $timestamps = false;
    public  $primaryKey = 'Id_Conagua';
    use SoftDeletes; //Implementamos
    protected $dates = ['deleted_at'];
}
