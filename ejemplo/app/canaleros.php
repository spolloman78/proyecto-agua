<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class canaleros extends Model
{
   protected $table = 'canalero';
   use SoftDeletes; //Implementamos
   protected $dates = ['deleted_at'];
   public $timestamps = false;
   public  $primaryKey = 'Id_canalero';
}
