@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Captura</h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Caja</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Captura</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Datos de la factura<i class="fas fa-hat-cowboy"></i></h4>
              </div>
              <div class="card-body">
                <div class="form-row align-items-center">

                    <div class="col-md-2">
                        <div class="form-group form-group-default">
                            <label for="num_factura"># Factura</label>
                            <input class="form-control form-control-sm" id="num_factura" placeholder="" value=F{{$noFacturas+1}} disabled>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group form-group-default">
                          <label for="ejidatarioN">Nombre</label>
                               <select class="form-control js-example-basic-single dynamic" name="ejidatarioN" id="ejidatarioN">
                                 @foreach($ejidatario as $ejidatarios)
                                   <?php $valor = ($ejidatarios->nombre).($ejidatarios->apPaterno)." ".($ejidatarios->apMaterno)?>
                                   <option value="{{$ejidatarios->Id_ejidatario}}">
                                   {{$valor}}
                                   </option>
                       @endforeach
                     </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group form-group-default">
                          <label for="tarjetaN">Tarjeta</label>
                            <select class="form-control js-example-basic-single dynamic" name="tarjetaN" id="tarjetaN">
                                @foreach ($ejidatario as $ejidatarios)
                                <option>
                                    {{$ejidatarios->tarjeta}}
                                </option>
                            @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <div class="form-group form-group-default">
                            <label for="no">No</label>
                            <input type="number" class="form-control form-control-sm" id="no" placeholder="" disabled>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <div class="form-group form-group-default">
                            <label for="superficio">Superficie</label>
                            <input type="number" class="form-control form-control-sm" id="superficie" placeholder="" disabled>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group form-group-default">
                            <label for="fecha">Fecha</label>
                            <input id="date"  class="form-control" placeholder="" id="fecha" value={{$now}} placeholder=".form-control-sm" disabled>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group form-group-default">
                        <button type="button" name= "btnMaq" id="btnMaq" class="btn btn-success" data-target="#modalMaq" onclick="modalMaq()" disabled><i class="fas fa-truck-moving"></i></button>
                        </div>

                    </div>



                </div>
                  <!--Tabla dinámica de parcelas ejecutada despues de seleccionar un ejidatario o tarjeta-->
                <div class="row">
                  <div class="col-md-12">
                      <div class="card">
                     <div class="card-header">
                <h4 class="card-title" id="parcela">Parcelas de: </h4>
              </div>

                    <table class="display table table-responsive table-striped" name="parcelasE" id="parcelasE" align="center" margin="auto">
                      <thead>
                        <tr>
                          <th>Identificador</th>
                          <th>Cuenta</th>
                          <th>Sec</th>
                          <th>Ejido</th>
                          <th>Municipio</th>
                          <th>Sup.Ha</th>
                          <th>Operación</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
                </div>







                <div class="row">
                  <div class="col-md-12">
                      <div class="card">
                          <div class="card-header">
                              <h4 class="card-title" id="parcela">Servicio de riego por parcela </h4>
                          </div>
                    <div class="table-responsive">
                      <table class="display table table-striped" name="conceptosP" id="conceptosP" align="center" margin="auto" >
                        <thead>
                          <tr class="encabezado">
                            <th>Identificador</th>
                            <th>Concepto</th>
                            <th>Tipo</th>
                            <th>Ciclo Año</th>
                            <th>Unidad</th>
                            <th>Cultivo</th>
                            <th>Superficie</th>
                            <th>Cantidad</th>
                            <th>Cuota</th>
                            <th>Importe</th>
                            <th>Operaciones</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                      </div>
                  </div>
                </div>

                  <!--Tabla de cooperaciones-->
                 <div class="row">
                  <div class="col-md-8">
                      <div class="card">
                          <div class="card-header">
                              <h4 class="card-title" id="parcela">Resumen: </h4>
                          </div>
                    <div class="table-responsive">
                      <table class="display table table-striped" name="tablaResumen" id="tablaResumen" align="center" margin="auto" >
                        <thead>
                          <tr class="encabezado">
                            <th>Concepto</th>
                            <th>Cantidad</th>
                            <th>Importe</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                      </div>



                  </div>

                        <div class="card" id="resumen"  style="width: 24rem;">
                     <div class="card-header">
                       <form id="pagaradeudo">
                         {{csrf_field()}}
                <h4 class="card-title" id="parcela">Resumen:</h4>
              </div>
              <div class="row">
              <div class="col-md-11">
                <div class="form-group form-group-default">
                            <label for="servRiego">Servicio de riego: </label>
                            <input class="form-control form-control-sm" id="riegoFact"name="servicio" disabled>
               </div>
             </div>
             <div class="col-md-11">
               <div class="form-group form-group-default">
                            <label for="cooperaciones">Cooperaciones: </label>
                            <input class="form-control form-control-sm" id="coopFact" name="cooperaciones"disabled>
                          </div>
                          </div>
                            <div class="col-md-11">
                              <div class="form-group form-group-default">
                            <label for="maquinaria">Maquinaria: </label>
                            <input class="form-control form-control-sm" id="maqFact" disabled>
                          </div>
                          </div>
                            <input  id="Id_factura" value="F{{$noFacturas+1}}" hidden name="id_factura"/>
                            <div class="col-md-11">
                              <div class="form-group form-group-default">
                            <label for="total">Total: </label>
                            <input class="form-control form-control-sm" type="number" id="totalFact" name="totalFact" disabled/>
                               <input class="form-control form-control-sm" type="number" id="deuda" name="deuda" hidden/>
                          </div>
                          </div>
                            <div class="col-md-11">
                              <div class="form-group form-group-default">
                            <label for="paga">Paga: </label>
                            <input class="form-control form-control-sm" id="pagaFact" name="cantidad" placeholder="Cantidad pagada" type="number">
                          </div>
                          </div>
                    </div>
                  </div>
                </div>

                  <div class="row">
                  <div class="col-lg-12">
                    <style type="text/css">
                      #btnGuardar{
                        margin-top: 8px;
                        margin-left: 12px;
                        float: right;
                      }
                      #btnCancelar{
                        margin-top: 8px;
                        margin-right: 12px;
                        float: right;
                      }
                    </style>
                  </form>
                        <button type="button" id="btnGuardar" onclick="terminada()" class="btn btn-success btn-lg">Guardar</button>
                        <button type="button" id="btnCancelar" onclick="cancelar()"class="btn btn-danger btn-lg">Cancelar</button>

                  </div>
                </div>



              </div>
            </div>
          </div>





          <!-- Modal concepto de pago de riego-->
          <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Operaciones de parcela</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                      <div class="col-md-5">
                          <div class="form-group form-group-default">
                              <label for="selectCon">Concepto</label>
                              <select class="form-control js-example-basic-single dynamic" name="selectCon" id="selectCon2">
                                @foreach ($concepto as $conceptos)
                                <option>
                                    {{$conceptos->concepto}}
                                </option>
                            @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="col-md-1">
                          <div class="form-group form-group-default">
                              <label for="tipoCon">Tipo</label>
                              <input type="text" class="form-control form-control-sm" id="tipoCon" name="tipoCon" placeholder="">
                          </div>
                      </div>
                      <div class="col-md-1">
                          <div class="form-group form-group-default">
                              <label for="canio">Ciclo Año</label>
                              <select class="js-example-basic-single dynamic" name="selectCiclo" id="selectCiclo2">
                                   @foreach ($ciclo as $conceptos)
                                <option>
                                    {{$conceptos->ciclo}}
                                </option>
                            @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="col-md-1">
                          <div class="form-group form-group-default">
                              <label for="unidad">Unidad</label>
                              <input class="form-control form-control-sm" id="unidad" name="unidad" placeholder="" min="1" value="0" >
                          </div>
                      </div>
                      <div class="col-md-1">
                          <div class="form-group form-group-default">
                              <label for="cuotaCon">Cuota</label>
                              <input type="number" class="form-control form-control-sm" id="cuotaCon" name="cuotaCon" placeholder="">
                          </div>
                      </div>
                      <div class="col-md-1">
                          <div class="form-group form-group-default">
                              <label for="riegos">Riegos</label>
                              <input type="number" class="form-control form-control-sm" id="riegos" placeholder="">
                          </div>
                      </div>
                      <div class="col-md-1">
                          <div class="form-group form-group-default">
                              <label for="cantidad">Cantidad</label>
                              <input type="number" class="form-control form-control-sm" id="cantidad" placeholder="">
                          </div>
                      </div>
                      <div class="col-md-1">
                          <div class="form-group form-group-default">
                              <label for="cuota">Importe</label>
                              <input type="number" class="form-control form-control-sm" id="importe" placeholder="">
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="form-group form-group-default">
                              <label for="cultivo">Cultivo</label>
                              <select class="js-example-basic-single dynamic" name="cultivo" id="cultivo">
                            @foreach ($cultivo as $cultivos)
                                <option>
                                    {{$cultivos->descripcion}}
                                </option>
                            @endforeach
                              </select>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal_grande">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLabel">Operaciones para parcela </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-4">
                      <h4>Concepto de pago parcela</h4>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-md-5">
                          <div class="form-group form-group-default">
                              <label for="selectCon">Concepto</label>
                              <select class="js-example-basic-single dynamic" style="width:100%;" name="selectCon" id="selectCon">
                                   @foreach ($concepto as $conceptos)
                                <option>
                                    {{$conceptos->concepto}}
                                </option>
                            @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="tipoCon">Tipo</label>
                              <input type="text" class="form-control form-control-sm" id="tipoCon" name="tipoCon" placeholder="" disabled>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="canio">Ciclo Año</label>
                              <select class="js-example-basic-single dynamic"  style="width:100%;"  name="selectCiclo" id="selectCiclo">
                                   @foreach ($ciclo as $conceptos)
                                <option>
                                    {{$conceptos->ciclo}}
                                </option>
                            @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="unidad">Unidad</label>
                              <input class="form-control form-control-sm" id="unidad" name="unidad"placeholder="" disabled>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="cuotaCon">Cuota</label>
                              <input type="number" class="form-control form-control-sm" id="cuotaCon" name="cuotaCon"  placeholder="" disabled>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="riegos" id="labelRiegos">Cantidad/Riegos</label>
                              <input type="number" class="form-control form-control-sm" id="riegosCon" min="1" value="0" required disabled>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="cantidad">Superficie</label>
                              <input class="form-control form-control-sm" id="cantidadCon" placeholder="" disabled>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="cuota">Importe</label>
                              <input type="number" class="form-control form-control-sm" id="importeCon" placeholder="" disabled>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group form-group-default">
                              <label for="cultivo">Cultivo</label>
                              <select class="js-example-basic-single dynamic"style="width:100%;" name="cultivo" id="cultivoCon">
                            @foreach ($cultivo as $cultivos)
                                <option value="{{$cultivos->Id_cultivo}}">
                                    {{$cultivos->descripcion}}
                                </option>
                            @endforeach
                              </select>
                          </div>
                      </div>
                  </div>

                </div>
                <form id="factura" Type="post" action="{{route('borrarxd')}}">
                  {{csrf_field()}}
                  <input id="cantidadcultibo" name="cantidadc" type="number" hidden/>
                <input id="folio" name="folio" hidden type="text"/>
                <input id="ejidatario" name="ejidatario" hidden type="number"/>
                <input id="concepto" name="concepto" hidden type="text"/>
                <input id="cultivo2" name="cultivo" hidden type="number"/>
                <!--Aquí el input es parcelas carnal-->
                <input id="parcelas" name="parcela" hidden type="number"/>
                <input id="total" name="total" hidden type="number"/>
                <input id="sup_cultivo" name="sup_cultivo" hidden type="number"/>
                <input id="descripcionMaq" name="descripcionMaq" hidden type="text"/>
                <input id="tipo" name="tipo" hidden type="text"/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                  <button type="button" class="btn btn-success" onclick="agregar()" id="btnAdd" disabled>Añadir</button>
                </div>
              </div>
  </form>
            </div>
          </div>







            <!-- Modal para conceptos de renta de maquinaria-->
          <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalMaq">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLabel">Renta de maquinaria</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                      <div class="col-md-5">
                          <div class="form-group form-group-default">
                              <label for="selectCon">Concepto</label>
                              <select class="js-example-basic-single dynamic" name="selectConMaq" id="selectConMaq">
                                   @foreach ($conceptoMaq as $conceptoMaq)
                                <option>
                                    {{$conceptoMaq->conceptoMaq}}
                                </option>
                            @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="tipoCon">Tipo</label>
                              <input type="text" class="form-control form-control-sm" id="tipoConMaq" name="tipoConMaq" placeholder="" disabled>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="canio">Ciclo Año</label>
                              <select class="js-example-basic-single dynamic"  style="width:100%;"  name="selectCicloMaq" id="selectCicloMaq">
                                   @foreach ($ciclo as $conceptos)
                                <option>
                                    {{$conceptos->ciclo}}
                                </option>
                            @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="unidad">Unidad</label>
                              <input class="form-control form-control-sm" id="unidadConMaq" name="unidadConMaq" placeholder="" disabled>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="cuotaCon">Cuota</label>
                              <input type="number" class="form-control form-control-sm" id="cuotaConMaq" name="cuotaConMaq"  placeholder="" disabled>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="cantidadMaq">Cantidad</label>
                              <input type="number" class="form-control form-control-sm" id="cantidadConMaq" min="1" value="0" required disabled>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group form-group-default">
                              <label for="cuota">Importe</label>
                              <input type="number" class="form-control form-control-sm" id="importeConMaq" placeholder="" disabled>
                          </div>
                      </div>

                      <div class="col-md-5">
                          <div class="form-group form-group-default">
                              <label for="encargadoMC">Encargado</label>
                              <input type="text" class="form-control form-control-sm" id="encargadoConMaq" name="encargadoConMaq" placeholder="" disabled>
                          </div>
                      </div>

                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                  <button type="button" class="btn btn-success" onclick="agregarMaq()" id="btnAddMaq" disabled>Añadir</button>
                </div>
              </div>
            </div>
          </div>



          <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />



            <script>
            var b=true;
                //Script para llenado de información al seleccionar el nombre del ejidatario
                $(document).ready(function() {

                $('input[id="totalFact"]').val("");
                $('input[id="deuda"]').val("");
                $('input[id="pagaFact"]').val("");
                $('input[id="riegoFact"]').val(0);
                $('input[id="coopFact"]').val(0);
                $('input[id="maqFact"]').val(0);

                $('#btnMaq').attr('disabled', 'disabled');
                $('#encargadoConMaq').attr('disabled', 'disabled');

                var selecte = document.getElementById("ejidatarioN");
                $('select[name="ejidatarioN"]').on('change', function(){
                var valor = $(this).val();
                var respusta=selecte.options[valor-1].text;
                 $('#ejidatario').val(valor);
                 if(valor) {$.ajax({
                         url: '/par/'+valor,
                         type:"GET",
                         dataType:"json",
                         success:function(data) {

                             $('input[name="tipoCon"]').empty();
                             $('input[name="cuotaCon"]').empty();
                             $('input[name="unidad"]').empty();

                             $('#btnMaq').removeAttr('disabled');
                             $('#btnCoop').removeAttr('disabled');
                             document.getElementById('parcela').innerHTML='Parcelas de: '+respusta;
                             $('select[name="tarjetaN"]').empty();

                             var idEjidatario = valor;

                             $("#btnMaq").val(idEjidatario);
                             $('select[name="tarjetaN"]').append('<option>'+data[0].tarjeta+'</option>');

                             $('select[name="tarjetaN"]').attr('disabled', 'disabled');

                             $('table[name="parcelasE"]').empty();
                             $('table[name="parcelasE"]').append('<thead><tr><th>Identificador</th><th>Cuenta</th><th>Sec</th><th>Ejido</th><th>Municipio</th><th>Sup.Ha</th><th>Deudor</th><th>Operación</th></tr></thead>');

                             $superficie = 0;                             data.forEach(miFuncion);
                             function miFuncion(elemento, indice) {

                            $parcelaC =  data[indice].idParcela;

                        $('table[name="parcelasE"]').append('<tr><td>'+$parcelaC+'</td><td>'+data[indice].cuenta+'</td><td>'+data[indice].seccion+'</td>      <td>'+data[indice].nombreEj+'</td><td>'+data[indice].municipio+'</td><td>'+data[indice].supF+'</td><td><i id="icono '+indice+'" class="fas fa-money-bill-wave fa-2x "></i>'+'</td><td><button type="button" class="btn btn-success" data-target="#modal_grande" onclick="modalPago('+$parcelaC+','+data[indice].supF+')"><i class="fas fa-plus"></i></button></td></tr>');
                        $.ajax({
                                url: '/deudorFact/'+$parcelaC,
                                type:"GET",
                                dataType:"json",
                                success:function(data){

                                  if(data.estado==3){

                                var a=document.getElementById("icono " +indice);
                                 a.style.color="red";
                                mensaje("La parcela  "+$parcelaC+"   presenta un adeudo ","danger","top","Captura de Factura");


                                  }else{
                                  var a=document.getElementById("icono "+indice);
                                  a.style.color="green";

                                   }
                                },error:function(x,xd,xdd){
                                  alert(x.responseText);
                                }});

                            $('input[id="no"]').val(indice+1);
                            $superficie +=  data[indice].supF;

                            $('input[id="superficie"]').val($superficie);

                             }

                         },
                         complete: function(){
                             $('#loader').css("visibility", "hidden");
                         }
                     });


                 }

             });

                 //Función para obtener datos de la parcela deacuerdo a la matrícula del ejidatario
                 var selectTar = document.getElementById("tarjetaN");
                $('select[name="tarjetaN"]').on('change', function(){
                var valor = $(this).val();
                 if(valor) {$.ajax({
                         url: '/mat/'+valor,
                         type:"GET",
                         dataType:"json",
                         beforeSend: function(){
                             $('#loader').css("visibility", "visible");
                         },

                         success:function(data) {
                             $('input[name="tipoCon"]').empty();
                             $('input[name="cuotaCon"]').empty();
                             $('input[name="unidad"]').empty();

                             $('#btnMaq').removeAttr('disabled');
                             $('#btnCoop').removeAttr('disabled');
                             document.getElementById('parcela').innerHTML='Parcelas de: '+ data[0].nombre+" "+data[0].apPaterno+" "+data[0].apMaterno;

                             var idEjidatario = data[0].Id_ejidatario;

                             $("#btnMaq").val(idEjidatario);
                             $('select[name="ejidatarioN"]').empty();
                             $('select[name="ejidatarioN"]').append('<option>'+data[0].nombre+" "+data[0].apPaterno+" "+data[0].apMaterno+'</option>');
                             $('select[name="ejidatarioN"]').attr('disabled', 'disabled');

                             $('table[name="parcelasE"]').empty();
                             $('table[name="parcelasE"]').append('<thead><tr><th>Identificador</th><th>Cuenta</th><th>Sec</th><th>Ejido</th><th>Municipio</th><th>Sup.Ha</th><th>Operación</th></tr></thead>');

                             $superficie = 0;                             data.forEach(miFuncion);
                             function miFuncion(elemento, indice) {

                                 $parcelaC =  data[indice].idParcela;

                          //  $('table[name="parcelasE"]').append('<tr><td>'+$parcelaC+'</td><td>'+data[indice].cuenta+'</td><td>'+data[indice].seccion+'</td>      <td>'+data[indice].nombreEj+'</td><td>'+data[indice].municipio+'</td><td>'+data[indice].supF+'</td><td><button type="button" class="btn btn-success" data-target="#modal_grande" onclick="modalPago('+$parcelaC+','+data[indice].supF+')"><i class="fas fa-plus"></i></button></td></tr>');
                            $('table[name="parcelasE"]').append('<tr><td>'+$parcelaC+'</td><td>'+data[indice].cuenta+'</td><td>'+data[indice].seccion+'</td>      <td>'+data[indice].nombreEj+'</td><td>'+data[indice].municipio+'</td><td>'+data[indice].supF+'</td><td><i id="icono '+indice+'" class="fas fa-money-bill-wave fa-2x "></i>'+'</td><td><button type="button" class="btn btn-success" data-target="#modal_grande" onclick="modalPago('+$parcelaC+','+data[indice].supF+')"><i class="fas fa-plus"></i></button></td></tr>');
                            $.ajax({
                                    url: '/deudorFact/'+$parcelaC,
                                    type:"GET",
                                    dataType:"json",
                                    success:function(data){

                                      if(data.estado==3){

                                    var a=document.getElementById("icono " +indice);
                                     a.style.color="red";
                                    mensaje("La parcela  "+$parcelaC+"   presenta un adeudo ","danger","top","Captura de Factura");


                                      }else{
                                      var a=document.getElementById("icono "+indice);
                                      a.style.color="green";

                                       }
                                    },error:function(x,xd,xdd){
                                      alert(x.responseText);
                                    }});
                            $('input[id="no"]').val(indice+1);
                            $superficie +=  data[indice].supF;

                            $('input[id="superficie"]').val($superficie);

                             }

                         },
                         complete: function(){
                             $('#loader').css("visibility", "hidden");
                         }
                     });


                 }

             });

                    //Función para llenado de campos en el modal

                var selectCon = document.getElementById("selectCon");
                $('select[name="selectCon"]').on('change', function(){
                var valor = $(this).val();
                var valor2 = valor.replace("/", "XX");
                 if(valor) {$.ajax({
                         url: '/con/'+valor2,
                         type:"GET",
                         dataType:"json",
                         beforeSend: function(){
                             $('#loader').css("visibility", "visible");
                         },

                         success:function(data) {
                             var tipo = data[0].tipo;
                             $('label[id="labelRiegos"]').empty(); $('label[id="labelRiegos"]').append("Riegos");


                             $('input[name="tipoCon"]').empty();
                             $('input[name="tipoCon"]').val(tipo);

                             $('input[name="cuotaCon"]').empty();
                            $('input[name="cuotaCon"]').val(data[0].cuota);

                             $('input[name="unidad"]').empty();
                             $('input[name="unidad"]').val(data[0].unidad);

                             $('input[id="riegosCon"]').val(0);

                             $('select[id="cultivoCon"]').removeAttr("disabled");
                             $('input[id="riegosCon"]').removeAttr("disabled");

                             if (tipo == "COO"){
                                 $('select[id="cultivoCon"]').attr('disabled', 'disabled');
                                 $('select[id="cultivoCon"]').visibility(false);

                                 $('label[id="labelRiegos"]').empty();
                                 $('label[id="labelRiegos"]').append("Cantidad");
                             }

                         },
                         complete: function(){
                             $('#loader').css("visibility", "hidden");
                         }
                     });


                 }

             });

                    //Función para el llenado de información al selecciona


                $('select[id="selectConMaq"]').on('change', function(){
                var valor = $(this).val();
                var valor2 = valor.replace("/", "XX");
                 if(valor) {$.ajax({
                         url: '/con/'+valor2,
                         type:"GET",
                         dataType:"json",
                         beforeSend: function(){
                             $('#loader').css("visibility", "visible");
                         },

                         success:function(data) {
                             var tipo = data[0].tipo;

                             $('input[id="tipoConMaq"]').empty();
                             $('input[id="tipoConMaq"]').val(tipo);

                             $('input[id="cuotaConMaq"]').empty();
                             $('input[id="cuotaConMaq"]').val(data[0].cuota);

                             $('input[id="unidadConMaq"]').empty();
                             $('input[id="unidadConMaq"]').val(data[0].unidad);

                             $('input[id="cantidadConMaq"]').val(0);
                             $('input[id="cantidadConMaq"]').removeAttr("disabled");

                             $('input[id="importeConMaq"]').val(0);

                             $('input[id="encargadoConMaq"]').val('');
                             $('input[id="encargadoConMaq"]').removeAttr("disabled");

                         },
                         complete: function(){
                             $('#loader').css("visibility", "hidden");
                         }
                     });
                 }
             });

                   //Función para obtener total de operación del modal
                         $('input[id="riegosCon"]').on('change', function(){
                             $('input[id="importeCon"]').empty();
                             var cuota = document.getElementById("cuotaCon").value;
                             var riegos = document.getElementById("riegosCon").value;
                             var cantidad = document.getElementById("cantidadCon").value;
                             var tipo = document.getElementById("tipoCon").value;
                             var total = 0;

                             if (tipo == "COO"){
                             total = (cuota * riegos);
                             $('input[id="importeCon"]').val(total);
                             }else{
                             total = (cuota * riegos * cantidad);
                             $('input[id="importeCon"]').val(total);

                             }
                             $('#btnAdd').removeAttr('disabled');
                         });

                    //Función para obtener total de renta de maquinaria
                         $('input[id="cantidadConMaq"]').on('change', function(){

                             var cuota = document.getElementById("cuotaConMaq").value;
                             var cantidad = document.getElementById("cantidadConMaq").value;
                             var total = 0;
                             total = (cuota * cantidad);
                             $('input[id="importeConMaq"]').val(total);


                             $('#btnAddMaq').removeAttr('disabled');
                         });






         });

                //Función para mandar datos al modal al seleccionar la parcela, los valores se ponen en ceros y se envía superficie y id

                 function modalPago(cuenta, cantidad){

		          $("#modal_grande").find('.modal-title').text('Operaciones de parcela: '+cuenta);


                  $('#btnAdd').attr('disabled', 'disabled');

                  $('input[id="riegosCon"]').val(0);
                  $('input[id="importeCon"]').val("");
                  $('input[id="tipoCon"]').val("");
                  $('input[id="cuotaCon"]').val("");
                  $('input[id="importeCon"]').val("");
                  $('input[id="unidad"]').val("");

                  $('input[id="cantidadCon"]').val(cantidad);

                  $('input[id="riegosCon"]').attr('disabled', 'disabled');
                  $("#modal_grande").modal('show');
                  var folio=document.getElementById("num_factura").value;
                  $("Id_factura").val(folio);
            }


            function modalMaq(){
                  $('#btnAddMaq').attr('disabled', 'disabled');

		          $("#modalMaq").find('.modal-title').text('Renta de maquinaria');

                  $('input[id="cantidadConMaq"]').val(0);
                  $('input[id="importeConMaq"]').val("");
                  $('input[id="tipoConMaq"]').val("");
                  $('input[id="cuotaConMaq"]').val("");
                  $('input[id="importeConMaq"]').val("");
                  $('input[id="unidadConMaq"]').val("");
                  $('input[id="encargadoConMaq"]').val("");

                  $("#modalMaq").modal('show');
            }

                //Función para añadir registros a la tabla de concepto de pago
                function agregar(){
                    b=true;

                  var concepto = document.getElementById("selectCon").value;
                  var tipo = document.getElementById("tipoCon").value;
                  var anio = document.getElementById("selectCiclo").value;
                  var unidad = document.getElementById("unidad").value;
                  var cuota = document.getElementById("cuotaCon").value;
                  var riegos = document.getElementById("riegosCon").value;
                  var cantidad = document.getElementById("cantidadCon").value;
                  var importe = document.getElementById("importeCon").value;
                  var cultivo = document.getElementById("cultivoCon").value;
                  var identificador = $("#modal_grande").find('.modal-title').text().split(":");
                  var folio=document.getElementById("num_factura").value;
                  var ejidatario=document.getElementById("ejidatarioN").value;
                //  alert(cultivo);
                  var parce = identificador[1].replace(" ","");
                  var descripcion = "FacturaRiego";
                   var conceptoR = concepto.replace(" ", "XXX");
                   var conceptoRR = conceptoR.replace(" ", "XXX");
                   var conceptoRRR = conceptoRR.replace("/", "YYY");
                    //obtenemos el valor del id de ejidatario

                   var idEjidatario = document.getElementById("btnMaq").value;


                  var valores = [folio, conceptoRRR, cantidad, importe, descripcion, parce, cultivo, riegos, idEjidatario, tipo];



                  //Datos del form en orden de la tabla
                  $("#folio").val(folio);
                  $("#concepto").val(concepto);
                  $("#sup_cultivo").val(cantidad);
                  $("#total").val(importe);
                  $("#descripcionMaq").val(descripcion);
                  $('#parcelas').val(parce);
                  $("#cultivo2").val(cultivo);
                  $("#cantidadcultibo").val(riegos);
                  $("#ejidatario").val(idEjidatario);
                  $("#tipo").val(tipo);




                  var frm=$("#factura");
                  var datos = frm.serialize();

                  $.ajaxSetup({
                       headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       }
                   });
                  $.ajax({
                    url:'/crearFact/',
                    type:'POST',
                    data:datos,
                     success:function(data){
                       if(data=="Error"){
                         swal({
                           title: 'Error',
                           text: 'El cultivo ya esta agotado.',
                           icon : "error",
                           buttons:false,
                           timer:3000,
                         });
                           }else{
                         var a = 0;
                         var b = parseFloat(importe);
                         var valNuevo = 0;


                       $("#modal_grande").modal('hide');

                        if (tipo == "COO"){

                        $('table[name="conceptosP"]').append('<tr><td>'+identificador[1]+'</td><td>'+concepto+'</td><td>'+tipo+'</td><td>'+anio+'</td><td>'+unidad+'</td><td>-</td><td>-</td><td>'+riegos+'</td><td>'+cuota+'</td><td>'+importe+'</td><td><button type="button" class="btn btn-danger" id="btnDelete" value='+valores+' data-toggle="popover" title="Popover title'+valores+'" onclick="deleteRow(this)"><i class="fas fa-times"></i></button></td></tr>');

                         $('table[name="tablaResumen"]').append('<tr><td>'+concepto+'</td><td>'+riegos+'</td><td>'+importe+'</td></tr>');

                        a = parseFloat($('input[id="coopFact"]').val());

                         valNuevo = a+b;

                         $('input[id="coopFact"]').val(valNuevo);

                        }else{

                        $('table[name="conceptosP"]').append('<tr><td>'+identificador[1]+'</td><td>'+concepto+'</td><td>'+tipo+'</td><td>'+anio+'</td><td>'+unidad+'</td><td>'+cultivo+'</td><td>'+cantidad+'</td><td>'+riegos+'</td><td>'+cuota+'</td><td>'+importe+'</td><td><button type="button" class="btn btn-danger" id="btnDelete" value='+valores+' data-toggle="popover" title="Popover title'+valores+'" onclick="deleteRow(this)"><i class="fas fa-times"></i></button></td></tr>');

                        $('table[name="tablaResumen"]').append('<tr><td>'+concepto+'</td><td>'+riegos+'</td><td>'+importe+'</td></tr>');

                        a = parseFloat($('input[id="riegoFact"]').val());
                        valNuevo = a+b;

                         $('input[id="riegoFact"]').val(valNuevo);
                        }




                         $('select[name="tarjetaN"]').attr('disabled', 'disabled');
                         $('select[name="ejidatarioN"]').attr('disabled', 'disabled');
                         calcularTotal();
                 }

                    },error:function(x,xs,xt){
                        alert(x.responseText);
                        }
                });

}


                function agregarMaq(){
                    b=true;

                  var concepto = document.getElementById("selectConMaq").value;
                  var tipo = document.getElementById("tipoConMaq").value;
                  var anio = document.getElementById("selectCicloMaq").value;
                  var unidad = document.getElementById("unidadConMaq").value;
                  var cuota = document.getElementById("cuotaConMaq").value;
                  var cantidad = document.getElementById("cantidadConMaq").value;
                  var importe = document.getElementById("importeConMaq").value;
                  var encargado = document.getElementById("encargadoConMaq").value;

                  var parce = 4223;
                  var cultivo = 1;

                  var folio = document.getElementById("num_factura").value;
                  var ejidatario = document.getElementById("ejidatarioN").value;
                  var riegos = 1;


                  var descripcion = "FacturaRiego";
                   var conceptoR = concepto.replace(" ", "XXX");
                   var conceptoRR = conceptoR.replace(" ", "XXX");
                   var conceptoRRR = conceptoRR.replace("/", "YYY");


                var idEjidatario = document.getElementById("btnMaq").value;


                  var valores = [folio, conceptoRRR, cantidad, importe, descripcion, parce, cultivo, riegos, idEjidatario, tipo];



                  $("#folio").val(folio);
                  $("#concepto").val(concepto);
                  $("#sup_cultivo").val(cantidad);
                  $("#total").val(importe);
                  $("#descripcionMaq").val(encargado);
                  $('#parcelas').val(parce);
                  $("#cultivo2").val(cultivo);
                  //Datos arbitrarios pero necesarios
                  $("#cantidadcultibo").val(riegos);
                  $("#ejidatario").val(idEjidatario);
                  $("#tipo").val(tipo);

                  var frm=$("#factura");
                  var datos = frm.serialize();

                     $.ajaxSetup({
                       headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       }
                   });
                  $.ajax({
                    url:'/crearFact/',
                    type:'POST',
                    data:datos,
                     success:function(data){

                         var a = 0;
                         var b = parseFloat(importe);
                         var valNuevo = 0;

                       $("#modalMaq").modal('hide');
                       $('table[name="conceptosP"]').append('<tr><td>'+parce+'</td><td>'+concepto+'</td><td>'+tipo+'</td><td>'+anio+'</td><td>'+unidad+'</td><td>-</td><td>-</td><td>'+cantidad+'</td><td>'+cuota+'</td><td>'+importe+'</td><td><button type="button" class="btn btn-danger" id="btnDelete" value='+valores+' data-toggle="popover" title="Popover title'+valores+'" onclick="deleteRow(this)"><i class="fas fa-times"></i></button></td></tr>');

                       $('table[name="tablaResumen"]').append('<tr><td>'+concepto+'</td><td>'+cantidad+'</td><td>'+importe+'</td></tr>');

                       a = parseFloat($('input[id="maqFact"]').val());
                         valNuevo = a+b;
                       $('input[id="maqFact"]').val(valNuevo);

                       $('select[name="tarjetaN"]').attr('disabled', 'disabled');
                       $('select[name="ejidatarioN"]').attr('disabled', 'disabled');
                       calcularTotal();


                },error:function(x,xs,xt){
                        alert(x.responseText);
                        }
                });
                }

                //función para borrar registro de la tabla de concepto de pago
                function deleteRow(r) {
                  var valoresFact = r.value;
                  var valores = valoresFact.split(",");
                  var folio = valores[0];
                  var concepto = valores[1];
                  var cantidad = valores[2];
                  var importe = valores[3];
                  var descripcion  = valores[4];
                  var identificador = valores[5];
                  var cultivo = valores[6];
                  var riegos = valores[7];
                  var ejidatario = valores[8];
                  var tipo = valores[9];

                  var a = 0;
                  var b = parseFloat(importe);
                  var valNuevo = 0;


                  if (tipo == "COO"){
                    a = parseFloat($('input[id="coopFact"]').val());
                    valNuevo = a-b;
                    $('input[id="coopFact"]').val(valNuevo);

                  }else if(tipo == "MAQ" ){
                    a = parseFloat($('input[id="maqFact"]').val());
                    valNuevo = a-b;
                    $('input[id="maqFact"]').val(valNuevo);

                  }else{
                    a = parseFloat($('input[id="riegoFact"]').val());
                    valNuevo = a-b;
                    $('input[id="riegoFact"]').val(valNuevo);
                  }


                  var conceptoR = concepto.replace("XXX", " ");
                  var conceptoRR = conceptoR.replace("XXX", " ");
                  var conceptoRRR = conceptoRR.replace("YYY", "/");

                  $("#folio").val(folio);
                  $("#concepto").val(conceptoRRR);
                  $("#sup_cultivo").val(cantidad);
                  $("#total").val(importe);
                  $("#descripcionMaq").val(descripcion);
                  $('#parcelas').val(identificador);
                  $("#cultivo").val(cultivo);
                  $("#ejidatario").val(ejidatario);
                  $("#cantidadc").val(riegos);
                  $("#tipo").val(tipo);

                  var frm=$("#factura");

                  var datos = frm.serialize();
                  $.ajaxSetup({
                       headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       }
                   });
                  $.ajax({
                    url:'/BorrarCFact/',
                    type:'POST',
                    data:datos,
                     success:function(data){
                       var i = r.parentNode.parentNode.rowIndex;
                       document.getElementById("conceptosP").deleteRow(i);
                       document.getElementById("tablaResumen").deleteRow(i);
                       calcularTotal();
                    },error:function(x,xs,xt){
                        alert(x.responseText);
                        }
                });



                }



                function cancelar(){
                  var folio=document.getElementById("num_factura").value;
                  $.ajax({
                    url:'/borrado/'+folio,
                    type:'get'
                  }).done(function (res){
                    location.reload();
                  });
                }
                function terminada(){

                    	window.onbeforeunload = null;
                  var folio=document.getElementById("num_factura").value;
                  var frm=$("#pagaradeudo");
                  var total=$("#riegoFact").val();
                  var cantidad=$("#pagaFact").val();
                //  alert(cantidad);
                  if(cantidad >total|| cantidad<0){
                    swal({
                      title: 'Error de Cantidad',
                      text: 'La cantidad proporcionada es mayor que el total.',
                      icon : "error",
                      buttons:false,
                      timer:2000,
                    });
                  }else{
                    b=false;
                  var datos = frm.serialize();
                  $.ajaxSetup({
                       headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       }
                   });
                  $.ajax({
                    url:'/pago/',
                    type:'POST',
                    data:datos,
                    error:function(x,l,o){
                    alert(x.responseText);
                    }
                  }).done(function (res){
                    b=false;
                    swal({
                      title: 'Factura Realizada',
                      text: 'Factura realizada con exito.',
                      icon : "success",
                      buttons:false,
                      timer:3000,
                    });
                 location.reload();
               });}
                }

                function calcularTotal() {

                  var totals = [0, 0, 0, 0, 0, 0, 0, 0, 0];
                  var $filas= $("#conceptosP tr:not('.total, .encabezado')");

                  $filas.each(function() {
                    $(this).find('td').each(function(i) {
                      if (i != 0)
                        totals[i - 1] += parseFloat($(this).html());
                    });
                  });

                    $(".total td").each(function(i) {
                    if (i != 0)
                    $(this).html(totals[i - 1]);
                    });

                  $('input[id="totalFact"]').val(totals[8]);
                  $('input[id="deuda"]').val(totals[8]);

                }
                var yea=document.getElementById("conceptosP").rows.length;
                 </script>
      <!--EVENTO PARA ELIMINAR SI SE ABANDONA LA PAGINA-->
<script>
window.addEventListener("beforeunload", function (e) {

            if(b){
          var confirmationMessage =true;
          console.log(confirmationMessage);
          (e || window.event).returnValue = confirmationMessage;
             return cancelar();

      /*   if(confirmationMessage==true){//Gecko + IE
               return cancelar();
             } else{
                return false;
              }*/ }                        //Webkit, Safari, Chrome
});

                         //Webkit, Safari, Chrome
</script>



          @endsection
