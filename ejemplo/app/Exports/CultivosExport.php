<?php

namespace App\Exports;
use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

use App\Cultivo;

class CultivosExport implements FromView
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $cultivo=cultivo::get();
        return view('Cultivos.excel',[
            'cultivo' => $cultivo
        ]);
    }
}
