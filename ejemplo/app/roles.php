<?php

namespace App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class roles extends Model
{
    protected $fillable = [
        'id_rol','descripcion'
    ];
    protected $table = 'roles';
    protected $primaryKey = 'Id_rol';
    use SoftDeletes; //Implementamos
    protected $dates = ['deleted_at'];
}
