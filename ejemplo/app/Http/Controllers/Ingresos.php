<?php

namespace App\Http\Controllers;
use App\ingreso;
use App\parcela;
use App\ejido;
use App\ejidatario;
use App\canaleros;
use App\municipio;
use App\tenencia;
use App\factura;
use App\ciclo;
use App\bitacora;
use App\factura_concepto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Exports\IngresoExport;
class Ingresos extends Controller
{
    public function __invoke(Request $request)
    {

    }

    public function __construct()
    {
      $this->middleware('auth');
      //$this->middleware('admin');
    }

    public function verIngresos()
    {
        $ldate = date('Y');
        $ci=substr($ldate,2)+0;
        $cl=$ci-1;
        $ciclo=$cl."/".$ci;
        $mes= now()->format('m');
        $now = now()->format('Y-m-d');
        $anio = now()->format('Y');
        $dinero=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')
          ->select(DB::raw('SUM(abonos.pago) as total'))->whereMonth('abonos.created_at','=',$mes)->whereYear('abonos.created_at','=',$anio)->get();
        $dineroDia=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')
          ->select(DB::raw('SUM(abonos.pago) as total'))->whereDate('abonos.created_at',$now)->where('factura.Id_status_factura','!=','4')->get();
        $dineroCiclo=factura::join('abonos','abonos.Id_factura','=','factura.Id_factura')
         ->join("ciclo",'ciclo.Id_ciclo','=','factura.Id_ciclo')
         ->select(DB::raw('SUM(abonos.pago) as total'))->where('ciclo.Ciclo','=',$ciclo)->get();
       return view("Ingresos.ingreso",compact('dinero','dineroDia','now','anio','dineroCiclo'));
    }

    public function store(Request $request){

    }

    public function getIngreso(){
      $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
      ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
      ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
      ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
      ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
      ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->join('abonos','abonos.Id_factura','=','factura.Id_factura')
      ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
      ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
      ->select('factura_concepto.Total','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','factura_concepto.descripcionM as des','ejidatario.tarjeta','abonos.pago as Total')
      ->where('ciclo.Ciclo','=','19/20')
      ->get();

      foreach ($factura as $key) {
      if($key->tipo=="MAQ"){
        $key->nombre=$key->des;
      }
      }
      return datatables($factura)->toJson();
    }

    public function ingresoFecha(Request $request){
            $in=$request->get('inicio');
            $fi=$request->get('fin');
            $inicio=$in."T00:00";
            $fin=$fi."T23:59";
            $factura=factura::join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
            ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
            ->join('ejidatario','ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
            ->join('municipio','municipio.Id_municipio','=','parcela.Id_municipio')
            ->join('ejido','ejido.Id_Ejido','=','parcela.Id_Ejido')
            ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
            ->join('abonos','abonos.Id_factura','=','factura.Id_factura')
            ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
            ->join('concepto','concepto.Id_Concepto','=','factura_concepto.Id_Concepto')
            ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
            ->select('factura_concepto.Total','ciclo.Ciclo as ciclo','status_factura.descripcionEstado as status_fact','concepto.tipo','ejido.Id_Ejido','concepto.descripcion_con','factura_concepto.sup_cultivo','cultivo.descripcion as cultivo','parcela.sup_Fis','municipio.descripcion','factura.created_at as fecha','ejido.nombre_ej as ejidos','factura.Id_factura','parcela.SEC','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','abonos.pago as Total')
           ->whereBetween('factura.created_at', array($inicio, $fin))->get();

         return datatables($factura)->toJson();
    }
    public function exportarExcel(Request $request){
      $export = new IngresoExport($request);
      $usuario=Auth::user()->id;
      $bitacora=new bitacora();
      $bitacora->Id_usuario=$usuario;
      $bitacora->id_bitacora=0;
      $bitacora->modulo="Ingresos";
      $bitacora->Id_movimiento=4;
      $bitacora->save();
      return $export->download('Reporte Ingresos.xlsx');

    }
}
