<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class concepto extends Model
{
    protected $table = 'concepto';
      public $timestamps = false;
      public  $primaryKey = 'Id_Concepto';
      protected $keyType = 'string';
      use SoftDeletes; //Implementamos
      protected $dates = ['deleted_at'];
}
