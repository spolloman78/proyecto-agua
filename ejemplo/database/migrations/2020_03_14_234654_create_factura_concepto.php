<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaConcepto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_concepto', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->String("Id_factura");
            $table->string("Id_Concepto");
            $table->float("sup_cultivo");
            $table->float("can_pagada");
            $table->float("Total");
            $table->Integer("cantidadC");
            $table->string("descripcionM");
            $table->unsignedInteger("Id_parcela");
            $table->unsignedInteger("Id_cultivo");
            $table->foreign('Id_Concepto')->references('Id_Concepto')->on('concepto');
            $table->foreign('Id_factura')->references('Id_factura')->on('factura');
            $table->foreign('Id_parcela')->references('Id_parcela')->on('parcela');
            $table->foreign('Id_cultivo')->references('Id_cultivo')->on('cultivo');
            $table->softDeletes(); //Nueva línea, para el borrado lógico


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('factura_concepto');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
