@extends('layouts.layout')
@section('content')
	<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Bitácora <i class="flaticon-list"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Bitácora</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Bitácora de operaciones<i class="fas fa-hat-cowboy"></i></h4>
                <div class="d-flex align-items-center">
                </div>

              </div>
              <div class="card-body">
								<form id="fechas" type="post" action="{{route('bitacoraFecha')}}">
									{{csrf_field()}}
								<div class="row">
										<div class="col-md-3">
												<div class="form-group form-group-default">
														<label for="num_factura">Desde:</label>
														<input type="date" class="form-control form-control-sm" id="fecha_ini"name="inicio" value="{{$anio}}-01-01" min="2018-01-01" max="{{$now}}">
												</div>
										</div>
										<div class="col-md-3">
												<div class="form-group form-group-default">
														<label for="num_factura">Hasta:</label>
														<input type="date" class="form-control form-control-sm" id="fecha_fin" name="fin" value="{{$now}}" max="{{$now}}">
												</div>
										</div>
								</div>
							</form>
                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                        <th># <i class="flaticon-list"></i></th>
                        <th>Usuario</th>
                        <th>Tipo de Movimiento</th>
                        <th>Modulo</th>
                        <th>Fecha</th>
                      </tr>
                    </thead>
                    <tbody id="cuerpo">
                      @foreach($bitacora as $row)
                      <tr>
                        <td>{!!$row->id_bitacora!!}</td>
                        <td>{!!$row->name!!} {!!$row->apPaterno!!} {!!$row->apMaterno!!}</td>
                        <td>{!!$row->descripcion!!}</td>
                        <td>{!!$row->modulo!!}</td>
                        <td>{!!$row->created_at!!}</td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
					<script>
function cambio(){
	var frm=$("#fechas");
	var datos = frm.serialize();
 $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
	});
 $.ajax({
	 url:'/fechaBitacora',
	 type:'post',
	 data:datos,
		success:function(data){
		 $('#cuerpo').html(data);
	 },error:function(x,xs,xt){
	 alert("Error  no se pudo encontrar informacion "+xt);
			 }
});
}

   $("#fecha_fin").change(function(){
	  cambio();
			                 });
		$("#fecha_ini").change(function(){
cambio();
																			});


					</script>

@endsection
