<?php

namespace App\Http\Controllers;
use App\parcela;
use App\ejido;
use App\ejidatario;
use App\canaleros;
use App\municipio;
use App\tenencia;
use App\bitacora;
use Maatwebsite\Excel\Excel;
use App\Exports\ParcelasExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class Parcelas extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function eliminar($id){
      parcela::find($id)->delete();
      $usuario=Auth::user()->id;
      $bitacora=new bitacora();
      $bitacora->Id_usuario=$usuario;
      $bitacora->id_bitacora=0;
      $bitacora->modulo="Parcelas";
      $bitacora->Id_movimiento=3;
      $bitacora->save();
    }

    public function verParcela()
    {
        $ejidos=ejido::all();
        $ejidatarios=ejidatario::all();
        $municipio=municipio::all();
        $canalero=canaleros::all();
        $tenencia=tenencia::all();
        $total=parcela::count();
        $ejidoT=ejido::count();
        $deudores=DB::table('factura_concepto')
       ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
       ->join("parcela","parcela.Id_parcela","=","factura_concepto.Id_parcela")
       ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
       ->where('status_factura.Id_status_factura','=','3')
       ->count();
        $parcelaT=parcela::join('factura_concepto','factura_concepto.Id_parcela','=','parcela.Id_parcela')
        ->join('factura','factura.Id_factura','=','factura_concepto.Id_factura')
        ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')->where('factura.Id_ciclo','=','3')->count();
        $parcela=parcela::join("ejidatario","ejidatario.Id_ejidatario","=","parcela.Id_ejidatario")
        ->join("ejido","ejido.Id_Ejido","=","parcela.Id_Ejido")
        ->join("tenencia",'tenencia.Id_tenencia','=','parcela.Id_tenencia')
        ->join("municipio",'municipio.Id_municipio','=','parcela.Id_municipio')
        ->join("canalero",'canalero.Id_canalero','=','parcela.Id_canalero')
        ->select('parcela.Id_parcela','ejido.Id_Ejido','parcela.Id_municipio','municipio.descripcion as muni','ejidatario.nombre as ejidon','ejidatario.apPaterno','ejidatario.apMaterno','ejido.nombre_ej','parcela.CP','parcela.SEC','parcela.cuenta',
         'parcela.LT','parcela.SLT','parcela.RA','parcela.PC','tenencia.Id_tenencia as TE','parcela.SR','parcela.EQ','parcela.sup_Fis','parcela.pro','canalero.nombre')->get();
         return view("Parcelas/parcela",compact('deudores','parcelaT','ejidoT','total','parcela','ejidos','ejidatarios','municipio','canalero','tenencia'));
    }



    public function llenado($id)
    {

       $datos=DB::table('parcela')
       ->join("ejidatario","ejidatario.Id_ejidatario","=","parcela.Id_ejidatario")
       ->join("ejido","ejido.Id_Ejido","=","parcela.Id_Ejido")
       ->join("tenencia",'tenencia.Id_tenencia','=','parcela.Id_tenencia')
       ->join("canalero",'canalero.Id_canalero','=','parcela.Id_canalero')
       ->join("municipio",'municipio.Id_municipio','=','parcela.Id_municipio')
       ->select('parcela.Id_parcela','ejido.Id_Ejido','parcela.Id_municipio','parcela.Id_ejidatario','parcela.Id_canalero','municipio.descripcion','ejidatario.nombre as ejidon','ejidatario.apMaterno','ejidatario.apPaterno','ejido.nombre_ej','parcela.CP','parcela.SEC','parcela.cuenta',
        'parcela.LT','parcela.SLT','parcela.RA','parcela.PC','tenencia.Id_tenencia','tenencia.descripcionT as TE','parcela.SR','parcela.EQ','parcela.sup_Fis','parcela.pro','canalero.nombre')
   			->where('Id_parcela', '=', $id)
   			->get();
       return ($datos=array('datos' => $datos));


    }
    public function cultivo($id)
    {
       $ldate = date('Y');
       $ci=substr($ldate,2)+0;
       $cl=$ci-1;
       $ciclo=$cl."/".$ci;
       $datos=DB::table('factura_concepto')
      ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
      ->join("parcela","parcela.Id_parcela","=","factura_concepto.Id_parcela")
      ->join("cultivo","cultivo.Id_cultivo",'=','factura_concepto.Id_cultivo')
      ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->select('cultivo.descripcion as cultivo','status_factura.Id_status_factura as status_fact','factura_concepto.can_pagada','ciclo.Ciclo as ciclo','factura_concepto.sup_cultivo','factura.created_at','factura.Id_factura')
      ->where('factura_concepto.Id_parcela','=',$id)
    //  ->where('ciclo.Ciclo','=',$ciclo)
      ->get()->last();
       return ($datos=array('datos' => $datos));
    }

    public function store(Request $parcela){
     if($parcela->ajax()){
       $v = \Validator::make($parcela->all(), [
           'cuenta' => ['required','unique:parcela'],
           'pro' => ['required','unique:parcela'],
           'RA'    => ['required'],
           'TE' => 'required'
       ]);
       if ($v->fails()) {
         echo $v->errors();
         return response()->json(['errors'=>$v->errors()->all()]);
       } else {
      $nueva=new parcela();
      $nueva->Id_parcela=0;
      $nueva->CP=$parcela->CP;
      $nueva->LT=$parcela->LT;
      $nueva->SLT=$parcela->SLT;
      $nueva->RA=$parcela->RA;
      $nueva->Id_tenencia=$parcela->TE;
      $nueva->SR=$parcela->SR;
      $nueva->EQ=$parcela->EQ;
      $nueva->SEC=$parcela->SEC;
      $nueva->pro=$parcela->pro;
      $nueva->PC=$parcela->PC;
      $nueva->cuenta=$parcela->cuenta;
      $nueva->Id_Ejido=$parcela->Id_Ejido;
      $nueva->Id_canalero=$parcela->Id_canalero;
      $nueva->Id_municipio=$parcela->Id_municipio;
      $nueva->Id_ejidatario=$parcela->Id_ejidatario;
      $nueva->sup_Fis=$parcela->sup;
      $nueva->save();
      $usuario=Auth::user()->id;
      $bitacora=new bitacora();
      $bitacora->Id_usuario=$usuario;
      $bitacora->id_bitacora=0;
      $bitacora->modulo="Parcelas";
      $bitacora->Id_movimiento=1;
      $bitacora->save();
    }}

    }
    public function actualizar(Request $request)
    {

     if($request->ajax()){

       $parcela=parcela::find($request->Id_parcela);
       $parcela->CP=$request->CP;
       $parcela->LT=$request->LT;
       $parcela->SLT=$request->SLT;
       $parcela->RA=$request->RA;
       $parcela->Id_tenencia=$request->TE;
       $parcela->SR=$request->SR;
       $parcela->EQ=$request->EQ;
       $parcela->pro=$request->PRO;
       $parcela->cuenta=$request->cuenta;
       $parcela->Id_Ejido=$request->ejido;
       $parcela->Id_canalero=$request->canalero;
       $parcela->Id_municipio=$request->municipio;
       $parcela->Id_ejidatario=$request->ejidatarios;
       $parcela->sup_Fis=$request->sup;
       $parcela->save();
       $usuario=Auth::user()->id;
       $bitacora=new bitacora();
       $bitacora->Id_usuario=$usuario;
       $bitacora->id_bitacora=0;
       $bitacora->modulo="Parcelas";
       $bitacora->Id_movimiento=2;
       $bitacora->save();
     }else{

     }

    }


   public function dividir(Request $request){
  if($request->ajax()){
     $parcela=parcela::find($request->Id_parcela);
     $extension=$parcela->sup_Fis;
     $cantidad=$request->sup;
     if(($request->cuenta)==($parcela->cuenta)){
       echo"error";
     }else{
      if($cantidad >$extension){
      echo"error";
      }else{
      $total=$extension-$cantidad;
      $nueva=new parcela();
      $nueva->Id_parcela=0;
      $nueva->CP=$parcela->CP;
      $nueva->LT=$parcela->LT;
      $nueva->SLT=$parcela->SLT;
      $nueva->RA=$parcela->RA;
      $nueva->Id_tenencia=$parcela->Id_tenencia;
      $nueva->SR=$parcela->SR;
      $nueva->EQ=$parcela->EQ;
      $nueva->SEC=$parcela->SEC;
      $nueva->pro=$parcela->pro;
      $nueva->PC=$parcela->PC;
      $nueva->cuenta=$parcela->cuenta;
      $nueva->Id_Ejido=$parcela->Id_Ejido;
      $nueva->Id_canalero=$parcela->Id_canalero;
      $nueva->Id_municipio=$parcela->Id_municipio;
      $nueva->Id_ejidatario=$parcela->Id_ejidatario;
      $nueva->sup_Fis=$total;
      $nueva->save();
      $parcela->sup_Fis=$request->supo;
      $parcela->cuenta=$request->cuenta;
      $parcela->save();
   }
 }}

}


    public function geteji()
  {
    $ejidatario = ejidatario::get();
    $ejidatariosArray[''] = 'Selecciona un ejidatario';
    foreach ($ejidatarios as $ejidatario) {
        $ejidatariosArray[$ejidatario->id] = $ejidatario->nombre;
    }
    return $ejidatariosArray;
  }
  public function exportarExcel(){
    $export = new ParcelasExport();
    $usuario=Auth::user()->id;
    $bitacora=new bitacora();
    $bitacora->Id_usuario=$usuario;
    $bitacora->id_bitacora=0;
    $bitacora->modulo="Parcelas";
    $bitacora->Id_movimiento=4;
    $bitacora->save();
    return $export->download('Parcelas.xlsx');
    //return Excel::download(new UsersExport, 'usuarios.xlsx');
  }


}
