@extends('layouts.layout')

@section('content')

<div class="main-panel">
    <div class="content">

      <div class="page-inner">

        <div class="page-header">
          <h4 class="page-title">Bitacora de Riego<i class="flaticon-agenda"></i></h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Caja</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Bitacora de Riego</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Bitacora de Riego</h4>


              </div>
              <div class="card-body">

                {{-- <div class="col-md-3">
                        <div class="form-group form-group-default">
                            <label for="cuenta">Ciclo Agrario</label>



                            <select class="form-control js-example-basic-single dynamic" name="ejidatarioN" id="ejidatarioN">
                         @foreach($ciclos as $row)
                              <option value="{!!$row->Ciclo!!}"> {!!$row->Ciclo!!}</option>
                            @endforeach
                              </select>
                        </div>
                    </div>--}}
                <div class="table-responsive">
                  <table  id="basic-datatables"class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                        <th>Folio</th>
                        <th>Estado de la Factura</th>
                        <th>Cuenta</th>
                        <th>Ejidatario</th>
                        <th>Ciclo Agrario</th>
                        <th>Canalero</th>
                        <th>Fecha</th>
                        <th>Operaciones</th>
                      </tr>
                    </thead>


                    <tbody>
                      @foreach($factura as $row)
                      <tr>
                        <td>{!!$row->Id_factura!!}</td>
                          <td>{!!$row->status_fact!!}</td>
                          <td>{!!$row->cuenta!!}</td>
                          <td>{!!$row->nombre!!} {!!$row->apPaterno!!}   {!!$row->apMaterno!!}</td>
                          <td>{!!$row->ciclo!!}</td>
                          <td>{!!$row->canalero!!}</td>
                          <td>{!!$row->created_at!!}</td>

                          <?php $date =Carbon\Carbon::parse($row->created_at)->format('Y-m-d');
                        $d=date_create($date);
                        $d2=date_create($now);
                        $cuenta=0;
                        if($row->cuenta==99999){
                          $cuenta=1;
                        }else{
                          $cuenta=0;
                        }
                        ?>
                      <td id="operaciones">
                      <div class="form-button-action">
                      <button type="button" data-toggle="tooltip" title="Información" class=" btn btn-link btn-info btn-lg" data-original-title="Ver Detalles"  data-toggle="modal" data-target="#addRowModal" onclick="modalEdit('{{$row->Id_factura}}','{!!$row->nombre!!}{!!$row->apPaterno!!}  {!!$row->apMaterno!!}','{{$row->total}}','{{$cuenta}}','{{$row->descripcionM}}')"  >
                      <i class="fas fa-info-circle"></i>
                      </button>

                      @if($row->status!=4)
                      <button type="button" data-toggle="tooltip" title="Completar" onclick="modalCambiar('{{$row->Id_factura}}','2')" class="btn btn-link btn-success btn-lg">
                      <i class="fas fa-check-double"></i>
                      </button>
                      <button type="button" data-toggle="tooltip" title="Calendario" onclick="detalles('{{$row->Id_factura}}')" class="btn btn-link btn-muted btn-lg">
                      <i class="flaticon-calendar"></i>
                      </button>
                      @endif

                      @if( $d2== $d)
                      <button type="button" data-toggle="tooltip" title="Cancelar" class="btn btn-link btn-danger" onclick="eliminar('{!!$row ->Id_factura!!}','4')"data-original-title="Eliminar">
                      <i class="fa fa-times"></i>
                    </button>@endif
                      </div>
                      </td>
                      </tr>
    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
<form id="factura">
    {{csrf_field()}}
  <input type="text" id="Id_factura" name="Id_factura"hidden/>
  <input type="number" id="estado" name="estado" hidden/>
</form>
<!-- MODAL PARA VER DATOS DE LA FACTURA-->
          <div class="modal fade bd-example-modal-lg" id="modals" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header no-bd ">
                  <h5 class="modal-title" >
                    <span class="fw-mediumbold">
                    Datos  </span>
                    <span class="fw-light">
                      Factura
                    </span>
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body ">
                  <form >
                      {{csrf_field()}}
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Folio</label>
                          <input id="folio" disabled class="form-control" placeholder="0.00" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default" >
                          <label>Total</label>
                          <input id="total" disabled name="descripcion"type="text" class="form-control" placeholder="Cultivo" required>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group form-group-default"  >
                          <label>Ejidatario</label>
                          <input id="Ejidatario" type="text" disabled class="form-control" placeholder="Cultivo" required>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group form-group-default" >
                          <label>Conceptos</label>
                          <table id="conceptos"  class="display table table-striped table-hover" >
                            <thead>
                              <tr>
                                <th>Parcela</th>
                                <th>Concepto</th>
                                <th>Superficie de Riego</th>
                                <th>Cultivo</th>
                                <th>Importe</th>
                              </tr>
                              </thead>
                              <tbody id="cuerpo">
                                <tr>
                                  <td></td>
                                    <td></td>
                                    <td></td>
                                  </tr>
                                </tbody>
                               </table>
                    </div>
                  </div>

                </div>
                </form>

             <div class="modal-footer no-bd">
               <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
             </div>
           </div>
         </div>
       </div>
     </div>

     <div class="modal fade " role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal_detalles">
       <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
             <h4 class="modal-title  no-bd" >
               <span class="fw-mediumbold">
               Calendarizar </span>
               <span class="fw-light">
                 Servicio <i class="flaticon-calendar" style="color:green; "></i>
               </span>
             </h4>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
           <div class="modal-body">
         <p class="small">En este panel usted podrá cambiar la calendarizacion establecida en el servicio.<br/></P>
           <form id="registroD"action="{{route('DivParcela')}}" method="post" >
               {{csrf_field()}}
                 <p class="text-danger" id="textodividir" style="margin-left:10px;"></p>
             <div class="row">
            <input id="Id_horario" name="Id_horario"hidden/>
                                 <div class="col-md-12">
                                   <div class="form-group form-group-default">
                                     <label>Fecha inicio</label>
                                     <input id="inicio"  name="inicio"type="date" class="form-control" placeholder="Cuenta" required>
                                   </div>
                                 </div>

                                 <div class="col-sm-12">
                                   <div class="form-group form-group-default">
                                     <label>Fecha Fin</label>
                                     <input id="fin"  name="fin"type="date" class="form-control" min="0" step="0.01" max="200"placeholder="Superficie Fisica" required>
                                   </div>
                                 </div>
                               </div>
           </div>
           <div class="modal-footer">
             <button type="button" id="dividi" class="btn btn-primary">  <i class="flaticon-calendar"></i> Aceptar Cambios </button>
             <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
           </div>
         </form>
         </div>
       </div>
       </div>
       </div>
     <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

            <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
            <script>
              // Funcion para eliminar
            function eliminar(id,ejido){
            swal({
              title: '¿Seguro que desea cancelar la factura '+id+' ?' ,
              text: "¡Esta accion no es reversible!",
              type: 'warning',
              icon : "error",
              buttons:{
                confirm: {
                  text : 'Confirmar',
                  className : 'btn btn-danger',
                },
                cancel: {
                  text : 'Cancelar',
                  visible: true,
                  className: 'btn btn-warning'
                }
              }
            }).then((Delete) => {
              if (Delete) {
                $('#Id_factura').val(id);
                $("#estado").val(ejido);
                var frm=$("#factura");
                var datos = frm.serialize();
                $.ajaxSetup({
                     headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
                 });
                $.ajax({
                  url:'/ModificarFactura/',
                  type:'post',
                  data:datos,
                   success:function(data){
                     swal({
                       title: 'Factura Cancelada!',
                       text: 'La factura ha sido cancelada.',
                       icon : "success",
                       buttons:false,
                       timer:3000,
                     });
                       location.reload();
                  },error:function(x,xs,xt){
                    alert(x.responseText);
                      }
              });
              } else {
                swal.close();
              }
            });

            }
       // aqui termina funcion de eliminar

       $("body").on("click","#dividi",function(event){
         var frm=$("#registroD");
         var datos = frm.serialize();
         $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
          $.ajax({
           type:'POST',
           url:'/Modificarhorario',
           data:datos,
         success:function(data){
       location.reload();
               $('#modal_detalles').modal('hide');
           swal({
             title: "Servicio Recalendarizado",
             text: "Se cambio el periodo de para ofrecer el servicio.",
             icon: "success",
             buttons:false,
             timer:3000,
           }); },
               error:function(x,xs,xt){
                 var xd=x.responseText.split("}");
                 alert(xd);
                  /*var obj2=JSON.parse(xd[0]+'}');
                  var obj = JSON.parse(xd[1]+"}");
                  var atributos = "";
                  for(var aux in obj2){
                  $('input[name="'+aux+'"]').closest('.form-group').addClass('has-error');}
*/
               }
         });
         });




            </script>
<script>
function detalles(folio){

$.ajax({
  url:'/gethorario/'+ folio,
  type:'get',
   success:function(data){
$("#inicio").val(data.datos[0].fechai);
$("#fin").val(data.datos[0].fecha);
$("#Id_horario").val(data.datos[0].Id_horario);
$('#modal_detalles').modal('show');
  },error:function(x,xs,xt){
          alert(x.responseText);
      }
});
}
  function modalEdit(id,ejido,total,cuenta,des){
    $('#modals').modal('show');
    $('#folio').val(id);
    if(cuenta==1){
      $('#Ejidatario').val(des);
    }else{
      $('#Ejidatario').val(ejido);
    }
    $('#total').val(total);
    $.ajax({
      url:'verConceptos/'+ id,
      type:'get',
       success:function(data){
              $('#cuerpo').html(data);
      },error:function(x,xs,xt){
              alert(x.responseText);
          }
  });
}

</script>
<script>
function modalCambiar(id,ejido){
  swal({
    title: '¿Seguro que desea completar la factura '+id +' ?',
    text: "¡Esta accion no es reversible!",
    type: 'warning',
    icon : "success",
    buttons:{
      confirm: {
        text : 'Si, deseo completarlo!',
        className : 'btn btn-success',
      },
      cancel: {
        text : 'Cancelar',
        visible: true,
        className: 'btn btn-danger'
      }
    }
}).then((Delete) => {
  if (Delete) {
    $('#Id_factura').val(id);
    $("#estado").val(ejido);
    var frm=$("#factura");
    var datos = frm.serialize();
    $.ajaxSetup({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });
    $.ajax({
      url:'/ModificarFactura/',
      type:'post',
      data:datos,
       success:function(data){
         swal({
           title: 'Servicio Completado!',
           text: 'El servicio de esta factura ha sido completado.',
           icon : "success",
           buttons:false,
           timer:3000,
         });
           location.reload();
      },error:function(x,xs,xt){
        alert(x.responseText);
          }
  });
  } else {
    swal.close();
  }
});

}
</script>
          @endsection
