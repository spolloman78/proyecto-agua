<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tenencia', function (Blueprint $table) {
          $table->Increments("Id_tenencia");
          $table->string("descripcionT");
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    DB::statement('SET FOREIGN_KEY_CHECKS = 0');
      Schema::dropIfExists('tenencia');
      DB::statement('SET FOREIGN_KEY_CHECKS = 1');
  }
}
