<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class cultivo extends Model
{
    protected $table = 'cultivo';
      public $timestamps = false;
      use SoftDeletes; //Implementamos
      protected $dates = ['deleted_at'];
      public  $primaryKey = 'Id_cultivo';
}
