<?php

namespace App\Exports;
use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

use App\ejidatario;

class EjidatariosExport implements FromView
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    
    public function view(): View
    {
        $ejidatarios=ejidatario::get();
        return view('Ejidatarios.excel',[
            'ejidatarios' => $ejidatarios
        ]);
    }
}
