<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
Schema::disableForeignKeyConstraints();
            $table->Increments("id");
            $table->string('name');
            $table->string('apPaterno');
            $table->string('apMaterno');
            $table->unsignedInteger('Id_rol');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->foreign('Id_rol')->references('Id_rol')->on('roles');
            $table->string('password');
            $table->rememberToken();
            $table->softDeletes(); //Nueva línea, para el borrado lógico

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('users');
         DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
