<!DOCTYPE html>
<html lang="en" class="wf-flaticon-n4-inactive wf-lato-n7-active wf-lato-n4-active wf-lato-n9-active wf-lato-n3-active wf-fontawesome5solid-n4-active wf-fontawesome5regular-n4-active wf-fontawesome5brands-n4-active wf-simplelineicons-n4-active wf-active"><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252"><style type="text/css">.swal-icon--error{border-color:#f27474;-webkit-animation:animateErrorIcon .5s;animation:animateErrorIcon .5s}.swal-icon--error__x-mark{position:relative;display:block;-webkit-animation:animateXMark .5s;animation:animateXMark .5s}.swal-icon--error__line{position:absolute;height:5px;width:47px;background-color:#f27474;display:block;top:37px;border-radius:2px}.swal-icon--error__line--left{-webkit-transform:rotate(45deg);transform:rotate(45deg);left:17px}.swal-icon--error__line--right{-webkit-transform:rotate(-45deg);transform:rotate(-45deg);right:16px}@-webkit-keyframes animateErrorIcon{0%{-webkit-transform:rotateX(100deg);transform:rotateX(100deg);opacity:0}to{-webkit-transform:rotateX(0deg);transform:rotateX(0deg);opacity:1}}@keyframes animateErrorIcon{0%{-webkit-transform:rotateX(100deg);transform:rotateX(100deg);opacity:0}to{-webkit-transform:rotateX(0deg);transform:rotateX(0deg);opacity:1}}@-webkit-keyframes animateXMark{0%{-webkit-transform:scale(.4);transform:scale(.4);margin-top:26px;opacity:0}50%{-webkit-transform:scale(.4);transform:scale(.4);margin-top:26px;opacity:0}80%{-webkit-transform:scale(1.15);transform:scale(1.15);margin-top:-6px}to{-webkit-transform:scale(1);transform:scale(1);margin-top:0;opacity:1}}@keyframes animateXMark{0%{-webkit-transform:scale(.4);transform:scale(.4);margin-top:26px;opacity:0}50%{-webkit-transform:scale(.4);transform:scale(.4);margin-top:26px;opacity:0}80%{-webkit-transform:scale(1.15);transform:scale(1.15);margin-top:-6px}to{-webkit-transform:scale(1);transform:scale(1);margin-top:0;opacity:1}}.swal-icon--warning{border-color:#f8bb86;-webkit-animation:pulseWarning .75s infinite alternate;animation:pulseWarning .75s infinite alternate}.swal-icon--warning__body{width:5px;height:47px;top:10px;border-radius:2px;margin-left:-2px}.swal-icon--warning__body,.swal-icon--warning__dot{position:absolute;left:50%;background-color:#f8bb86}.swal-icon--warning__dot{width:7px;height:7px;border-radius:50%;margin-left:-4px;bottom:-11px}@-webkit-keyframes pulseWarning{0%{border-color:#f8d486}to{border-color:#f8bb86}}@keyframes pulseWarning{0%{border-color:#f8d486}to{border-color:#f8bb86}}.swal-icon--success{border-color:#a5dc86}.swal-icon--success:after,.swal-icon--success:before{content:"";border-radius:50%;position:absolute;width:60px;height:120px;background:#fff;-webkit-transform:rotate(45deg);transform:rotate(45deg)}.swal-icon--success:before{border-radius:120px 0 0 120px;top:-7px;left:-33px;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);-webkit-transform-origin:60px 60px;transform-origin:60px 60px}.swal-icon--success:after{border-radius:0 120px 120px 0;top:-11px;left:30px;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);-webkit-transform-origin:0 60px;transform-origin:0 60px;-webkit-animation:rotatePlaceholder 4.25s ease-in;animation:rotatePlaceholder 4.25s ease-in}.swal-icon--success__ring{width:80px;height:80px;border:4px solid hsla(98,55%,69%,.2);border-radius:50%;box-sizing:content-box;position:absolute;left:-4px;top:-4px;z-index:2}.swal-icon--success__hide-corners{width:5px;height:90px;background-color:#fff;padding:1px;position:absolute;left:28px;top:8px;z-index:1;-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}.swal-icon--success__line{height:5px;background-color:#a5dc86;display:block;border-radius:2px;position:absolute;z-index:2}.swal-icon--success__line--tip{width:25px;left:14px;top:46px;-webkit-transform:rotate(45deg);transform:rotate(45deg);-webkit-animation:animateSuccessTip .75s;animation:animateSuccessTip .75s}.swal-icon--success__line--long{width:47px;right:8px;top:38px;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);-webkit-animation:animateSuccessLong .75s;animation:animateSuccessLong .75s}@-webkit-keyframes rotatePlaceholder{0%{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}5%{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}12%{-webkit-transform:rotate(-405deg);transform:rotate(-405deg)}to{-webkit-transform:rotate(-405deg);transform:rotate(-405deg)}}@keyframes rotatePlaceholder{0%{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}5%{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}12%{-webkit-transform:rotate(-405deg);transform:rotate(-405deg)}to{-webkit-transform:rotate(-405deg);transform:rotate(-405deg)}}@-webkit-keyframes animateSuccessTip{0%{width:0;left:1px;top:19px}54%{width:0;left:1px;top:19px}70%{width:50px;left:-8px;top:37px}84%{width:17px;left:21px;top:48px}to{width:25px;left:14px;top:45px}}@keyframes animateSuccessTip{0%{width:0;left:1px;top:19px}54%{width:0;left:1px;top:19px}70%{width:50px;left:-8px;top:37px}84%{width:17px;left:21px;top:48px}to{width:25px;left:14px;top:45px}}@-webkit-keyframes animateSuccessLong{0%{width:0;right:46px;top:54px}65%{width:0;right:46px;top:54px}84%{width:55px;right:0;top:35px}to{width:47px;right:8px;top:38px}}@keyframes animateSuccessLong{0%{width:0;right:46px;top:54px}65%{width:0;right:46px;top:54px}84%{width:55px;right:0;top:35px}to{width:47px;right:8px;top:38px}}.swal-icon--info{border-color:#c9dae1}.swal-icon--info:before{width:5px;height:29px;bottom:17px;border-radius:2px;margin-left:-2px}.swal-icon--info:after,.swal-icon--info:before{content:"";position:absolute;left:50%;background-color:#c9dae1}.swal-icon--info:after{width:7px;height:7px;border-radius:50%;margin-left:-3px;top:19px}.swal-icon{width:80px;height:80px;border-width:4px;border-style:solid;border-radius:50%;padding:0;position:relative;box-sizing:content-box;margin:20px auto}.swal-icon:first-child{margin-top:32px}.swal-icon--custom{width:auto;height:auto;max-width:100%;border:none;border-radius:0}.swal-icon img{max-width:100%;max-height:100%}.swal-title{color:rgba(0,0,0,.65);font-weight:600;text-transform:none;position:relative;display:block;padding:13px 16px;font-size:27px;line-height:normal;text-align:center;margin-bottom:0}.swal-title:first-child{margin-top:26px}.swal-title:not(:first-child){padding-bottom:0}.swal-title:not(:last-child){margin-bottom:13px}.swal-text{font-size:16px;position:relative;float:none;line-height:normal;vertical-align:top;text-align:left;display:inline-block;margin:0;padding:0 10px;font-weight:400;color:rgba(0,0,0,.64);max-width:calc(100% - 20px);overflow-wrap:break-word;box-sizing:border-box}.swal-text:first-child{margin-top:45px}.swal-text:last-child{margin-bottom:45px}.swal-footer{text-align:right;padding-top:13px;margin-top:13px;padding:13px 16px;border-radius:inherit;border-top-left-radius:0;border-top-right-radius:0}.swal-button-container{margin:5px;display:inline-block;position:relative}.swal-button{background-color:#7cd1f9;color:#fff;border:none;box-shadow:none;border-radius:5px;font-weight:600;font-size:14px;padding:10px 24px;margin:0;cursor:pointer}.swal-button[not:disabled]:hover{background-color:#78cbf2}.swal-button:active{background-color:#70bce0}.swal-button:focus{outline:none;box-shadow:0 0 0 1px #fff,0 0 0 3px rgba(43,114,165,.29)}.swal-button[disabled]{opacity:.5;cursor:default}.swal-button::-moz-focus-inner{border:0}.swal-button--cancel{color:#555;background-color:#efefef}.swal-button--cancel[not:disabled]:hover{background-color:#e8e8e8}.swal-button--cancel:active{background-color:#d7d7d7}.swal-button--cancel:focus{box-shadow:0 0 0 1px #fff,0 0 0 3px rgba(116,136,150,.29)}.swal-button--danger{background-color:#e64942}.swal-button--danger[not:disabled]:hover{background-color:#df4740}.swal-button--danger:active{background-color:#cf423b}.swal-button--danger:focus{box-shadow:0 0 0 1px #fff,0 0 0 3px rgba(165,43,43,.29)}.swal-content{padding:0 20px;margin-top:20px;font-size:medium}.swal-content:last-child{margin-bottom:20px}.swal-content__input,.swal-content__textarea{-webkit-appearance:none;background-color:#fff;border:none;font-size:14px;display:block;box-sizing:border-box;width:100%;border:1px solid rgba(0,0,0,.14);padding:10px 13px;border-radius:2px;transition:border-color .2s}.swal-content__input:focus,.swal-content__textarea:focus{outline:none;border-color:#6db8ff}.swal-content__textarea{resize:vertical}.swal-button--loading{color:transparent}.swal-button--loading~.swal-button__loader{opacity:1}.swal-button__loader{position:absolute;height:auto;width:43px;z-index:2;left:50%;top:50%;-webkit-transform:translateX(-50%) translateY(-50%);transform:translateX(-50%) translateY(-50%);text-align:center;pointer-events:none;opacity:0}.swal-button__loader div{display:inline-block;float:none;vertical-align:baseline;width:9px;height:9px;padding:0;border:none;margin:2px;opacity:.4;border-radius:7px;background-color:hsla(0,0%,100%,.9);transition:background .2s;-webkit-animation:swal-loading-anim 1s infinite;animation:swal-loading-anim 1s infinite}.swal-button__loader div:nth-child(3n+2){-webkit-animation-delay:.15s;animation-delay:.15s}.swal-button__loader div:nth-child(3n+3){-webkit-animation-delay:.3s;animation-delay:.3s}@-webkit-keyframes swal-loading-anim{0%{opacity:.4}20%{opacity:.4}50%{opacity:1}to{opacity:.4}}@keyframes swal-loading-anim{0%{opacity:.4}20%{opacity:.4}50%{opacity:1}to{opacity:.4}}.swal-overlay{position:fixed;top:0;bottom:0;left:0;right:0;text-align:center;font-size:0;overflow-y:auto;background-color:rgba(0,0,0,.4);z-index:10000;pointer-events:none;opacity:0;transition:opacity .3s}.swal-overlay:before{content:" ";display:inline-block;vertical-align:middle;height:100%}.swal-overlay--show-modal{opacity:1;pointer-events:auto}.swal-overlay--show-modal .swal-modal{opacity:1;pointer-events:auto;box-sizing:border-box;-webkit-animation:showSweetAlert .3s;animation:showSweetAlert .3s;will-change:transform}.swal-modal{width:478px;opacity:0;pointer-events:none;background-color:#fff;text-align:center;border-radius:5px;position:static;margin:20px auto;display:inline-block;vertical-align:middle;-webkit-transform:scale(1);transform:scale(1);-webkit-transform-origin:50% 50%;transform-origin:50% 50%;z-index:10001;transition:opacity .2s,-webkit-transform .3s;transition:transform .3s,opacity .2s;transition:transform .3s,opacity .2s,-webkit-transform .3s}@media (max-width:500px){.swal-modal{width:calc(100% - 20px)}}@-webkit-keyframes showSweetAlert{0%{-webkit-transform:scale(1);transform:scale(1)}1%{-webkit-transform:scale(.5);transform:scale(.5)}45%{-webkit-transform:scale(1.05);transform:scale(1.05)}80%{-webkit-transform:scale(.95);transform:scale(.95)}to{-webkit-transform:scale(1);transform:scale(1)}}@keyframes showSweetAlert{0%{-webkit-transform:scale(1);transform:scale(1)}1%{-webkit-transform:scale(.5);transform:scale(.5)}45%{-webkit-transform:scale(1.05);transform:scale(1.05)}80%{-webkit-transform:scale(.95);transform:scale(.95)}to{-webkit-transform:scale(1);transform:scale(1)}}</style>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title></title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="../assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	
	<link rel="stylesheet" href="../assets/css/altantis2.css">
	<link rel="stylesheet" href="../assets/css/loader.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="../assets/css/demo.css">

	<!-- SELECT2 Buscar en select -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

		<!-- SELECT2 Buscar en select -->
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />-->

		<!--select picker
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
		-->
	<!--select picker
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	-->
</head>
<body>

	
	<div class="wrapper">

	<div class="main-header" data-background-color="green">
			<div class="nav-top">
				<div class="container d-flex flex-row">
					<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon">
							<i class="icon-menu"></i>
						</span>
					</button>
					<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
					<!-- Logo Header -->
					<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/index.html" class="logo d-flex align-items-center">
						<img src="./Atlantis Bootstrap 4 Admin Dashboard_files/logo.svg" alt="navbar brand" class="navbar-brand">
					</a>
					<!-- End Logo Header -->

					<!-- Navbar Header -->
					<nav class="navbar navbar-header navbar-expand-lg p-0">

						<div class="container-fluid p-0">
							<div class="collapse" id="search-nav">
								<form class="navbar-left navbar-form nav-search ml-md-3">
									<div class="input-group">
										<div class="input-group-prepend">
											<button type="submit" class="btn btn-search pr-1">
												<i class="fa fa-search search-icon"></i>
											</button>
										</div>
										<input type="text" placeholder="Search ..." class="form-control">
									</div>
								</form>
							</div>
							<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
								<li class="nav-item toggle-nav-search hidden-caret">
									<a class="nav-link" data-toggle="collapse" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
										<i class="fa fa-search"></i>
									</a>
								</li>
								<li class="nav-item dropdown hidden-caret">
									<a class="nav-link dropdown-toggle" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-envelope"></i>
									</a>
									<ul class="dropdown-menu messages-notif-box animated fadeIn" aria-labelledby="messageDropdown">
										<li>
											<div class="dropdown-title d-flex justify-content-between align-items-center">
												Messages 									
												<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#" class="small">Mark all as read</a>
											</div>
										</li>
										<li>
											<div class="scroll-wrapper message-notif-scroll scrollbar-outer" style="position: relative;"><div class="message-notif-scroll scrollbar-outer scroll-content" style="height: auto; margin-bottom: 0px; margin-right: 0px; max-height: 0px;">
												<div class="notif-center">
													<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="notif-img"> 
															<img src="./Atlantis Bootstrap 4 Admin Dashboard_files/jm_denis.jpg" alt="Img Profile">
														</div>
														<div class="notif-content">
															<span class="subject">Jimmy Denis</span>
															<span class="block">
																How are you ?
															</span>
															<span class="time">5 minutes ago</span> 
														</div>
													</a>
													<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="notif-img"> 
															<img src="./Atlantis Bootstrap 4 Admin Dashboard_files/chadengle.jpg" alt="Img Profile">
														</div>
														<div class="notif-content">
															<span class="subject">Chad</span>
															<span class="block">
																Ok, Thanks !
															</span>
															<span class="time">12 minutes ago</span> 
														</div>
													</a>
													<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="notif-img"> 
															<img src="./Atlantis Bootstrap 4 Admin Dashboard_files/mlane.jpg" alt="Img Profile">
														</div>
														<div class="notif-content">
															<span class="subject">Jhon Doe</span>
															<span class="block">
																Ready for the meeting today...
															</span>
															<span class="time">12 minutes ago</span> 
														</div>
													</a>
													<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="notif-img"> 
															<img src="./Atlantis Bootstrap 4 Admin Dashboard_files/talha.jpg" alt="Img Profile">
														</div>
														<div class="notif-content">
															<span class="subject">Talha</span>
															<span class="block">
																Hi, Apa Kabar ?
															</span>
															<span class="time">17 minutes ago</span> 
														</div>
													</a>
												</div>
											</div><div class="scroll-element scroll-x" style=""><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar ui-draggable ui-draggable-handle"></div></div></div><div class="scroll-element scroll-y" style=""><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar ui-draggable ui-draggable-handle"></div></div></div></div>
										</li>
										<li>
											<a class="see-all" href="javascript:void(0);">See all messages<i class="fa fa-angle-right"></i> </a>
										</li>
									</ul>
								</li>
								<li class="nav-item dropdown hidden-caret">
									<a class="nav-link dropdown-toggle" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-bell"></i>
										<span class="notification">4</span>
									</a>
									<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
										<li>
											<div class="dropdown-title">You have 4 new notification</div>
										</li>
										<li>
											<div class="scroll-wrapper notif-scroll scrollbar-outer" style="position: relative;"><div class="notif-scroll scrollbar-outer scroll-content" style="height: auto; margin-bottom: 0px; margin-right: 0px; max-height: 0px;">
												<div class="notif-center">
													<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
														<div class="notif-content">
															<span class="block">
																New user registered
															</span>
															<span class="time">5 minutes ago</span> 
														</div>
													</a>
													<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="notif-icon notif-success"> <i class="fa fa-comment"></i> </div>
														<div class="notif-content">
															<span class="block">
																Rahmad commented on Admin
															</span>
															<span class="time">12 minutes ago</span> 
														</div>
													</a>
													<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="notif-img"> 
															<img src="./Atlantis Bootstrap 4 Admin Dashboard_files/profile2.jpg" alt="Img Profile">
														</div>
														<div class="notif-content">
															<span class="block">
																Reza send messages to you
															</span>
															<span class="time">12 minutes ago</span> 
														</div>
													</a>
													<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="notif-icon notif-danger"> <i class="fa fa-heart"></i> </div>
														<div class="notif-content">
															<span class="block">
																Farrah liked Admin
															</span>
															<span class="time">17 minutes ago</span> 
														</div>
													</a>
												</div>
											</div><div class="scroll-element scroll-x" style=""><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar ui-draggable ui-draggable-handle"></div></div></div><div class="scroll-element scroll-y" style=""><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar ui-draggable ui-draggable-handle"></div></div></div></div>
										</li>
										<li>
											<a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
										</li>
									</ul>
								</li>
								<li class="nav-item dropdown hidden-caret">
									<a class="nav-link" data-toggle="dropdown" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#" aria-expanded="false">
										<i class="fas fa-layer-group"></i>
									</a>
									<div class="dropdown-menu quick-actions quick-actions-info animated fadeIn">
										<div class="quick-actions-header">
											<span class="title mb-1">Quick Actions</span>
											<span class="subtitle op-8">Shortcuts</span>
										</div>
										<div class="scroll-wrapper quick-actions-scroll scrollbar-outer" style="position: relative;"><div class="quick-actions-scroll scrollbar-outer scroll-content" style="height: 244.965px; margin-bottom: 0px; margin-right: 0px; max-height: none;">
											<div class="quick-actions-items">
												<div class="row m-0">
													<a class="col-6 col-md-4 p-0" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="quick-actions-item">
															<div class="avatar-item bg-danger rounded-circle">
																<i class="far fa-calendar-alt"></i>
															</div>
															<span class="text">Calendar</span>
														</div>
													</a>
													<a class="col-6 col-md-4 p-0" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="quick-actions-item">
															<div class="avatar-item bg-warning rounded-circle">
																<i class="fas fa-map"></i>
															</div>
															<span class="text">Maps</span>
														</div>
													</a>
													<a class="col-6 col-md-4 p-0" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="quick-actions-item">
															<div class="avatar-item bg-info rounded-circle">
																<i class="fas fa-file-excel"></i>
															</div>
															<span class="text">Reports</span>
														</div>
													</a>
													<a class="col-6 col-md-4 p-0" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="quick-actions-item">
															<div class="avatar-item bg-success rounded-circle">
																<i class="fas fa-envelope"></i>
															</div>
															<span class="text">Emails</span>
														</div>
													</a>
													<a class="col-6 col-md-4 p-0" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="quick-actions-item">
															<div class="avatar-item bg-primary rounded-circle">
																<i class="fas fa-file-invoice-dollar"></i>
															</div>
															<span class="text">Invoice</span>
														</div>
													</a>
													<a class="col-6 col-md-4 p-0" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
														<div class="quick-actions-item">
															<div class="avatar-item bg-secondary rounded-circle">
																<i class="fas fa-credit-card"></i>
															</div>
															<span class="text">Payments</span>
														</div>
													</a>
												</div>
											</div>
										</div><div class="scroll-element scroll-x" style=""><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar ui-draggable ui-draggable-handle" style="width: 100px;"></div></div></div><div class="scroll-element scroll-y" style=""><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar ui-draggable ui-draggable-handle" style="height: 100px;"></div></div></div></div>
									</div>
								</li>
								<li class="nav-item">
									<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#" class="nav-link quick-sidebar-toggler">
										<i class="fa fa-th"></i>
									</a>
								</li>
								<li class="nav-item dropdown hidden-caret">
									<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#" aria-expanded="false">
										<div class="avatar-sm">
											<img src="./Atlantis Bootstrap 4 Admin Dashboard_files/profile.jpg" alt="..." class="avatar-img rounded-circle">
										</div>
									</a>
									<ul class="dropdown-menu dropdown-user animated fadeIn">
										<div class="scroll-wrapper dropdown-user-scroll scrollbar-outer" style="position: relative;"><div class="dropdown-user-scroll scrollbar-outer scroll-content" style="height: 269.219px; margin-bottom: 0px; margin-right: 0px; max-height: none;">
											<li>
												<div class="user-box">
													<div class="avatar-lg"><img src="./Atlantis Bootstrap 4 Admin Dashboard_files/profile.jpg" alt="image profile" class="avatar-img rounded"></div>
													<div class="u-text">
														<h4>Hizrian</h4>
														<p class="text-muted">hello@example.com</p><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/profile.html" class="btn btn-xs btn-secondary btn-sm">View Profile</a>
													</div>
												</div>
											</li>
											<li>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">My Profile</a>
												<a class="dropdown-item" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">My Balance</a>
												<a class="dropdown-item" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Inbox</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Account Setting</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Logout</a>
											</li>
										</div><div class="scroll-element scroll-x" style=""><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar ui-draggable ui-draggable-handle" style="width: 100px;"></div></div></div><div class="scroll-element scroll-y" style=""><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar ui-draggable ui-draggable-handle" style="height: 100px;"></div></div></div></div>
									</ul>
								</li>
							</ul>
						</div>
					</nav>
					<!-- End Navbar -->
				</div>
			</div>
			<div class="nav-bottom bg-white">
				<h3 class="title-menu d-flex d-lg-none"> 
					Menu 
					<div class="close-menu"> <i class="flaticon-cross"></i></div>
				</h3>
				<div class="container d-flex flex-row">
					<ul class="nav page-navigation page-navigation-secondary">
						@if(Auth::user()->role->descripcion != "Tesorero")
							<li class="nav-item submenu">
								<a class="nav-link" href="{{route('usuario')}}">
									<i class="link-icon fas fa-users"></i>
									<span class="menu-title">Usuarios</span>
								</a>
							</li>
							<li class="nav-item submenu">
								<a class="nav-link" href="{{route('canalero')}}">
									<i class="link-icon fas fa-fill-drip"></i>
									<span class="menu-title">Canaleros</span>
								</a>
							</li>
						@endif
						<li class="nav-item submenu">
							<a class="nav-link" href="{{route('ejidatario')}}">
								<i class="link-icon icon-people"></i>
								<span class="menu-title">Ejidatarios</span>
							</a>
						</li>
							<li class="nav-item submenu">
								<a data-toggle="collapse" href="#sidebarLayouts">
									<i class="icon-map"></i>
									<p>Ejidos</p>
									<span class="caret"></span>
								</a>
								<div class="collapse" id="sidebarLayouts">
									<ul class="nav nav-collapse">
										<li>
											@if(Auth::user()->role->descripcion != "Tesorero")
												<a href="{{route('ejido')}}">
													<span class="sub-item">Registro de Ejidos</span>
												</a>
											@endif
											<a href="{{route('parcela')}}">
												<span class="sub-item">Registro de Parcelas</span>
											</a>
											@if(Auth::user()->role->descripcion != "Tesorero")
												<a href="{{route('verConagua')}}">
													<span class="sub-item">Registro de CONAGUA</span>
												</a>
											@endif
										</li>
									</ul>
								</div>
							</li>
						<li class="nav-item submenu">
							<a class="nav-link" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
								<i class="link-icon icon-grid"></i>
								<span class="menu-title">Apps</span>
							</a>
							<div class="navbar-dropdown animated fadeIn">
								<ul>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/boards.html">Boards</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/projects.html">Projects</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/email-inbox.html">Email Inbox</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/email-detail.html">Email Detail</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/email-compose.html">Email Inbox</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/messages.html">Messages</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/conversations.html">Conversations</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item submenu">
							<a class="nav-link" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
								<i class="link-icon icon-disc"></i>
								<span class="menu-title">Finance</span>
							</a>
							<div class="navbar-dropdown animated fadeIn">
								<ul>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">Annual Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">HR Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">Finance Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">Revenue Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">IPO Report</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item submenu mega-menu dropdown">
							<a class="nav-link" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
								<i class="link-icon icon-film"></i>
								<span class="menu-title">Project</span>
							</a>
							<div class="navbar-dropdown animated fadeIn">
								<div class="col-group-wrapper row">
									<div class="col-group col-md-4">
										<div class="row">
											<div class="col-12">
												<p class="category-heading">Basic Elements</p>
												<div class="submenu-item">
													<div class="row">
														<div class="col-md-6">
															<ul>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Accordion</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Buttons</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Badges</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Breadcrumbs</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Dropdown</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Modals</a></li>
															</ul>
														</div>
														<div class="col-md-6">
															<ul>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Progress bar</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Pagination</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Tabs</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Typography</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Tooltip</a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-group col-md-4">
										<div class="row">
											<div class="col-12">
												<p class="category-heading">Advanced Elements</p>
												<div class="submenu-item">
													<div class="row">
														<div class="col-md-6">
															<ul>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Datatables</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Carousel</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Clipboard</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Chart.js</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Loader</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Slider</a></li>
															</ul>
														</div>
														<div class="col-md-6">
															<ul>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Popup</a></li>
																<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Notification</a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-group col-md-4">
										<p class="category-heading">Icons</p>
										<ul class="submenu-item">
											<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Flaticons</a></li>
											<li><a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">Font Awesome</a></li>
											<li><a class="3">Simple Line Icons</a></li>
										</ul>
									</div>
								</div>
							</div>
						</li>
						<li class="nav-item submenu">
							<a class="nav-link" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
								<i class="link-icon icon-book-open"></i>
								<span class="menu-title">HR</span>
							</a>
							<div class="navbar-dropdown animated fadeIn">
								<ul>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">Annual Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">HR Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">Finance Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">Revenue Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">IPO Report</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item submenu show-submenu">
							<a class="nav-link" href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/#">
								<i class="link-icon icon-pie-chart"></i>
								<span class="menu-title">Revenue</span>
							</a>
							<div class="navbar-dropdown animated fadeIn">
								<ul>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">Annual Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">HR Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">Finance Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">Revenue Report</a>
									</li>
									<li>
										<a href="http://demo.themekita.com/atlantis/livepreview/examples/demo5/starter-template.html">IPO Report</a>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="../assets/img/fondo1.jpg"class="avatar-img rounded-circle"/>

						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									{{Auth::user()->name}}
									<span class="user-level">{{Auth::user()->role->descripcion}}</span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="#profile">
											<span class="link-collapse">Perfil</span>
										</a>
									</li>
									<li>
										<a href="#edit">
											<span class="link-collapse">Editar Perfil</span>
										</a>
									</li>
								{{--	<li>
										<a href="#settings">
											<span class="link-collapse">Configuración</span>
										</a>
									</li>--}}
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-success">
						@if(Auth::user()->role->descripcion != "Tesorero")
							<li class="nav-item ">
								<a href="{{route('usuario')}}">
									<i class="fas fa-users"></i>
									<p>Usuarios</p>
								</a>
							</li>
							<li class="nav-item ">
								<a href="{{route('canalero')}}">
									<i class="fas fa-fill-drip"></i>
									<p>Canaleros</p>
								</a>
							</li>
						@endif
							<li class="nav-item ">
								<a href="{{route('ejidatario')}}">
									<i class="icon-people"></i>
									<p>Ejidatarios</p>
								</a>
							</li>
							<li class="nav-item ">
								<a data-toggle="collapse" href="#sidebarLayouts">
									<i class="icon-map"></i>
									<p>Ejidos</p>
									<span class="caret"></span>
								</a>
								<div class="collapse" id="sidebarLayouts">
									<ul class="nav nav-collapse">
										<li>
											@if(Auth::user()->role->descripcion != "Tesorero")
												<a href="{{route('ejido')}}">
													<span class="sub-item">Registro de Ejidos</span>
												</a>
											@endif
											<a href="{{route('parcela')}}">
												<span class="sub-item">Registro de Parcelas</span>
											</a>
											@if(Auth::user()->role->descripcion != "Tesorero")
												<a href="{{route('verConagua')}}">
													<span class="sub-item">Registro de CONAGUA</span>
												</a>
											@endif
										</li>
									</ul>
								</div>
							</li>
						@if(Auth::user()->role->descripcion!="Tesorero")
							<li class="nav-item ">
								<a href="{{route('cultivo')}}">
									<i class="icon-drop"></i>
									<p>Cultivos</p>
								</a>
							</li>
						@endif

						@if(Auth::user()->role->descripcion!="Administrador")
							<li class="nav-item ">
								<a data-toggle="collapse" href="#tables">
									<i class="icon-tag"></i>
									<p>Tesoreria</p>
									<span class="caret"></span>
								</a>
								<div class="collapse" id="tables">
									<ul class="nav nav-collapse">
										<li>
											<a href="tables/tables.html">
												<span class="sub-item">Egresos</span>
											</a>
										</li>
										<li>
											<a href="maps/jqvmap.html">
												<span class="sub-item">Ingresos</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
						@endif

							<li class="nav-item ">
								<a data-toggle="collapse" href="#charts">
									<i class="icon-basket"></i>
									<p>Caja</p>
									<span class="caret"></span>
								</a>
								<div class="collapse" id="charts">
									<ul class="nav nav-collapse">
										<li>
											<a href="{{route('verfact')}}">
												<span class="sub-item">Catálogo de Facturas</span>
											</a>
										</li>
										<li>
											<a href="{{route('ingresos')}}">
												<span class="sub-item">Ingresos</span>
											</a>
										</li>
										<li>
											<a href="{{route('egresos')}}">
												<span class="sub-item">Egresos</span>
											</a>
										</li>
										@if(Auth::user()->role->descripcion!="Tesorero")
											<li>
												<a href="{{route('concepto')}}">
													<span class="sub-item">Conceptos</span>
												</a>
											</li>

											<li>
												<a href="{{route('factura')}}">
													<span class="sub-item">Captura de Factura</span>
												</a>
											</li>
										@endif

									</ul>
								</div>
							</li>
								@if(Auth::user()->role->descripcion!="Tesorero")
								<li class="nav-item ">
									<a href="{{route('deudores')}}">
										<i class="flaticon-coins"></i>
										<p>Deudores</p>
									</a>
								</li>
								<li class="nav-item ">
									<a href="{{route('bitacora')}}">
										<i class="flaticon-file-1"></i>
										<p>Bitácora</p>
									</a>
								</li>
								@endif
				</div>
			</div>
		</div>

		 <div class="loader-wrapper">
			<div class="loader1"><span class="loader-inner"></span></div>
		</div>
		@yield('content')
		<!-- End Custom template -->
	</div>

	<!--   Core JS Files   -->
	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="../assets/js/core/popper.min.js"></script>
	<script src="../assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="../assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="../assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>

	<!-- Chart JS -->
	<script src="../assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="../assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Datatables -->
	<script src="../assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<script src="../assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

	<!-- Sweet Alert -->
	<script src="../assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Atlantis JS -->
	<script src="../assets/js/atlantis.min.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="../assets/js/setting-demo.js"></script>
	<script src="../assets/js/demo.js"></script>
	<!-- select picker
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
	-->
	<!-- SELECT2 Buscar en select -->
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>



	<!-- select picker
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
	-->
	<!-- SELECT2 Buscar en select -->
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

	<script>
	// funcion para generar una notificacion
	function mensaje(mensaje,tipo,ubicacion,titulo){
		var placementFrom = ubicacion;
		var placementAlign = "right";
		var state =tipo ;
		var content = {};
		content.message = mensaje;
		content.title = titulo;
		content.icon = 'flaticon-add';
		content.url = "{{route('cultivo')}}";
		content.target = '_blank';
		$.notify(content,{
			type: state,
			placement: {
				from: placementFrom,
				align: placementAlign
			},
			time: 50,
			delay: 10,
		});
	}

	$(window).on("load",function(){
		$(".loader-wrapper").fadeOut("slow");
	});

	$(document).ready(function() {
			$('#basic-datatables').DataTable({
			});

			$('#multi-filter-select').DataTable( {
				"pageLength": 5,
				initComplete: function () {
					this.api().columns().every( function () {
						var column = this;
						var select = $('<select class="form-control"><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
								);

							column
							.search( val ? '^'+val+'$' : '', true, false )
							.draw();
						} );

						column.data().unique().sort().each( function ( d, j ) {
							select.append( '<option value="'+d+'">'+d+'</option>' )
						} );
					} );
				}
			});

			$('.js-example-basic-single').select2();
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});

			var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

			// SELECT2 Buscar en select
			$('.js-example-basic-single').select2();

		});
	</script>

</body>
</html>
