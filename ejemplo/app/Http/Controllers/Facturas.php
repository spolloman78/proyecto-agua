<?php

namespace App\Http\Controllers;
use App\usuario;
use App\User;
use App\factura;
use App\ciclo;
use App\parcela;
use App\bitacora;
use App\cultivo;
use App\horario;
use App\abonos;
use App\facturaconcepto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
use App\Exports\FacturasExport;


class Facturas extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

    }

    public function __construct()
    {
      $this->middleware('auth');
      $this->middleware('admin')->except('verfacturar');
    }

    public function eliminar($id){
      factura::find($id)->delete();
    }

    public function nuevaFactura()
    {
        $ejidatario=DB::table('ejidatario')
            ->select('*')
            ->get();
        $noFacturas = DB::table('factura')->count();
        $now = now()->format('d-m-y');
        $concepto=DB::table('concepto')->select('Id_Concepto as concepto')->where('tipo', '!=', 'MAQ')->get();
        $conceptoMaq=DB::table('concepto')->select('Id_Concepto as conceptoMaq')->where('tipo', '=', 'MAQ')->get();
        $cultivo=DB::table('cultivo')->select('Id_cultivo','descripcion as descripcion')->get();
        $ciclo = DB::table('ciclo')->select('Ciclo as ciclo')->get();

      return view('Facturas.factura',compact('ejidatario', 'noFacturas', 'now','concepto','conceptoMaq', 'cultivo', 'ciclo'));


    }
    public function crear(Request $request){
      $factura=factura::find($request->folio);
      $cultivo=cultivo::find($request->cultivo);
      $cantidadCultivo=$request->cantidadc;
      $capacidad=$cultivo->cantidad_cultivo;

      if($factura){
        $con=DB::table('factura_concepto')->where('Id_parcela','=',$request->parcela)
        ->where('Id_factura','=',$request->folio)
        ->where('Id_cultivo','=',$request->cultivo)->get();

        $concepto=new facturaconcepto();
        $concepto->Id_factura=$request->folio;
        $concepto->Id_Concepto=$request->concepto;
        $concepto->sup_cultivo=$request->sup_cultivo;
        $concepto->can_pagada=0;
        $concepto->Total=$request->total;
        $concepto->Id_parcela=$request->parcela;
        $concepto->Id_cultivo=$request->cultivo;
        $concepto->cantidadC=1;
        $concepto->descripcionM=$request->descripcionMaq;
        $concepto->save();
        if( $con==true || $cantidadCultivo>$capacidad || $capacidad==0){
           if($cantidadCultivo>$capacidad){
             echo"Error";}
        }else{
          $total=$capacidad-$cantidadCultivo;
          $cultivo->cantidad_cultivo=$total;
          $cultivo->save();
        }
      }else{
        $facturaNueva=new factura();
        $facturaNueva->Id_factura=$request->folio;
        $facturaNueva->Id_status_factura=1;
        $facturaNueva->Id_ciclo=3;
        $facturaNueva->Id_ejidatario=$request->ejidatario;
        $facturaNueva->save();
        $concepto=new facturaconcepto();
        $concepto->Id_factura=$request->folio;
        $concepto->Id_Concepto=$request->concepto;
        $concepto->sup_cultivo=$request->sup_cultivo;
        $concepto->can_pagada=0;
        $concepto->Total=$request->total;
        $concepto->cantidadC=1;
        $concepto->descripcionM=$request->descripcionMaq;
        $concepto->Id_parcela=$request->parcela;
        $concepto->Id_cultivo=$request->cultivo;
        $concepto->save();

        if($cantidadCultivo>$capacidad){

        }else{
          $total=$capacidad-$cantidadCultivo;
          $cultivo->cantidad_cultivo=$total;
          $cultivo->save();
        }
      }
    }
    public function eliminarconcepto(Request $request){
      $factura=DB::table('factura_concepto')->where('Id_parcela','=',$request->parcela)
      ->where('Id_factura','=',$request->folio)
      ->where('Id_Concepto','=',$request->concepto)->delete();

    /*$cultivo=cultivo::find($request->cultivo);
      $cantidadCultivo=$request->cantidadc;
      $capacidad=$cultivo->cantidad_cultivo;
      $total=$capacidad+$cantidadCultivo;
      $cultivo->cantidad_cultivo=$total;
      $cultivo->save();*/

    }
    public function pago(){

    }

    public function verfacturar(){
       $now = now()->format('Y-m-d');
      $factura=DB::table('factura')
      ->join('ejidatario', 'ejidatario.Id_ejidatario', '=', 'factura.Id_ejidatario')
      ->join('factura_concepto','factura_concepto.Id_factura','=','factura.Id_factura')
      ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
      ->join('canalero','canalero.Id_canalero','=','parcela.Id_canalero')
      ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->where('ciclo.Ciclo','=','19/20')
      ->select('ciclo.Ciclo as ciclo',DB::raw('SUM(factura_concepto.Total) as total'),'factura.Id_status_factura as status','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','factura.Id_factura','canalero.nombre as canalero','factura.created_at','status_factura.descripcionEstado as status_fact',"factura_concepto.descripcionM")
      ->where("status_factura.Id_status_factura",'!=','2')->where("status_factura.Id_status_factura",'!=','4')
      ->groupBy('factura_concepto.descripcionM','factura_concepto.Id_factura','ciclo.Ciclo','factura.Id_status_factura','parcela.cuenta','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','factura.Id_factura','canalero.nombre','factura.created_at','status_factura.descripcionEstado')->get();

      $ciclos=ciclo::all();

    return view('Facturas.verFactura',compact("now",'factura','ciclos'));
    }
    public function modificar(Request $request){
    $datos = factura::find($request->Id_factura);
    $factura=DB::table('factura_concepto')->where('Id_factura','=',$request->Id_factura)->get();
    $datos->Id_status_factura=$request->estado;
    $datos->save();
    foreach ($factura as $key) {
DB::table("horario")->where('Id_factura','=',$request->Id_factura)
->where('Id_parcela','=',$key->Id_parcela)->delete();
    }
    if($request->estado==4){
      $usuario=Auth::user()->id;
      $bitacora=new bitacora();
      $bitacora->Id_usuario=$usuario;
      $bitacora->id_bitacora=0;
      $bitacora->modulo="Facturas";
      $bitacora->Id_movimiento=3;
      $bitacora->save();

     }else{
      $usuario=Auth::user()->id;
      $bitacora=new bitacora();
      $bitacora->Id_usuario=$usuario;
      $bitacora->id_bitacora=0;
      $bitacora->modulo="Facturas";
      $bitacora->Id_movimiento=2;
      $bitacora->save();
     }
    }


    public function facturaciclo($ciclo)
    {
    $valor=str_replace("P","/" , $ciclo);
     $factura=DB::table('factura')
      ->join('ejidatario', 'ejidatario.Id_ejidatario', '=', 'factura.Id_ejidatario')
      ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
        ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->where('ciclo.Ciclo','=',$valor)
      ->select('*')->get();
    return  response()->json($factura);
    }
    /*Query para obtener parcelas del ejidatario
    SELECT ejidatario.nombre, parcela.cuenta, parcela.SEC, ejido.nombre_ej, municipio.descripcion, parcela.sup_Fis FROM parcela INNER JOIN ejidatario ON ejidatario.Id_ejidatario = parcela.Id_ejidatario INNER JOIN ejido ON ejido.Id_Ejido = parcela.Id_Ejido INNER JOIN municipio ON municipio.Id_municipio = parcela.Id_municipio
    */
    public function byMatricula($id) {
      $parcelas=DB::table('parcela')
        ->join('ejidatario', 'ejidatario.Id_ejidatario', '=', 'parcela.Id_ejidatario')
        ->join('ejido', 'ejido.Id_Ejido', '=', 'parcela.Id_Ejido')
        ->join('municipio', 'municipio.Id_municipio', '=', 'parcela.Id_municipio')
        ->join('factura_concepto','factura_concepto.Id_parcela','=','parcela.Id_parcela')
        ->join('factura','factura.Id_factura','=','factura_concepto.Id_factura')
        ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
            ->select("factura.Id_status_factura","ejidatario.Id_ejidatario","ejidatario.nombre as nombre","ejidatario.apPaterno as apPaterno","ejidatario.apMaterno as apMaterno","ejidatario.tarjeta as tarjeta","parcela.cuenta as cuenta", "parcela.SEC as seccion", "ejido.nombre_ej as nombreEj", "municipio.descripcion as municipio", "parcela.sup_Fis as supF", "parcela.Id_parcela as idParcela")
            ->where("ejidatario.tarjeta",'=',$id)
            ->groupBy('parcela.Id_parcela','factura.Id_status_factura','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','parcela.cuenta','parcela.SEC','ejido.nombre_ej','municipio.descripcion','parcela.sup_Fis', 'ejidatario.Id_ejidatario')->get();

        return json_encode($parcelas);

   }


    public function byParcela($id) {
      $parcelas=DB::table('parcela')
        ->join('ejidatario', 'ejidatario.Id_ejidatario', '=', 'parcela.Id_ejidatario')
        ->join('ejido', 'ejido.Id_Ejido', '=', 'parcela.Id_Ejido')
        ->join('municipio', 'municipio.Id_municipio', '=', 'parcela.Id_municipio')
        ->select("ejidatario.nombre as nombre","ejidatario.apPaterno as apPaterno","ejidatario.apMaterno as apMaterno","ejidatario.tarjeta as tarjeta","parcela.cuenta as cuenta", "parcela.SEC as seccion", "ejido.nombre_ej as nombreEj", "municipio.descripcion as municipio", "parcela.sup_Fis as supF", "parcela.Id_parcela as idParcela")
            ->where("parcela.Id_ejidatario",'=',$id)
                  ->groupBy('parcela.Id_parcela','ejidatario.nombre','ejidatario.apPaterno','ejidatario.apMaterno','ejidatario.tarjeta','parcela.cuenta','parcela.SEC','ejido.nombre_ej','municipio.descripcion','parcela.sup_Fis')
            ->get();
        return json_encode($parcelas);

    }
    public function deudor($id){
      $estado=parcela::join('factura_concepto','factura_concepto.Id_parcela','=','parcela.Id_parcela')
      ->join('factura','factura.Id_factura','=','factura_concepto.Id_factura')
      ->join("ciclo",'ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->join('ejidatario', 'ejidatario.Id_ejidatario', '=', 'parcela.Id_ejidatario')
      ->select("factura.Id_status_factura as estado")
      ->where("parcela.Id_parcela",'=',$id)
      ->groupBy('parcela.Id_parcela','factura.Id_status_factura')->get();
      if(sizeof($estado)>0){
     foreach ($estado as $key) {
       $numero=["estado"=>$key->estado];
     }
     return json_encode($numero);
      }else{
        $numero=["estado" => "1"];

         return json_encode($numero);
      }
    }

    public function byConcepto($id) {
      $valor=str_replace("XX","/" , $id);
      $datosCon=DB::table('concepto')->select("tipo as tipo","precio as cuota", "unidad as unidad")->where("Id_Concepto",'=',$valor)->get();

        return json_encode($datosCon);

   }
   public function borrado($id){
      factura::find($id)->delete();
      DB::table('factura_concepto')->where('Id_factura','=',$id)
     ->where('Id_factura','=',$id)->delete();
   }
   public function edicion($id){
  $carbon = new \Carbon\Carbon();
  $date = $carbon->now();
  $endDate = $date->addDays(4);
    $factura=facturaconcepto::where('Id_factura','=',$id)->get();
    if($factura){
     foreach ($factura as $key) {
       $key->can_pagada=$key->Total;
       $key->save();
       $abono= new abonos();
       $abono->Id_factura=$id;
       $abono->pago=$key->can_pagada;
       $abono->save();
     }}
  $factu=facturaconcepto::where('Id_factura','=',$id)->groupBy("Id_parcela","Id_Concepto")->get();
  if($factu){
     foreach ($factura as $key) {
       $horario=new horario();
       $horario->Id_factura=$id;
       $horario->Id_parcela=$key->Id_parcela;
       $horario->updated_at=$endDate;
       $horario->save();
     }}
   }
   public function pagar(Request $request){
     $carbon = new \Carbon\Carbon();
     $date = $carbon->now();
     $endDate = $date->addDays(4);
     $v = \Validator::make($request->all(), [
         'cantidad' => ['required']
     ]);
     if ($v->fails()) {
       echo $v->errors();
       return response()->json(['errors'=>$v->errors()->all()]);}else{

     $status=factura::find($request->id_factura);
     $factura =facturaconcepto::join("factura","factura.Id_factura","=","factura_concepto.Id_factura")->where('factura.Id_factura','=',$request->id_factura)->get();
     $cantidad=$request->deuda+0;//factura::join("factura_concepto","factura_concepto.Id_factura","=","factura.Id_factura")->where('factura.Id_factura','=',$request->id_factura)->sum("Total");
     $pago=$request->cantidad+0;
         echo $factura;
     if($pago==$cantidad){
       echo "hola";
       foreach ($factura as $key) {
         $key->can_pagada=$key->Total;
         $key->save();
         $abono= new abonos();
         $abono->Id_factura=$request->id_factura;
         $abono->pago=$pago;
         $abono->save();
       }
        $status->Id_status_factura=1;
        $status->save();
        $usuario=Auth::user()->id;
        $bitacora=new bitacora();
        $bitacora->Id_usuario=$usuario;
        $bitacora->id_bitacora=0;
        $bitacora->modulo="Facturas";
        $bitacora->Id_movimiento=5;
        $bitacora->save();
     }
     else if($pago>$cantidad){
       echo"Error";
     }else{
       $deuda=0;
     foreach ($factura as $key) {
       $can=$key->can_pagada+0;
       $total=$key->Total+0;
       $deuda=$total-$can;
       $operacio=$deuda-$pago;
       if(($operacio)<=0){
        $pago=(-1)*$operacio;
        $key->can_pagada=$key->Total;
        $status->Id_status_factura=1;
       }else{
       $key->can_pagada=$can+$pago;
       $status->Id_status_factura=3;
       }
        $abono= new abonos();
        $abono->Id_factura=$request->id_factura;
        $abono->pago=$pago;
        $abono->save();
        $key->save();
        $status->save();
        $usuario=Auth::user()->id;
        $bitacora=new bitacora();
        $bitacora->Id_usuario=$usuario;
        $bitacora->id_bitacora=0;
        $bitacora->modulo="Facturas";
        $bitacora->Id_movimiento=2;
        $bitacora->save();
     }
   } $factu=facturaconcepto::where('Id_factura','=',$request->id_factura)->select('Id_parcela')->groupBy("factura_concepto.Id_parcela")->get();
     if($factu){
        foreach ($factu as $key) {
          $horario=new horario();
          $horario->Id_factura=$request->id_factura;
          $horario->Id_parcela=$key->Id_parcela;
          $horario->updated_at=$endDate;
          $horario->save();
        }}
 }
   }




    public function verconceptos(Request $request,$id){
      $output="";
      $datos=DB::table('factura')
                  ->join('factura_concepto', 'factura_concepto.Id_factura', '=', 'factura.Id_factura')
                  ->join('cultivo','cultivo.Id_cultivo','=','factura_concepto.Id_cultivo')
                  ->join('parcela','parcela.Id_parcela','=','factura_concepto.Id_parcela')
                  ->where('factura.Id_factura','=',$id)
                  ->select('Id_Concepto','sup_cultivo','parcela.Id_parcela','Total','descripcion','can_pagada','factura.Id_factura')->get();
      if($datos)
      {
        foreach ($datos as $key => $product) {
         $output.='<tr>'.
        '<td>'.$product->Id_parcela.'</td>'.
        '<td>'.$product->Id_Concepto.'</td>'.
        '<td>'.$product->sup_cultivo.'</td>'.
        '<td>'.$product->descripcion.'</td>'.
        '<td>'.$product->Total.'</td>'.
        '</tr>';
        }
        return Response($output);
      }
    }
    public function exportarExcel(){
      $export = new FacturasExport();
      $usuario=Auth::user()->id;
      $bitacora=new bitacora();
      $bitacora->Id_usuario=$usuario;
      $bitacora->id_bitacora=0;
      $bitacora->modulo="Facturas";
      $bitacora->Id_movimiento=4;
      $bitacora->save();
      return $export->download('Corte de Caja.xlsx');

    }
}
