<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitacora extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('bitacora', function (Blueprint $table) {
            $table->Increments("id_bitacora");
            $table->unsignedInteger("Id_usuario");
            $table->unsignedInteger("Id_movimiento");
            $table->string("modulo",100);
            $table->foreign('Id_usuario')->references('id')->on('users');
            $table->foreign('Id_movimiento')->references('Id_movimiento')->on('tipo_movimiento');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitacora');
    }
}
