<?php

namespace App\Http\Controllers;

use App\parcela;
use App\ejido;
use App\ejidatario;
use App\canaleros;
use App\municipio;
use App\factura;
use App\bitacora;
use App\abonos;
use App\facturaconcepto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Exports\DeudoresExport;

class Deudores extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('admin');
  }
   public function verDeudores()
    {
       $ldate = date('Y');
       $ci=substr($ldate,2)+0;
       $cl=$ci-1;
       $ciclo=$cl."/".$ci;
       $deudoresT=DB::table('factura_concepto')
      ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
      ->join("parcela","parcela.Id_parcela","=","factura_concepto.Id_parcela")
      ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->where('status_factura.Id_status_factura','=','3')
      ->count();
      $deudoresTe=DB::table('factura_concepto')
     ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
     ->join("ejidatario","ejidatario.Id_ejidatario","=","factura.Id_ejidatario")
     ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
     ->where('status_factura.Id_status_factura','=','3')
     ->select(DB::raw('count(ejidatario.Id_ejidatario)as total'))
     ->groupBy('ejidatario.Id_ejidatario')
     ->get();
      $num=0;
      foreach ($deudoresTe as $key) {
      $num++;
      }
       $deudores=DB::table('factura_concepto')
      ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
      ->join("parcela","parcela.Id_parcela","=","factura_concepto.Id_parcela")
      ->join("cultivo","cultivo.Id_cultivo",'=','factura_concepto.Id_cultivo')
      ->join("ejidatario",'ejidatario.Id_ejidatario','=','factura.Id_ejidatario')
      ->join('ciclo','ciclo.Id_ciclo','=','factura.Id_ciclo')
      ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
      ->select('ejidatario.nombre','parcela.Id_parcela as tarjeta',DB::raw('SUM(factura_concepto.Total) as total'),DB::raw('SUM(factura_concepto.can_pagada) as pagado'),'ejidatario.apPaterno','ejidatario.apMaterno','ciclo.Ciclo as ciclo','factura.Id_factura','factura.created_at as data')
    //  ->where('ciclo.Ciclo','=',"18/19")
      ->where('status_factura.Id_status_factura','=','3')
      ->groupBy('factura.Id_factura','factura_concepto.Id_factura','parcela.Id_parcela','ejidatario.nombre','ciclo.Ciclo','factura.created_at','ejidatario.apPaterno','ejidatario.apMaterno')
      ->get();
      $valor=0;
      $sum=DB::table('factura_concepto')
     ->join("factura","factura.Id_factura","=","factura_concepto.Id_factura")
     ->join('status_factura','status_factura.Id_status_factura','=','factura.Id_status_factura')
     ->where('status_factura.Id_status_factura','=','3')
     ->select(DB::raw('sum(factura_concepto.Total)as total'))
     ->get();

       return view("Deudores/deudores",compact('sum','num','deudores','deudoresT','valor'));
    }


    public function actualizar($id_factura){
    $datos = factura::find($id_factura)->get();
    return ($datos=array('datos' => $datos));
  }

  public function pagar(Request $request){
    $v = \Validator::make($request->all(), [
        'cantidad' => ['required']
    ]);
    if ($v->fails()) {
      echo $v->errors();
      return response()->json(['errors'=>$v->errors()->all()]);}else{

    $status=factura::find($request->id_factura);
    $factura =facturaconcepto::join("factura","factura.Id_factura","=","factura_concepto.Id_factura")->where('factura.Id_factura','=',$request->id_factura)->get();
    $cantidad=$request->deuda+0;//factura::join("factura_concepto","factura_concepto.Id_factura","=","factura.Id_factura")->where('factura.Id_factura','=',$request->id_factura)->sum("Total");
    $pago=$request->cantidad+0;
        echo $factura;
    if($pago==$cantidad){
      echo "hola";
      foreach ($factura as $key) {
        $key->can_pagada=$key->Total;
        $key->save();
        $abono= new abonos();
        $abono->Id_factura=$request->id_factura;
        $abono->pago=$pago;
        $abono->save();
      }
       $status->Id_status_factura=2;
       $status->save();
       $usuario=Auth::user()->id;
       $bitacora=new bitacora();
       $bitacora->Id_usuario=$usuario;
       $bitacora->id_bitacora=0;
       $bitacora->modulo="Abonos";
       $bitacora->Id_movimiento=5;
       $bitacora->save();
    }
    else if($pago>$cantidad){
      echo"Error";
    }else{
      $deuda=0;
    foreach ($factura as $key) {
      $can=$key->can_pagada+0;
      $total=$key->Total+0;
      $deuda=$total-$can;
      $operacio=$deuda-$pago;
      if(($operacio)<=0){
       $pago=(-1)*$operacio;
       $key->can_pagada=$key->Total;
       $status->Id_status_factura=2;
      }else{
      $key->can_pagada=$can+$pago;
      $status->Id_status_factura=3;
      }
       $abono= new abonos();
       $abono->Id_factura=$request->id_factura;
       $abono->pago=$pago;
       $abono->save();
       $key->save();
       $status->save();
       $usuario=Auth::user()->id;
       $bitacora=new bitacora();
       $bitacora->Id_usuario=$usuario;
       $bitacora->id_bitacora=0;
       $bitacora->modulo="Abonos";
       $bitacora->Id_movimiento=2;
       $bitacora->save();
    }
  }
}
  }

  public function exportarExcel(){
      $export = new DeudoresExport();
      $usuario=Auth::user()->id;
      $bitacora=new bitacora();
      $bitacora->Id_usuario=$usuario;
      $bitacora->id_bitacora=0;
      $bitacora->modulo="Deudores";
      $bitacora->Id_movimiento=4;
      $bitacora->save();
      return $export->download('Deudores.xlsx');
    }

}
