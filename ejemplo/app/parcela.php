<?php

namespace App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class parcela extends Model
{
    protected $table = 'parcela';
    public  $primaryKey = 'Id_parcela';
    public $timestamps = false;
    use SoftDeletes; //Implementamos
    protected $dates = ['deleted_at'];
}
