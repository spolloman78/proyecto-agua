<?php

namespace App\Exports;

use App\Ejido;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class EjidosExport implements FromView
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $ejido=ejido::get();
        return view('Ejidos.excel',[
            'ejido' => $ejido
        ]);
    }
}
