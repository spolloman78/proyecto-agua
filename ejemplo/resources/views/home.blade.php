@extends('layouts.layout')
@section('content')
<div class="main-panel">
  <div class="content">
    <div class="panel-header bg-success-gradient">
      <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
          <div>
            <h1 class="text-white pb-2 fw-bold">Bienvenide {{Auth::user()->name}}  </h1>
            <h5 class="text-white op-7 mb-2">@php echo date("d") . " / " . date("m") . " / " . date("Y") @endphp</h5>
          </div>
          @if(Auth::user()->role->descripcion!="Tesorero")
          <div class="ml-md-auto py-2 py-md-0">
            <a href="{{route('factura')}}" class="btn btn-white btn-border btn-round mr-2">Capturar Factura</a>
          </div>
          @endif
        </div>
      </div>
    </div>
    <br><br><br>

    <div class="page-inner mt--5">
      <div class="row">

        @if(Auth::user()->role->descripcion != "Tesorero")
        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('usuario')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Usuarios</strong></div>
              <br>
              <i class="fas fa-users fa-3x"></i>
              <br>&nbsp;&nbsp;
            </div>
            </a>
          </div>
        </div>

        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('canalero')}}" style="text-decoration: none; color: white;">
              <div class="card-body p-3 text-center">
                <div class="card-title"> <strong>Canaleros</strong></div>
                <br>
                <i class="fas fa-fill-drip fa-3x"></i>
                <br>&nbsp;&nbsp;
              </div>
            </a>
          </div>
        </div>
        @endif
        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('ejidatario')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Ejidatarios</strong></div>
              <br><i class="icon-people fa-3x"></i>

              <br>&nbsp;&nbsp;
            </div>
            </a>
          </div>
        </div>
        @if(Auth::user()->role->descripcion != "Tesorero")
        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('ejido')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Ejidos</strong></div>
              <br>
              <i class="icon-map fa-3x"></i>
              <br>&nbsp;&nbsp;
            </div>
            </a>
          </div>
        </div>
        @endif
        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('parcela')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Parcelas</strong></div>
              <br>
              <i class="icon-direction fa-3x"></i>
              <br>&nbsp;&nbsp;
            </div>
            </a>
          </div>
        </div>
        @if(Auth::user()->role->descripcion != "Tesorero")
        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('verConagua')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>CONAGUA</strong></div>
              <br>
              <i class="icon-drop fa-3x"></i>
              <br>&nbsp;&nbsp;
            </div>
            </a>
          </div>
        </div>

        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('cultivo')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Cultivos</strong></div>
              <br>
              <i class="fas fa-tree fa-3x"></i>
              <br>&nbsp;&nbsp;
            </div>
            </a>
          </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('verfact')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Facturas</strong></div>
              <br>
              <i class="fas fa-book-open fa-3x"></i>
              <br>&nbsp;&nbsp;
            </div>
            </a>
          </div>
        </div>
        @endif

        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('ingresos')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Ingresos</strong></div>
              <br>
              <i class="fas fa-cart-plus fa-3x"></i>
              <br>&nbsp;&nbsp;
            </div>
            </a>
          </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('egresos')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Egresos</strong></div>
              <br>
              <i class="fas fa-cart-arrow-down  fa-3x"></i>
              <br>&nbsp;&nbsp;
            </div>
            </a>
          </div>
        </div>
        @if(Auth::user()->role->descripcion!="Tesorero")
        {{--<div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('concepto')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Conceptos</strong></div>
              <div style="margin-top: 10px;"></div>
              <i class="flaticon-list fa-3x"></i>
              <div style="margin-bottom: 10px;"></div>
            </div>
            </a>
          </div>
        </div>--}}
        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('deudores')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Deudores</strong></div>
              <div style="margin-top: 10px;"></div>
              <i class="flaticon-coins fa-3x"></i>
              <div style="margin-bottom: 10px;"></div>
            </div>
            </a>
          </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
          <div class="card card-success">
            <a href="{{route('bitacora')}}" style="text-decoration: none; color: white;">
            <div class="card-body p-3 text-center">
              <div class="card-title"> <strong>Bitácora</strong></div>
              <div style="margin-top: 10px;"></div>
              <i class="flaticon-file-1 fa-3x"></i>
              <div style="margin-bottom: 10px;"></div>
            </div>
            </a>
          </div>
        </div>
      </div>
      @endif
    <div class="page-inner">
      <div class="row">
  @if(Auth::user()->role->descripcion!="Tesorero")
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="card-title">
                  <h4 class="card-title"  style="padding: 1% 0 0 0;">Parcelas asignadas a Canaleros</h4>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="pieChart" style="width: 50%; height: 50%"></canvas>
              </div>
            </div>
          </div>
        </div> @endif
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="card-title">
                  <h4 class="card-title"  style="padding: 1% 0 0 0;">Resumen Ingresoss</h4>
                </div>
                <div class="ml-auto col-sm-4 text-align:right">
                  <div class="d-flex ">

                  </div>
                </div>
              </div>
            </div>
            <div class="card-body pb-0">
              <div class="d-flex">
                <div class="avatar avatar-lg">
                 <span class="avatar-title rounded-circle border border-white" style="background-color:#00A631;">I</span>
                </div>
                <div class="flex-1 pt-1 ml-2">
                  <h6 class="fw-bold mt-3 mb-0">  Ingresos de {{$now}}</h6>
                  <small class="text-muted">Ingresos por dia</small>
                </div>
                <div class="d-flex ml-auto align-items-center">
                  <h3 class="text-info fw-bold">
                    @foreach($dineroDia as $row)
                  $  {{$row->total}}
                @endforeach</h3>
                </div>
              </div>
              <div class="separator-dashed"></div>
              <div class="d-flex">
                <div class="avatar avatar-lg">
                    <span class="avatar-title rounded-circle border border-white" style="background-color:#00A631;">I</span>
                    </div>
                <div class="flex-1 pt-1 ml-2">

                  <h6 class="fw-bold mt-3 mb-0">Ingresos de {{$meso}}</h6>

                <small class="text-muted">Ingresos por mes</small>
                </div>
                <div class="d-flex ml-auto align-items-center">
                  <h3 class="text-info fw-bold">  @foreach($dinero as $row)
                  $  {{$row->totalDia}}
                  <input id="totalmes" value="{{$row->totalDia}}" hidden/>
                    @endforeach</h3>
                </div>
              </div>
              <div class="separator-dashed"></div>
              <div class="d-flex">
    <div class="avatar avatar-lg">
                  <span class="avatar-title rounded-circle border border-white" style="background-color:#00A631;">I</span>
                </div>

                <div class="flex-1 pt-1 ml-2">
                  <h6 class="fw-bold mt-3 mb-0">Ciclo Agrario {{$ciclo}}</h6>

<small class="text-muted">Ingresos por ciclo agrario</small>
                </div>
                <div class="d-flex ml-auto align-items-center">
                  <h3 class="text-info fw-bold">
                     @foreach($dineroCiclo as $row)
                  $  {{$row->totalCiclo}}
                  <input id="totalCiclo" value="{{$row->totalCiclo}}" hidden/>
                    @endforeach</h3>
                </div>
              </div>
              <div class="separator-dashed"></div>
              <div class="pull-in">

              </div>
            </div>
          </div>
        </div>  @if(Auth::user()->role->descripcion!="Tesorero")
         <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="card-title">
                  <h4 class="card-title"  style="padding: 1% 0 0 0;">Distribucion de Cultivos</h4>
                </div>
                <div class="ml-auto col-sm-2 text-align:right">
                  <div class="d-flex ">

                  </div>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="multipleBarChart"></canvas>
              </div>
            </div>
          </div>
        </div>
@endif
       <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="card-title">
                  <h4 class="card-title"  style="padding: 1% 0 0 0;">Ingresos por Dia durante el ciclo {{$ciclo}}</h4>
                </div>
                <div class="ml-auto col-sm-4 text-align:right">
                  <div class="d-flex ">

                  </div>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="lineChart"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
           <div class="card">
             <div class="card-header">
              <div class="row">
                <div class="card-title">
                  <h4 class="card-title"  style="padding: 1% 0 0 0;">Ingresos  a lo largo del Mes {{$meso}}</h4>
                </div>
                <div class="ml-auto col-sm-4 text-align:right">
                  <div class="d-flex ">

                  </div>
                </div>
              </div>
            </div>
             <div class="card-body">
               <div class="chart-container">
                 <canvas id="barChart"></canvas>
               </div>
             </div>
           </div>
         </div>
           @if(Auth::user()->role->descripcion=="Tesorero")
         <div class="col-md-6">
           <div class="card">
             <div class="card-header">
               <div class="card-title">Resumen Gastos</div>
             </div>
             <div class="card-body pb-0">
               <div class="d-flex">
                 <div class="avatar avatar-lg">
                  <span class="avatar-title rounded-circle border border-white" style="background-color:#F97D16 ;">E</span>
                 </div>
                 <div class="flex-1 pt-1 ml-2">

                   <h6 class="fw-bold mt-3 mb-0">  Egresos de {{$now}}</h6>

                   <small class="text-muted">Gastos por dia</small>
                 </div>
                 <div class="d-flex ml-auto align-items-center">
                   <h3 class="text-info fw-bold">
                     @foreach($egresoD as $row)
                   $  {{$row->totalDia}}
                 @endforeach</h3>
                 </div>
               </div>
               <div class="separator-dashed"></div>
               <div class="d-flex">
                 <div class="avatar avatar-lg">
                     <span class="avatar-title rounded-circle border border-white"style="background-color:#F97D16;">E</span>
                     </div>
                 <div class="flex-1 pt-1 ml-2">

                   <h6 class="fw-bold mt-3 mb-0">Egresos de {{$meso}}</h6>

                 <small class="text-muted">Egresos por mes</small>
                 </div>
                 <div class="d-flex ml-auto align-items-center">
                   <h3 class="text-info fw-bold">  @foreach($egresoM as $row)
                   $  {{$row->totalDia}}
                   <input id="totalmes" value="{{$row->totalDia}}" hidden/>
                     @endforeach</h3>
                 </div>
               </div>
               <div class="separator-dashed"></div>

         </div>
@endif
     <!--<div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="card-title">Doughnut Chart</div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="doughnutChart" style="width: 50%; height: 50%"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="card-title">Radar Chart</div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="radarChart"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="card-title">Bubble Chart</div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="bubbleChart"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <div class="card-title">Multiple Line Chart</div>
            </div>
            <div class="card-body">
              <div class="chart-container">
                <canvas id="multipleLineChart"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Cantidad de Cultivos Disponibles</div>
              </div>
              <div class="card-body">

                <div class="chart-container">
                  <canvas id="barChart"></canvas>
                </div>
              </div>
            </div>
          </div>
          <footer class="footer">
            <div class="container-fluid">
              <nav class="pull-left">

              </nav>
              <div class="copyright ml-auto">
              <img src="../assets/img/itsi-login.png" height="59" width="60"class="navbar-brand"/>
              </div>
            </div>
          </footer>

-->
      </div>
    </div>
  </div>

  </div>

</div>



<!-- End Custom template -->
</div>

<script src="../../assets/js/plugin/chart.js/chart.min.js"></script>
	<script src="../assets/js/plugin/chart-circle/circles.min.js"></script>

<script>
/*
$.ajax({
url:'/obtenerCultivo',
type:'get'
}).done(function (cultivo){

            });*/



                $.ajax({
                url:'/obtenernum',
                type:'get'
                }).done(function (cultivo){
                  var name = [];
                  var marks = [];
                  var total=cultivo.datos.total;
                    for (var i=0; i< cultivo.datos.dato.length; i++) {

                                 name.push(cultivo.datos.dato[i].nombre);

                                  var cantiad=cultivo.datos.dato[i].cantidad/total*100;
                                  marks.push(cantiad);

                              }

                var myPieChart = new Chart(pieChart, {
                  type: 'pie',
                  data: {
                    datasets: [{
                      data: marks,
                      backgroundColor :["#6CC40C","#0CA463","#0A7D62","#23A249","#17D594","#f3545d","#17D511"],
                      borderWidth: 0
                    }],
                    labels: name
                  },
                  options : {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                      position : 'bottom',
                      labels : {
                        fontColor: 'rgb(154, 154, 154)',
                        fontSize: 11,
                        usePointStyle : true,
                        padding: 20
                      }
                    },
                    pieceLabel: {
                      render: 'percentage',
                      fontColor: 'white',
                      fontSize: 14,
                    },
                    tooltips: false,
                    layout: {
                      padding: {
                        left: 20,
                        right: 20,
                        top: 20,
                        bottom: 20
                      }
                    }
                  }
                }) });

                $.ajax({
                url:'/obtenerCultivo',
                type:'get'
                }).done(function (cultivo){
                  var name = [];
                  var marks = [];
                  var valores= [];
                    for (var i=0; i< cultivo.datos.length; i++) {

                                 name.push(cultivo.datos[i].descripcion.substr(0,3));

                                  var cantiad=cultivo.datos[i].capacidad_cultivo-cultivo.datos[i].cantidad_cultivo;
                                  marks.push(cantiad);
                                  valores.push(cultivo.datos[i].cantidad_cultivo);

                              }

                var myMultipleBarChart = new Chart(multipleBarChart, {
                  type: 'bar',
                  data: {
                    labels:name,
                    datasets : [{
                      label: "# Cultivos Ocupados",
                      backgroundColor: '#FA9916',
                      borderColor: '#FA9916',
                      data:marks,
                    }, {
                      label: "# Cultivos Disponibles",
                      backgroundColor: '#0FC174',
                      borderColor: '#0FC174',
                      data: valores,
                    }],
                  },
                  options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                      position : 'bottom'
                    },
                    title: {
                      display: true,
                      text: 'Cultivos'
                    },
                    tooltips: {
                      mode: 'index',
                      intersect: false
                    },
                    responsive: true,
                    scales: {
                      xAxes: [{
                        stacked: true,
                      }],
                      yAxes: [{
                        stacked: true
                      }]
                    }
                  }
                });});
                $.ajax({
                url:'/VentasCiclo',
                type:'get'
                }).done(function (cultivo){

                  var marks = [];
                  var total=cultivo.datos.total;
                    for (var i=0; i< cultivo.datos.length; i++) {


                                  marks.push(cultivo.datos[i].totalCiclo);

                              }
                var myLineChart = new Chart(lineChart, {
                  type: 'line',
                  data: {
                    labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dec"],
                    datasets: [{
                      label: "Ingresos por Mes",
                      borderColor: "#1d7af3",
                      pointBorderColor: "#FFF",
                      pointBackgroundColor: "#1d7af3",
                      pointBorderWidth: 2,
                      pointHoverRadius: 4,
                      pointHoverBorderWidth: 1,
                      pointRadius: 4,
                      backgroundColor: 'transparent',
                      fill: true,
                      borderWidth: 2,
                      data: marks
                    }]
                  },
                  options : {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                      position: 'bottom',
                      labels : {
                        padding: 10,
                        fontColor: '#1d7af3',
                      }
                    },
                    tooltips: {
                      bodySpacing: 4,
                      mode:"nearest",
                      intersect: 0,
                      position:"nearest",
                      xPadding:10,
                      yPadding:10,
                      caretPadding:10
                    },
                    layout:{
                      padding:{left:15,right:15,top:15,bottom:15}
                    }
                  }
                });});
                $.ajax({
                url:'/VentasDiarias',
                type:'get'
                }).done(function (cultivo){
                  var name = [];
                  var marks = [];
                    for (var i=0; i< cultivo.datos.length; i++) {

                                 name.push(cultivo.datos[i].date);

                                  var cantiad=cultivo.datos[i].totalDia;
                                  marks.push(cantiad);

                              }

                              var myBarChart = new Chart(barChart, {
                                type: 'bar',
                                data: {
                                  labels:name,
                                  datasets : [{
                                    label: "Capacidad de Cultivos",
                                    backgroundColor: 'rgb(15, 193, 116)',
                                    borderColor: 'rgb(0, 166, 49 )',
                                    data: marks,
                                  }],
                                },
                                options: {
                                  responsive: true,
                                  maintainAspectRatio: false,
                                  scales: {
                                    yAxes: [{
                                      ticks: {
                                        beginAtZero:true
                                      }
                                    }]
                                  },
                                }
                              });
                });


</script>
      <script>

        var lineChart = document.getElementById('lineChart').getContext('2d'),
        barChart = document.getElementById('barChart').getContext('2d'),
        pieChart = document.getElementById('pieChart').getContext('2d'),
  multipleBarChart = document.getElementById('multipleBarChart').getContext('2d'),
        doughnutChart = document.getElementById('doughnutChart').getContext('2d'),
        radarChart = document.getElementById('radarChart').getContext('2d'),
        bubbleChart = document.getElementById('bubbleChart').getContext('2d'),
        multipleLineChart = document.getElementById('multipleLineChart').getContext('2d'),

        htmlLegendsChart = document.getElementById('htmlLegendsChart').getContext('2d');






        var myDoughnutChart = new Chart(doughnutChart, {
          type: 'doughnut',
          data: {
            datasets: [{
              data: [10, 20, 30],
              backgroundColor: ['#f3545d','#fdaf4b','#1d7af3']
            }],

            labels: [
            'Red',
            'Yellow',
            'Blue'
            ]
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
            legend : {
              position: 'bottom'
            },
            layout: {
              padding: {
                left: 20,
                right: 20,
                top: 20,
                bottom: 20
              }
            }
          }
        });

        var myRadarChart = new Chart(radarChart, {
          type: 'radar',
          data: {
            labels: ['Running', 'Swimming', 'Eating', 'Cycling', 'Jumping'],
            datasets: [{
              data: [20, 10, 30, 2, 30],
              borderColor: '#1d7af3',
              backgroundColor : 'rgba(29, 122, 243, 0.25)',
              pointBackgroundColor: "#1d7af3",
              pointHoverRadius: 4,
              pointRadius: 3,
              label: 'Team 1'
            }, {
              data: [10, 20, 15, 30, 22],
              borderColor: '#716aca',
              backgroundColor: 'rgba(113, 106, 202, 0.25)',
              pointBackgroundColor: "#716aca",
              pointHoverRadius: 4,
              pointRadius: 3,
              label: 'Team 2'
            },
            ]
          },
          options : {
            responsive: true,
            maintainAspectRatio: false,
            legend : {
              position: 'bottom'
            }
          }
        });

        var myBubbleChart = new Chart(bubbleChart,{
          type: 'bubble',
          data: {
            datasets:[{
              label: "Car",
              data:[{x:25,y:17,r:25},{x:30,y:25,r:28}, {x:35,y:30,r:8}],
              backgroundColor:"#716aca"
            },
            {
              label: "Motorcycles",
              data:[{x:10,y:17,r:20},{x:30,y:10,r:7}, {x:35,y:20,r:10}],
              backgroundColor:"#1d7af3"
            }],
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
              position: 'bottom'
            },
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero:true
                }
              }],
              xAxes: [{
                ticks: {
                  beginAtZero:true
                }
              }]
            },
          }
        });

        var myMultipleLineChart = new Chart(multipleLineChart, {
          type: 'line',
          data: {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [{
              label: "Python",
              borderColor: "#1d7af3",
              pointBorderColor: "#FFF",
              pointBackgroundColor: "#1d7af3",
              pointBorderWidth: 2,
              pointHoverRadius: 4,
              pointHoverBorderWidth: 1,
              pointRadius: 4,
              backgroundColor: 'transparent',
              fill: true,
              borderWidth: 2,
              data: [30, 45, 45, 68, 69, 90, 100, 158, 177, 200, 245, 256]
            },{
              label: "PHP",
              borderColor: "#59d05d",
              pointBorderColor: "#FFF",
              pointBackgroundColor: "#59d05d",
              pointBorderWidth: 2,
              pointHoverRadius: 4,
              pointHoverBorderWidth: 1,
              pointRadius: 4,
              backgroundColor: 'transparent',
              fill: true,
              borderWidth: 2,
              data: [10, 20, 55, 75, 80, 48, 59, 55, 23, 107, 60, 87]
            }, {
              label: "Ruby",
              borderColor: "#f3545d",
              pointBorderColor: "#FFF",
              pointBackgroundColor: "#f3545d",
              pointBorderWidth: 2,
              pointHoverRadius: 4,
              pointHoverBorderWidth: 1,
              pointRadius: 4,
              backgroundColor: 'transparent',
              fill: true,
              borderWidth: 2,
              data: [10, 30, 58, 79, 90, 105, 117, 160, 185, 210, 185, 194]
            }]
          },
          options : {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
              position: 'top',
            },
            tooltips: {
              bodySpacing: 4,
              mode:"nearest",
              intersect: 0,
              position:"nearest",
              xPadding:10,
              yPadding:10,
              caretPadding:10
            },
            layout:{
              padding:{left:15,right:15,top:15,bottom:15}
            }
          }
        });



        // Chart with HTML Legends



        var myHtmlLegendsChart = new Chart(htmlLegendsChart, {
          type: 'line',
          data: {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [ {
              label: "Subscribers",
              borderColor: gradientStroke2,
              pointBackgroundColor: gradientStroke2,
              pointRadius: 0,
              backgroundColor: gradientFill2,
              legendColor: '#f3545d',
              fill: true,
              borderWidth: 1,
              data: [154, 184, 175, 203, 210, 231, 240, 278, 252, 312, 320, 374]
            }, {
              label: "New Visitors",
              borderColor: gradientStroke3,
              pointBackgroundColor: gradientStroke3,
              pointRadius: 0,
              backgroundColor: gradientFill3,
              legendColor: '#fdaf4b',
              fill: true,
              borderWidth: 1,
              data: [256, 230, 245, 287, 240, 250, 230, 295, 331, 431, 456, 521]
            }, {
              label: "Active Users",
              borderColor: gradientStroke,
              pointBackgroundColor: gradientStroke,
              pointRadius: 0,
              backgroundColor: gradientFill,
              legendColor: '#177dff',
              fill: true,
              borderWidth: 1,
              data: [542, 480, 430, 550, 530, 453, 380, 434, 568, 610, 700, 900]
            }]
          },
          options : {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
              display: false
            },
            tooltips: {
              bodySpacing: 4,
              mode:"nearest",
              intersect: 0,
              position:"nearest",
              xPadding:10,
              yPadding:10,
              caretPadding:10
            },
            layout:{
              padding:{left:15,right:15,top:15,bottom:15}
            },
            scales: {
              yAxes: [{
                ticks: {
                  fontColor: "rgba(0,0,0,0.5)",
                  fontStyle: "500",
                  beginAtZero: false,
                  maxTicksLimit: 5,
                  padding: 20
                },
                gridLines: {
                  drawTicks: false,
                  display: false
                }
              }],
              xAxes: [{
                gridLines: {
                  zeroLineColor: "transparent"
                },
                ticks: {
                  padding: 20,
                  fontColor: "rgba(0,0,0,0.5)",
                  fontStyle: "500"
                }
              }]
            },
            legendCallback: function(chart) {
              var text = [];
              text.push('<ul class="' + chart.id + '-legend html-legend">');
              for (var i = 0; i < chart.data.datasets.length; i++) {
                text.push('<li><span style="background-color:' + chart.data.datasets[i].legendColor + '"></span>');
                if (chart.data.datasets[i].label) {
                  text.push(chart.data.datasets[i].label);
                }
                text.push('</li>');
              }
              text.push('</ul>');
              return text.join('');
            }
          }
        });

        var myLegendContainer = document.getElementById("myChartLegend");

        // generate HTML legend
        myLegendContainer.innerHTML = myHtmlLegendsChart.generateLegend();

        // bind onClick event to all LI-tags of the legend
        var legendItems = myLegendContainer.getElementsByTagName('li');
        for (var i = 0; i < legendItems.length; i += 1) {
          legendItems[i].addEventListener("click", legendClickCallback, false);
        }

      </script>
    </div>
</div>


@endsection
