@extends('layouts.layout')

@section('content')
<div class="main-panel">
<div class="content">
      <div class="page-inner">
        <h4 class="page-title">Ayuda</h4>
        <div class="row">
          <div class="col-md-12">
            <div class="card card-space">
              <div class="card-header">
                <h4 class="card-title">Preguntas Frecuentes</h4>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-3">
                    <div class="nav flex-column nav-pills nav-success nav-pills-no-bd nav-pills-icons" id="v-pills-tab" role="tablist" aria-orientation="vertical">

                      <a class="nav-link active show" id="v-pills-home-tab-icons" data-toggle="pill"  href="#v-pills-home-icons"  role="tab" aria-controls="v-pills-home-icons" aria-selected="true">
                        <i class="far fa-question-circle"></i>
                        Acerca de
                      </a>
                     @if(Auth::user()->role->descripcion != "Tesorero")
                      <a class="nav-link" id="v-pills-profile-tab-icons" data-toggle="pill" href="#v-pills-profile-icons" role="tab" aria-controls="v-pills-profile-icons" aria-selected="false">
                        <i class="flaticon-user-4"></i>
                        Perfil
                      </a> @endif
                      <a class="nav-link" id="v-pills-buy-tab-icons" data-toggle="pill" href="#v-pills-buy-icons" role="tab" aria-controls="v-pills-buy-icons" aria-selected="false">
                        <i class="icon-people"></i>
                        Ejidatarios
                      </a>@if(Auth::user()->role->descripcion != "Tesorero")
                      <a class="nav-link" id="v-pills-quality-tab-icons" data-toggle="pill" href="#v-pills-quality-icons" role="tab" aria-controls="v-pills-quality-icons" aria-selected="false">
                        <i class="icon-map"></i>
                       Ejidos
                      </a>
                      <a class="nav-link" id="v-pills-tractor-tab-icons" data-toggle="pill" href="#v-pills-tractor-icons" role="tab" aria-controls="v-pills-tractor-icons" aria-selected="false">

                        <i class="fas fa-tree"></i>
                       Cultivos
                     </a>@endif
                      <a class="nav-link" id="v-pills-caja-tab-icons" data-toggle="pill" href="#v-pills-caja-icons" role="tab" aria-controls="v-pills-caja-icons" aria-selected="false">

                        <i class="icon-basket"></i>
                       Caja
                      </a>
                      @if(Auth::user()->role->descripcion != "Tesorero")
                      <a class="nav-link" id="v-pills-deuda-tab-icons" data-toggle="pill" href="#v-pills-deuda-icons" role="tab" aria-controls="v-pills-deuda-icons" aria-selected="false">

                        <i class="flaticon-coins"></i>
                       Deudores
                      </a>
                      <a class="nav-link" id="v-pills-bita-tab-icons" data-toggle="pill" href="#v-pills-bita-icons" role="tab" aria-controls="v-pills-bita-icons" aria-selected="false">

                        <i class="flaticon-list"></i>
                       Bitácora
                     </a>@endif
                    </div>
                  </div>
                  <div class="col-12 col-md-9">
                    <div class="tab-content" id="v-pills-tabContent">
                      <div class="tab-pane fade active show" id="v-pills-home-icons" role="tabpanel" aria-labelledby="v-pills-home-icons">
                        <div class="accordion accordion-secondary">
                          <div class="card">
                            <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" role="button">
                              <div class="span-icon">
                                <div class="fas fa-atlas"></div>
                              </div>
                              <div class="span-title">
                              Antecedentes
                              </div>
                              <div class="span-mode"></div>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                              <div class="card-body">
                                La asociación de agricultores del Valle de Álvaro Obregón y Tarímbaro A.C. es una persona moral constituida como asociación civil sin fines de lucro desde el 19 de Agosto de 1993 y es encargada de suministrar y administrar el servicio de venta de agua a los ejidatarios para el riego de sus cultivos, así como de actividades relacionadas al desarrollo agrícola de la región que van desde el mantenimiento de los canales hasta renta de maquinaria ligera para los usuarios con el propósito de facilitar la administración de los sistemas e infraestructuras de irrigación en cumplimiento de la legislación vigente.<br>
Sus objetivos sociales son los siguientes:<br>
<ol><li>Asumir la operación, conservación y administración de las obras de infraestructura hidráulica ubicadas en las secciones de riego 3, 4, 5, 6, 7, 8 y 9, zona 3 de la segunda unidad del Distrito de Riego 020, pertenecientes al módulo III, mismas que serán concesionadas por la Comisión Nacional del Agua.</li><br>
<li>Promover el uso eficiente y racional del agua, así como su reuso.</li><br>
<li>Difundir, promover, y coadyuvar en la supervisión del debido cumplimiento de la ley de aguas nacionales y su reglamento.</li> <br>
<li>Promover, y en su caso, gestionar fuentes de financiamiento para sus asociados, para la administración de la infraestructura.</li> <br>
<li>Fomentar y coadyuvar, cuando lo soliciten los asociados, en el desarrollo de obras y servicios que permitan una mejor administración de las aguas, y tener mejor posición competitiva en los mercados.</li><br>
<li>Establecer comisiones de trabajo de diversa índole y organizar los foros conducentes para garantizar una mayor participación de sus asociados y la atención y solución de los problemas comunes o generalizados.</li><br>
<li>Participar en la conciliación de intereses entre sus asociados para prevenir y dirimir controversias entre los mismos.</li> <br>
<li>Establecer   los  mecanismos  y  procedimientos,  incluyendo  la constitución    de   fondos   y       fideicomisos,   que  permitan  la transparente  administración  de  los  recursos   que   aporten   los  asociados  y  terceros  para  los  fines  de  la asociación o para  programas o acciones específicas que   acuerden  sus asociados.</li><br>
<li>La celebración de todos los actos y contratos y la ejecución de las operaciones, así como el otorgamiento de documentos necesarios para el cumplimiento de su objeto.</li></ol><br>
Actualmente la asociación cuenta con un registro de 2500 usuarios, para llevar un control de sus actividades utilizan un conjunto de herramientas que constan de una biblioteca digital de mapas del INEGI,  una base de datos en Excel con un historial de más de 8 años y un sistema con macros que opera de manera local utilizado para las distintas operaciones que se realizan dentro de la asociación.

                              </div>
                            </div>
                          </div>
                          <div class="card">
                            <div class="card-header collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" role="button">
                            <div class="span-icon">
                              <div class="fab fa-buromobelexperte"></div>
                            </div>
                            <div class="span-title">
                              Características Principales del Sistema
                            </div>
                            <div class="span-mode"></div>
                          </div>
                          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                              El sistema cuenta con 3 tipos de usuarios con acceso a diferentes funcionalidades de acceso al sistema, el administrador es el único usuario con acceso total al sistema.<br>
                              A continuación se describen las funcionalidades a la que cada usuario tiene acceso.<br>
                              <ul><li><b>Para el usuario Administrador:</b> </li><br>
                              <ul><li><b>Modulo de Usuarios:</b></li><br>
                              <ul><li>Consulta, registro, edición y eliminado de datos de usuarios.</li></ul><br>
                                </ul>
                              <ul><li><b>Modulo de Canaleros:</b></li><br>
                              <ul><li>Consulta, registro, edición y eliminado de datos de canaleros.</ul></li><br>
                            </ul>
                              <ul><li><b>Modulo de Ejidatarios:</b></li><br>
                              <ul><li>  Consulta, registro, edición y eliminado de datos de ejidatarios.</li></ul><br>
                            </ul>
                              <ul><li><b>Modulo de Ejidos:</b></li><br>
                              <ul><li>Consulta, registro, edición y eliminado de datos de ejidos.</li><br>
                              <li>Consulta, registro, edición y eliminado de datos de parcelas.</li><br>
                              <li>Consulta, registro, edición y eliminado de datos de CONAGUA.</li><br>
                            </ul></ul>
                              <ul><li><b>Modulo de Cultivos:</b></li><br>
                              <ul><li>Consulta, registro, edición y eliminado de datos de cultivos.</li></ul><br>
                            </ul>
                              <ul><li><b>Modulo de Caja:</b></li><br>
                              <ul><li>Consulta y registro de datos de Ingresos.</li><br>
                              <li>Consulta y registro de datos de Egresos.</li> <br>
                              <li>Consulta y registro, edición y eliminado de datos de conceptos.</li><br>
                              <li>Captura de datos de Factura.</li><br>
                              <li>Consulta, edición y eliminación de datos de la Bitacora de Riego.</li><br>
                              <li>Consulta de Histórico de Facturas.</li><br>
                              <li>Generación de Excel con datos de Histórico de Facturas.</li><br>
                            </ul></ul>
                            <ul><li><b>Modulo de Deudores:</b></li><br>
                              <ul><li>Consulta, edición y eliminación de datos de Deudores.</li><br>
                              </ul></ul>
                            <ul><li><b>Modulo de Bitácora:</b></li><br>
                              <ul><li>Consulta de movimientos hechos en el sistema.</li><br>
                              </ul></ul>
                            <li><b>Para el ususario Tesorero: </b></li><br>
                              <ul><li><b>Modulo de Ingresos:</b></li><br>
                              <ul><li>Consulta y registro de datos de Ingresos.</li><br>
                              </ul></ul>



                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header collapsed" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" role="button">
                            <div class="span-icon">
                              <div class="far fa-address-card"></div>
                            </div>
                            <div class="span-title">
                              Acerca de Nosotros
                            </div>
                            <div class="span-mode"></div>
                          </div>
                          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                              <p style="text-align: center"><b>“Proyecto Asociación de Agricultores del Valle de Álvaro Obregón y Tarímbaro A.C”</b></p><br>
                              <b>Desarrollado por:</b><br>
                              <p style="text-align: center"><b>Equipo Urí</b></p><br>
                              <ul>
                                <li>Sergio Raúl Aparicio Muñoz.</li><br>
                                <li>Jeanette Monserrat Avilés Dávalos.</li><br>
                                <li>Carlos Alexis Cedeño Martínez.</li><br>
                                <li>Carlos Iván Monzón Chávez. </li><br>
                                <li>Francisco Antonio Olivares Abad.</li><br>
                                <li>Yoi Andrés Reyes Rosales.</li>
                              </ul>
                              <p style="text-align: center"><b> Ingeniería de Software - Departamento de Sistemas y Computación </b></p><br>
                              <p style="text-align: center"><b> Instituto Tecnológico de Morelia </b></p><br>
                              <p style="text-align: center"><b> Copyright&copy; 2020 - Urí - Todos los derechos reservados </b></p>


                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-profile-icons" role="tabpanel" aria-labelledby="v-pills-profile-icons">
                      <h5 class="mt-3">Perfil</h5>
                      <hr>
                      En la sección "Perfil" dentro del sistema usted podrá encontrar información especifica acerca del usuario con el que usted ha entrado al sistema. Este modulo ubicado en la esquina superior derecha de la pantalla le proporcionará los siguientes datos del usuario:
                      <ul>
                        <li>Usuario.</li>
                        <li>Nombre del Usuario.</li>
                        <li>Apellido Materno.</li>
                        <li>Rol (Dentro del Sistema).</li>

                    </div>
                    <div class="tab-pane fade" id="v-pills-buy-icons" role="tabpanel" aria-labelledby="v-pills-buy-tab-icons">
                      <h5 class="mt-3">Ejidatarios</h5>
                      <hr>
                      En este modulo del sistema usted podrá consultar en una tabla la siguiente información de los ejidatarios registrados en el sistema:<br>
                      <ul>
                        <li>Id del Registro</li>
                        <li>Nombre del Ejidatario</li>
                        <li>Apellido Paterno</li>
                        <li>Apellido Materno</li>
                        <li>Tarjeta</li>
                        <li>Tarjeta Anterior</li>
                      </ul>
                      En la parte superior de la tabla se encuentra el número de ejidatarios registrados en el sistema, la cantidad de ejidatarios registrados excentos de servicio y la cantidad de ejidatarios registrados con servicio.<br>
                      Debajo de esta información en la parte superior derecha de la tabla podremos encontrar 3 funcionalidades, las cuáles son:<br>
                      <ul>
                        <li>Agregar Ejidatario</li>
                        <li>Generar Excel: Al dar clic izquierdo en este botón se descargará automaticamente un excel con los siguientes datos de los Ejidatarios registrados en el sistema: Nombre, Apellido Paterno, Apellido Materno y Tarjeta.</li>
                        <li>Barra de búsqueda: Se puede hacer una busqueda dentro de la tabla de Ejidatarios con cualquier dato que se ingrese.</li>

                      </ul>

                      Además se cuentan con dos operaciones que se pueden realizar sobre los datos de cualquier ejidatario en la tabla:<br>
                      <ul>
                        <li>Editar</li>
                        <li>Eliminar</li>
                      </ul>








                      </ul>

                    </div>

                    <div class="tab-pane fade" id="v-pills-quality-icons" role="tabpanel" aria-labelledby="v-pills-quality-tab-icons">
                      <div class="accordion accordion-secondary">
                        <div class="card">
                          <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour" role="button">
                            <div class="span-icon">
                              <div class="fas fa-road"></div>
                            </div>
                            <div class="span-title">
                              Ejidos
                            </div>
                            <div class="span-mode"></div>
                          </div>

                          <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#accordion" role="button">
                            <div class="card-body">
                      En este modulo del sistema usted podrá consultar en una tabla la siguiente información de los ejidos registrados en el sistema:<br>
                      <ul>
                        <li>Id del Registro</li>
                        <li>Nombre del Ejido</li>
                      </ul>
                      En la parte superior derecha de la tabla podremos encontrar 2 funcionalidades, las cuáles son:<br>
                      <ul>
                        <li>Agregar Ejido</li>
                        <li>Barra de búsqueda: Se puede hacer una busqueda dentro de la tabla de Ejidos con cualquier dato que se ingrese.</li>

                      </ul>

                      Además se cuentan con dos operaciones que se pueden realizar sobre los datos de cualquier ejido en la tabla:<br>
                      <ul>
                        <li>Editar</li>
                        <li>Eliminar</li>
                      </ul>

                      </ul>
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header collapsed" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive" role="button">
                          <div class="span-icon">
                            <div class="fas fa-seedling"></div>
                          </div>
                          <div class="span-title">
                            Parcelas
                          </div>
                          <div class="span-mode"></div>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                          <div class="card-body">
                      En este modulo del sistema usted podrá consultar en una tabla la siguiente información de las parcelas registradas en el sistema:<br>
                      <ul>
                        <li>Id del Registro</li>
                        <li>Nombre Completo del Ejidatario</li>
                        <li>Municipio</li>
                        <li>Canalero</li>
                        <li>Nombre del Ejido</li>
                        <li>Cuenta</li>
                        <li>SEC</li>
                        <li>CP</li>
                        <li>LT</li>
                        <li>SLT</li>
                        <li>RA</li>
                        <li>PC</li>
                        <li>TE</li>
                        <li>SR</li>
                        <li>EQ</li>
                        <li>Superficie Física</li>
                        <li>PRO</li>
                      </ul>
                      En la parte superior de la tabla se encuentra el número de parcelas registradas en el sistema, la cantidad de ejidos registrados, la cantidad de parcelas con cultivos y la cantidad de parcelas deudoras.<br>
                      Debajo de esta información en la parte superior derecha de la tabla podremos encontrar 3 funcionalidades, las cuáles son:<br>
                      <ul>
                        <li>Agregar Parcela</li>
                        <li>Generar Excel: Al dar clic izquierdo en este botón se descargará automaticamente un excel con los siguientes datos de las parcelas registradas en el sistema:<br>
                        <ul>
                        <li>Id del Registro</li>
                        <li>Nombre Completo del Ejidatario</li>
                        <li>Municipio</li>
                        <li>Canalero</li>
                        <li>Nombre del Ejido</li>
                        <li>Cuenta</li>
                        <li>SEC</li>
                        <li>CP</li>
                        <li>LT</li>
                        <li>SLT</li>
                        <li>RA</li>
                        <li>PC</li>
                        <li>TE</li>
                        <li>SR</li>
                        <li>EQ</li>
                        <li>Superficie Física</li>
                        <li>PRO</li>
                        </ul>
                        </li>
                        <li>Barra de búsqueda: Se puede hacer una busqueda dentro de la tabla de Parcelas con cualquier dato que se ingrese.</li>

                      </ul>

                      Además se cuentan con cuatro operaciones que se pueden realizar sobre los datos de cualquier parcela en la tabla:<br>
                      <ul>
                        <li>Editar</li>
                        <li>Dividir Parcela</li>
                        <li>Información de Parcela</li>
                        <li>Eliminar</li>
                      </ul>

                      </ul>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header collapsed" id="headingSix" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix" role="button">
                          <div class="span-icon">
                            <div class="fas fa-tint"></div>
                          </div>
                          <div class="span-title">
                            Conagua
                          </div>
                          <div class="span-mode"></div>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                          <div class="card-body">
                            En este modulo del sistema usted podrá consultar en una tabla la siguiente información de CONAGUA relacionada con los ejidatarios registrados en el sistema:<br>
                      <ul>
                        <li>Id del Registro</li>
                        <li>USIPAD</li>
                        <li>Nombre Completo del Ejidatario</li>
                        <li>CTA_SIPAD</li>
                        <li>UNI</li>
                        <li>ZO</li>
                        <li>MOD</li>
                        <li>SRA</li>
                        <li>SSRA</li>
                        <li>EST</li>
                        <li>GR</li>
                        <li>PRE</li>
                        <li>SUPFISICA</li>
                        <li>SUPRIEGO</li>
                        <li>SEC_ORIG</li>
                      </ul>
                      En la parte superior derecha de la tabla encontraremos 3 funcionalidades, las cuáles son:<br>
                      <ul>
                        <li>Agregar CONAGUA </li>
                        <li>Generar Excel: Al dar clic izquierdo en este botón se descargará automaticamente un excel con los siguientes datos de los registros de CONAGUA en el sistema:<br>
                        <ul>
                        <li>Id del Registro</li>
                        <li>USIPAD</li>
                        <li>Nombre Completo del Ejidatario</li>
                        <li>CTA_SIPAD</li>
                        <li>UNI</li>
                        <li>ZO</li>
                        <li>MOD</li>
                        <li>SRA</li>
                        <li>SSRA</li>
                        <li>EST</li>
                        <li>GR</li>
                        <li>PRE</li>
                        <li>SUPFISICA</li>
                        <li>SUPRIEGO</li>
                        <li>SEC_ORIG</li>
                        </ul>
                        </li>
                        <li>Barra de búsqueda: Se puede hacer una busqueda dentro de la tabla de CONAGUA con cualquier dato que se ingrese.</li>

                      </ul>

                      Además se cuentan con dos operaciones que se pueden realizar sobre los datos de cualquier registro de CONAGUA en la tabla:<br>
                      <ul>
                        <li>Editar</li>
                        <li>Eliminar</li>
                      </ul>

                      </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="tab-pane fade" id="v-pills-tractor-icons" role="tabpanel" aria-labelledby="v-pills-tractor-tab-icons">
                      <h5 class="mt-3">Cultivos</h5>
                      <hr>
                      En este modulo del sistema usted podrá consultar en una tabla la siguiente información de los cultivos registrados en el sistema:<br>
                      <ul>
                        <li>Id del Registro</li>
                        <li>Nombre del Cultivo</li>
                        <li>Riegos Totales</li>
                        <li>Riegos Disponibles</li>
                        <li>Prioridad</li>
                      </ul>
                      En la parte superior derecha de la tabla podremos encontrar 3 funcionalidades, las cuáles son:<br>
                      <ul>
                        <li>Agregar Cultivo</li>
                        <li>Generar Excel: Al dar clic izquierdo en este botón se descargará automaticamente un excel con los siguientes datos de los Cultivos registrados en el sistema: Descripción, Capacidad de Cultivo, Cantidad de Cultivos y Prioridad.</li>
                        <li>Barra de búsqueda: Se puede hacer una busqueda dentro de la tabla de Cultivos con cualquier dato que se ingrese.</li>

                      </ul>

                      Además se cuentan con dos operaciones que se pueden realizar sobre los datos de cualquier cultivo en la tabla:<br>
                      <ul>
                        <li>Editar</li>
                        <li>Eliminar</li>
                      </ul>








                      </ul>

                    </div>



                    <div class="tab-pane fade" id="v-pills-caja-icons" role="tabpanel" aria-labelledby="v-pills-caja-tab-icons">
                      <div class="accordion accordion-secondary">
                        <div class="card">
                          <div class="card-header" id="headingSeven" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven" role="button">
                            <div class="span-icon">
                              <div class="fas fa-coins"></div>
                            </div>
                            <div class="span-title">
                              Ingresos
                            </div>
                            <div class="span-mode"></div>
                          </div>

                          <div id="collapseSeven" class="collapse show" aria-labelledby="headingSeven" data-parent="#accordion" role="button">
                            <div class="card-body">
                      En este modulo del sistema usted podrá consultar en una tabla la siguiente información de los ingresos registrados en el sistema:<br>
                      <ul>
                        <li>Factura</li>
                        <li>Fecha</li>
                        <li>Tarjeta</li>
                        <li>Usuario</li>
                        <li>Ciclo</li>
                        <li>Cuenta</li>
                        <li>SEC</li>
                        <li>Eji.</li>
                        <li>Nombre del Ejido</li>
                        <li>Municipio</li>
                        <li>Cultivo</li>
                        <li>Superficie Física</li>
                        <li>Concepto</li>
                        <li>Tipo</li>
                        <li>Cantidad</li>
                        <li>Importe</li>
                        <li>Status</li>

                      </ul>
                      En la parte superior izquierda y derecha de la tabla podremos encontrar 2 funcionalidades, las cuáles son:<br>
                      <ul>
                        <li>Agregar Ingreso</li>
                        <li>Barra de búsqueda: Se puede hacer una busqueda dentro de la tabla de Ingresos con cualquier dato que se ingrese. Además se puede hacer una búsqueda en un rango de fechas establecidas por el usuario.</li>

                      </ul>

                      En la parte inferior izquierda de la tabla se encuentra un botón con la leyenda "Exportar" con el cual al hacer clic izquierdo en él podremos descargar un excel con los datos de los ingresos registrados en el sistema.<br>
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header collapsed" id="headingEight" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight" role="button">
                          <div class="span-icon">
                            <div class="fas fa-money-bill-wave"></div>
                          </div>
                          <div class="span-title">
                            Egresos
                          </div>
                          <div class="span-mode"></div>
                        </div>
                        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                          <div class="card-body">
                      En este modulo del sistema usted podrá consultar en una tabla la siguiente información de los egresos registrados en el sistema:<br>
                      <ul>
                        <li>Concepto</li>
                        <li>Monto Total</li>
                        <li>Periodo</li>

                      </ul>
                       En la parte superior izquierda y derecha de la tabla podremos encontrar 2 funcionalidades, las cuáles son:<br>
                      <ul>
                        <li>Agregar Egreso</li>
                        <li>Barra de búsqueda: Se puede hacer una busqueda dentro de la tabla de Egresos con cualquier dato que se ingrese. Además se puede hacer una búsqueda en un rango de fechas establecidas por el usuario.</li>

                      </ul>

                      En la parte inferior izquierda de la tabla se encuentra un botón con la leyenda "Exportar" con el cual al hacer clic izquierdo en él podremos descargar un excel con los datos de los egresos registrados en el sistema.<br>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header collapsed" id="headingNine" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine" role="button">
                          <div class="span-icon">
                            <div class="fas fa-receipt"></div>
                          </div>
                          <div class="span-title">
                            Conceptos
                          </div>
                          <div class="span-mode"></div>
                        </div>
                        <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                          <div class="card-body">
                            En este modulo del sistema usted podrá consultar en una tabla la siguiente información de relacionada con los conceptos registrados en el sistema:<br>
                      <ul>
                        <li>Id del Registro</li>
                        <li>Descripción del Concepto</li>
                        <li>Precio</li>
                        <li>Tipo</li>
                        <li>No_de Riesgos</li>

                      </ul>
                      En la parte superior derecha de la tabla encontraremos 2 funcionalidades, las cuáles son:<br>
                      <ul>
                        <li>Agregar Concepto </li>
                        <li>Barra de búsqueda: Se puede hacer una busqueda dentro de la tabla de Conceptos con cualquier dato que se ingrese.</li>

                      </ul>

                      Además se cuentan con dos operaciones que se pueden realizar sobre los datos de cualquier registro de Conceptos en la tabla:<br>
                      <ul>
                        <li>Editar</li>
                        <li>Eliminar</li>
                      </ul>

                      </ul>
                          </div>
                        </div>
                      </div>
                @if(Auth::user()->role->descripcion != "Tesorero")
                      <div class="card">
                        <div class="card-header collapsed" id="headingTen" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen" role="button">
                          <div class="span-icon">
                            <div class="fas fa-file-invoice-dollar"></div>
                          </div>
                          <div class="span-title">
                            Captura de Factura
                          </div>
                          <div class="span-mode"></div>
                        </div>
                        <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                          <div class="card-body">
                            En este modulo del sistema usted podrá capturar los siguientes datos de una factura:<br>
                      <ul>
                        <li>No. de Factura (Ingresado automáticamente por el sistema).</li>
                        <li>Nombre Completo del Ejidatario</li>
                        <li>Tarjeta del Ejidatario</li>
                        <li>NO</li>
                        <li>Superficie</li>
                        <li>Fecha</li>
                        <li>Superficie</li>
                        <li>Concepto de Maquinaria</li>

                      </ul>
                      Dada la información anterior se podrán obtener las parcelas del ejidatario y los servicios de riego que existen para cada parcela del ejidatario. <br>
                      Con esto obtendremos el resumen de la factura con los siguientes datos:<br>
                      <ul>
                        <li>Concepto</li>
                        <li>Cantidad</li>
                        <li>Importe</li>
                      </ul>
                      Con todos los datos anteriores se obtendrá un resumen de la factura con los siguientes datos:<br>
                      <ul>
                        <li>Servicios de Riego</li>
                        <li>Cooperaciones</li>
                        <li>Maquinaria</li>
                        <li>Total</li>
                        <li>Cantidad Pagada</li>
                      </ul>

                      Después de lo anterior podremos guardar la factura en el sistema y estará disponible para su consulta en el Histórico de Facturas.<br> En caso de que no queramos guardar la factura generada se podrá hacer clic en el botón Cancelar.
                          </div>
                        </div>
                      </div>

                      <div class="card">
                        <div class="card-header collapsed" id="headingOnce" data-toggle="collapse" data-target="#collapseOnce" aria-expanded="false" aria-controls="collapseOnce" role="button">
                          <div class="span-icon">
                            <div class="far fa-sticky-note"></div>
                          </div>
                          <div class="span-title">
                            Bitacora de Riego
                          </div>
                          <div class="span-mode"></div>
                        </div>
                        <div id="collapseOnce" class="collapse" aria-labelledby="headingOnce" data-parent="#accordion">
                          <div class="card-body">
                            En este modulo del sistema usted podrá verificar los estados de las facturas generadas anteriormente en el sistema.<br>En la Bitacora de Riego se cuenta con una tabla con los siguientes datos de las facturas registradas en el sistema:
                      <ul>
                        <li>Folio de la Factura</li>
                        <li>Estado de la Factura</li>
                        <li>Cuenta</li>
                        <li>Ejidatario</li>
                        <li>Ciclo Agrario</li>
                        <li>Canalero</li>
                        <li>Fecha</li>

                      </ul>

                      En la parte superior derecha de la tabla podremos hacer uso de la siguiente función:<br>
                      <ul>
                        <li>Barra de búsqueda: Se puede hacer una busqueda dentro de la tabla de Bitacora de Riego con cualquier dato que se ingrese.</li>

                      </ul>

                      Además se cuentan con dos operaciones que se pueden realizar sobre los datos de cualquier registro de factura en la tabla:<br>
                      <ul>
                        <li>Cambiar Estado de la Factura</li>
                        <li>Eliminar</li>
                      </ul>

                          </div>
                        </div>
                      </div>

                      <div class="card">
                        <div class="card-header collapsed" id="headingDoce" data-toggle="collapse" data-target="#collapseDoce" aria-expanded="false" aria-controls="collapseDoce" role="button">
                          <div class="span-icon">
                            <div class="fas fa-history"></div>
                          </div>
                          <div class="span-title">
                            Histórico de Facturas
                          </div>
                          <div class="span-mode"></div>
                        </div>
                        <div id="collapseDoce" class="collapse" aria-labelledby="headingDoce" data-parent="#accordion">
                          <div class="card-body">
                            En este modulo del sistema usted podrá consultar en una tabla la siguiente información de relacionada con los todas las facturas registradas en el sistema, incluyendo las que ya han sido liquidadas:<br>
                      <ul>
                        <li>Id del Registro</li>
                        <li>Fecha</li>
                        <li>Tarjeta</li>
                        <li>Usuario</li>
                        <li>Ciclo</li>
                        <li>Cuenta</li>
                        <li>SEC</li>
                        <li>Eji.</li>
                        <li>Nombre del Ejido</li>
                        <li>Municipio</li>
                        <li>Cultivo</li>
                        <li>Superficie Física</li>
                        <li>Concepto</li>
                        <li>Tipo</li>
                        <li>Cantidad</li>
                        <li>Importe</li>
                        <li>Status</li>

                      </ul>
                      En la parte superior de la tabla encontraremos 4 criterios para realizar una búsqueda dentro de la tabla los cuáles son:<br>
                      <ul>
                        <li>Inicio de Rango de Fecha </li>
                        <li>Fin de Rango de Fecha </li>
                        <li>Ciclo Agrario </li>
                        <li>Tipo </li>

                      </ul>

                      Además en la parte superior de estos 4 criterios tendremos un botón para exportar un excel con el cual, al dar clic sobre él, descargará automáticamente un documento de Excel con los siguientes datos del Histórico de Facturas:<br>
                      <ul>
                        <li>Id del Registro</li>
                        <li>Fecha</li>
                        <li>Tarjeta</li>
                        <li>Usuario</li>
                        <li>Ciclo</li>
                        <li>Cuenta</li>
                        <li>SEC</li>
                        <li>Eji.</li>
                        <li>Nombre del Ejido</li>
                        <li>Municipio</li>
                        <li>Cultivo</li>
                        <li>Superficie Física</li>
                        <li>Concepto</li>
                        <li>Tipo</li>
                        <li>Cantidad</li>
                        <li>Importe</li>
                        <li>Status</li>
                      </ul>

                      </ul>
                          </div>
                        </div>
                      </div>@endif
                    </div>
                  </div>

                  <div class="tab-pane fade" id="v-pills-deuda-icons" role="tabpanel" aria-labelledby="v-pills-deuda-tab-icons">
                      <h5 class="mt-3">Deudores</h5>
                      <hr>
                      En este modulo del sistema usted podrá consultar en una tabla la siguiente información de los deudores registrados en el sistema:<br>
                      <ul>
                        <li>Id del Registro</li>
                        <li>Ejidatario</li>
                        <li>Parcela</li>
                        <li>Pagado</li>
                        <li>Deuda</li>
                        <li>Ciclo</li>
                        <li>Fecha</li>
                      </ul>
                      En la parte superior del catálogo encontraremos la siguiente información de izquierda a derecha: Número de Parcelas Deudoras, Ejidatarios Deudores y Deuda Total en el Sistema.
                      Debajo de esta información en la parte superior derecha de la tabla podremos encontrar una funcionalidad, que es:<br>
                      <ul>
                        <li>Barra de búsqueda: Se puede hacer una busqueda dentro de la tabla de Deudores con cualquier dato que se ingrese.</li>

                      </ul>

                      Además se cuenta con una operación que se puede realizar sobre los datos de cualquier deudor en la tabla:<br>
                      <ul>
                        <li>Pagar Deuda</li>
                      </ul>








                      </ul>

                    </div>

                    <div class="tab-pane fade" id="v-pills-bita-icons" role="tabpanel" aria-labelledby="v-pills-bita-tab-icons">
                      <h5 class="mt-3">Bitácora</h5>
                      <hr>
                      En este modulo el Administrador del sistema podrá consultar la información de todos los movimientos hechos en el sistema, pudiendo consultar en la tabla los siguientes datos:<br>
                      <ul>
                        <li>Id del Registro</li>
                        <li>Usuario</li>
                        <li>Tipo de Movimiento</li>
                        <li>Modulo</li>
                        <li>Fecha</li>

                      </ul>

                      En la parte superior izquierda de la tabla podremos encontrar dos criterios para realizar una búsqueda en la tabla:<br>
                      <ul>
                        <li>Fecha de Inicio de Rango</li>
                        <li>Fecha de Fin de Rango</li>

                      </ul>
                      Y en la parte superior derecha de la tabla se encuentra la barra de búsqueda en la cual se podrá buscar en la tabla al ingresar cualquier tipo de dato.


                    </div>




                </div>
              </div>
            </div>
          </div>

          <div class="card-footer">

						</div>
					</div>
				</div>
			</div>
		</div>
  </div>
</div>

  	<script src="../../assets/js/core/popper.min.js"></script>

  	<!-- jQuery UI -->
  	<script src="../../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
  	<script src="../../assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>


      @endsection
