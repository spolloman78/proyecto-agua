<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParcela extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcela', function (Blueprint $table) {
          Schema::disableForeignKeyConstraints();
           $table->Increments("Id_parcela");
           $table->unsignedInteger("Id_ejidatario");
           $table->unsignedInteger("Id_Ejido");
           $table->unsignedInteger("Id_municipio");
           $table->unsignedInteger("Id_canalero");
           $table->string("cuenta");
           $table->integer("SEC");
           $table->integer("CP");
           $table->integer("LT");
           $table->integer("SLT");
           $table->integer("RA");
           $table->integer("PC");
           $table->unsignedInteger("Id_tenencia");
           //$table->integer("TE");
           $table->integer("SR");
           $table->integer("EQ");
           $table->float("sup_Fis");
           $table->integer("pro");
           $table->foreign('Id_canalero')->references('Id_canalero')->on('canalero');
           $table->foreign("Id_municipio")->references("Id_municipio")->on('municipio');
           $table->foreign('Id_ejidatario')->references('Id_ejidatario')->on('ejidatario');
           $table->foreign('Id_Ejido')->references('Id_Ejido')->on('ejido');
           $table->foreign('Id_tenencia')->references('Id_tenencia')->on('tenencia');
           $table->softDeletes(); //Nueva línea, para el borrado lógico

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('parcela');
         DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
