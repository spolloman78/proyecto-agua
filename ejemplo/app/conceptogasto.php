<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class conceptogasto extends Model
{
  protected $table = 'concepto_gasto';
    public $timestamps = false;
    use SoftDeletes; //Implementamos
    protected $dates = ['deleted_at'];
    public  $primaryKey = 'Id_Concepto_gasto';
}
